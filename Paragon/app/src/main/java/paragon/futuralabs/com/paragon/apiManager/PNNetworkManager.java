package paragon.futuralabs.com.paragon.apiManager;

import android.content.Context;
import android.util.Log;


import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;

import org.json.JSONArray;
import org.json.JSONObject;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.entity.StringEntity;
import paragon.futuralabs.com.paragon.R;

import static paragon.futuralabs.com.paragon.Utilities.ConnectionDetector.isInternetOn;
import static paragon.futuralabs.com.paragon.Utilities.ConnectionDetector.toast;

/**
 * Created by Shamir on 23-06-2017.
 */

public class PNNetworkManager implements PNNetworkConstants {
    static final int DEFAULT_TIMEOUT = 60 * 1000;

    static final String TAG = "ALAPPLICATION";
    private static AsyncHttpClient client;

    private static synchronized AsyncHttpClient getClient() {
        if (client == null) {

            client = new AsyncHttpClient();
            client.setTimeout(DEFAULT_TIMEOUT);


        }
        return client;
    }


    private static String getAbsoluteUrl(String relativeUrl) {
        return BASE_URL + relativeUrl;
    }


    public interface NetworkInterface {

        public void onResponse(PNApiResponseWrapper baseResponse);


    }

    public static void getWithHeader(final Context applicationContext, String url, final NetworkInterface networkInterface) {
        if (!isInternetOn(applicationContext)) {
            toast(applicationContext, applicationContext.getResources().getString(R.string.no_network));
            networkInterface.onResponse(new PNApiResponseWrapper(null, false, applicationContext.getResources().getString(R.string.no_network)));
            return;
        }

       // ALSharedPreference ALSP = new ALSharedPreference(applicationContext);
       // getClient().addHeader("x-auth", ALSP.getToken());
        getClient().get(applicationContext, getAbsoluteUrl(url), new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                networkInterface.onResponse(new PNApiResponseWrapper(response, true, null));
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONArray timeline) {


            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                toast(applicationContext, PNNetworkConstants.API_ERROR_MESSAGE);
                networkInterface.onResponse(new PNApiResponseWrapper(null, false, PNNetworkConstants.API_ERROR_MESSAGE));
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                super.onFailure(statusCode, headers, responseString, throwable);
                toast(applicationContext, PNNetworkConstants.API_ERROR_MESSAGE);
                networkInterface.onResponse(new PNApiResponseWrapper(null, false, PNNetworkConstants.API_ERROR_MESSAGE));

            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                toast(applicationContext, PNNetworkConstants.API_ERROR_MESSAGE);
                networkInterface.onResponse(new PNApiResponseWrapper(null, false, PNNetworkConstants.API_ERROR_MESSAGE));
            }
        });

    }

    public static void get(final Context applicationContext, String url, final NetworkInterface networkInterface) {

        if (!isInternetOn(applicationContext)) {
            toast(applicationContext, applicationContext.getResources().getString(R.string.no_network));
            networkInterface.onResponse(new PNApiResponseWrapper(null, false, applicationContext.getResources().getString(R.string.no_network)));
            return;
        }
        getClient().get(applicationContext, getAbsoluteUrl(url), new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {


                networkInterface.onResponse(new PNApiResponseWrapper(response, true, null));
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONArray timeline) {


            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                toast(applicationContext, PNNetworkConstants.API_ERROR_MESSAGE);
                networkInterface.onResponse(new PNApiResponseWrapper(null, false, PNNetworkConstants.API_ERROR_MESSAGE));
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                super.onFailure(statusCode, headers, responseString, throwable);
                toast(applicationContext, PNNetworkConstants.API_ERROR_MESSAGE);
                networkInterface.onResponse(new PNApiResponseWrapper(null, false, PNNetworkConstants.API_ERROR_MESSAGE));

            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                toast(applicationContext, PNNetworkConstants.API_ERROR_MESSAGE);
                networkInterface.onResponse(new PNApiResponseWrapper(null, false, PNNetworkConstants.API_ERROR_MESSAGE));
            }
        });

    }

    public static AsyncHttpClient post(final Context applicationContext, String url, String jsonRequestObject, final NetworkInterface networkInterface) {

        if (!isInternetOn(applicationContext)) {
            toast(applicationContext, applicationContext.getResources().getString(R.string.no_network));
            networkInterface.onResponse(new PNApiResponseWrapper(null, false, applicationContext.getResources().getString(R.string.no_network)));
            return null;
        }
        Log.i("URLIva",url+" -> "+jsonRequestObject);
        StringEntity entity = null;
        if (null != jsonRequestObject) {
            entity = new StringEntity(jsonRequestObject, "UTF-8");
        }

        getClient().post(applicationContext, getAbsoluteUrl(url), entity, PNNetworkConstants.REQUEST_BODY_CONTENT_TYPE, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                Log.i("ResponseIva -> ",response.toString());
                networkInterface.onResponse(new PNApiResponseWrapper(response, true, null));
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONArray timeline) {


            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                networkInterface.onResponse(new PNApiResponseWrapper(null, false, PNNetworkConstants.API_ERROR_MESSAGE));
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                super.onFailure(statusCode, headers, responseString, throwable);
                networkInterface.onResponse(new PNApiResponseWrapper(null, false, PNNetworkConstants.API_ERROR_MESSAGE));

            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                networkInterface.onResponse(new PNApiResponseWrapper(null, false, PNNetworkConstants.API_ERROR_MESSAGE));
            }
        });
        return getClient();
    }

    public static AsyncHttpClient postWithHeader(final Context applicationContext, String url, String jsonRequestObject, final NetworkInterface networkInterface) {

        StringEntity entity = null;
        if (null != jsonRequestObject) {
            entity = new StringEntity(jsonRequestObject, "UTF-8");
        }
        if (!isInternetOn(applicationContext)) {
            toast(applicationContext, applicationContext.getResources().getString(R.string.no_network));
            networkInterface.onResponse(new PNApiResponseWrapper(null, false, applicationContext.getResources().getString(R.string.no_network)));
            return null;
        }

       // ALSharedPreference ALSP = new ALSharedPreference(applicationContext);
        //getClient().addHeader("x-auth", ALSP.getToken());
        getClient().post(applicationContext, getAbsoluteUrl(url), entity, PNNetworkConstants.REQUEST_BODY_CONTENT_TYPE, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                networkInterface.onResponse(new PNApiResponseWrapper(response, true, null));
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONArray timeline) {


            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                networkInterface.onResponse(new PNApiResponseWrapper(null, false, PNNetworkConstants.API_ERROR_MESSAGE));
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                super.onFailure(statusCode, headers, responseString, throwable);
                networkInterface.onResponse(new PNApiResponseWrapper(null, false, PNNetworkConstants.API_ERROR_MESSAGE));

            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                networkInterface.onResponse(new PNApiResponseWrapper(null, false, PNNetworkConstants.API_ERROR_MESSAGE));
            }
        });
        return getClient();
    }


}
