
package paragon.futuralabs.com.paragon.models;

import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class AddressList {

    @SerializedName("Address")
    private String mAddress;
    @SerializedName("ContactName")
    private String mContactName;
    @SerializedName("ContactNumber")
    private String mContactNumber;
    @SerializedName("DeliveryAddressId")
    private Long mDeliveryAddressId;
    @SerializedName("LandMark")
    private String mLandMark;
    @SerializedName("PostCode")
    private String mPostCode;

    public String getAddress() {
        return mAddress;
    }

    public void setAddress(String Address) {
        mAddress = Address;
    }

    public String getContactName() {
        return mContactName;
    }

    public void setContactName(String ContactName) {
        mContactName = ContactName;
    }

    public String getContactNumber() {
        return mContactNumber;
    }

    public void setContactNumber(String ContactNumber) {
        mContactNumber = ContactNumber;
    }

    public Long getDeliveryAddressId() {
        return mDeliveryAddressId;
    }

    public void setDeliveryAddressId(Long DeliveryAddressId) {
        mDeliveryAddressId = DeliveryAddressId;
    }

    public String getLandMark() {
        return mLandMark;
    }

    public void setLandMark(String LandMark) {
        mLandMark = LandMark;
    }

    public String getPostCode() {
        return mPostCode;
    }

    public void setPostCode(String PostCode) {
        mPostCode = PostCode;
    }

}
