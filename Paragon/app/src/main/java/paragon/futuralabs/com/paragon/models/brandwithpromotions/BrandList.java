
package paragon.futuralabs.com.paragon.models.brandwithpromotions;

import java.util.List;
import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class BrandList {

    @SerializedName("BrandId")
    private Long mBrandId;
    @SerializedName("BrandLogo")
    private String mBrandLogo;
    @SerializedName("BrandName")
    private String mBrandName;
    @SerializedName("Description")
    private String mDescription;
    @SerializedName("Promotions")
    private List<Promotion> mPromotions;

    public Long getBrandId() {
        return mBrandId;
    }

    public void setBrandId(Long BrandId) {
        mBrandId = BrandId;
    }

    public String getBrandLogo() {
        return mBrandLogo;
    }

    public void setBrandLogo(String BrandLogo) {
        mBrandLogo = BrandLogo;
    }

    public String getBrandName() {
        return mBrandName;
    }

    public void setBrandName(String BrandName) {
        mBrandName = BrandName;
    }

    public String getDescription() {
        return mDescription;
    }

    public void setDescription(String Description) {
        mDescription = Description;
    }

    public List<Promotion> getPromotions() {
        return mPromotions;
    }

    public void setPromotions(List<Promotion> Promotions) {
        mPromotions = Promotions;
    }

}
