package paragon.futuralabs.com.paragon.Adapters;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import paragon.futuralabs.com.paragon.Activity.PNHDMenuItemDetails;
import paragon.futuralabs.com.paragon.Activity.PNHDMenuItems;
import paragon.futuralabs.com.paragon.Activity.PNMenuItemDetails;
import paragon.futuralabs.com.paragon.R;
import paragon.futuralabs.com.paragon.Utilities.MyAlert;
import paragon.futuralabs.com.paragon.apiManager.PNApiResponseWrapper;
import paragon.futuralabs.com.paragon.apiManager.PNNetworkConstants;
import paragon.futuralabs.com.paragon.apiManager.PNNetworkManager;
import paragon.futuralabs.com.paragon.apiManager.SplitResponse;
import paragon.futuralabs.com.paragon.models.MenuItemsList;
import paragon.futuralabs.com.paragon.models.homedelivery.MenuItem;

import static paragon.futuralabs.com.paragon.Utilities.AppConstants.FROM;
import static paragon.futuralabs.com.paragon.Utilities.AppConstants.Item;
import static paragon.futuralabs.com.paragon.Utilities.AppConstants.MENU;
import static paragon.futuralabs.com.paragon.Utilities.ConnectionDetector.toast;
import static paragon.futuralabs.com.paragon.apiManager.PNNetworkManager.post;

/**
 * Created by Shamir on 14-03-2018.
 */

public class RVHDMenuItem extends RecyclerView.Adapter<RVHDMenuItem.ViewHolder> {
    List<MenuItem> menuItemsList;
    Context context;
    SplitResponse splitResponse;

    public RVHDMenuItem(Context context, List<MenuItem> menuItemsList) {
        this.context = context;
        this.menuItemsList=menuItemsList;


    }

    @Override
    public RVHDMenuItem.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.rv_hdmenu_item, parent, false);
        RVHDMenuItem.ViewHolder viewHolder = new RVHDMenuItem.ViewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final RVHDMenuItem.ViewHolder holder, final int position) {

        /*String url=menuItemsList.get(position).getImage();
        Picasso.with(context)
                .load(url)
                .error(R.drawable.dish_item_placeholder)
                .placeholder(R.drawable.dish_item_placeholder)
                .resize(100,100).centerCrop()
                .into(holder.imvImage);*/
        holder.textView.setText(menuItemsList.get(position).getDishName());
        holder.textViewDesc.setText(menuItemsList.get(position).getDescription());
        if (menuItemsList.get(position).getVegOrNonVeg()==1)
            holder.imvVeg.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_veg));
        else if (menuItemsList.get(position).getVegOrNonVeg()==0)
            holder.imvVeg.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_nonveg));
        else holder.imvVeg.setVisibility(View.GONE);
       /* if (menuItemsList.get(position).getIsContainNuts())
            holder.tvConNuts.setText("Contain Nuts");
        else
            holder.tvConNuts.setText("No Nuts");*/
        if (menuItemsList.get(position).getIsRecommended())
            holder.tvRecommended.setText("Recommended");
        else
            holder.tvRecommended.setText("Not Recommended");
        holder.tvPrice.setText(menuItemsList.get(position).getCurrency()+" "+menuItemsList.get(position).getPrice());
        holder.tvTotal.setText(menuItemsList.get(position).getCurrency()+" "+menuItemsList.get(position).getPrice());
        holder.imvMinus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int val=Integer.parseInt(holder.tvQuantity.getText().toString().trim());
                if (val!=1){
                    val=val-1;
                    holder.tvQuantity.setText(String.valueOf(val));
                    holder.tvTotal.setText(menuItemsList.get(position).getCurrency()+" "+(val*Double.parseDouble(menuItemsList.get(position).getPrice())));
                }
            }
        });
        holder.imvPlus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int val=Integer.parseInt(holder.tvQuantity.getText().toString().trim());
                if (val<100){
                    val=val+1;
                    holder.tvQuantity.setText(String.valueOf(val));
                    holder.tvTotal.setText(menuItemsList.get(position).getCurrency()+" "+(val*Double.parseDouble(menuItemsList.get(position).getPrice())));
                }
            }
        });

        holder.tvAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addToCart(holder.tvQuantity.getText().toString().trim(),position);
            }
        });

    }

    @Override
    public int getItemCount() {
        return menuItemsList.size();

    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView imvImage,imvVeg;
        TextView textView,textViewDesc,tvConNuts,tvRecommended,tvPrice,tvAdd,tvQuantity,tvTotal;
        ImageView imvMinus,imvPlus;

        public ViewHolder(View itemView) {
            super(itemView);
            imvImage =  itemView.findViewById(R.id.imageView);
            imvVeg =   itemView.findViewById(R.id.imvVeg);
            textView= itemView.findViewById(R.id.textView);
            textViewDesc= itemView.findViewById(R.id.textViewDesc);
            tvConNuts= itemView.findViewById(R.id.tvConNuts);
            tvRecommended= itemView.findViewById(R.id.tvRecommended);
            tvPrice= itemView.findViewById(R.id.tvPrice);
            imvMinus=  itemView.findViewById(R.id.imvMinus);
            imvPlus=  itemView.findViewById(R.id.imvPlus);
            tvAdd= itemView.findViewById(R.id.tvAdd);
            tvQuantity= itemView.findViewById(R.id.tvQuantity);
            tvTotal= itemView.findViewById(R.id.tvTotal);
        }

    }

    private void addToCart(String qty,int pos) {
        ((PNHDMenuItems)context).showProgress();
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("CartId", "0");
            jsonObject.put("CustomerId",((PNHDMenuItems)context).PNSP.getuserid());
            jsonObject.put("MenuItemsId",menuItemsList.get(pos).getMenuItemsId());
            jsonObject.put("MenuCode", "12pgn");
            jsonObject.put("Quantity", qty);
            jsonObject.put("Price", menuItemsList.get(pos).getPrice());
            jsonObject.put("OutletId", ((PNHDMenuItems)context).PNSP.getOutlet());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        post(context, PNNetworkConstants.AddToHomeDeliveryCart, jsonObject.toString(), new PNNetworkManager.NetworkInterface() {
            @Override
            public void onResponse(PNApiResponseWrapper baseResponse) {
                ((PNHDMenuItems)context).dismissProgres();
                if (baseResponse.isSuccess()) {
                    splitResponse = new SplitResponse(baseResponse.getJsonObjectResponse());
                    if (splitResponse.getStatus().equals("1")) {
                            toast(context,"Added to cart");
                        ((PNHDMenuItems)context).getCartIteams();

                    } else if (splitResponse.getStatus().equals("2")) {
                        toast(context,splitResponse.getMessage());
                        ((PNHDMenuItems)context).getCartIteams();
                    }else
                    {
                        toast(context, splitResponse.getMessage());
                    }
                } else {
                    toast(context, baseResponse.getMessage());
                }

            }
        });
    }
}

