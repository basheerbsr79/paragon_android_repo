
package paragon.futuralabs.com.paragon.models;

import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class PromotionList {

    @SerializedName("Description")
    private String mDescription;
    @SerializedName("EndDate")
    private String mEndDate;
    @SerializedName("EndTime")
    private String mEndTime;
    @SerializedName("Image")
    private String mImage;
    @SerializedName("OutletName")
    private String mOutletName;
    @SerializedName("PromotionId")
    private Long mPromotionId;
    @SerializedName("PromotionName")
    private String mPromotionName;
    @SerializedName("StartDate")
    private String mStartDate;
    @SerializedName("StartTime")
    private String mStartTime;
    @SerializedName("PrmoType")
    private String mPrmoType;

    public String getDescription() {
        return mDescription;
    }

    public void setDescription(String Description) {
        mDescription = Description;
    }

    public String getEndDate() {
        return mEndDate;
    }

    public void setEndDate(String EndDate) {
        mEndDate = EndDate;
    }

    public String getEndTime() {
        return mEndTime;
    }

    public void setEndTime(String EndTime) {
        mEndTime = EndTime;
    }

    public String getImage() {
        return mImage;
    }

    public void setImage(String Image) {
        mImage = Image;
    }

    public String getOutletName() {
        return mOutletName;
    }

    public void setOutletName(String OutletName) {
        mOutletName = OutletName;
    }

    public Long getPromotionId() {
        return mPromotionId;
    }

    public void setPromotionId(Long PromotionId) {
        mPromotionId = PromotionId;
    }

    public String getPromotionName() {
        return mPromotionName;
    }

    public void setPromotionName(String PromotionName) {
        mPromotionName = PromotionName;
    }

    public String getStartDate() {
        return mStartDate;
    }

    public void setStartDate(String StartDate) {
        mStartDate = StartDate;
    }

    public String getStartTime() {
        return mStartTime;
    }

    public void setStartTime(String StartTime) {
        mStartTime = StartTime;
    }

    public String getPrmoType() {
        return mPrmoType;
    }

    public void setPrmoType(String PrmoType) {
        mPrmoType = PrmoType;
    }

}
