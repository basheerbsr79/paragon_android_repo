
package paragon.futuralabs.com.paragon.models;

import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class StateList {

    @SerializedName("State")
    private String mState;
    @SerializedName("StateId")
    private Long mStateId;

    public String getState() {
        return mState;
    }

    public void setState(String State) {
        mState = State;
    }

    public Long getStateId() {
        return mStateId;
    }

    public void setStateId(Long StateId) {
        mStateId = StateId;
    }

}
