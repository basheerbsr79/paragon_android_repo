
package paragon.futuralabs.com.paragon.models;

import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class AreaList {

    @SerializedName("AreaName")
    private String mAreaName;
    @SerializedName("Currency")
    private String mCurrency;
    @SerializedName("MinimumOrderAmount")
    private String mMinimumOrderAmount;
    @SerializedName("OrderAreaId")
    private String mOrderAreaId;

    public String getAreaName() {
        return mAreaName;
    }

    public void setAreaName(String AreaName) {
        mAreaName = AreaName;
    }

    public String getCurrency() {
        return mCurrency;
    }

    public void setCurrency(String Currency) {
        mCurrency = Currency;
    }

    public String getMinimumOrderAmount() {
        return mMinimumOrderAmount;
    }

    public void setMinimumOrderAmount(String MinimumOrderAmount) {
        mMinimumOrderAmount = MinimumOrderAmount;
    }

    public String getOrderAreaId() {
        return mOrderAreaId;
    }

    public void setOrderAreaId(String OrderAreaId) {
        mOrderAreaId = OrderAreaId;
    }

}
