package paragon.futuralabs.com.paragon.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import paragon.futuralabs.com.paragon.Activity.PNAddToCart;
import paragon.futuralabs.com.paragon.R;
import paragon.futuralabs.com.paragon.apiManager.PNApiResponseWrapper;
import paragon.futuralabs.com.paragon.apiManager.PNNetworkConstants;
import paragon.futuralabs.com.paragon.apiManager.PNNetworkManager;
import paragon.futuralabs.com.paragon.apiManager.SplitResponse;
import paragon.futuralabs.com.paragon.models.homedelivery.HDList;
import paragon.futuralabs.com.paragon.models.homedelivery.MenuItem;

import static paragon.futuralabs.com.paragon.Utilities.ConnectionDetector.toast;
import static paragon.futuralabs.com.paragon.apiManager.PNNetworkManager.post;

/**
 * Created by Shamir on 01-08-2018.
 */

public class ExpandListAdapter extends BaseExpandableListAdapter {

    private Context context;
    List<HDList> hdLists;
    SplitResponse splitResponse;

    public ExpandListAdapter(Context context, List<HDList> hdLists) {
        this.context = context;
        this.hdLists = hdLists;
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        List<MenuItem> chList = hdLists.get(groupPosition).getMenuItems();
        return chList.get(childPosition);
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition,
                             boolean isLastChild, View convertView, ViewGroup parent) {

        final MenuItem menuItemsList = (MenuItem) getChild(groupPosition, childPosition);
        View v = convertView;

        if (v == null) {
            LayoutInflater infalInflater = (LayoutInflater) context
                    .getSystemService(context.LAYOUT_INFLATER_SERVICE);
            v = infalInflater.inflate(R.layout.child_item,parent, false);
        }
        final ImageView imvImage,imvVeg;
        final TextView textView,textViewDesc,tvConNuts,tvRecommended,tvPrice,tvAdd,tvQuantity,tvTotal;
        final ImageView imvMinus,imvPlus;
        final LinearLayout llAddToCart,llOutOfStock;
        imvImage =  v.findViewById(R.id.imageView);
        imvVeg =   v.findViewById(R.id.imvVeg);
        textView= v.findViewById(R.id.textView);
        textViewDesc= v.findViewById(R.id.textViewDesc);
        tvConNuts= v.findViewById(R.id.tvConNuts);
        tvRecommended= v.findViewById(R.id.tvRecommended);
        tvPrice= v.findViewById(R.id.tvPrice);
        imvMinus=  v.findViewById(R.id.imvMinus);
        imvPlus=  v.findViewById(R.id.imvPlus);
        tvAdd= v.findViewById(R.id.tvAdd);
        tvQuantity= v.findViewById(R.id.tvQuantity);
        tvTotal= v.findViewById(R.id.tvTotal);
        llAddToCart=v.findViewById(R.id.llAddToCart);
        llOutOfStock=v.findViewById(R.id.llOutOfStock);

        if (menuItemsList.getInStock()==0) {
            llAddToCart.setVisibility(View.GONE);
            llOutOfStock.setVisibility(View.VISIBLE);
        }else{
            llAddToCart.setVisibility(View.VISIBLE);
            llOutOfStock.setVisibility(View.GONE);
        }

        textView.setText(menuItemsList.getDishName().trim());
        textViewDesc.setText(menuItemsList.getDescription());
        if (menuItemsList.getVegOrNonVeg()==1)
            imvVeg.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_veg));
        else  if (menuItemsList.getVegOrNonVeg()==0)
            imvVeg.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_nonveg));
        else imvVeg.setVisibility(View.GONE);
       /* if (menuItemsList.getIsContainNuts())
            tvConNuts.setText("Contain Nuts");
        else
            tvConNuts.setText("No Nuts");*/
        if (menuItemsList.getIsRecommended())
            tvRecommended.setText("Recommended");
        else
            tvRecommended.setText("Not Recommended");
        tvPrice.setText(menuItemsList.getCurrency()+" "+menuItemsList.getPrice());
        tvTotal.setText(menuItemsList.getCurrency()+" "+menuItemsList.getPrice());
        imvMinus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int val=Integer.parseInt(tvQuantity.getText().toString().trim());
                if (val!=1){
                    val=val-1;
                    tvQuantity.setText(String.valueOf(val));
                    tvTotal.setText(menuItemsList.getCurrency()+" "+
                            (val*Double.parseDouble(menuItemsList.getPrice())));
                }
            }
        });
        imvPlus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int val=Integer.parseInt(tvQuantity.getText().toString().trim());
                if (val<100){
                    val=val+1;
                    tvQuantity.setText(String.valueOf(val));
                    tvTotal.setText(menuItemsList.getCurrency()+" "+
                            (val*Double.parseDouble(menuItemsList.getPrice())));
                }
            }
        });

        tvAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addToCart(tvQuantity.getText().toString().trim(),menuItemsList);
            }
        });

        return v;
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        List<MenuItem> menuItemsList=hdLists.get(groupPosition).getMenuItems();
        return menuItemsList.size();
    }


    @Override
    public Object getGroup(int groupPosition) {
        return hdLists.get(groupPosition);
    }

    @Override
    public int getGroupCount() {
        return hdLists.size();
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded,
                             View convertView, ViewGroup parent) {
        final HDList hdList = (HDList) getGroup(groupPosition);
        View v = convertView;
        if (v == null) {
            LayoutInflater inf = (LayoutInflater) context
                    .getSystemService(context.LAYOUT_INFLATER_SERVICE);
            //convertView = inf.inflate(R.layout.group_item, null);
            v = inf.inflate(R.layout.group_item,parent, false);
        }
        TextView tvCatagory = v.findViewById(R.id.tvCatagory);
        tvCatagory.setText(" "+hdList.getMenuCategoryName().trim());
        if (isExpanded) {
            tvCatagory.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_up, 0);
            tvCatagory.setBackground(context.getResources().getDrawable(R.drawable.border_grey));
        }else {
            tvCatagory.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_down, 0);
            tvCatagory.setBackground(context.getResources().getDrawable(R.drawable.boarder_shadow_effect));
        }

        return v;
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }

    public void addToCart(String qty,MenuItem menuItemsList) {
        ((PNAddToCart)context).showProgress();
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("CartId", "0");
            jsonObject.put("CustomerId",((PNAddToCart)context).PNSP.getuserid());
            jsonObject.put("MenuItemsId",menuItemsList.getMenuItemsId());
            jsonObject.put("MenuCode", "12pgn");
            jsonObject.put("Quantity", qty);
            jsonObject.put("Price", menuItemsList.getPrice());
            jsonObject.put("OutletId", ((PNAddToCart)context).PNSP.getOutlet());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        post(context, PNNetworkConstants.AddToHomeDeliveryCart, jsonObject.toString(), new PNNetworkManager.NetworkInterface() {
            @Override
            public void onResponse(PNApiResponseWrapper baseResponse) {
                ((PNAddToCart)context).dismissProgres();
                if (baseResponse.isSuccess()) {
                    splitResponse = new SplitResponse(baseResponse.getJsonObjectResponse());
                    if (splitResponse.getStatus().equals("1")) {
                        toast(context,"Added to cart");
                        ((PNAddToCart)context).getCartIteams();

                    } else if (splitResponse.getStatus().equals("2")) {
                        toast(context,splitResponse.getMessage());
                        ((PNAddToCart)context).getCartIteams();
                    }else
                    {
                        toast(context, splitResponse.getMessage());
                    }
                } else {
                    toast(context, baseResponse.getMessage());
                }

            }
        });
    }

}

