package paragon.futuralabs.com.paragon.Activity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import paragon.futuralabs.com.paragon.R;
import paragon.futuralabs.com.paragon.Utilities.MyAlert;
import paragon.futuralabs.com.paragon.apiManager.PNApiResponseWrapper;
import paragon.futuralabs.com.paragon.apiManager.PNNetworkConstants;
import paragon.futuralabs.com.paragon.apiManager.PNNetworkManager;
import paragon.futuralabs.com.paragon.apiManager.SplitResponse;
import paragon.futuralabs.com.paragon.models.news.Datum;
import paragon.futuralabs.com.paragon.models.news.LikeList;
import paragon.futuralabs.com.paragon.models.news.NewsResponse;

import static paragon.futuralabs.com.paragon.Utilities.AppConstants.MgrillFB;
import static paragon.futuralabs.com.paragon.Utilities.AppConstants.ParagonFB;
import static paragon.futuralabs.com.paragon.Utilities.AppConstants.ParagonIG;
import static paragon.futuralabs.com.paragon.Utilities.AppConstants.SalkaraFB;
import static paragon.futuralabs.com.paragon.Utilities.AppConstants.position;
import static paragon.futuralabs.com.paragon.Utilities.ConnectionDetector.toast;
import static paragon.futuralabs.com.paragon.apiManager.PNNetworkManager.post;

public class PNNewsRoomDetail extends PNBaseActivity {

    TextView tvHeading;
    ImageView ic_back_arrow;
    ImageView imageView,ivSend,ivLike,ivComment,ivFacebook,ivInstagram;
    TextView tvName,tvTitle,tvDescription,tvLink,tvLike,tvComment;
    RecyclerView recyclerView;
    EditText edAddComment;
    ScrollView scrollView;
    NewsResponse newsResponse;
    Datum newsRoomList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pnnews_detail);

        init();
        handleEvents();
    }

    @Override
    protected void onResume() {
        getNews();
        super.onResume();
    }

    private void handleEvents() {
        tvHeading.setText("News Room");

        ic_back_arrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        ivLike.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addLike();
            }
        });

        ivComment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                scrollView.fullScroll(ScrollView.FOCUS_DOWN);
            }
        });

        ivSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!edAddComment.getText().toString().equals(""))
                    addComment();
                else
                    toast(PNNewsRoomDetail.this,"Add comment");
            }
        });

        ivFacebook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(PNSP.getcurrentactivity().equals("1")){
                    loadUrl(ParagonFB);
                }else if(PNSP.getcurrentactivity().equals("2")){
                    loadUrl(SalkaraFB);
                }else if(PNSP.getcurrentactivity().equals("3")){
                    loadUrl(MgrillFB);
                }
            }
        });

        ivInstagram.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(PNSP.getcurrentactivity().equals("1")){
                    loadUrl(ParagonIG);
                }
            }
        });
    }

    private void init() {
        scrollView=findViewById(R.id.scrollView);
        tvHeading= findViewById(R.id.txt_title_toolbar);
        ic_back_arrow= findViewById(R.id.img_back_toolbar);
        imageView= findViewById(R.id.imageView);
        tvName= findViewById(R.id.tvName);
        tvTitle= findViewById(R.id.tvTitle);
        tvDescription= findViewById(R.id.tvDescription);
        tvLink= findViewById(R.id.tvLink);
        ivSend=findViewById(R.id.ivSend);
        ivLike=findViewById(R.id.ivLike);
        tvLike=findViewById(R.id.tvLike);
        ivComment=findViewById(R.id.ivComment);
        ivFacebook=findViewById(R.id.ivFacebook);
        ivInstagram=findViewById(R.id.ivInstagram);
        tvComment=findViewById(R.id.tvComment);
        edAddComment=findViewById(R.id.edAddComment);
        recyclerView=findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        if(PNSP.getcurrentactivity().equals("1")){
        }else if(PNSP.getcurrentactivity().equals("2")){
            ivInstagram.setVisibility(View.GONE);
        }else if(PNSP.getcurrentactivity().equals("3")){
            ivInstagram.setVisibility(View.GONE);
        }else if(PNSP.getcurrentactivity().equals("4")){
            ivFacebook.setVisibility(View.GONE);
            ivInstagram.setVisibility(View.GONE);
        }
    }

    public void getNews(){
        showProgress();
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("OutletId", PNSP.getOutlet());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        post(this, PNNetworkConstants.BindNews, jsonObject.toString(), new PNNetworkManager.NetworkInterface() {
            @Override
            public void onResponse(PNApiResponseWrapper baseResponse) {
                dismissProgres();
                if (baseResponse.isSuccess()) {
                    splitResponse = new SplitResponse(baseResponse.getJsonObjectResponse());
                    if (splitResponse.getStatus().equals("1")) {
                        Gson gson = new Gson();
                        newsResponse = gson.fromJson(splitResponse.getResponse(), NewsResponse.class);
                        if (newsResponse.getData().size() > 0) {
                            newsRoomList = newsResponse.getData().get(Integer.parseInt(getIntent().getStringExtra(position)));
                            setData(newsRoomList);
                        } else {
                            MyAlert alert = new MyAlert();
                            alert.showAlertFinish(PNNewsRoomDetail.this, "No news found");
                        }
                    }
                } else {
                    toast(getApplicationContext(), baseResponse.getMessage());
                }

            }
        });


    }

    public void setData(Datum newsRoomList) {
        String url=newsRoomList.getImage();
        Picasso.with(this)
                .load(url)
                .resize(200,400)
                .into(imageView);

        tvName.setText(newsRoomList.getNewsName());
        tvTitle.setText(newsRoomList.getHeading());
        tvDescription.setText(newsRoomList.getDescription());
        tvLink.setText(newsRoomList.getLink());
        tvLike.setText(newsRoomList.getLikeList().size()+"");
        tvComment.setText(newsRoomList.getCommentsList().size()+"");

        List<LikeList> like=newsRoomList.getLikeList();
boolean isLike=false;
        for (int i = 0; i < like.size(); i++) {
            if (like.get(i).getCustomerId().equals(PNSP.getuserid())) {
                isLike=true;
            }

        }
        if (isLike) ivLike.setImageDrawable(getResources().getDrawable(R.drawable.ic_like));
        else ivLike.setImageDrawable(getResources().getDrawable(R.drawable.ic_like_white));

        /*RVNewsComments adapter=new RVNewsComments(this,newsRoomList.getCommentsList());
        recyclerView.setAdapter(adapter);*/
    }

    public void addLike(){
        showProgress();
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("CustomerId", PNSP.getuserid());
            jsonObject.put("NewId", newsRoomList.getNewsId());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        post(this, PNNetworkConstants.AddNewLike, jsonObject.toString(), new PNNetworkManager.NetworkInterface() {
            @Override
            public void onResponse(PNApiResponseWrapper baseResponse) {
                dismissProgres();
                if (baseResponse.isSuccess()) {
                    splitResponse = new SplitResponse(baseResponse.getJsonObjectResponse());
                    if (splitResponse.getStatus().equals("1")) {
                       getNews();

                    }
                } else {
                    toast(getApplicationContext(), baseResponse.getMessage());
                }

            }
        });


    }

    public void addComment(){
        showProgress();
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("CustomerId", PNSP.getuserid());
            jsonObject.put("NewId", newsRoomList.getNewsId());
            jsonObject.put("Comments", edAddComment.getText().toString().trim());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        post(this, PNNetworkConstants.AddNewsComments, jsonObject.toString(), new PNNetworkManager.NetworkInterface() {
            @Override
            public void onResponse(PNApiResponseWrapper baseResponse) {
                dismissProgres();
                if (baseResponse.isSuccess()) {
                    splitResponse = new SplitResponse(baseResponse.getJsonObjectResponse());
                    if (splitResponse.getStatus().equals("1")) {
                        edAddComment.setText("");
                        getNews();

                    } else {
                        MyAlert alert = new MyAlert();
                        alert.showAlertFinish(PNNewsRoomDetail.this, "No news found");
                    }
                } else {
                    toast(getApplicationContext(), baseResponse.getMessage());
                }

            }
        });


    }

    public void loadUrl(String url){
        Uri uri = Uri.parse(url);
        Intent intent = new Intent(Intent.ACTION_VIEW, uri);
        startActivity(intent);
    }

}
