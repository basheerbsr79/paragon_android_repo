package paragon.futuralabs.com.paragon.Adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import paragon.futuralabs.com.paragon.Activity.PNPromotionList;
import paragon.futuralabs.com.paragon.Activity.paragon.DashBoardActivity;
import paragon.futuralabs.com.paragon.R;
import paragon.futuralabs.com.paragon.models.brandwithpromotions.BrandList;
import paragon.futuralabs.com.paragon.models.brandwithpromotions.Promotion;
import paragon.futuralabs.com.paragon.paragonhelper.ParagonSP;

import static paragon.futuralabs.com.paragon.Utilities.AppConstants.BrandId;
import static paragon.futuralabs.com.paragon.Utilities.AppConstants.BrandName;
import static paragon.futuralabs.com.paragon.Utilities.AppConstants.Item;
import static paragon.futuralabs.com.paragon.Utilities.ConnectionDetector.toast;

/**
 * Created by Shamir on 05-02-2018.
 */

public class RVBrandHome extends RecyclerView.Adapter<RVBrandHome.ViewHolder> {
    List<BrandList> brandList;
    Context context;

    public RVBrandHome(Context context, List<BrandList> brandList) {
        this.context = context;
        this.brandList=brandList;

    }

    @Override
    public RVBrandHome.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.rv_brand_home, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RVBrandHome.ViewHolder holder, final int position) {

        String url=brandList.get(position).getBrandLogo();
        Picasso.with(context)
                .load(url)
                .into(holder.imvImage);

        List<Promotion> promotionList=brandList.get(position).getPromotions();
        int c=0;
        for (int i = 0; i < promotionList.size(); i++) {
            if (promotionList.get(i).getIsRead().equalsIgnoreCase("No")){
                c++;
            }
        }

        if (c>0){
            holder.relLayBadge.setVisibility(View.VISIBLE);
            holder.tvBadge.setText(c+"");
        }else {
            holder.relLayBadge.setVisibility(View.GONE);
        }

        holder.imvImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ParagonSP sp=new ParagonSP(context);
                sp.setcurrentactivity(brandList.get(position).getBrandId()+"");
                sp.setpromotionFrom("");
                sp.setBrandImage(brandList.get(position).getBrandLogo());
                Intent in = new Intent(context, DashBoardActivity.class);
                in.putExtra(BrandId,brandList.get(position).getBrandId());
                in.putExtra(BrandName,brandList.get(position).getBrandName());
                context.startActivity(in);
            }
        });

        holder.imvPromotion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent in = new Intent(context, PNPromotionList.class);
                in.putExtra(Item,position+"");
                context.startActivity(in);
            }
        });
        holder.tvBadge.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent in = new Intent(context, PNPromotionList.class);
                in.putExtra(Item,position+"");
                context.startActivity(in);
            }
        });

    }

    @Override
    public int getItemCount() {
        return brandList.size();

    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView imvImage,imvPromotion;
        RelativeLayout relLayBadge;
        TextView tvBadge;

        public ViewHolder(View itemView) {
            super(itemView);
            imvImage =   itemView.findViewById(R.id.imvImage);
            imvPromotion=   itemView.findViewById(R.id.imvPromotion);
            relLayBadge= itemView.findViewById(R.id.relLayBadge);
            tvBadge= itemView.findViewById(R.id.tvBadge);
        }

    }
}
