package paragon.futuralabs.com.paragon.Activity;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v7.app.ActionBar;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.webkit.WebView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.google.gson.Gson;
import com.joanzapata.pdfview.PDFView;


import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import paragon.futuralabs.com.paragon.R;
import paragon.futuralabs.com.paragon.Utilities.ConnectionDetector;
import paragon.futuralabs.com.paragon.Utilities.MyAlert;
import paragon.futuralabs.com.paragon.Utilities.NetFailedAlert;
import paragon.futuralabs.com.paragon.apiManager.PNApiResponseWrapper;
import paragon.futuralabs.com.paragon.apiManager.PNNetworkConstants;
import paragon.futuralabs.com.paragon.apiManager.PNNetworkManager;
import paragon.futuralabs.com.paragon.apiManager.SplitResponse;
import paragon.futuralabs.com.paragon.models.CityList;
import paragon.futuralabs.com.paragon.models.CityResponse;
import paragon.futuralabs.com.paragon.models.CountryList;
import paragon.futuralabs.com.paragon.models.CountryResponse;
import paragon.futuralabs.com.paragon.models.LocationByCityResponse;
import paragon.futuralabs.com.paragon.models.LocationList;
import paragon.futuralabs.com.paragon.models.NationalitiesList;
import paragon.futuralabs.com.paragon.models.NationalitiesResponse;
import paragon.futuralabs.com.paragon.models.StateList;
import paragon.futuralabs.com.paragon.models.StatesResponse;
import paragon.futuralabs.com.paragon.paragonhelper.ParagonSP;

import static paragon.futuralabs.com.paragon.Utilities.ConnectionDetector.toast;
import static paragon.futuralabs.com.paragon.Utilities.Utils.isValidEmail;
import static paragon.futuralabs.com.paragon.Utilities.Utils.setDateToEditText;
import static paragon.futuralabs.com.paragon.apiManager.PNNetworkManager.post;

public class RegisterActivity extends PNBaseActivity {
    Context context;
    Button btn_submit;
    EditText ed_name, ed_adres, ed_email, ed_psd, ed_cnfrm_psd, ed_phone, ed_dob, ed_mrgdate;
    EditText edCountry, edState, edCity, edTitle, edCountryCode, edLocation;
    AutoCompleteTextView edNationality;
    TextView signin, tvTerms;
    CheckBox cbTerms;
    String from;
    int y, m, d;
    String dtest, mtest;
    static final int DATE_ID = 0;
    ParagonSP sp;
    String userid;
    boolean booldob;
    String device_id;
    SplitResponse splitResponse;
    CountryResponse countryResponse;
    List<CountryList> countryLists;
    StatesResponse statesResponse;
    List<StateList> stateLists;
    CityResponse cityResponse;
    List<CityList> cityLists;
    LocationByCityResponse locationByCityResponse;
    List<LocationList> locationLists;
    NationalitiesResponse nationalitiesResponse;
    List<NationalitiesList> nationalitiesLists;

    public static final String[] listItems = new String[]{"Mr", "Mrs", "Ms"};
    String[] countries, states, cities, countryCode, locations, nationalities;
    String countryId = "", stateId = "", cityId = "", locationsId = "";

    private static final String TAG = RegisterActivity.class.getSimpleName();
    public static final String SAMPLE_FILE = "terms.pdf";

    boolean isAddress = false,addressAlert=true;


    String htmlText = "<p>  Please read the following Terms of Use carefully before using the Paragon Application.&nbsp; By accessing and using the App/Website and/or services of Paragon, you agree to be bound by these Terms of Use. If you do not agree to be bound by these Terms of Use, please do not access or use the App/Website.</p>\n" +
            "<ol>\n" +
            "<p><li><strong><u>1.AGREEMENT TERMS</u></strong></li></p>\n" +
            "</ol>\n" +
            "<ul>\n" +
            "<p><li>\u25CF These Terms of Use together with all the schedules and annexures thereto and the privacy policy (\"Agreement/Terms of Use\") is a binding contract between Paragon Group, a Company Incorporated and registered under the Companies Act,1956 and having its registered office at &nbsp;Kannur Road, Calicut&nbsp; (hereinafter referred to &ldquo;Paragon&rdquo; or \"Our&rdquo; or \"us&rdquo; or \"We&rdquo;, which expression shall, unless it be repugnant to the context or meaning thereof, be deemed to mean and include its successors and assigns) and the user designated by the registration data provided at the time of registration with Paragon (herein referred to as \"User&rdquo; or \"you&rdquo; or \"your&rdquo;)</li></p>\n" +
            "</ul>\n" +
            "<ul>\n" +
            "<p><li>\u25CF The User enters into this Agreement on his own free will, and agrees to be bound by it (include any and all Usage Rules as may be applicable and the Privacy Policy), by clicking on \"I Accept&rdquo; or \"I Agree&rdquo; button on the App/Website or otherwise as part of the registration process or by accessing and using our services in any way.</li></p>\n" +
            "</ul>\n" +
            "<ul>\n" +
            "<p><li>\u25CF The User acknowledges and agrees that the mutual obligations of the Parties as contained herein are adequate consideration for the covenants of parties contained in this Agreement.</li></p>\n" +
            "</ul>\n" +
            "<ul>\n" +
            "<p><li>\u25CF Paragon shall in its sole discretion, have the right at any time and without prior notice, to modify this Agreement and any of the terms thereof. These modifications shall become effective immediately. Paragon will publish the modified Agreement on its App/Website to inform Users about the same. If a User does not receive the notification, or does not acknowledge the acceptance of the modifications, for any reason, the modified Agreement will still be effective from the time it is published by Paragon. Any User using the Paragon, after the implementation of such modifications, shall be taken as an acceptance by the User of the modified Terms of Use.</li></p>\n" +
            "</ul>\n" +
            "<ul>\n" +
            "<p><li>\u25CF Certain features of our products and services may be subject to additional guidelines, terms or rules (\"Usage Rules&rdquo;), which will be posted by Paragon in respect of such products and services at appropriate places on the App/Website and shall be deemed to be incorporated herein by reference.</li></p>\n" +
            "</ul>\n" +
            "<ul>\n" +
            "<p><li>\u25CF This Agreement governs the access and use of all products and services offered by us.</li></p>\n" +
            "</ul>\n" +
            "<p><li><strong> <u>2.SERVICES PROVIDED</u></strong></li></p>\n" +
            "<ul>\n" +
            "<p><li>\u25CF Subject to the provisions hereof, Paragon shall provide the below services and such other services as Paragon deems fit, which may be updated from time to time at Paragon&rsquo;s sole discretion (\"Service&rdquo; or \"Services&rdquo;):</li></p>\n" +
            "<p><li>\u25CF Use of Paragon App/Website and/or their sub domains on a computer, laptop, tablet and/or mobile phone and any functionality available on the Website or desktop or Mobile Application.</li></p>\n" +
            "<p><li>\u25CF Use of Paragon Application Programming Interface (\"API&rdquo;) protocols in order to receive or transmit data with Paragon's back-end server system.</li></p>\n" +
            "<p><li>\u25CF Access to data Paragon Content (as defined below).</li></p>\n" +
            "</ul>\n" +
            "<ul>\n" +
            "<p><li>\u25CF The User will have the option to avail of any or all such Services as he/she may desire, but subject, at all times, to these Terms of Use.</li></p>\n" +
            "<p><li>\u25CF In order to avail of the Services, the User hereby authorizes Paragon to act on its behalf to retrieve information related to its accounts / memberships with third party companies by using their log-in credentials and passwords on such third party websites and whose information/content they have provided to Paragon and to store any and all such data in accordance with the privacy policy (as referred to in \"Agreement Terms&rdquo; above).</li></p>\n" +
            "<p><li>\u25CF The User acknowledges that, while Paragon may retrieve data as mentioned above, it is the User's responsibility to verify all details of such data, including point balances and expiry dates directly with the third party with which the User is associated. Paragon shall not be held liable for any losses or damages suffered by the User as a result of any discrepancies or mistakes in such data.</li></p>\n" +
            "<p><li>\u25CF Paragon reserves the right to access the User's account, including any data contained therein, the User's activity with regards to Services provided and for other internal operational and business purposes. Please see the privacy policy (referred to in \"Agreement Terms&rdquo;) for further details.</li></p>\n" +
            "<p><li>\u25CF The Paragon vouchers will be valid for a limited period</li></p>\n" +
            "<p><li>\u25CF Vouchers shall automatically expire.</li></p>\n" +
            "<p><li>\u25CF These vouchers cannot be revalidated post expiry.</li></p>\n" +
            "<p><li>\u25CF The Paragon vouchers will abide to T&amp;C of the vouchers</li></p>\n" +
            "<p><li>\u25CF The Paragon vouchers cannot be encashed</li></p>\n" +
            "<p><li>\u25CF The Paragon vouchers are issued on this app</li></p>\n" +
            "<p><li>\u25CF The Paragon vouchers cannot be exchanged or use in par</li></p>\n" +
            "<p><li>\u25CF The Paragon promotions are valid as listed n the respective outlets and time</li></p>\n" +
            "<p><li>\u25CF The Paragon benefits are only valid in the participating restaurants listed in the app</li></p>\n" +
            "</ul>\n" +
            "<p><li><strong> <u>3.CONTENT RIGHTS</u></strong></li></p>\n" +
            "<ul>\n" +
            "<p><li>\u25CF \"Paragon Content&rdquo; means any data and information which may have been developed by Paragon itself, or sourced from other third parties (including other users) (as mentioned in Services provided). Paragon Content is the sole property of Paragon or a third-party provider and may be protected by Indian copyright laws and/or other relevant legislations pertaining to intellectual property rights.</li></p>\n" +
            "<p><li>\u25CF No User may use copy, display, distribute, sell, post, exploit, create derivate works based on, reproduce in any manner, or otherwise use any Paragon Content in any way other than as specifically permitted under these Terms of Use, without procuring the prior written consent of Paragon or the relevant third party owner of the content. Nothing in this Agreement should be construed as granting or creation of any rights in the Paragon Content in favor of any User.</li></p>\n" +
            "<p><li>\u25CF \"User Content&rdquo; means any data and information in any form provided to Paragon by the User. The User agrees not to provide any content which is objectionable to a reasonable person. The User represents and warrants that all content submitted to Paragon belongs to him/her, that he/she has a right to use the User Content and to upload the same on the App/Website/provide the same to Paragon, and that no such action by the User, nor the displaying of the User Content on the App/Website and/or its access by third parties, including other users, will infringe on, or otherwise violate the rights of any person. The User hereby grants to Paragon a royalty free license to display the User Content on the App/Website where it may be accessed by third parties (including other users).</li></p>\n" +
            "<p><li>\u25CF The User allows Paragon the right (at no charge) to store and use their content for the purpose of the Paragon App/ Website. Paragon reserves the right to remove or modify any User Content for any reason, including User Content that it believes violates these Terms of Use or any of its internal policies.</li></p>\n" +
            "</ul>\n" +
            "<p><li><strong> <u>4.REPRESENTATIONS, WARRANTIES AND COVENANTS</u></strong></li></p>\n" +
            "<ul>\n" +
            "<p><li>\u25CF The User represents, warrants and covenants that he/she:</li></p>\n" +
            "<p><li>\u25CF Is of the minimum age of 18 years and has the authority to enter into this Agreement with Paragon and has provided information or content that is true, accurate and up to date.</li></p>\n" +
            "<p><li>\u25CF Will adhere by all terms and conditions of this Agreement in his/her use of the App/Website and/or the Services</li></p>\n" +
            "<p><li>\u25CF Understands that the Services are for personal use only and agrees and undertakes that he/she shall use the Services only for himself/herself.</li></p>\n" +
            "<p><li>\u25CF Shall be solely responsible for managing his/her data and user account/s with Paragon.</li></p>\n" +
            "<p><li>\u25CF Will solely be responsible for protecting the account information, passwords and content submitted through the App/Website.</li></p>\n" +
            "<p><li>\u25CF Will not copy, duplicate, reproduce, sell or exploit any of the Services or any Paragon Content or his/her personal use or provide the same to any third party without the prior written consent of Paragon /the relevant third party owner of such Paragon Content.</li></p>\n" +
            "<p><li>\u25CF Will not use the Services or any Paragon Content to violate any applicable laws or regulations or in a manner so as to lead to any sort of infringement of any intellectual property (trademark/ copyright/patent) or other proprietary rights of Paragon or any other third party to whom such content belongs.</li></p>\n" +
            "<p><li>\u25CF Will not copy, reproduce, exploit or otherwise use the trade name, trade mark, logo or other branding of Paragon or any other third party to which the User has access as a result of the App/Website, without the prior written consent of Paragon or the relevant other third party.</li></p>\n" +
            "<p><li>\u25CF Agrees to follow all terms and conditions of membership / usage with third party companies they are associated with / will be associated with in the future.</li></p>\n" +
            "<p><li>\u25CF Will not attempt to or try to break any security measures or protocols related to the App/Website.</li></p>\n" +
            "</ul>\n" +
            "<p><li><strong> <u>5.DISCLAIMERS AND LIMITATIONS OF LIABILITY</u></strong></li></p>\n" +
            "<ul>\n" +
            "<p><li>\u25CF The User acknowledges that the Services and/or the Paragon Content may or may not meet his/her expectations. It is Paragon&rsquo;s endeavor to provide the most accurate and hassle free information to users in an uninterrupted and secured manner, but the accuracy, reliability and security of the Services and Paragon Content cannot be guaranteed and the User acknowledges that Paragon Content could have errors or may not be up to date, for which Paragon cannot be held accountable or liable at any time.</li></p>\n" +
            "<p><li>\u25CF Paragon takes no responsibility or liability for interruption of services due to maintenance or down time or certain events beyond the control of Paragon or the failure to provide Services of the App/ Website at any time. Without prejudice the generality of the foregoing, Paragon shall not be responsible for any loss of data or deletion of any User Content or data that may have been provided by the User resulting from such interruption of service.</li></p>\n" +
            "<p><li>\u25CF The User acknowledges that Paragon may as part of the features of the App/ Website, display advertisements / promotions / offers / enrolments, provide links to third party websites with whom Paragon may have a business or contractual relationship for which Paragon may receive monetary gain when the User engages with third parties through such advertisements / promotions / offers/ enrolments but that any such engagement is at the sole risk of the User as to costs and consequences. Paragon shall not be responsible or liable in any manner for any losses suffered or costs incurred by the User by virtue of any such engagement.</li></p>\n" +
            "<p><li>\u25CF Should the User be able to transfer points or other reward benefits of one third party organization to another third party organization or otherwise use such points/other reward benefits in connection with the services of disparate third party organizations, the User acknowledges that such transfer or use would be subject to the terms and conditions of the relevant third party organizations, of which Paragon may not have any knowledge. It should be noted that the Services are merely a conduit for any such dealings and the loss or gain of points/benefits would be governed solely by the terms and conditions set forth in this respect by the relevant third party organization. Paragon shall, in no way be responsible or liable for any loss of points/rewards suffered by the User as a result of any transfer/use of such points or rewards in the manner described herein.</li></p>\n" +
            "<p><li>\u25CF In no event or circumstance shall Paragon or its partners, employees, representatives, subsidiaries, directors, agents or members be liable for any direct, indirect, incidental, special, consequential or punitive damages or any loss of profits or revenues whether incurred directly or indirectly, or any loss of data, use, copyright, patent, goodwill, or other tangible losses, or claims from third parties resulting out of this agreement, terms and services provided by Paragon.</li></p>\n" +
            "<p><li>\u25CF In no event shall Paragon&rsquo;s be liable for any claim.</li></p>\n" +
            "<p><li>\u25CF A User will not be entitled to any sort of compensation for any delays, suspension or termination of Services by Paragon at any point of time.</li></p>\n" +
            "<p><li>\u25CF Paragon shall endeavor and make all reasonable efforts to provide Users of the App/Website with uninterrupted services, if for any reason any or all services provided by Paragon are interrupted, Paragon will not have any liability to any User or third party.</li></p>\n" +
            "</ul>\n" +
            "<p><li><strong> <u>6.INDEMNIFICATION</u></strong></li></p>\n" +
            "<ul>\n" +
            "<p><li>\u25CF The User will indemnify, defend and hold Paragon harmless, and Paragon 's officers, shareholders, directors, employees, agents (collectively, the \" Paragon Indemnified Parties&rdquo;), from and against all liabilities, obligations, causes of action, losses, damages, deficiencies, penalties, taxes, levies, fines, judgments, settlements, expenses (including attorneys' and accountants' fees and disbursements) including without limitation, damage to property or bodily injury, and costs arising from a claim, demand, proceeding, suit or action by a third party (\"Third Party Claims&rdquo;), incurred by or asserted against any of the Paragon Indemnified Parties to the extent such Third Party Claims relate to, arise out of or result from User's (i) use of the Service(s), Paragon Content and/or User Content and/or (ii ) placing of User Content on the App/Website, including but not limited to:</li></p>\n" +
            "<p><li>\u25CF Any willfully or intentionally wrongful, or negligent, act or omission of the User or any of its employee, agent or subcontractor, relating to this Agreement;</li></p>\n" +
            "<p><li>\u25CF User's failure to perform or improper performance under this Agreement, or breach of or inaccuracy in any of User's representations or warranties contained in this Agreement;</li></p>\n" +
            "<p><li>\u25CF The creation, use, or maintenance of the User Content, including, without limitation, any allegation that any User Content or any other information or content provided by the User Infringes a third person's copyright, trademark or other proprietary or intellectual property right, or misappropriates a third person's trade secrets or is unlawful, threatening, abusive, harassing, tortious, defamatory, obscene, harmful, libelous, invasive of another's privacy, hateful, or racially or ethnically objectionable;</li></p>\n" +
            "<p><li>\u25CF Any dispute or litigation between an Indemnified Party and a third party or any other third party claim caused by, arising from or relating to your and/or the User's actions or omissions in relation to this Agreement, our products and/or services, any User Content or the User Account;</li></p>\n" +
            "<p><li>\u25CF The User's negligence or misconduct; or</li></p>\n" +
            "<p><li>\u25CF The User's business dealings with any of the Service Providers.</li></p>\n" +
            "<p><li>\u25CF User's failure to comply with any other laws.</li></p>\n" +
            "</ul>\n" +
            "<ul>\n" +
            "<p><li>\u25CF This indemnity shall survive termination of this Agreement. To the fullest extent permitted by law, the foregoing indemnity will apply regardless of any fault, negligence, or breach of warranty or contract of or by us and/or our suppliers, licensors, affiliates, partners, subsidiaries, employees, representatives, agents and/or members.</li></p>\n" +
            "</ul>\n" +
            "<p><li><strong> <u>7.TERM AND TERMINATION</u></strong></li></p>\n" +
            "<ul>\n" +
            "<p><li>\u25CF By enrolling for the Services provided through the App/Website by Paragon by providing their personal information and details and clicking on \"I accept / I agree&rdquo; button on the App/ Website with respect to the Terms of Use and the Privacy Policy, a User agrees to these Terms of Use and becomes bound by them, on and from the time that the User click on the \"I accept/I agree&rdquo; button.</li></p>\n" +
            "<p><li>\u25CF A User can/ may terminate this Agreement at any time by electronic means provided via the App/ website. Non usage of the App/Website or Services for any period of time will not amount to termination of this Agreement unless expressly terminated, as provided under this Agreement.</li></p>\n" +
            "<p><li>\u25CF Paragon shall in its sole discretion have the right at any time and without prior notice to restrict, suspend, terminate, delete content or details of any user for any or all Services or this Agreement. Any such cases may be notified to the User as per Paragon's discretion.</li></p>\n" +
            "</ul>\n" +
            "<p><li><strong> <u>8.GENERAL</u></strong></li></p>\n" +
            "<ul>\n" +
            "<p><li>\u25CF This Agreement is an understanding of all the parties involved in Paragon. Any modifications or amendments will be valid from the time of publishing the same by Paragon and these become binding to all parties.</li></p>\n" +
            "<p><li>\u25CF Paragon may assign this Agreement, in whole or in part, at any time with or without notice. No user can assign their rights or obligations to any other person or party.</li></p>\n" +
            "<p><li>\u25CF This Agreement shall be governed by the laws of India and the parties submit to the exclusive jurisdiction of the courts of Mumbai, India.</li></p>\n" +
            "<p><li>\u25CF No waiver of any term of these Terms shall be deemed a further or continuing waiver of such term or any other term, and Paragon&rsquo;s failure to assert any right or provision under these Terms shall not constitute a waiver of such right or provision.</li></p>\n" +
            "<p><li>\u25CF Any dispute relating to this Agreement shall be submitted to a sole arbitrator appointed by Paragon for arbitration in Mumbai, India. Arbitration under this Agreement shall be conducted as per the Arbitration and Conciliation Act, 1996 and rules and ordinances thereto, as amended from time to time. The arbitrator's award shall be binding and may be entered as a judgment in any court of competent jurisdiction.</li></p>\n" +
            "<p><li>\u25CF Any provision of this Agreement that contemplates performance or observance subsequent to any termination or expiration of this Agreement or by its nature should survive termination or expiration of this Agreement will survive this Agreement for the period of such performance or observance.</li></p>\n" +
            "<p><li>\u25CF Paragon will communicate with the user by multiple mediums including but not limited to notices/push notifications on the App/ Website or notifications via email, sms, telephone calls or any other means of communication that Paragon may deem fit. The User agrees to receive communications from Paragon directly or indirectly or via any third party associated with Paragon.</li></p>\n" +
            "</ul></p>\n" +
            "<p>Should the User be desirous of contacting Paragon he/she may do so at the email address training@paragonrestaurant.net or <a href=\"mailto:managercrm@panasiamarketing.com\">managercrm@panasiamarketing.com</a> or at the contact details provided on the App/Website.</p>";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        context = RegisterActivity.this;
        final View snackbarview = getWindow().getDecorView();
        btn_submit = findViewById(R.id.btn_submit_register);
        ed_name = findViewById(R.id.ed_name_register);
        ed_adres = findViewById(R.id.ed_address_register);
        ed_email = findViewById(R.id.ed_email_register);
        ed_psd = findViewById(R.id.ed_psd_register);
        ed_cnfrm_psd = findViewById(R.id.ed_cnfrmpsd_register);
        ed_phone = findViewById(R.id.ed_phone_register);
        ed_dob = findViewById(R.id.ed_dob_register);
        ed_mrgdate = findViewById(R.id.ed_mrg_date_register);
        signin = findViewById(R.id.txt_register_signin);
        edCountry = findViewById(R.id.ed_Country);
        edState = findViewById(R.id.ed_State);
        edCity = findViewById(R.id.ed_City);
        edTitle = findViewById(R.id.edTitle);
        edCountryCode = findViewById(R.id.edCountryCode);
        edLocation = findViewById(R.id.ed_Location);
        edNationality = findViewById(R.id.ed_Nationality);
        tvTerms = findViewById(R.id.tvTerms);
        cbTerms = findViewById(R.id.cbTerms);

        sp = new ParagonSP(getApplicationContext());
        device_id = Settings.Secure.getString(getApplicationContext().getContentResolver(),
                Settings.Secure.ANDROID_ID);
        getCountry();
        getNationalities();
        final MyAlert alert = new MyAlert();
        ed_dob.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setDateToEditText(view, context, "Select DOB", 2);
            }
        });

        ed_mrgdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setDateToEditText(view, context, "Select Anniversary", 2);
            }
        });
        edTitle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectItem(view, RegisterActivity.this, edTitle, listItems);
            }
        });

        edState.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (edCountry.getText().toString().trim().equals("")) {
                    toast(context, "Select Country");
                }
            }
        });
        edCity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (edState.getText().toString().trim().equals("")) {
                    toast(context, "Select State");
                }
            }
        });
        edLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (edCity.getText().toString().trim().equals("")) {
                    toast(context, "Select City");
                }
            }
        });

        ed_adres.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() == 2) {
                    if (addressAlert) {
                        addressAlert=false;
                        alert.showAlert(context, "This address will be used for your home delivery when you order, so please enter all details correctly.");
                    }
                }

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        tvTerms.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                termsAlertDialog();
            }
        });


        signin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        btn_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                postRegister();

            }
        });
    }


    protected Dialog onCreateDialog(int id) {
        switch (id) {
            case DATE_ID:
                // set date picker as current date

                DatePickerDialog dialog = new DatePickerDialog(this, datePickerListener, y, m, d);
                dialog.setCancelable(false);
                dialog.setCanceledOnTouchOutside(false);
                dialog.setButton(DialogInterface.BUTTON_NEGATIVE, "", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        //Toast.makeText(CheckOut.this,"clicked cancel",Toast.LENGTH_LONG).show();
                    }
                });

                dialog.getDatePicker().setMaxDate(new Date().getTime());
                dialog.getDatePicker().setSpinnersShown(false);

                return dialog;


        }
        return null;
    }

    private DatePickerDialog.OnDateSetListener datePickerListener = new DatePickerDialog.OnDateSetListener() {

        // when dialog box is closed, below method will be called.
        public void onDateSet(DatePicker view, int selectedYear,
                              int selectedMonth, int selectedDay) {
            y = selectedYear;
            m = selectedMonth;


            if (selectedMonth < 9) {
                m = 0 + selectedMonth;
                mtest = "0" + String.valueOf(selectedMonth + 1);

                m = Integer.parseInt(mtest);

            } else {
                mtest = String.valueOf(selectedMonth + 1);

            }

            if (selectedDay < 10) {
                d = 0 + selectedDay;
                dtest = "0" + String.valueOf(selectedDay);

                d = Integer.parseInt(dtest);

            } else {
                dtest = String.valueOf(selectedDay);
                d = Integer.parseInt(dtest);
            }
            // set selected date into datepicker also

            if (booldob) {
                ed_mrgdate.setText(new StringBuilder().append(dtest)
                        .append("-").append(mtest).append("-").append(y));
                ed_mrgdate.setEnabled(true);
                ed_dob.setEnabled(true);
            } else {

                ed_dob.setText(new StringBuilder().append(dtest)
                        .append("-").append(mtest).append("-").append(y));
                ed_dob.setEnabled(true);
                ed_mrgdate.setEnabled(true);

            }
        }


    };

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.
                INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        return true;
    }

    public void postRegister() {
        MyAlert alert = new MyAlert();
        if (edTitle.getText().toString().toString().trim().equals("")) {
            alert.showAlert(context, "Select title");
            return;
        }
        if (ed_name.getText().toString().toString().trim().equals("")) {
            alert.showAlert(context, "Enter Name");
            return;
        }
        if (!isValidEmail(ed_email.getText().toString().toString().trim())) {
            alert.showAlert(context, "Enter Valid Email");
            return;
        }
        if (edCountryCode.getText().toString().toString().trim().equals("")) {
            alert.showAlert(context, "Select Phone Code");
            return;
        }
        if (ed_phone.getText().toString().toString().trim().equals("")) {
            alert.showAlert(context, "Enter Phone Number");
            return;
        }
        if (ed_dob.getText().toString().toString().trim().equals("")) {
            alert.showAlert(context, "Select Date of Birth");
            return;
        }
        if (ed_psd.getText().toString().toString().trim().equals("")) {
            alert.showAlert(context, "Enter Password");
            return;
        }
        if (ed_cnfrm_psd.getText().toString().toString().trim().equals("")) {
            alert.showAlert(context, "Enter Confirm Password");
            return;
        }
        if (!ed_cnfrm_psd.getText().toString().toString().trim().equals(ed_psd.getText().toString().toString().trim())) {
            alert.showAlert(context, "Password mismach.Please check");
            return;
        }
        if (!cbTerms.isChecked()) {
            alert.showAlert(context, "Check terms and condition");
            return;
        }

        showProgress();
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("CustomerId", "0");
            jsonObject.put("Title", edTitle.getText().toString().trim());
            jsonObject.put("FirstName", ed_name.getText().toString().trim());
            jsonObject.put("LastName", "");
            if (!ed_adres.getText().toString().trim().equals("")) {
                isAddress = true;
            }
            jsonObject.put("Address", ed_adres.getText().toString().trim());
            jsonObject.put("Email", ed_email.getText().toString().trim());
            jsonObject.put("CountryCode", edCountryCode.getText().toString().trim());
            jsonObject.put("Mobile", ed_phone.getText().toString().trim());
            jsonObject.put("LocationName", edLocation.getText().toString().trim());
            String[] split = ed_dob.getText().toString().trim().split("-");
            jsonObject.put("Bday", split[0]);
            jsonObject.put("Bmonth", split[1]);
            jsonObject.put("Byear", split[2]);
            // jsonObject.put("Anniversary", ed_mrgdate.getText().toString().trim());
            if (!ed_mrgdate.getText().toString().trim().equals("")) {
                String[] split1 = ed_mrgdate.getText().toString().trim().split("-");
                jsonObject.put("WeddingDate", split1[0]);
                jsonObject.put("WeddingMonth", split1[1]);
                jsonObject.put("WeddingYear", split1[2]);
            } else {
                jsonObject.put("WeddingDate", "");
                jsonObject.put("WeddingMonth", "");
                jsonObject.put("WeddingYear", "");
            }
            jsonObject.put("Password", ed_psd.getText().toString().trim());
            jsonObject.put("DeviceType", "android");
            jsonObject.put("DeviceToken", sp.getdeviceId());
            jsonObject.put("Nationality", edNationality.getText().toString().trim());
            jsonObject.put("RegisteredFrom", "Customer App");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        post(RegisterActivity.this, PNNetworkConstants.SAVE_CUSTOMER, jsonObject.toString(), new PNNetworkManager.NetworkInterface() {
            @Override
            public void onResponse(PNApiResponseWrapper baseResponse) {
                dismissProgres();
                if (baseResponse.isSuccess()) {
                    splitResponse = new SplitResponse(baseResponse.getJsonObjectResponse());
                    if (splitResponse.getStatus().equals("1") || splitResponse.getStatus().equals("2")) {
                        try {
                            // userid= splitResponse.getData().getString("CustomerId");
                            sp.setlogin("success");
                            sp.setuserid(splitResponse.getCustomerId());
                            sp.setname(ed_name.getText().toString().trim());
                            sp.setphone(ed_phone.getText().toString().trim());
                            sp.setemail(ed_email.getText().toString().trim());
                            sp.setaddress(ed_adres.getText().toString().trim());
                            if (isAddress) {
                                saveAddress();
                            }
                            Intent in = new Intent(context, SelectResturantActivity.class);
                            in.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(in);
                            finish();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    } else {
                        toast(getApplicationContext(), splitResponse.getMessage());
                    }
                } else {
                    toast(getApplicationContext(), baseResponse.getMessage());
                }

            }
        });
    }

    public void selectItem(View v1, Activity activity, final EditText editText, final String[] listItems) {
        final PopupWindow pwindo;
        LayoutInflater inflater = (LayoutInflater) activity
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View layout = inflater.inflate(R.layout.select_list,
                (ViewGroup) v1.findViewById(R.id.linLaySel));

        pwindo = new PopupWindow(layout, ActionBar.LayoutParams.WRAP_CONTENT, ActionBar.LayoutParams.WRAP_CONTENT, true);
        pwindo.showAsDropDown(editText, 0, 0);


        ListView ll = (ListView) pwindo.getContentView().findViewById(R.id.lvSelectCity);


        ArrayAdapter<String> adapter = new ArrayAdapter<String>(activity,
                android.R.layout.simple_list_item_1, android.R.id.text1, listItems);


        // Assign adapter to ListView
        ll.setAdapter(adapter);
        ll.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                pwindo.dismiss();
                editText.setText(listItems[position]);
                if (editText == edCountry) {
                    countryId = countryLists.get(position).getCountryId() + "";
                    getStates(countryId);
                } else if (editText == edState) {
                    stateId = stateLists.get(position).getStateId() + "";
                    getCities(stateId);
                } else if (editText == edCity) {
                    cityId = cityLists.get(position).getCityID() + "";
                    getLocations(cityId);
                } else if (editText == edLocation) {
                    locationsId = locationLists.get(position).getLocationId() + "";
                }
            }
        });
    }

    public void getNationalities() {
        final String[] list = new String[0];
        nationalitiesLists = new ArrayList(Arrays.asList(list));
        showProgress();
        JSONObject jsonObject = new JSONObject();

        post(RegisterActivity.this, PNNetworkConstants.BindNationality, jsonObject.toString(), new PNNetworkManager.NetworkInterface() {
            @Override
            public void onResponse(PNApiResponseWrapper baseResponse) {
                dismissProgres();
                if (baseResponse.isSuccess()) {
                    splitResponse = new SplitResponse(baseResponse.getJsonObjectResponse());
                    if (splitResponse.getStatus().equals("1")) {
                        Gson gson = new Gson();
                        nationalitiesResponse = gson.fromJson(splitResponse.getResponse(), NationalitiesResponse.class);
                        nationalitiesLists.addAll(nationalitiesResponse.getData());
                        if (nationalitiesLists.size() > 0) {
                            nationalities = new String[nationalitiesLists.size()];
                            for (int i = 0; i < nationalitiesLists.size(); i++) {
                                nationalities[i] = nationalitiesLists.get(i).getNationality();
                            }

                            ArrayAdapter<String> adapter = new ArrayAdapter<String>
                                    (RegisterActivity.this, android.R.layout.select_dialog_item, nationalities);
                            edNationality.setThreshold(1);//will start working from first character
                            edNationality.setAdapter(adapter);
                        }

                    } else {
                        toast(getApplicationContext(), splitResponse.getMessage());
                    }
                } else {
                    toast(getApplicationContext(), baseResponse.getMessage());
                }

            }
        });

    }

    public void getCountry() {
        final String[] list = new String[0];
        countryLists = new ArrayList(Arrays.asList(list));
        showProgress();
        JSONObject jsonObject = new JSONObject();

        post(RegisterActivity.this, PNNetworkConstants.BindCountries, jsonObject.toString(), new PNNetworkManager.NetworkInterface() {
            @Override
            public void onResponse(PNApiResponseWrapper baseResponse) {
                dismissProgres();
                if (baseResponse.isSuccess()) {
                    splitResponse = new SplitResponse(baseResponse.getJsonObjectResponse());
                    if (splitResponse.getStatus().equals("1")) {
                        Gson gson = new Gson();
                        countryResponse = gson.fromJson(splitResponse.getResponse(), CountryResponse.class);
                        countryLists.addAll(countryResponse.getData());
                        if (countryLists.size() > 0) {
                            countries = new String[countryLists.size()];
                            countryCode = new String[countryLists.size()];
                            for (int i = 0; i < countryLists.size(); i++) {
                                countries[i] = countryLists.get(i).getCountry();
                                countryCode[i] = countryLists.get(i).getCountryCode();
                            }

                            edCountry.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    selectItem(view, RegisterActivity.this, edCountry, countries);
                                }
                            });
                            edCountryCode.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    selectItem(view, RegisterActivity.this, edCountryCode, countryCode);
                                }
                            });
                        }

                    } else {
                        toast(getApplicationContext(), splitResponse.getMessage());
                    }
                } else {
                    toast(getApplicationContext(), baseResponse.getMessage());
                }

            }
        });

    }

    public void getStates(String countryId) {
        final String[] list = new String[0];
        stateLists = new ArrayList(Arrays.asList(list));
        showProgress();
        JSONObject jsonObject = new JSONObject();

        try {
            jsonObject.put("CountryId", countryId);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        post(RegisterActivity.this, PNNetworkConstants.BindStates, jsonObject.toString(), new PNNetworkManager.NetworkInterface() {
            @Override
            public void onResponse(PNApiResponseWrapper baseResponse) {
                dismissProgres();
                if (baseResponse.isSuccess()) {
                    splitResponse = new SplitResponse(baseResponse.getJsonObjectResponse());
                    if (splitResponse.getStatus().equals("1")) {
                        Gson gson = new Gson();
                        statesResponse = gson.fromJson(splitResponse.getResponse(), StatesResponse.class);
                        stateLists.addAll(statesResponse.getData());
                        if (stateLists.size() > 0) {
                            states = new String[stateLists.size()];
                            for (int i = 0; i < stateLists.size(); i++) {
                                states[i] = stateLists.get(i).getState();
                            }

                            edState.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    selectItem(view, RegisterActivity.this, edState, states);
                                }
                            });
                        }

                    } else {
                        toast(getApplicationContext(), splitResponse.getMessage());
                    }
                } else {
                    toast(getApplicationContext(), baseResponse.getMessage());
                }

            }
        });

    }

    public void getCities(String stateId) {
        final String[] list = new String[0];
        cityLists = new ArrayList(Arrays.asList(list));
        showProgress();
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("StateId", stateId);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        post(RegisterActivity.this, PNNetworkConstants.BindCities, jsonObject.toString(), new PNNetworkManager.NetworkInterface() {
            @Override
            public void onResponse(PNApiResponseWrapper baseResponse) {
                dismissProgres();
                if (baseResponse.isSuccess()) {
                    splitResponse = new SplitResponse(baseResponse.getJsonObjectResponse());
                    if (splitResponse.getStatus().equals("1")) {
                        Gson gson = new Gson();
                        cityResponse = gson.fromJson(splitResponse.getResponse(), CityResponse.class);
                        cityLists.addAll(cityResponse.getData());
                        if (cityLists.size() > 0) {
                            cities = new String[cityLists.size()];
                            for (int i = 0; i < cityLists.size(); i++) {
                                cities[i] = cityLists.get(i).getCity();
                            }

                            edCity.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    selectItem(view, RegisterActivity.this, edCity, cities);
                                }
                            });
                        }

                    } else {
                        toast(getApplicationContext(), splitResponse.getMessage());
                    }
                } else {
                    toast(getApplicationContext(), baseResponse.getMessage());
                }

            }
        });

    }

    public void getLocations(String cityId) {
        final String[] list = new String[0];
        locationLists = new ArrayList(Arrays.asList(list));
        showProgress();
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("CityId", cityId);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        post(RegisterActivity.this, PNNetworkConstants.BindLocation, jsonObject.toString(), new PNNetworkManager.NetworkInterface() {
            @Override
            public void onResponse(PNApiResponseWrapper baseResponse) {
                dismissProgres();
                if (baseResponse.isSuccess()) {
                    splitResponse = new SplitResponse(baseResponse.getJsonObjectResponse());
                    if (splitResponse.getStatus().equals("1")) {
                        Gson gson = new Gson();
                        locationByCityResponse = gson.fromJson(splitResponse.getResponse(), LocationByCityResponse.class);
                        locationLists.addAll(locationByCityResponse.getData());
                        if (locationLists.size() > 0) {
                            locations = new String[locationLists.size()];
                            for (int i = 0; i < locationLists.size(); i++) {
                                locations[i] = locationLists.get(i).getLocation();
                            }

                            edLocation.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    selectItem(view, RegisterActivity.this, edLocation, locations);
                                }
                            });
                        }

                    } else {
                        toast(getApplicationContext(), splitResponse.getMessage());
                    }
                } else {
                    toast(getApplicationContext(), baseResponse.getMessage());
                }

            }
        });

    }

    public void termsAlertDialog() {
        String mimeType = "text/html";
        String encoding = "utf-8";
        final Dialog alertDialog = new Dialog(this, android.R.style.Theme_Light);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(R.layout.terms_and_conditions);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        final Button btnAccept = (Button) alertDialog.findViewById(R.id.btnAccept);
        final Button btnDecline = (Button) alertDialog.findViewById(R.id.btnDecline);
       /* PDFView pdfView= (PDFView)alertDialog. findViewById(R.id.pdfview);
        pdfView.fromAsset(SAMPLE_FILE)
                .defaultPage(1)
                .showMinimap(true)
                .enableSwipe(true)
                .load();*/
       /*WebView webView=(WebView) alertDialog.findViewById(R.id.webView);
        webView.loadData(htmlText, mimeType, encoding);*/
        TextView tvText = (TextView) alertDialog.findViewById(R.id.tvText);
        tvText.setText(Html.fromHtml(htmlText));
        tvText.setMovementMethod(new ScrollingMovementMethod());


        btnAccept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                cbTerms.setChecked(true);
                alertDialog.dismiss();
            }
        });
        btnDecline.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cbTerms.setChecked(false);
                alertDialog.dismiss();
            }
        });
        alertDialog.setCancelable(false);
        alertDialog.show();
    }

    public void saveAddress() {
        //showProgress();
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("DeliveryAddressId", "0");
            jsonObject.put("CustomerId", PNSP.getuserid());
            jsonObject.put("Address", ed_adres.getText().toString().trim());
            jsonObject.put("PostCode", "");
            jsonObject.put("LandMark", "");
            jsonObject.put("ContactName", ed_name.getText().toString().trim());
            jsonObject.put("ContactNumber", ed_phone.getText().toString().trim());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        post(RegisterActivity.this, PNNetworkConstants.SaveDeliveryAddress, jsonObject.toString(), new PNNetworkManager.NetworkInterface() {
            @Override
            public void onResponse(PNApiResponseWrapper baseResponse) {
                //dismissProgres();
                if (baseResponse.isSuccess()) {
                    splitResponse = new SplitResponse(baseResponse.getJsonObjectResponse());

                    MyAlert alert = new MyAlert();
                    if (splitResponse.getStatus().equals("1")) {

                        try {
                            // userid= splitResponse.getData().getString("CustomerId");
                            // alert.showAlertFinish(PNAddAddress.this, splitResponse.getMessage());
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    } else {
                        toast(getApplicationContext(), splitResponse.getMessage());
                    }
                } else {
                    toast(getApplicationContext(), baseResponse.getMessage());
                }

            }
        });
    }

}
