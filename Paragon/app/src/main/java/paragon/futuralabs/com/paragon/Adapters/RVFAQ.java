package paragon.futuralabs.com.paragon.Adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import paragon.futuralabs.com.paragon.Activity.PNFAQ;
import paragon.futuralabs.com.paragon.R;
import paragon.futuralabs.com.paragon.models.FAQList;
import paragon.futuralabs.com.paragon.models.FavLocationList;

/**
 * Created by Shamir on 26-06-2018.
 */

public class RVFAQ extends RecyclerView.Adapter<RVFAQ.ViewHolder> {
    List<FAQList> lists;
    Context context;

    public RVFAQ(Context context,List<FAQList> lists) {
        this.context = context;
        this.lists = lists;

    }

    @Override
    public RVFAQ.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.rv_faq_item, parent, false);
        RVFAQ.ViewHolder viewHolder = new RVFAQ.ViewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RVFAQ.ViewHolder holder, int position) {

        holder.tvQtn.setText("Qtn : "+lists.get(position).getFaqQus());
        holder.tvAns.setText("Ans : "+lists.get(position).getFaqAns());

    }

    @Override
    public int getItemCount() {
        return lists.size();

    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView tvQtn,tvAns;

        public ViewHolder(View itemView) {
            super(itemView);
            tvQtn =   itemView.findViewById(R.id.tvQtn);
            tvAns =   itemView.findViewById(R.id.tvAns);
        }

    }
}


