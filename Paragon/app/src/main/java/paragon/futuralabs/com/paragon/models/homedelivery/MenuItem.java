
package paragon.futuralabs.com.paragon.models.homedelivery;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

@SuppressWarnings("unused")
public class MenuItem implements Serializable{

    @SerializedName("Available")
    private String mAvailable;
    @SerializedName("Currency")
    private String mCurrency;
    @SerializedName("Description")
    private String mDescription;
    @SerializedName("DishName")
    private String mDishName;
    @SerializedName("Image")
    private String mImage;
    @SerializedName("IsContainNuts")
    private int mIsContainNuts;
    @SerializedName("IsRecommended")
    private Boolean mIsRecommended;
    @SerializedName("MenuItemsId")
    private Long mMenuItemsId;
    @SerializedName("POSID")
    private String mPOSID;
    @SerializedName("Price")
    private String mPrice;
    @SerializedName("VegOrNonVeg")
    private int mVegOrNonVeg;
    @SerializedName("InStock")
    private Long mInStock;


    public String getAvailable() {
        return mAvailable;
    }

    public void setAvailable(String Available) {
        mAvailable = Available;
    }

    public String getCurrency() {
        return mCurrency;
    }

    public void setCurrency(String Currency) {
        mCurrency = Currency;
    }

    public String getDescription() {
        return mDescription;
    }

    public void setDescription(String Description) {
        mDescription = Description;
    }

    public String getDishName() {
        return mDishName;
    }

    public void setDishName(String DishName) {
        mDishName = DishName;
    }

    public String getImage() {
        return mImage;
    }

    public void setImage(String Image) {
        mImage = Image;
    }

    public int getIsContainNuts() {
        return mIsContainNuts;
    }

    public void setIsContainNuts(int IsContainNuts) {
        mIsContainNuts = IsContainNuts;
    }

    public Boolean getIsRecommended() {
        return mIsRecommended;
    }

    public void setIsRecommended(Boolean IsRecommended) {
        mIsRecommended = IsRecommended;
    }

    public Long getMenuItemsId() {
        return mMenuItemsId;
    }

    public void setMenuItemsId(Long MenuItemsId) {
        mMenuItemsId = MenuItemsId;
    }

    public String getPOSID() {
        return mPOSID;
    }

    public void setPOSID(String POSID) {
        mPOSID = POSID;
    }

    public String getPrice() {
        return mPrice;
    }

    public void setPrice(String Price) {
        mPrice = Price;
    }

    public int getVegOrNonVeg() {
        return mVegOrNonVeg;
    }

    public void setVegOrNonVeg(int VegOrNonVeg) {
        mVegOrNonVeg = VegOrNonVeg;
    }

    public Long getInStock() {
        return mInStock;
    }

    public void setInStock(Long InStock) {
        mInStock = InStock;
    }

}
