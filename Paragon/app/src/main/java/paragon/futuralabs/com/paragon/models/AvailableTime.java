
package paragon.futuralabs.com.paragon.models;

import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class AvailableTime {

    @SerializedName("AvailableFrom")
    private String mAvailableFrom;
    @SerializedName("AvailableTo")
    private String mAvailableTo;
    @SerializedName("Day")
    private String mDay;
    @SerializedName("WeekDaysId")
    private Long mWeekDaysId;

    public String getAvailableFrom() {
        return mAvailableFrom;
    }

    public void setAvailableFrom(String AvailableFrom) {
        mAvailableFrom = AvailableFrom;
    }

    public String getAvailableTo() {
        return mAvailableTo;
    }

    public void setAvailableTo(String AvailableTo) {
        mAvailableTo = AvailableTo;
    }

    public String getDay() {
        return mDay;
    }

    public void setDay(String Day) {
        mDay = Day;
    }

    public Long getWeekDaysId() {
        return mWeekDaysId;
    }

    public void setWeekDaysId(Long WeekDaysId) {
        mWeekDaysId = WeekDaysId;
    }

}
