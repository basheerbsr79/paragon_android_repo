
package paragon.futuralabs.com.paragon.models;

import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class Top5BillList {

    @SerializedName("BillDate")
    private String mBillDate;
    @SerializedName("ERecieptBillDetails")
    private Long mBillMasterId;
    @SerializedName("BillNo")
    private String mBillNo;
    @SerializedName("Currency")
    private String mCurrency;
    @SerializedName("Discount")
    private Long mDiscount;
    @SerializedName("OutletsName")
    private String mOutletsName;
    @SerializedName("TotalBillAmt")
    private Long mTotalBillAmt;

    public String getBillDate() {
        return mBillDate;
    }

    public void setBillDate(String BillDate) {
        mBillDate = BillDate;
    }

    public Long getBillMasterId() {
        return mBillMasterId;
    }

    public void setBillMasterId(Long BillMasterId) {
        mBillMasterId = BillMasterId;
    }

    public String getBillNo() {
        return mBillNo;
    }

    public void setBillNo(String BillNo) {
        mBillNo = BillNo;
    }

    public String getCurrency() {
        return mCurrency;
    }

    public void setCurrency(String Currency) {
        mCurrency = Currency;
    }

    public Long getDiscount() {
        return mDiscount;
    }

    public void setDiscount(Long Discount) {
        mDiscount = Discount;
    }

    public String getOutletsName() {
        return mOutletsName;
    }

    public void setOutletsName(String OutletsName) {
        mOutletsName = OutletsName;
    }

    public Long getTotalBillAmt() {
        return mTotalBillAmt;
    }

    public void setTotalBillAmt(Long TotalBillAmt) {
        mTotalBillAmt = TotalBillAmt;
    }

}
