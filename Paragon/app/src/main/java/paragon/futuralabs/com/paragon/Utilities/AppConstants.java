package paragon.futuralabs.com.paragon.Utilities;

/**
 * Created by Shamir on 26/02/16.
 */
public interface AppConstants {
    public  static  String BrandId="BrandId";
    public  static  String BrandName="BrandName";
    public  static  String FROM="FROM";
    public  static  String Paragon="Paragon";
    public  static  String Salkara="Salkara";
    public  static  String Mgrill="Mgrill";
    public  static  String BrownTown="BrownTown";
    public  static  String CatId="CatId";
    public  static  String CatName="CatName";
    public  static  String CatImage="CatImage";
    public  static  String Item="Item";
    public  static  String MENU="MENU";
    public  static  String HOMEDELIVERY="HOMEDELIVERY";
    public  static  String position="position";
    public  static  String Price="Price";
    public  static  String Currency="Currency";
    public  static  String OrderId="OrderId";

    public  static  String ParagonFB="https://www.facebook.com/calicutparagon/";
    public  static  String SalkaraFB="https://www.facebook.com/salkararestaurant/";
    public  static  String MgrillFB="https://www.facebook.com/mgrillrestaurant/";
    public  static  String ParagonIG="https://www.instagram.com/calicutparagon";
}
