package paragon.futuralabs.com.paragon.Activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import paragon.futuralabs.com.paragon.R;
import paragon.futuralabs.com.paragon.models.Image;
import paragon.futuralabs.com.paragon.models.MenuItemsList;
import paragon.futuralabs.com.paragon.models.VouchersList;

import static paragon.futuralabs.com.paragon.Utilities.AppConstants.FROM;
import static paragon.futuralabs.com.paragon.Utilities.AppConstants.Item;
import static paragon.futuralabs.com.paragon.Utilities.AppConstants.Mgrill;
import static paragon.futuralabs.com.paragon.Utilities.AppConstants.Salkara;

public class PNVoucherDetails extends PNBaseActivity {

    LinearLayout linLayContainer;
    TextView tvHeading;
    ImageView ic_back_arrow;
    ImageView imageView;
    TextView tvOutletName, tvVoucherName, tvVoucherCode, tvDescription, tvAmount, tvExpire, tvTerms, tvTime;
    VouchersList vouchersList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pnvoucher_details);
        init();
        handleEvents();
        trackEvent("Vouchers", "View Vouchers Details");
    }

    private void handleEvents() {
        tvHeading.setText("Voucher Details");
        ic_back_arrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        String url = vouchersList.getVoucherPhoto();
        if (!url.equals("")) {
            Picasso.with(PNVoucherDetails.this)
                    .load(url)
                    .error(R.drawable.videoimage)
                    .placeholder(R.drawable.videoimage)
                    .into(imageView);
        }
        tvOutletName.setText(vouchersList.getOutletName());
        tvVoucherName.setText(vouchersList.getVoucherName());
        tvVoucherCode.setText("Code : " + vouchersList.getVoucherCode());
        tvDescription.setText(vouchersList.getVoucherDescription());
        if (vouchersList.getDiscountType().equalsIgnoreCase("Amount"))
            tvAmount.setText("Amount : " + vouchersList.getCurrency() + " " + vouchersList.getVoucherAmount());
        else tvAmount.setText(vouchersList.getVoucherAmount() + " %");
        tvExpire.setText("Valid Upto : " + vouchersList.getExpiryDate());
        tvTime.setText(vouchersList.getMealPeriod());
        tvTerms.setText("Terms And Conditions : " + vouchersList.getTermsAndConditions());

    }

    private void init() {
        linLayContainer = findViewById(R.id.linLayContainer);
        ic_back_arrow = findViewById(R.id.img_back_toolbar);
        imageView = findViewById(R.id.imageView);
        tvHeading = findViewById(R.id.txt_title_toolbar);
        tvOutletName = findViewById(R.id.tvOutletName);
        tvVoucherName = findViewById(R.id.tvVoucherName);
        tvVoucherCode = findViewById(R.id.tvVoucherCode);
        tvDescription = findViewById(R.id.tvDescription);
        tvAmount = findViewById(R.id.tvAmount);
        tvExpire = findViewById(R.id.tvExpire);
        tvTerms = findViewById(R.id.tvTerms);
        tvTime = findViewById(R.id.tvTime);
        Intent intent = this.getIntent();
        Bundle bundle = intent.getExtras();
        vouchersList = (VouchersList) bundle.getSerializable(Item);
    }
}
