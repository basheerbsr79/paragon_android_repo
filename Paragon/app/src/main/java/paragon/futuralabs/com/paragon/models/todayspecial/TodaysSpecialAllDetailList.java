package paragon.futuralabs.com.paragon.models.todayspecial;

import java.io.Serializable;

/**
 * Created by Shamir on 23-07-2018.
 */

public class TodaysSpecialAllDetailList implements Serializable {

    String mImage,mName,mTime,mPrmoType;

    public String getImage() {
        return mImage;
    }

    public void setImage(String Image) {
        mImage = Image;
    }

    public String getName() {
        return mName;
    }

    public void setName(String Name) {
        mName = Name;
    }

    public String getTime() {
        return mTime;
    }

    public void setTime(String Time) {
        mTime = Time;
    }

    public String getPrmoType() {
        return mPrmoType;
    }

    public void setPrmoType(String PrmoType) {
        mPrmoType = PrmoType;
    }


}
