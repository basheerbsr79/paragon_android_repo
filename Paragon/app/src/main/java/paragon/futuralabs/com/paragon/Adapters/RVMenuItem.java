package paragon.futuralabs.com.paragon.Adapters;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import paragon.futuralabs.com.paragon.Activity.PNCart;
import paragon.futuralabs.com.paragon.Activity.PNHDMenuItemDetails;
import paragon.futuralabs.com.paragon.Activity.PNHDMenuItems;
import paragon.futuralabs.com.paragon.Activity.PNMenuItemDetails;
import paragon.futuralabs.com.paragon.Activity.PNMenuItems;
import paragon.futuralabs.com.paragon.R;
import paragon.futuralabs.com.paragon.apiManager.PNApiResponseWrapper;
import paragon.futuralabs.com.paragon.apiManager.PNNetworkConstants;
import paragon.futuralabs.com.paragon.apiManager.PNNetworkManager;
import paragon.futuralabs.com.paragon.apiManager.SplitResponse;
import paragon.futuralabs.com.paragon.models.MenuCatagoryList;
import paragon.futuralabs.com.paragon.models.MenuItemsList;

import static paragon.futuralabs.com.paragon.Utilities.AppConstants.CatId;
import static paragon.futuralabs.com.paragon.Utilities.AppConstants.FROM;
import static paragon.futuralabs.com.paragon.Utilities.AppConstants.Item;
import static paragon.futuralabs.com.paragon.Utilities.AppConstants.MENU;
import static paragon.futuralabs.com.paragon.Utilities.ConnectionDetector.toast;
import static paragon.futuralabs.com.paragon.apiManager.PNNetworkManager.post;

/**
 * Created by Shamir on 19-02-2018.
 */

public class RVMenuItem extends RecyclerView.Adapter<RVMenuItem.ViewHolder> {
    List<MenuItemsList> menuItemsList;
    Context context;
    String type;
    SplitResponse splitResponse;

    public RVMenuItem(Context context, List<MenuItemsList> menuItemsList,String type) {
        this.context = context;
        this.menuItemsList=menuItemsList;
        this.type=type;

    }

    @Override
    public RVMenuItem.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.rv_menu_item, parent, false);
        RVMenuItem.ViewHolder viewHolder = new RVMenuItem.ViewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final RVMenuItem.ViewHolder holder, final int position) {

        /*String url=menuItemsList.get(position).getImage();
        Picasso.with(context)
                .load(url)
                .error(R.drawable.dish_item_placeholder)
                .placeholder(R.drawable.dish_item_placeholder)
                .resize(100,100).centerCrop()
                .into(holder.imvImage);*/
        holder.textView.setText(menuItemsList.get(position).getDishName().trim());
        holder.textViewDesc.setText(menuItemsList.get(position).getDescription());
        if (menuItemsList.get(position).getVegOrNonVeg()==1)
            holder.imvVeg.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_veg));
        else if (menuItemsList.get(position).getVegOrNonVeg()==0)
            holder.imvVeg.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_nonveg));
        else holder.imvVeg.setVisibility(View.GONE);
        if (menuItemsList.get(position).getIsFavorite()==0)
            holder.imvHeart.setImageDrawable(context.getResources().getDrawable(R.drawable.heart_grey));
        else
            holder.imvHeart.setImageDrawable(context.getResources().getDrawable(R.drawable.heart_red));
        /*if (menuItemsList.get(position).getIsContainNuts())
            holder.tvConNuts.setText("Contain Nuts");
        else
            holder.tvConNuts.setText("No Nuts");*/
        if (menuItemsList.get(position).getIsRecommended())
            holder.tvRecommended.setText("Recommended");
        else
            holder.tvRecommended.setText("Not Recommended");
        holder.tvPrice.setText(menuItemsList.get(position).getCurrency()+" "+menuItemsList.get(position).getPrice());
        holder.tvOrderNow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                  Intent in = new Intent(context, PNHDMenuItems.class);
                    context.startActivity(in);
            }
        });

        holder.imvHeart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (menuItemsList.get(position).getIsFavorite()==0)
                    addRemFavorite(holder.imvHeart,position,"1");
                else
                    addRemFavorite(holder.imvHeart,position,"0");

            }
        });
    }

    @Override
    public int getItemCount() {
        return menuItemsList.size();

    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView imvImage,imvVeg,imvHeart;
        TextView textView,textViewDesc,tvConNuts,tvRecommended,tvPrice,tvOrderNow;

        public ViewHolder(View itemView) {
            super(itemView);
            imvImage =   itemView.findViewById(R.id.imageView);
            imvVeg =   itemView.findViewById(R.id.imvVeg);
            imvHeart=   itemView.findViewById(R.id.imvHeart);
            textView= itemView.findViewById(R.id.textView);
            textViewDesc= itemView.findViewById(R.id.textViewDesc);
            tvConNuts= itemView.findViewById(R.id.tvConNuts);
            tvRecommended= itemView.findViewById(R.id.tvRecommended);
            tvPrice= itemView.findViewById(R.id.tvPrice);
            tvOrderNow= itemView.findViewById(R.id.tvOrderNow);

        }


    }

    private void addRemFavorite(final ImageView imageView, final int pos, final String status) {
        ((PNMenuItems)context).showProgress();
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("CustomerId", ((PNMenuItems)context).PNSP.getuserid());
            jsonObject.put("MenuItemsId", menuItemsList.get(pos).getMenuItemsId());
            jsonObject.put("Status",status);
            jsonObject.put("OutletId", ((PNMenuItems)context).PNSP.getOutlet());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        post(context, PNNetworkConstants.ManageFavoriteMenu, jsonObject.toString(), new PNNetworkManager.NetworkInterface() {
            @Override
            public void onResponse(PNApiResponseWrapper baseResponse) {
                ((PNMenuItems)context).dismissProgres();
                if (baseResponse.isSuccess()) {
                    splitResponse = new SplitResponse(baseResponse.getJsonObjectResponse());
                    if (splitResponse.getStatus().equals("1")) {
                        if (status.equals("0")) {
                            menuItemsList.get(pos).setIsFavorite(Long.parseLong("0"));
                            imageView.setImageDrawable(context.getResources().getDrawable(R.drawable.heart_grey));
                            toast(context,"Removed from favourite dish");
                        }else {
                            menuItemsList.get(pos).setIsFavorite(Long.parseLong("1"));
                            imageView.setImageDrawable(context.getResources().getDrawable(R.drawable.heart_red));
                            toast(context,"Added to favourite dish");
                        }

                    } else {
                        toast(context, splitResponse.getMessage());
                    }
                } else {
                    toast(context, baseResponse.getMessage());
                }

            }
        });
    }
}
