package paragon.futuralabs.com.paragon.apiManager;

/**
 * Created by Shamir on 03-02-2018.
 */

public interface PNNetworkConstants {

    //public static final String BASE_URL = "http://crmapi.netstager.com/jsonwebservice.asmx/";
    public static final String BASE_URL = "http://crm.paragonrestaurant.net/jsonwebservice.asmx/";
    public static final String REQUEST_BODY_CONTENT_TYPE = "application/json";
    public static final String API_ERROR_MESSAGE = "Some error occurred";

    public static final String SAVE_CUSTOMER = "SaveCustomer";
    public static final String CustomerLogin = "CustomerLogin";
    public static final String BindBrands = "BindBrands";
    public static final String BindOutlets = "BindOutlets";
    public static final String BindPromotions = "BindPromotions";
    public static final String BindMenuCategory = "BindMenuCategory";
    public static final String BindMenuItems = "BindMenuItems";
    public static final String BindFeedbackQuestions = "BindFeedbackQuestions";
    public static final String BindHomeDelivery = "BindHomeDelivery";
    public static final String BindFeedbackOptions = "BindFeedbackOptions";
    public static final String SaveCustomerFeedback = "SaveCustomerFeedback";
    public static final String BindContactUsDetails = "BindContactUsDetails";
    public static final String SaveContactUsMessage = "SaveContactUsMessage";
    public static final String BindLocationDetails = "BindLocationDetails";
    public static final String BindTodaySpecials = "BindTodaySpecials";
    public static final String BindCountries = "BindCountries";
    public static final String BindStates = "BindStates";
    public static final String BindCities = "BindCities";
    public static final String BindLocation = "BindLocation";
    public static final String BindCustomerDetails = "BindCustomerDetails";
    public static final String AddToHomeDeliveryCart = "AddToHomeDeliveryCart";
    public static final String UpdateCartItemQuantity = "UpdateCartItemQuantity";
    public static final String DeleteCartItem = "DeleteCartItem";
    public static final String BindCartItems = "BindCartItems";
    public static final String SaveImComing = "SaveImComing";
    public static final String SaveImComing_New = "SaveImComing_New";
    public static final String SaveDeliveryAddress = "SaveDeliveryAddress";
    public static final String GetAllDeliveryAddress = "GetAllDeliveryAddress";
    public static final String SaveHomeDeliveryOrder = "SaveHomeDeliveryOrder";
    public static final String BindMenuCategoryForHomeDelivery = "BindMenuCategoryForHomeDelivery";
    public static final String ManageFavoriteMenu="ManageFavoriteMenu";
    public static final String BindMenuItemsHomeDeliveryWithCustomer="BindMenuItemsHomeDeliveryWithCustomer";
    public static final String sp_api_menufavorite_getbycustomers="sp_api_menufavorite_getbycustomers";
    public static final String ManageFavoriteRestAllList="ManageFavoriteRestAllList";
    public static final String MyVoucher="MyVoucher";
    public static final String ManageFavoriteLocationAllList="ManageFavoriteLocationAllList";
    public static final String GetTop5billing="GetTop5billing";
    public static final String BindOrdeDetails="BindOrdeDetails";
    public static final String GetTop5eRecipt="GetTop5eRecipt";
    public static final String BindBrandsWithCustomer="BindBrandsWithCustomer";
    public static final String SaveCustomerPromotion="SaveCustomerPromotion";
    public static final String CustomerForgotPassword="CustomerForgotPassword";
    public static final String CustomerForgotPasswordPhone="CustomerForgotPasswordPhone";
    public static final String BindNationality="BindNationality";
    public static final String AddEventtracking="AddEventtracking";
    public static final String BindImComingWithCustomer="BindImComingWithCustomer";
    public static final String DeleteImComingWithCustomer="DeleteImComingWithCustomer";
    public static final String BindFaqsDetails="BindFaqsDetails";
    public static final String BindNews="BindNews";
    public static final String BindOrderAreaDetails="BindOrderAreaDetails";
    public static final String BindOrderDetailsCustomer="BindOrderDetailsCustomer";
    public static final String GetCustomerProfile="GetCustomerProfile";
    public static final String UpdateCustomerProfile="UpdateCustomerProfile";
    public static final String AddNewLike="AddNewLike";
    public static final String AddNewsComments="AddNewsComments";
    public static final String CustomerChagePassword="CustomerChagePassword";
    public static final String BindAppVersionById="BindAppVersionById";
}
