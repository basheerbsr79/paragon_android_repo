package paragon.futuralabs.com.paragon.Adapters;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import paragon.futuralabs.com.paragon.Activity.PNHDMenuItems;
import paragon.futuralabs.com.paragon.Activity.PNMenuItems;
import paragon.futuralabs.com.paragon.R;
import paragon.futuralabs.com.paragon.models.BrandList;
import paragon.futuralabs.com.paragon.models.MenuCatagoryList;
import paragon.futuralabs.com.paragon.paragonhelper.ParagonSP;

import static paragon.futuralabs.com.paragon.Utilities.AppConstants.BrandId;
import static paragon.futuralabs.com.paragon.Utilities.AppConstants.CatId;
import static paragon.futuralabs.com.paragon.Utilities.AppConstants.CatImage;
import static paragon.futuralabs.com.paragon.Utilities.AppConstants.CatName;
import static paragon.futuralabs.com.paragon.Utilities.AppConstants.FROM;
import static paragon.futuralabs.com.paragon.Utilities.AppConstants.Item;
import static paragon.futuralabs.com.paragon.Utilities.AppConstants.MENU;

/**
 * Created by Shamir on 19-02-2018.
 */

public class RVMenuCatagory extends RecyclerView.Adapter<RVMenuCatagory.ViewHolder> {
    List<MenuCatagoryList> menuCatagoryLists;
    Context context;
    String type;

    public RVMenuCatagory(Context context, List<MenuCatagoryList> menuCatagoryLists, String type) {
        this.context = context;
        this.menuCatagoryLists = menuCatagoryLists;
        this.type = type;

    }

    @Override
    public RVMenuCatagory.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.rv_menu_catagory, parent, false);
        RVMenuCatagory.ViewHolder viewHolder = new RVMenuCatagory.ViewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RVMenuCatagory.ViewHolder holder, int position) {

        String url = menuCatagoryLists.get(position).getCategoryImg();
        Picasso.with(context)
                .load(url)
                .error(R.drawable.menu_item_placeholder)
                .placeholder(R.drawable.menu_item_placeholder)
                .resize(600, 600).centerCrop()
                .into(holder.imvImage);
        holder.textView.setText(menuCatagoryLists.get(position).getMenuCategoryName().trim());
        holder.textViewDesc.setText(menuCatagoryLists.get(position).getDescription());

    }

    @Override
    public int getItemCount() {
        return menuCatagoryLists.size();

    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        ImageView imvImage;
        TextView textView, textViewDesc;

        public ViewHolder(View itemView) {
            super(itemView);
            imvImage = itemView.findViewById(R.id.imageView);
            textView = itemView.findViewById(R.id.textView);
            textViewDesc = itemView.findViewById(R.id.textViewDesc);

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            Intent intent = null;
            if (type.equalsIgnoreCase(MENU))
                intent = new Intent(context, PNMenuItems.class);
            else
                intent = new Intent(context, PNHDMenuItems.class);
            Bundle bundle = new Bundle();
            bundle.putSerializable(Item, menuCatagoryLists.get(getPosition()));
            intent.putExtras(bundle);
            context.startActivity(intent);
        }
    }
}
