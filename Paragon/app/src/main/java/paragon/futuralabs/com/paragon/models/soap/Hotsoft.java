
package paragon.futuralabs.com.paragon.models.soap;

import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class Hotsoft {

    @SerializedName("Response")
    private paragon.futuralabs.com.paragon.models.soap.Response mResponse;

    public paragon.futuralabs.com.paragon.models.soap.Response getResponse() {
        return mResponse;
    }

    public void setResponse(paragon.futuralabs.com.paragon.models.soap.Response Response) {
        mResponse = Response;
    }

}
