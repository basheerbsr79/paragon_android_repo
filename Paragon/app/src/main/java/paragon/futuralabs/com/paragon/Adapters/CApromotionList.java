package paragon.futuralabs.com.paragon.Adapters;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

import paragon.futuralabs.com.paragon.R;
import paragon.futuralabs.com.paragon.models.PromotionList;

/**
 * Created by Shamir on 02/04/16.
 */
public class CApromotionList extends BaseAdapter {
  ViewHolder holder;
    protected static final android.content.Context Context = null;
    private Activity activity;
    LayoutInflater inflater;
    List<PromotionList> list;

    public CApromotionList(Activity a, List<PromotionList> list) {
        activity = a;
       this.list=list;

        inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }




    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public int getViewTypeCount() {
        return list.size();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {



        View vi = convertView;

        if (convertView == null) {

            vi = inflater.inflate(R.layout.ca_promotion_list, null);
            holder = new ViewHolder();

            holder.title   =    vi.findViewById(R.id.txt_title_promotion);
            holder.sub= vi.findViewById(R.id.txt_title_promotion_sub);
            holder.title.setText(list.get(position).getPromotionName());
                holder.sub.setText(list.get(position).getDescription());


            vi.setTag(holder);

        } else
        {
            holder = (ViewHolder) vi.getTag();
        }


        return vi;


    }

    class ViewHolder{
        TextView title,sub;

    }
}
