package paragon.futuralabs.com.paragon.paragonhelper;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by User on 3/21/2016.
 */
public class ParagonSP {


    final static String login="success";
    final static String userid="2";
    final static String devicetoken="devicetoken";
    final static String notification="0";
    final static String current_activity="1";//1-paragon activity,,2-salkara,,3-mgrill
    final static String Outlet="Outlet";
    final static String name="name";
    final static String phone="phone";
    final static String email="email";
    final static String address="address";
    final static String promotionFrom="promotionFrom";

    final static String IsOnlineOrdering="IsOnlineOrdering";
    final static String IsHomeDelivery="IsHomeDelivery";
    final static String IsTakeawy="IsTakeawy";
    final static String BrandImage="BrandImage";





    private final static String SHARED_PREFS = "settings";

    private SharedPreferences prefs;
    private SharedPreferences.Editor edit;

    public ParagonSP(Context context) {
        prefs = context
                .getSharedPreferences(SHARED_PREFS, Context.MODE_PRIVATE);
    }


    public void setlogin(String Text) {

        edit = prefs.edit();
        edit.putString(login, Text);

        edit.commit();
    }

    public String getlogin() {

        return prefs.getString(login, "");
    }

    public void setuserid(String Text) {

        edit = prefs.edit();
        edit.putString(userid, Text);

        edit.commit();
    }

    public String getuserid() {

        return prefs.getString(userid, "");
    }
    public void setdeviceId(String deviceId) {

        edit = prefs.edit();
        edit.putString(devicetoken, deviceId);

        edit.commit();
    }

    public String getdeviceId() {

        return prefs.getString(devicetoken, "");
    }

    public void setnotificationreceived(String deviceId) {

        edit = prefs.edit();
        edit.putString(notification, deviceId);

        edit.commit();
    }

    public String getnotificationreceived() {

        return prefs.getString(notification, "");
    }
    public void setcurrentactivity(String deviceId) {

        edit = prefs.edit();
        edit.putString(current_activity, deviceId);

        edit.commit();
    }

    public String getcurrentactivity() {

        return prefs.getString(current_activity, "");
    }

    public void setOutlet(String text) {

        edit = prefs.edit();
        edit.putString(Outlet, text);

        edit.commit();
    }

    public String getOutlet() {

        return prefs.getString(Outlet, "");
    }

    public void setname(String text) {

        edit = prefs.edit();
        edit.putString(name, text);

        edit.commit();
    }

    public String getname() {

        return prefs.getString(name, "");
    }

    public void setphone(String text) {

        edit = prefs.edit();
        edit.putString(phone, text);

        edit.commit();
    }

    public String getphone() {

        return prefs.getString(phone, "");
    }

    public void setemail(String text) {

        edit = prefs.edit();
        edit.putString(email, text);

        edit.commit();
    }

    public String getemail() {

        return prefs.getString(email, "");
    }

    public void setaddress(String text) {

        edit = prefs.edit();
        edit.putString(address, text);

        edit.commit();
    }

    public String getaddress() {

        return prefs.getString(address, "");
    }

    public void setpromotionFrom(String text) {

        edit = prefs.edit();
        edit.putString(promotionFrom, text);

        edit.commit();
    }

    public String getpromotionFrom() {

        return prefs.getString(promotionFrom, "");
    }

    public void setIsOnlineOrdering(boolean text) {
        edit = prefs.edit();
        edit.putBoolean(IsOnlineOrdering, text);
        edit.commit();
    }

    public boolean getIsOnlineOrdering() {
        return prefs.getBoolean(IsOnlineOrdering, false);
    }

    public void setIsTakeawy(boolean text) {
        edit = prefs.edit();
        edit.putBoolean(IsTakeawy, text);
        edit.commit();
    }

    public boolean getIsTakeawy() {
        return prefs.getBoolean(IsTakeawy, false);
    }

    public void setIsHomeDelivery(boolean text) {
        edit = prefs.edit();
        edit.putBoolean(IsHomeDelivery, text);
        edit.commit();
    }

    public boolean getIsHomeDelivery() {
        return prefs.getBoolean(IsHomeDelivery, false);
    }

    public void setBrandImage(String text) {

        edit = prefs.edit();
        edit.putString(BrandImage, text);

        edit.commit();
    }

    public String getBrandImage() {

        return prefs.getString(BrandImage, "");
    }
}
