package paragon.futuralabs.com.paragon.Utilities;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Context;
import android.location.LocationManager;
import android.text.TextUtils;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Toast;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import paragon.futuralabs.com.paragon.R;

/**
 * Created by Shamir on 05-02-2018.
 */

public class Utils {

    public static String getMonth(String month){

        String months[] = {"January", "February", "March", "April" ,"May", "June", "July", "August", "September",
                "October", "November","December"};
        return months[Integer.parseInt(month)];
    }

    public static void setDateToEditText(final View v, final Context context, String title, final int from) {
        final Calendar mcurrentDate = Calendar.getInstance();
        int mYear = mcurrentDate.get(Calendar.YEAR);
        int mMonth = mcurrentDate.get(Calendar.MONTH);
        int mDay = mcurrentDate.get(Calendar.DAY_OF_MONTH);
        DatePickerDialog mDatePicker = new DatePickerDialog(context, android.R.style.Theme_Holo_Light_Dialog/*R.style.TimePickerTheme*/,
                new DatePickerDialog.OnDateSetListener() {
                    public void onDateSet(DatePicker datepicker,
                                          int selectedyear, int selectedmonth, int selectedday) {
                        String da = selectedday < 10 ? "0" + Integer.toString(selectedday) : Integer.toString(selectedday);
                        String m = selectedmonth < 9 ? "0" + Integer.toString(selectedmonth+1) : Integer.toString(selectedmonth+1);
                        String y = Integer.toString(selectedyear);
                        if (from==1)
                        ((EditText) v).setText(da + "-" + getMonth(selectedmonth+""));
                        else
                            ((EditText) v).setText(da + "-" + m+"-"+y);
                    }
                }, mYear, mMonth, mDay);
        mDatePicker.setTitle(title + "");
        mDatePicker.getDatePicker().getSpinnersShown();
        //mDatePicker.getDatePicker().setMinDate(System.currentTimeMillis());
        mDatePicker.show();
    }

    public static boolean validateEditText(Context activity, String state) {
        if (state == null || state.trim().isEmpty()) {
            return false;
        }
        return true;
    }

    public final static boolean isValidEmail(CharSequence target) {
        return !TextUtils.isEmpty(target) && android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
    }

    public static void toast(Context activity, String text) {
        Toast.makeText(activity, text + "...", Toast.LENGTH_SHORT).show();
    }

    public static String getDate() {
        Date c = Calendar.getInstance().getTime();
        SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
        String formattedDate = df.format(c);
        return formattedDate;

    }

    public static String setTimeFormat(String time) {
       /* String[] split=time.split("-");
        time=split[0];*/
        String inputPattern = "HH:mm:ss";
        String outputPattern = "h:mm a";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

        Date date = null;
        String str = null;

        try {
            date = inputFormat.parse(time);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }

    public static String getCurrentDate() {
        DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
        String date = df.format(Calendar.getInstance().getTime());
        return date;
    }

    public static String getPreviousDate() {
        DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DATE, -2);
        String date = df.format(calendar.getTime());
        return date;
    }

    public static String getConvertedDate(String date) {
        SimpleDateFormat dateFormat = new SimpleDateFormat(
                "yyyy-MM-dd");
        Date myDate = null;
        try {
            myDate = dateFormat.parse(date);

        } catch (ParseException e) {
            e.printStackTrace();
        }
        SimpleDateFormat timeFormat = new SimpleDateFormat("dd/MM/yyyy");
        String finalDate = timeFormat.format(myDate);
        return finalDate;
    }

    public static String notifDateTime() {

        Date date = new Date();

        SimpleDateFormat timeFormat = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss a");
        String finalDate = timeFormat.format(date);
        return finalDate;
    }

    public static boolean isGPSOn(Activity activity) {
        LocationManager manager = (LocationManager) activity.getSystemService(Context.LOCATION_SERVICE);
        boolean statusOfGPS = manager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        if (statusOfGPS) {
            return true;
        } else {
            return false;
        }
    }
}
