package paragon.futuralabs.com.paragon.Activity.paragon;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import paragon.futuralabs.com.paragon.Activity.PNBaseActivity;
import paragon.futuralabs.com.paragon.Adapters.CApromotionList;
import paragon.futuralabs.com.paragon.Adapters.RVPromotionHome;
import paragon.futuralabs.com.paragon.R;
import paragon.futuralabs.com.paragon.Utilities.Json;
import paragon.futuralabs.com.paragon.Utilities.MyAlert;
import paragon.futuralabs.com.paragon.apiManager.PNApiResponseWrapper;
import paragon.futuralabs.com.paragon.apiManager.PNNetworkConstants;
import paragon.futuralabs.com.paragon.apiManager.PNNetworkManager;
import paragon.futuralabs.com.paragon.apiManager.SplitResponse;
import paragon.futuralabs.com.paragon.models.PromotionList;
import paragon.futuralabs.com.paragon.models.PromotionResponse;
import paragon.futuralabs.com.paragon.paragonhelper.ParagonSP;

import static paragon.futuralabs.com.paragon.Utilities.ConnectionDetector.toast;
import static paragon.futuralabs.com.paragon.apiManager.PNNetworkManager.post;

public class PromotionListActivity extends PNBaseActivity {
    Context context;
    ParagonSP sp;
    ImageView img_back_toolbar, img_menu_toolbar;
    TextView txt_title_toolbar;

    RecyclerView promotionlist;
    ProgressDialog mDialog;
    SplitResponse splitResponse;
    PromotionResponse promotionResponse;
    List<PromotionList> promotionLists;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_promotion_list);
        //setting context
        context = PromotionListActivity.this;


        sp = new ParagonSP(getApplicationContext());
        mDialog = new ProgressDialog(context);
        getPromotions();
        trackEvent("Promotion", "View Promotion");
        //decalaring variables
        img_back_toolbar = findViewById(R.id.img_back_toolbar);
        img_menu_toolbar = findViewById(R.id.img_menu_toolbar);
        txt_title_toolbar = findViewById(R.id.txt_title_toolbar);

        promotionlist = findViewById(R.id.list_promotion_list);
        promotionlist.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        promotionlist.setHasFixedSize(true);

        //setting toolbar title
        txt_title_toolbar.setText(getString(R.string.tiltle_promotion));
        //back event
        img_back_toolbar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });


    }

    private void getPromotions() {
        final String[] list = new String[0];
        promotionLists = new ArrayList(Arrays.asList(list));
        showProgress();
        JSONObject jsonObject = new JSONObject();

        try {
            jsonObject.put("OutletId", sp.getOutlet());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        post(PromotionListActivity.this, PNNetworkConstants.BindPromotions, jsonObject.toString(), new PNNetworkManager.NetworkInterface() {
            @Override
            public void onResponse(PNApiResponseWrapper baseResponse) {
                dismissProgres();
                if (baseResponse.isSuccess()) {
                    splitResponse = new SplitResponse(baseResponse.getJsonObjectResponse());
                    if (splitResponse.getStatus().equals("1")) {
                        Gson gson = new Gson();
                        promotionResponse = gson.fromJson(splitResponse.getResponse(), PromotionResponse.class);
                        promotionLists.addAll(promotionResponse.getData());
                        if (promotionLists.size() > 0) {
                            setPromotions(promotionLists);
                        } else {
                            MyAlert alert = new MyAlert();
                            alert.showAlertFinish(PromotionListActivity.this, "No promotion found");
                        }

                    } else {
                        MyAlert alert = new MyAlert();
                        alert.showAlertFinish(PromotionListActivity.this, "No promotion found");
                    }
                } else {
                    toast(getApplicationContext(), baseResponse.getMessage());
                }

            }
        });

    }

    private void setPromotions(List<PromotionList> promotionLists) {

        RVPromotionHome adapter = new RVPromotionHome(PromotionListActivity.this, promotionLists);
        promotionlist.setAdapter(adapter);

    }


}
