package paragon.futuralabs.com.paragon.Adapters;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import paragon.futuralabs.com.paragon.Activity.PNCart;
import paragon.futuralabs.com.paragon.Activity.PNHDMenuItems;
import paragon.futuralabs.com.paragon.Activity.PNIamComing;
import paragon.futuralabs.com.paragon.Activity.PNMenuItems;
import paragon.futuralabs.com.paragon.R;
import paragon.futuralabs.com.paragon.apiManager.PNApiResponseWrapper;
import paragon.futuralabs.com.paragon.apiManager.PNNetworkConstants;
import paragon.futuralabs.com.paragon.apiManager.PNNetworkManager;
import paragon.futuralabs.com.paragon.apiManager.SplitResponse;
import paragon.futuralabs.com.paragon.models.IamComingList;
import paragon.futuralabs.com.paragon.models.MenuCatagoryList;

import static paragon.futuralabs.com.paragon.Utilities.AppConstants.FROM;
import static paragon.futuralabs.com.paragon.Utilities.AppConstants.Item;
import static paragon.futuralabs.com.paragon.Utilities.AppConstants.MENU;
import static paragon.futuralabs.com.paragon.Utilities.ConnectionDetector.toast;
import static paragon.futuralabs.com.paragon.apiManager.PNNetworkManager.post;

/**
 * Created by Shamir on 01-06-2018.
 */

public class RVIamComing extends RecyclerView.Adapter<RVIamComing.ViewHolder> {
    List<IamComingList> list;
    Context context;
    SplitResponse splitResponse;
    String[] status=new String[]{"Pending","You are in queue","Your table is ready","Your table is ready"};

    public RVIamComing(Context context, List<IamComingList> list) {
        this.context = context;
        this.list=list;
    }

    @Override
    public RVIamComing.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.rv_iam_coming, parent, false);
        RVIamComing.ViewHolder viewHolder = new RVIamComing.ViewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RVIamComing.ViewHolder holder, final int position) {

        holder.tvDate.setText(list.get(position).getVisitDate());
        holder.tvTime.setText(list.get(position).getExpectedTime());
        holder.tvNoPerson.setText(list.get(position).getNoOfPersons()+"");
        holder.tvStatus.setText(status[list.get(position).getImStatus()]);

        if (list.get(position).getImStatus()>0)
            holder.imvDelete.setVisibility(View.GONE);

        holder.imvDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                deleteItem(position);
            }
        });

    }

    @Override
    public int getItemCount() {
        return list.size();

    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView imvDelete;
        TextView tvDate,tvTime,tvNoPerson,tvStatus;

        public ViewHolder(View itemView) {
            super(itemView);
            imvDelete =  itemView.findViewById(R.id.imvDelete);
            tvDate= itemView.findViewById(R.id.tvDate);
            tvTime= itemView.findViewById(R.id.tvTime);
            tvNoPerson= itemView.findViewById(R.id.tvPersonNo);
            tvStatus=itemView.findViewById(R.id.tvStatus);
        }

    }

    private void deleteItem(int pos) {
        ((PNIamComing)context).showProgress();
        JSONObject jsonObject = new JSONObject();
        try {
             jsonObject.put("ImComingId", list.get(pos).getImComingId());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        post(context, PNNetworkConstants.DeleteImComingWithCustomer, jsonObject.toString(), new PNNetworkManager.NetworkInterface() {
            @Override
            public void onResponse(PNApiResponseWrapper baseResponse) {
                ((PNIamComing)context).dismissProgres();
                if (baseResponse.isSuccess()) {
                    splitResponse = new SplitResponse(baseResponse.getJsonObjectResponse());
                    if (splitResponse.getStatus().equals("1")) {
                        toast(context,splitResponse.getMessage());
                        ((PNIamComing)context).getIamComingList(true);

                    } else {
                        toast(context, splitResponse.getMessage());
                        ((PNIamComing)context).getIamComingList(true);
                    }
                } else {
                    toast(context, baseResponse.getMessage());
                }

            }
        });
    }
}
