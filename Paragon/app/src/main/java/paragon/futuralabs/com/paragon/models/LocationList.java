
package paragon.futuralabs.com.paragon.models;

import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class LocationList {

    @SerializedName("Location")
    private String mLocation;
    @SerializedName("LocationId")
    private Long mLocationId;

    public String getLocation() {
        return mLocation;
    }

    public void setLocation(String Location) {
        mLocation = Location;
    }

    public Long getLocationId() {
        return mLocationId;
    }

    public void setLocationId(Long LocationId) {
        mLocationId = LocationId;
    }

}
