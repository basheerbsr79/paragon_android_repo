package paragon.futuralabs.com.paragon.Activity;

import android.app.Activity;
import android.content.Context;
import android.support.v7.app.ActionBar;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.PopupWindow;

import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import paragon.futuralabs.com.paragon.R;
import paragon.futuralabs.com.paragon.Utilities.MyAlert;
import paragon.futuralabs.com.paragon.apiManager.PNApiResponseWrapper;
import paragon.futuralabs.com.paragon.apiManager.PNNetworkConstants;
import paragon.futuralabs.com.paragon.apiManager.PNNetworkManager;
import paragon.futuralabs.com.paragon.apiManager.SplitResponse;
import paragon.futuralabs.com.paragon.models.CountryList;
import paragon.futuralabs.com.paragon.models.CountryResponse;
import paragon.futuralabs.com.paragon.models.NationalitiesList;
import paragon.futuralabs.com.paragon.models.NationalitiesResponse;
import paragon.futuralabs.com.paragon.models.ProfileList;
import paragon.futuralabs.com.paragon.models.ProfileResponse;

import static paragon.futuralabs.com.paragon.Activity.RegisterActivity.listItems;
import static paragon.futuralabs.com.paragon.Utilities.ConnectionDetector.toast;
import static paragon.futuralabs.com.paragon.Utilities.Utils.isValidEmail;
import static paragon.futuralabs.com.paragon.Utilities.Utils.setDateToEditText;
import static paragon.futuralabs.com.paragon.apiManager.PNNetworkManager.post;

public class PNEditProfile extends PNBaseActivity {
    Context context;
    ImageView ivBack;
    EditText edTitle,edName,edEmail,edCountryCode,edPhone,edAddress,edLocation,edDOB,edWA;
    Button btnUpdate;
    AutoCompleteTextView edNationality;
    ProfileResponse profileResponse;
    CountryResponse countryResponse;
    List<CountryList> countryLists;
    NationalitiesResponse nationalitiesResponse;
    List<NationalitiesList> nationalitiesLists;
    String[] countries, countryCode, nationalities;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pnedit_profile);

        init();
        handleEvents();
        getProfile();
        getCountry();
        getNationalities();
    }

    public void handleEvents() {
        ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        edTitle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectItem(view, PNEditProfile.this, edTitle, listItems);
            }
        });

        edDOB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setDateToEditText(v, PNEditProfile.this, "Select DOB", 2);
            }
        });

        edWA.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setDateToEditText(v, PNEditProfile.this, "Select Anniversary", 2);
            }
        });

        btnUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                postProfile();
            }
        });
    }

    public void init() {
        context = PNEditProfile.this;
        ivBack=findViewById(R.id.ivBack);
        edTitle=findViewById(R.id.edTitle);
        edName=findViewById(R.id.edName);
        edEmail=findViewById(R.id.edEmail);
        edCountryCode=findViewById(R.id.edCountryCode);
        edPhone=findViewById(R.id.edPhone);
        edNationality=findViewById(R.id.edNationality);
        edAddress=findViewById(R.id.edAddress);
        edLocation=findViewById(R.id.edLocation);
        edDOB=findViewById(R.id.edDOB);
        edWA=findViewById(R.id.edWA);
        btnUpdate=findViewById(R.id.btnUpdate);
    }

    public void getProfile(){
        showProgress();
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("CustomerId", PNSP.getuserid());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        post(this, PNNetworkConstants.GetCustomerProfile, jsonObject.toString(), new PNNetworkManager.NetworkInterface() {
            @Override
            public void onResponse(PNApiResponseWrapper baseResponse) {
                dismissProgres();
                if (baseResponse.isSuccess()) {
                    splitResponse = new SplitResponse(baseResponse.getJsonObjectResponse());
                    if (splitResponse.getStatus().equals("1")) {
                        try {
                            Gson gson = new Gson();
                            profileResponse = gson.fromJson(splitResponse.getResponse(), ProfileResponse.class);
                            if (profileResponse.getData().size()>0)
                                setData(profileResponse);


                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    } else {
                        toast(getApplicationContext(), splitResponse.getMessage());
                    }
                } else {
                    toast(getApplicationContext(), baseResponse.getMessage());
                }

            }
        });


    }

    public void setData(ProfileResponse profileResponse) {
        ProfileList list=profileResponse.getData().get(0);
        edTitle.setText(list.getTitle());
        edName.setText(list.getFirstName());
        edEmail.setText(list.getEmail());
        edCountryCode.setText(list.getCountryCode());
        edPhone.setText(list.getMobile());
        edNationality.setText(list.getNationality());
        edAddress.setText(list.getAddress());
        edLocation.setText(list.getLocationName());
        edDOB.setText(list.getBirthDayDate()+"-"+list.getBirthDayMonth()+"-"+list.getBirthDayYear());
        if (!list.getWeddingDate().equals("0") && !list.getWeddingDate().equals(""))
        edWA.setText(list.getWeddingDate()+"-"+list.getWeddingMonth()+"-"+list.getWeddingYear());

    }

    public void selectItem(View v1, Activity activity, final EditText editText, final String[] listItems) {
        final PopupWindow pwindo;
        LayoutInflater inflater = (LayoutInflater) activity
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View layout = inflater.inflate(R.layout.select_list,
                (ViewGroup) v1.findViewById(R.id.linLaySel));

        pwindo = new PopupWindow(layout, ActionBar.LayoutParams.WRAP_CONTENT, ActionBar.LayoutParams.WRAP_CONTENT, true);
        pwindo.showAsDropDown(editText, 0, 0);


        ListView ll = (ListView) pwindo.getContentView().findViewById(R.id.lvSelectCity);


        ArrayAdapter<String> adapter = new ArrayAdapter<String>(activity,
                android.R.layout.simple_list_item_1, android.R.id.text1, listItems);


        // Assign adapter to ListView
        ll.setAdapter(adapter);
        ll.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                pwindo.dismiss();
                editText.setText(listItems[position]);
            }
        });
    }

    public void postProfile() {
        MyAlert alert = new MyAlert();
        if (edTitle.getText().toString().toString().trim().equals("")) {
            alert.showAlert(context, "Select title");
            return;
        }
        if (edName.getText().toString().toString().trim().equals("")) {
            alert.showAlert(context, "Enter Name");
            return;
        }
        if (!isValidEmail(edEmail.getText().toString().toString().trim())) {
            alert.showAlert(context, "Enter Valid Email");
            return;
        }
        if (edCountryCode.getText().toString().toString().trim().equals("")) {
            alert.showAlert(context, "Select Phone Code");
            return;
        }
        if (edPhone.getText().toString().toString().trim().equals("")) {
            alert.showAlert(context, "Enter Phone Number");
            return;
        }
        if (edDOB.getText().toString().toString().trim().equals("")) {
            alert.showAlert(context, "Select Date of Birth");
            return;
        }

        showProgress();
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("CustomerId", PNSP.getuserid());
            jsonObject.put("Title", edTitle.getText().toString().trim());
            jsonObject.put("FirstName", edName.getText().toString().trim());
            jsonObject.put("LastName", "");
            jsonObject.put("Mobile", edPhone.getText().toString().trim());
            jsonObject.put("CountryCode", edCountryCode.getText().toString().trim());
            jsonObject.put("Email", edEmail.getText().toString().trim());
            jsonObject.put("Nationality", edNationality.getText().toString().trim());
            if (!edWA.getText().toString().trim().equals("")) {
                String[] split1 = edWA.getText().toString().trim().split("-");
                jsonObject.put("WeddingDate", split1[0]);
                jsonObject.put("WeddingMonth", split1[1]);
                jsonObject.put("WeddingYear", split1[2]);
            }else {
                jsonObject.put("WeddingDate","");
                jsonObject.put("WeddingMonth", "");
                jsonObject.put("WeddingYear", "");
            }
            String[] split = edDOB.getText().toString().trim().split("-");
            jsonObject.put("BirthDayDate", split[0]);
            jsonObject.put("BirthDayMonth", split[1]);
            jsonObject.put("BirthDayYear", split[2]);
            jsonObject.put("Address", edAddress.getText().toString().trim());
            jsonObject.put("LocationName", edLocation.getText().toString().trim());


        } catch (JSONException e) {
            e.printStackTrace();
        }
        post(this, PNNetworkConstants.UpdateCustomerProfile, jsonObject.toString(), new PNNetworkManager.NetworkInterface() {
            @Override
            public void onResponse(PNApiResponseWrapper baseResponse) {
                dismissProgres();
                if (baseResponse.isSuccess()) {
                    splitResponse = new SplitResponse(baseResponse.getJsonObjectResponse());
                    if (splitResponse.getStatus().equals("1")) {
                        try {
                            // userid= splitResponse.getData().getString("CustomerId");
                            MyAlert myAlert =new MyAlert();
                            myAlert.showAlertFinish(PNEditProfile.this,"Profile Updated.");
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    } else {
                        toast(getApplicationContext(), splitResponse.getMessage());
                    }
                } else {
                    toast(getApplicationContext(), baseResponse.getMessage());
                }

            }
        });
    }

    public void getNationalities() {
        final String[] list = new String[0];
        nationalitiesLists = new ArrayList(Arrays.asList(list));
        showProgress();
        JSONObject jsonObject = new JSONObject();

        post(this, PNNetworkConstants.BindNationality, jsonObject.toString(), new PNNetworkManager.NetworkInterface() {
            @Override
            public void onResponse(PNApiResponseWrapper baseResponse) {
                dismissProgres();
                if (baseResponse.isSuccess()) {
                    splitResponse = new SplitResponse(baseResponse.getJsonObjectResponse());
                    if (splitResponse.getStatus().equals("1")) {
                        Gson gson = new Gson();
                        nationalitiesResponse = gson.fromJson(splitResponse.getResponse(), NationalitiesResponse.class);
                        nationalitiesLists.addAll(nationalitiesResponse.getData());
                        if (nationalitiesLists.size() > 0) {
                            nationalities = new String[nationalitiesLists.size()];
                            for (int i = 0; i < nationalitiesLists.size(); i++) {
                                nationalities[i] = nationalitiesLists.get(i).getNationality();
                            }

                            ArrayAdapter<String> adapter = new ArrayAdapter<String>
                                    (PNEditProfile.this, android.R.layout.select_dialog_item, nationalities);
                            edNationality.setThreshold(1);//will start working from first character
                            edNationality.setAdapter(adapter);
                        }

                    } else {
                        toast(getApplicationContext(), splitResponse.getMessage());
                    }
                } else {
                    toast(getApplicationContext(), baseResponse.getMessage());
                }

            }
        });

    }

    public void getCountry() {
        final String[] list = new String[0];
        countryLists = new ArrayList(Arrays.asList(list));
        showProgress();
        JSONObject jsonObject = new JSONObject();

        post(this, PNNetworkConstants.BindCountries, jsonObject.toString(), new PNNetworkManager.NetworkInterface() {
            @Override
            public void onResponse(PNApiResponseWrapper baseResponse) {
                dismissProgres();
                if (baseResponse.isSuccess()) {
                    splitResponse = new SplitResponse(baseResponse.getJsonObjectResponse());
                    if (splitResponse.getStatus().equals("1")) {
                        Gson gson = new Gson();
                        countryResponse = gson.fromJson(splitResponse.getResponse(), CountryResponse.class);
                        countryLists.addAll(countryResponse.getData());
                        if (countryLists.size() > 0) {
                            countries = new String[countryLists.size()];
                            countryCode = new String[countryLists.size()];
                            for (int i = 0; i < countryLists.size(); i++) {
                                countries[i] = countryLists.get(i).getCountry();
                                countryCode[i] = countryLists.get(i).getCountryCode();
                            }

                            edCountryCode.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    selectItem(view, PNEditProfile.this, edCountryCode, countryCode);
                                }
                            });
                        }

                    } else {
                        toast(getApplicationContext(), splitResponse.getMessage());
                    }
                } else {
                    toast(getApplicationContext(), baseResponse.getMessage());
                }

            }
        });

    }
}
