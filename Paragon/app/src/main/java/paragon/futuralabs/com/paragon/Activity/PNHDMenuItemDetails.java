package paragon.futuralabs.com.paragon.Activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import paragon.futuralabs.com.paragon.R;
import paragon.futuralabs.com.paragon.Utilities.MyAlert;
import paragon.futuralabs.com.paragon.apiManager.PNApiResponseWrapper;
import paragon.futuralabs.com.paragon.apiManager.PNNetworkConstants;
import paragon.futuralabs.com.paragon.apiManager.PNNetworkManager;
import paragon.futuralabs.com.paragon.apiManager.SplitResponse;
import paragon.futuralabs.com.paragon.models.MenuItemsList;

import static paragon.futuralabs.com.paragon.Utilities.AppConstants.FROM;
import static paragon.futuralabs.com.paragon.Utilities.AppConstants.Item;
import static paragon.futuralabs.com.paragon.Utilities.AppConstants.Mgrill;
import static paragon.futuralabs.com.paragon.Utilities.AppConstants.Salkara;
import static paragon.futuralabs.com.paragon.Utilities.ConnectionDetector.toast;
import static paragon.futuralabs.com.paragon.apiManager.PNNetworkManager.post;

public class PNHDMenuItemDetails extends PNBaseActivity {

    LinearLayout linLayContainer;
    TextView tvHeading;
    ImageView ic_back_arrow;
    MenuItemsList menuItemsList;
    ImageView imageView;
    TextView tvDishName,tvVegNonVeg,tvIsRecommended,tvIsContainsNuts,tvDescription,tvPrice;
    EditText edQuantity;
    Button btnAdd;
    SplitResponse splitResponse;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pnhdmenu_item_details);
        init();
        handleEvents();
    }

    private void handleEvents() {
        tvHeading.setText("Item Details");
        ic_back_arrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        String url=menuItemsList.getImage();
        Picasso.with(PNHDMenuItemDetails.this)
                .load(url)
                .error(R.drawable.menu_item_placeholder)
                .placeholder(R.drawable.menu_item_placeholder)
                .into(imageView);
        tvDishName.setText("Dish Name: "+menuItemsList.getDishName());
        tvVegNonVeg.setText("Veg or NonVeg: "+menuItemsList.getVegOrNonVeg());
        tvIsRecommended.setText("Recommended: "+menuItemsList.getIsRecommended());
        tvIsContainsNuts.setText("Contains Nuts: "+menuItemsList.getIsContainNuts());
        tvDescription.setText("Description: "+menuItemsList.getDescription());
        tvPrice.setText("Price: "+menuItemsList.getCurrency()+" "+menuItemsList.getPrice());

        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (edQuantity.getText().toString().trim().equals("") || edQuantity.getText().toString().trim().equals("0"))
                    Toast.makeText(PNHDMenuItemDetails.this,"Enter Quantity...",Toast.LENGTH_SHORT).show();
                else
                    addToCart();
            }
        });

    }

    private void addToCart() {
        showProgress();
        showProgress();
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("CartId", "0");
            jsonObject.put("CustomerId",PNSP.getuserid());
            jsonObject.put("MenuItemsId",menuItemsList.getMenuItemsId());
            jsonObject.put("MenuCode", "12pgn");
            jsonObject.put("Quantity", edQuantity.getText().toString().trim());
            jsonObject.put("Price", menuItemsList.getPrice());
            jsonObject.put("OutletId", PNSP.getOutlet());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        post(PNHDMenuItemDetails.this, PNNetworkConstants.AddToHomeDeliveryCart, jsonObject.toString(), new PNNetworkManager.NetworkInterface() {
            @Override
            public void onResponse(PNApiResponseWrapper baseResponse) {
                dismissProgres();
                if (baseResponse.isSuccess()) {
                    splitResponse = new SplitResponse(baseResponse.getJsonObjectResponse());

                    MyAlert alert=new MyAlert();
                    if (splitResponse.getStatus().equals("1")) {

                        try {
                            alert.showAlertFinish(PNHDMenuItemDetails.this,splitResponse.getMessage());
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    } else if (splitResponse.getStatus().equals("2")) {
                        alert.showAlertFinish(PNHDMenuItemDetails.this,splitResponse.getMessage());
                    }else
                    {
                        toast(getApplicationContext(), splitResponse.getMessage());
                    }
                } else {
                    toast(getApplicationContext(), baseResponse.getMessage());
                }

            }
        });
    }

    private void init() {
        linLayContainer= findViewById(R.id.linLayContainer);
        ic_back_arrow= findViewById(R.id.img_back_toolbar);
        tvHeading= findViewById(R.id.txt_title_toolbar);
        imageView=  findViewById(R.id.imageView);
        tvDishName=  findViewById(R.id.tvDishName);
        tvVegNonVeg=  findViewById(R.id.tvVegNonVeg);
        tvIsRecommended=  findViewById(R.id.tvIsRecommended);
        tvIsContainsNuts=  findViewById(R.id.tvIsContainsNuts);
        tvDescription=  findViewById(R.id.tvDescription);
        tvPrice=  findViewById(R.id.tvPrice);
        edQuantity= findViewById(R.id.edQuantity);
        btnAdd= findViewById(R.id.btnAdd);
        Intent intent = this.getIntent();
        Bundle bundle = intent.getExtras();
        menuItemsList = (MenuItemsList) bundle.getSerializable(Item);
    }
}
