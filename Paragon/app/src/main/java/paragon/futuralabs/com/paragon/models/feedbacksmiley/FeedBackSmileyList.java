
package paragon.futuralabs.com.paragon.models.feedbacksmiley;

import java.util.List;
import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class FeedBackSmileyList {

    @SerializedName("FQId")
    private Long mFQId;
    @SerializedName("Option")
    private List<Option> mOption;
    @SerializedName("Question")
    private String mQuestion;

    public Long getFQId() {
        return mFQId;
    }

    public void setFQId(Long FQId) {
        mFQId = FQId;
    }

    public List<paragon.futuralabs.com.paragon.models.feedbacksmiley.Option> getOption() {
        return mOption;
    }

    public void setOption(List<paragon.futuralabs.com.paragon.models.feedbacksmiley.Option> Option) {
        mOption = Option;
    }

    public String getQuestion() {
        return mQuestion;
    }

    public void setQuestion(String Question) {
        mQuestion = Question;
    }

}
