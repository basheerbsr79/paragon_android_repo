package paragon.futuralabs.com.paragon.Adapters;

/**
 * Created by Shamir on 3/22/2016.
 */
import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import paragon.futuralabs.com.paragon.R;
import paragon.futuralabs.com.paragon.models.PromotionList;

public class CustomPagerAdapter extends PagerAdapter {

    Context mContext;
    LayoutInflater mLayoutInflater;
    List<PromotionList> list;
   public CustomPagerAdapter(Context context,List<PromotionList> list) {
        mContext = context;
       this.list=list;
        mLayoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((LinearLayout) object);
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        View itemView = mLayoutInflater.inflate(R.layout.custom_pager, container, false);
        ImageView imageView =  itemView.findViewById(R.id.img_promotion);
        TextView texttitle =   itemView.findViewById(R.id.txt_title);
        TextView textdesc =   itemView.findViewById(R.id.txt_desc);
        TextView tvDate= itemView.findViewById(R.id.tvDate);
        TextView tvTime= itemView.findViewById(R.id.tvTime);
        String url=list.get(position).getImage();
        Picasso.with(mContext).load(url).placeholder(R.drawable.menu_item_placeholder).error(R.drawable.menu_item_placeholder).into(imageView);

        texttitle.setText(list.get(position).getPromotionName());
        textdesc.setText(list.get(position).getDescription());
        tvDate.setText("Expires on : "+list.get(position).getEndDate());
        tvTime.setText(list.get(position).getStartTime()+" - "+list.get(position).getEndTime());

        container.addView(itemView);

        return itemView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((LinearLayout) object);
    }
}
