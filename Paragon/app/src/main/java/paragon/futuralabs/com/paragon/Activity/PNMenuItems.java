package paragon.futuralabs.com.paragon.Activity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.graphics.Palette;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import paragon.futuralabs.com.paragon.Adapters.RVMenuCatagory;
import paragon.futuralabs.com.paragon.Adapters.RVMenuItem;
import paragon.futuralabs.com.paragon.R;
import paragon.futuralabs.com.paragon.apiManager.PNApiResponseWrapper;
import paragon.futuralabs.com.paragon.apiManager.PNNetworkConstants;
import paragon.futuralabs.com.paragon.apiManager.PNNetworkManager;
import paragon.futuralabs.com.paragon.apiManager.SplitResponse;
import paragon.futuralabs.com.paragon.models.MenuCatagoryList;
import paragon.futuralabs.com.paragon.models.MenuCatagoryResponse;
import paragon.futuralabs.com.paragon.models.MenuItemsList;
import paragon.futuralabs.com.paragon.models.MenuItemsResponse;

import static paragon.futuralabs.com.paragon.Utilities.AppConstants.CatId;
import static paragon.futuralabs.com.paragon.Utilities.AppConstants.FROM;
import static paragon.futuralabs.com.paragon.Utilities.AppConstants.Item;
import static paragon.futuralabs.com.paragon.Utilities.AppConstants.MENU;
import static paragon.futuralabs.com.paragon.Utilities.AppConstants.Mgrill;
import static paragon.futuralabs.com.paragon.Utilities.AppConstants.Salkara;
import static paragon.futuralabs.com.paragon.Utilities.ConnectionDetector.toast;
import static paragon.futuralabs.com.paragon.apiManager.PNNetworkManager.post;

public class PNMenuItems extends PNBaseActivity {

    RecyclerView recyclerView;
    ImageView imvImage;
    TextView tvDesc,tvOrder;
    SplitResponse splitResponse;
    MenuItemsResponse menuItemsResponse;
    List<MenuItemsList> menuItemsLists;
    MenuCatagoryList menuCatagoryList;

    CollapsingToolbarLayout collapsingToolbarLayout;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pnmenu_items);

        init();
        handleEvents();
        getMenuItems();
        trackEvent("Menu","View Menu Items");
    }

    private void handleEvents() {
        setToolBar();
        String url=menuCatagoryList.getCategoryImg();
        Picasso.with(PNMenuItems.this)
                .load(url)
                .placeholder(R.drawable.menu_item_placeholder)
                .error(R.drawable.menu_item_placeholder)
                .resize(600, 600).centerCrop()
                .into(imvImage);

        tvOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Intent in = new Intent(PNMenuItems.this, PNHDMenuItems.class);
                Intent in = new Intent(PNMenuItems.this, PNAddToCart.class);
                in.putExtra(CatId,menuCatagoryList.getMenuCategoryId()+"");
                startActivity(in);
            }
        });
    }

    private void setToolBar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);

        toolbar.setNavigationIcon(R.drawable.ic_back_arrow);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               onBackPressed();
            }
        });

        collapsingToolbarLayout = (CollapsingToolbarLayout) findViewById(R.id.toolbar_layout);
        collapsingToolbarLayout.setTitle(menuCatagoryList.getMenuCategoryName());
        tvDesc.setText(menuCatagoryList.getDescription());
        dynamicToolbarColor();
        toolbarTextAppernce();
    }

    private void dynamicToolbarColor() {

        Bitmap bitmap = BitmapFactory.decodeResource(getResources(),
                R.drawable.ic_back_arrow);
        Palette.from(bitmap).generate(new Palette.PaletteAsyncListener() {

            @Override
            public void onGenerated(Palette palette) {
                collapsingToolbarLayout.setContentScrimColor(palette.getMutedColor(getResources().getColor(R.color.mask)));
                collapsingToolbarLayout.setStatusBarScrimColor(palette.getMutedColor(getResources().getColor(R.color.mask)));
            }
        });
    }

    private void toolbarTextAppernce() {
        collapsingToolbarLayout.setCollapsedTitleTextAppearance(R.style.collapsedappbar);
        collapsingToolbarLayout.setExpandedTitleTextAppearance(R.style.expandedappbar);
    }

    private void init() {
        imvImage= findViewById(R.id.imvImage);
        tvDesc= findViewById(R.id.tvDesc);
        tvOrder= findViewById(R.id.tvOrder);
        recyclerView= findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        Intent intent = this.getIntent();
        Bundle bundle = intent.getExtras();
        menuCatagoryList = (MenuCatagoryList) bundle.getSerializable(Item);

    }

    public void getMenuItems(){
        final String[] list=new String[0];
        menuItemsLists=new ArrayList(Arrays.asList(list));
        showProgress();
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("OutletId", PNSP.getOutlet());
            jsonObject.put("MenuCategoryId", menuCatagoryList.getMenuCategoryId());
            jsonObject.put("CustomerId", PNSP.getuserid());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        post(PNMenuItems.this, PNNetworkConstants.BindMenuItemsHomeDeliveryWithCustomer, jsonObject.toString(), new PNNetworkManager.NetworkInterface() {
            @Override
            public void onResponse(PNApiResponseWrapper baseResponse) {
                dismissProgres();
                if (baseResponse.isSuccess()) {
                    splitResponse = new SplitResponse(baseResponse.getJsonObjectResponse());
                    if (splitResponse.getStatus().equals("1")) {
                        try {
                            Gson gson = new Gson();
                            menuItemsResponse = gson.fromJson(splitResponse.getResponse(), MenuItemsResponse.class);
                            menuItemsLists.addAll(menuItemsResponse.getData());
                            if (menuItemsLists.size()>0)
                            setData(menuItemsLists);
                            else
                                toast(getApplicationContext(),"No items found");

                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    } else {
                        toast(getApplicationContext(), splitResponse.getMessage());
                    }
                } else {
                    toast(getApplicationContext(), baseResponse.getMessage());
                }

            }
        });


    }

    private void setData(List<MenuItemsList> list) {

        if(PNSP.getIsOnlineOrdering() || PNSP.getIsTakeawy())
            tvOrder.setVisibility(View.VISIBLE);
        else
            tvOrder.setVisibility(View.GONE);

        /*PNSP.setIsOnlineOrdering(list.get(0).getIsOnlineOrdering());
        PNSP.setIsTakeawy(list.get(0).getIsTakeawy());
        PNSP.setIsHomeDelivery(list.get(0).getIsHomeDelivery());*/

        RVMenuItem rvMenuItem=new RVMenuItem(PNMenuItems.this,list,MENU);
        recyclerView.setAdapter(rvMenuItem);
    }
}
