
package paragon.futuralabs.com.paragon.models;

import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class IamComingList {

    @SerializedName("CustomerId")
    private Long mCustomerId;
    @SerializedName("ExpectedTime")
    private String mExpectedTime;
    @SerializedName("ImComingId")
    private Long mImComingId;
    @SerializedName("NoOfPersons")
    private Long mNoOfPersons;
    @SerializedName("OutletId")
    private Long mOutletId;
    @SerializedName("VisitDate")
    private String mVisitDate;
    @SerializedName("ImStatus")
    private int mImStatus;

    public Long getCustomerId() {
        return mCustomerId;
    }

    public void setCustomerId(Long CustomerId) {
        mCustomerId = CustomerId;
    }

    public String getExpectedTime() {
        return mExpectedTime;
    }

    public void setExpectedTime(String ExpectedTime) {
        mExpectedTime = ExpectedTime;
    }

    public Long getImComingId() {
        return mImComingId;
    }

    public void setImComingId(Long ImComingId) {
        mImComingId = ImComingId;
    }

    public Long getNoOfPersons() {
        return mNoOfPersons;
    }

    public void setNoOfPersons(Long NoOfPersons) {
        mNoOfPersons = NoOfPersons;
    }

    public Long getOutletId() {
        return mOutletId;
    }

    public void setOutletId(Long OutletId) {
        mOutletId = OutletId;
    }

    public String getVisitDate() {
        return mVisitDate;
    }

    public void setVisitDate(String VisitDate) {
        mVisitDate = VisitDate;
    }

    public int getImStatus() {
        return mImStatus;
    }

    public void setImStatus(int ImStatus) {
        mImStatus = ImStatus;
    }

}
