package paragon.futuralabs.com.paragon.Activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import paragon.futuralabs.com.paragon.R;

public class PNVisitWeb extends PNBaseActivity {

    TextView tvHeading;
    ImageView ic_back_arrow;
    ImageView imageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pnvisit_web);

        init();
        handleEvents();
    }

    public void handleEvents() {
        tvHeading.setText("Menu");
        String url = PNSP.getBrandImage();
        Picasso.with(this)
                .load(url)
                .resize(600, 200)
                .into(imageView);

        ic_back_arrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }

    public void init() {
        tvHeading= findViewById(R.id.txt_title_toolbar);
        ic_back_arrow= findViewById(R.id.img_back_toolbar);
        imageView=findViewById(R.id.imageView);
    }
}
