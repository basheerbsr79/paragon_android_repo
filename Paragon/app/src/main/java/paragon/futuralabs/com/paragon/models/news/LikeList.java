
package paragon.futuralabs.com.paragon.models.news;

import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class LikeList {

    @SerializedName("AddedDate")
    private String mAddedDate;
    @SerializedName("CustomerId")
    private String mCustomerId;
    @SerializedName("CustomerName")
    private String mCustomerName;
    @SerializedName("LikeStatus")
    private Long mLikeStatus;

    public String getAddedDate() {
        return mAddedDate;
    }

    public void setAddedDate(String AddedDate) {
        mAddedDate = AddedDate;
    }

    public String getCustomerId() {
        return mCustomerId;
    }

    public void setCustomerId(String CustomerId) {
        mCustomerId = CustomerId;
    }

    public String getCustomerName() {
        return mCustomerName;
    }

    public void setCustomerName(String CustomerName) {
        mCustomerName = CustomerName;
    }

    public Long getLikeStatus() {
        return mLikeStatus;
    }

    public void setLikeStatus(Long LikeStatus) {
        mLikeStatus = LikeStatus;
    }

}
