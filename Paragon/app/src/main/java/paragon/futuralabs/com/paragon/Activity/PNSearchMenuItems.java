package paragon.futuralabs.com.paragon.Activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;

import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

import paragon.futuralabs.com.paragon.Adapters.ExpandListAdapter;
import paragon.futuralabs.com.paragon.Adapters.RVSearchMenu;
import paragon.futuralabs.com.paragon.R;
import paragon.futuralabs.com.paragon.apiManager.PNApiResponseWrapper;
import paragon.futuralabs.com.paragon.apiManager.PNNetworkConstants;
import paragon.futuralabs.com.paragon.apiManager.PNNetworkManager;
import paragon.futuralabs.com.paragon.apiManager.SplitResponse;
import paragon.futuralabs.com.paragon.models.MenuItemsList;
import paragon.futuralabs.com.paragon.models.homedelivery.HDList;
import paragon.futuralabs.com.paragon.models.homedelivery.HDResponse;
import paragon.futuralabs.com.paragon.models.homedelivery.MenuItem;

import static paragon.futuralabs.com.paragon.Utilities.AppConstants.CatId;
import static paragon.futuralabs.com.paragon.Utilities.AppConstants.Item;
import static paragon.futuralabs.com.paragon.Utilities.ConnectionDetector.toast;
import static paragon.futuralabs.com.paragon.apiManager.PNNetworkManager.post;

public class PNSearchMenuItems extends PNBaseActivity {

    EditText edSearch;
    ImageView ivClose;
    RecyclerView recyclerView;
    HDResponse hdResponse;
    List<HDList> hdLists;
    List<MenuItem> allMenuItemList;
    private static final Pattern END_OF_SENTENCE = Pattern.compile("\\.\\s+");

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pnsearch_menu_items);

        init();
        handleEvents();
        getMenuItems();

    }

    public void init() {
        edSearch=findViewById(R.id.edSearch);
        ivClose=findViewById(R.id.ivClose);
        recyclerView=findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
    }

    public void handleEvents(){

        edSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
if (s.length()>1){
    RVSearchMenu rvSearchMenu=new RVSearchMenu(PNSearchMenuItems.this,allMenuItemList,String.valueOf(s));
    recyclerView.setAdapter(rvSearchMenu);
}else {
    recyclerView.setAdapter(null);
}
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        ivClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
finish();
            }
        });
    }

    public void getMenuItems(){
        final String[] list=new String[0];
        hdLists=new ArrayList(Arrays.asList(list));
        showProgress();
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("OutletId", PNSP.getOutlet());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        post(this, PNNetworkConstants.BindMenuCategoryForHomeDelivery, jsonObject.toString(), new PNNetworkManager.NetworkInterface() {
            @Override
            public void onResponse(PNApiResponseWrapper baseResponse) {
                dismissProgres();
                if (baseResponse.isSuccess()) {
                    splitResponse = new SplitResponse(baseResponse.getJsonObjectResponse());
                    if (splitResponse.getStatus().equals("1")) {
                        try {
                            Gson gson = new Gson();
                            hdResponse = gson.fromJson(splitResponse.getResponse(), HDResponse.class);
                            hdLists.addAll(hdResponse.getData());
                            if (hdLists.size()>0)
                                setData(hdLists);
                            else
                                toast(getApplicationContext(),"No items found");

                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    } else {
                        toast(getApplicationContext(), splitResponse.getMessage());
                    }
                } else {
                    toast(getApplicationContext(), baseResponse.getMessage());
                }

            }
        });


    }

    public void setData(List<HDList> hdLists) {

        final String[] list=new String[0];
        allMenuItemList=new ArrayList(Arrays.asList(list));

        for (int i = 0; i < hdLists.size(); i++) {
            List<MenuItem> menuItems=hdLists.get(i).getMenuItems();
            allMenuItemList.addAll(menuItems);
        }

        Log.i("IvaallMenuSZ",allMenuItemList.size()+"");


    }
}
