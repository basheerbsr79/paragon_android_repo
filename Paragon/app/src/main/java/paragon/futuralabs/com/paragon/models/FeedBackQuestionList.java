
package paragon.futuralabs.com.paragon.models;

import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class FeedBackQuestionList {

    @SerializedName("FQId")
    private Long mFQId;
    @SerializedName("Question")
    private String mQuestion;

    public Long getFQId() {
        return mFQId;
    }

    public void setFQId(Long FQId) {
        mFQId = FQId;
    }

    public String getQuestion() {
        return mQuestion;
    }

    public void setQuestion(String Question) {
        mQuestion = Question;
    }

}
