package paragon.futuralabs.com.paragon.Activity;

import android.app.ActionBar;
import android.app.TimePickerDialog;
import android.content.Context;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.TimePicker;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import paragon.futuralabs.com.paragon.R;
import paragon.futuralabs.com.paragon.Utilities.MyAlert;
import paragon.futuralabs.com.paragon.apiManager.PNApiResponseWrapper;
import paragon.futuralabs.com.paragon.apiManager.PNNetworkConstants;
import paragon.futuralabs.com.paragon.apiManager.PNNetworkManager;
import paragon.futuralabs.com.paragon.apiManager.SplitResponse;

import static paragon.futuralabs.com.paragon.Utilities.AppConstants.FROM;
import static paragon.futuralabs.com.paragon.Utilities.AppConstants.Mgrill;
import static paragon.futuralabs.com.paragon.Utilities.AppConstants.Salkara;
import static paragon.futuralabs.com.paragon.Utilities.ConnectionDetector.toast;
import static paragon.futuralabs.com.paragon.Utilities.Utils.validateEditText;
import static paragon.futuralabs.com.paragon.apiManager.PNNetworkManager.post;

public class PNIamComingAdd extends PNBaseActivity {

    LinearLayout linLayContainer;
    TextView tvHeading, tvExpectedTime;
    EditText edNofSeats,edSpecInst;
    ImageView ic_back_arrow;
    Button btnSubmit;
    String mAMPM;

    int mHour, mMinute;

    SplitResponse splitResponse;

    String[] count = new String[]{"1", "2", "3", "4", "5"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pniam_coming);
        init();
        handleEvents();
        trackEvent("I Am Coming", "Add I Am Coming");
    }

    private void handleEvents() {
        tvHeading.setText("I am coming");
        ic_back_arrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        tvExpectedTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectTime();
            }
        });

        edNofSeats.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDropDownList(PNIamComingAdd.this, edNofSeats, count, edNofSeats.getWidth());
            }
        });

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (validate()) {
                    saveIamComing();
                }
            }
        });
    }

    private void init() {
        linLayContainer = findViewById(R.id.linLayContainer);
        ic_back_arrow = findViewById(R.id.img_back_toolbar);
        tvHeading = findViewById(R.id.txt_title_toolbar);
        tvExpectedTime = findViewById(R.id.tvExpectedTime);
        edNofSeats = findViewById(R.id.edNofSeats);
        edSpecInst = findViewById(R.id.edSpecInst);
        btnSubmit = findViewById(R.id.btnSubmit);
    }

    public void selectTime() {
        final Calendar c = Calendar.getInstance();
        mHour = c.get(Calendar.HOUR_OF_DAY);
        mMinute = c.get(Calendar.MINUTE);
        final long[] diff = new long[1];
        final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");

        // Launch Time Picker Dialog
        TimePickerDialog timePickerDialog = null;
        timePickerDialog = new TimePickerDialog(PNIamComingAdd.this, R.style.TimePickerTheme,
                new TimePickerDialog.OnTimeSetListener() {

                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay,
                                          int minute) {

                        if (hourOfDay < mHour) {
                            MyAlert alert = new MyAlert();
                            alert.showAlert(PNIamComingAdd.this, "You cannot book at this time");
                            tvExpectedTime.setText("");
                            return;
                        }

                        if (hourOfDay == mHour) {
                            if (minute < mMinute) {
                                MyAlert alert = new MyAlert();
                                alert.showAlert(PNIamComingAdd.this, "You cannot book at this time");
                                tvExpectedTime.setText("");
                                return;
                            }
                        }

                        if (hourOfDay == 23 && minute > 14) {
                            MyAlert alert = new MyAlert();
                            alert.showAlert(PNIamComingAdd.this, "You cannot book at this time");
                            tvExpectedTime.setText("");
                            return;
                        }

                        if (hourOfDay < 12) {
                            mAMPM = "AM";
                        } else {
                            if (hourOfDay != 12) {
                                mHour = hourOfDay - 12;
                            }

                            mAMPM = "PM";
                        }
                        String hr = "", mn = "";
                        if (hourOfDay < 10) {
                            hr = "0" + String.valueOf(hourOfDay);
                        } else {
                            hr = String.valueOf(hourOfDay);
                        }
                        if (minute < 10) {
                            mn = "0" + String.valueOf(minute);
                        } else {
                            mn = String.valueOf(minute);
                        }
                        tvExpectedTime.setText(hr + ":" + mn + ":00");
                    }
                }, mHour, mMinute, false);
        timePickerDialog.show();
    }

    public boolean validate() {
        if (!validateEditText(getApplicationContext(), tvExpectedTime.getText().toString().trim())) {
            toast(this, "Select Expected Time");
            return false;
        }
        if (!validateEditText(getApplicationContext(), edNofSeats.getText().toString().trim())) {
            toast(this, "Select No of seats");
            return false;
        }
        return true;
    }

    private void saveIamComing() {
        showProgress();
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("ImComingId", "0");
            jsonObject.put("CustomerId", PNSP.getuserid());
            jsonObject.put("NoOfPersons", edNofSeats.getText().toString().trim());
            jsonObject.put("ExpectedTime", tvExpectedTime.getText().toString().trim());
            jsonObject.put("OutletId", PNSP.getOutlet());
            jsonObject.put("SpecialInstruction", edSpecInst.getText().toString().trim());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        post(PNIamComingAdd.this, PNNetworkConstants.SaveImComing_New, jsonObject.toString(), new PNNetworkManager.NetworkInterface() {
            @Override
            public void onResponse(PNApiResponseWrapper baseResponse) {
                dismissProgres();
                if (baseResponse.isSuccess()) {
                    splitResponse = new SplitResponse(baseResponse.getJsonObjectResponse());

                    MyAlert alert = new MyAlert();
                    if (splitResponse.getStatus().equals("1")) {

                        try {
                            trackEvent("I Am Coming", "Added I Am Coming");
                            if (splitResponse.getIsPeak().equals("0"))
                                alert.showAlertFinish(PNIamComingAdd.this, "Thank you for informing us about your arrival at Paragon. We look forward to welcome you.");
                            else
                                alert.showAlertFinish(PNIamComingAdd.this, "Thank you for informing us at this peak time of dining. You will be added to queue, when you arrive at the restaurant through our app.");
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    } else if (splitResponse.getStatus().equals("2")) {

                        try {
                            trackEvent("I Am Coming", "Updated I Am Coming");
                            if (splitResponse.getIsPeak().equals("0"))
                                alert.showAlertFinish(PNIamComingAdd.this, "Thank you for informing us about your arrival at Paragon. We look forward to welcome you.");
                            else
                                alert.showAlertFinish(PNIamComingAdd.this, "Thank you for informing us at this peak time of dining. You will be added to queue, when you arrive at the restaurant through our app.");

                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    } else {
                        toast(getApplicationContext(), splitResponse.getMessage());
                    }
                } else {
                    toast(getApplicationContext(), baseResponse.getMessage());
                }

            }
        });
    }

    public void showDropDownList(final Context context, final EditText editText, String[] arrayList, int width) {
        final String[] itemClicked = {null};
        final PopupWindow pwindo;
        LayoutInflater inflater = (LayoutInflater) context.getApplicationContext()
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View layout = inflater.inflate(R.layout.custom_listview,
                null);
        pwindo = new PopupWindow(layout, ActionBar.LayoutParams.WRAP_CONTENT, ActionBar.LayoutParams.WRAP_CONTENT, true);
        pwindo.setWidth(width);
        pwindo.setBackgroundDrawable(new BitmapDrawable());
        pwindo.setFocusable(true);
        pwindo.showAsDropDown(editText, 0, 0);
        pwindo.setOutsideTouchable(true);
        pwindo.showAtLocation(layout, Gravity.CENTER, 0, 0);
        final ListView ll = (ListView) pwindo.getContentView().findViewById(R.id.lst_custom);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(context, android.R.layout.simple_list_item_1, arrayList);
        ll.setAdapter(adapter);
        ll.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                editText.setText(parent.getItemAtPosition(position).toString());
                pwindo.dismiss();

            }
        });

    }

}
