package paragon.futuralabs.com.paragon.Activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import paragon.futuralabs.com.paragon.Adapters.RVBrandHome;
import paragon.futuralabs.com.paragon.Adapters.RVPromotion;
import paragon.futuralabs.com.paragon.R;
import paragon.futuralabs.com.paragon.apiManager.PNApiResponseWrapper;
import paragon.futuralabs.com.paragon.apiManager.PNNetworkConstants;
import paragon.futuralabs.com.paragon.apiManager.PNNetworkManager;
import paragon.futuralabs.com.paragon.apiManager.SplitResponse;
import paragon.futuralabs.com.paragon.models.brandwithpromotions.BrandResponse;
import paragon.futuralabs.com.paragon.models.brandwithpromotions.Promotion;

import static paragon.futuralabs.com.paragon.Utilities.AppConstants.FROM;
import static paragon.futuralabs.com.paragon.Utilities.AppConstants.Item;
import static paragon.futuralabs.com.paragon.Utilities.AppConstants.Mgrill;
import static paragon.futuralabs.com.paragon.Utilities.AppConstants.Salkara;
import static paragon.futuralabs.com.paragon.Utilities.ConnectionDetector.toast;
import static paragon.futuralabs.com.paragon.apiManager.PNNetworkManager.post;

public class PNPromotionList extends PNBaseActivity {

    LinearLayout linLayContainer;
    TextView tvHeading;
    ImageView ic_back_arrow;
    RecyclerView recyclerView;
    SplitResponse splitResponse;
    BrandResponse brandResponse;
    List<Promotion> promotionList;
    int pos=-1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pnpromotion_list);
        init();
        handleEvents();
        trackEvent("Promotion","View Promotions");

    }

    @Override
    protected void onResume() {
        super.onResume();
        getBrands();
    }

    private void handleEvents() {
        tvHeading.setText("Promotions");
        ic_back_arrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }

    private void init() {
        linLayContainer= findViewById(R.id.linLayContainer);
        ic_back_arrow= findViewById(R.id.img_back_toolbar);
        tvHeading= findViewById(R.id.txt_title_toolbar);
        recyclerView= findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
pos=Integer.parseInt(getIntent().getStringExtra(Item));

    }

    private void getBrands() {
        final String[] list=new String[0];
        promotionList=new ArrayList(Arrays.asList(list));
        showProgress();
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("CustomerId", PNSP.getuserid());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        post(PNPromotionList.this, PNNetworkConstants.BindBrandsWithCustomer, jsonObject.toString(), new PNNetworkManager.NetworkInterface() {
            @Override
            public void onResponse(PNApiResponseWrapper baseResponse) {
                dismissProgres();
                if (baseResponse.isSuccess()) {
                    splitResponse = new SplitResponse(baseResponse.getJsonObjectResponse());
                    if (splitResponse.getStatus().equals("1")) {
                        Gson gson = new Gson();
                        brandResponse = gson.fromJson(splitResponse.getResponse(), BrandResponse.class);
                        setData(brandResponse);

                    } else {
                        toast(getApplicationContext(), splitResponse.getMessage());
                    }
                } else {
                    toast(getApplicationContext(), baseResponse.getMessage());
                }

            }
        });

    }

    private void setData(BrandResponse brandResponse) {
        RVPromotion rvPromotion=new RVPromotion(PNPromotionList.this,brandResponse.getData().get(pos));
        recyclerView.setAdapter(rvPromotion);
    }
}
