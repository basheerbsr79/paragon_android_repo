package paragon.futuralabs.com.paragon.Adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import paragon.futuralabs.com.paragon.Activity.PNPromotionDetails;
import paragon.futuralabs.com.paragon.Activity.paragon.PromotionActivity;
import paragon.futuralabs.com.paragon.R;
import paragon.futuralabs.com.paragon.apiManager.SplitResponse;
import paragon.futuralabs.com.paragon.models.PromotionList;
import paragon.futuralabs.com.paragon.models.brandwithpromotions.BrandList;
import paragon.futuralabs.com.paragon.models.brandwithpromotions.Promotion;

import static paragon.futuralabs.com.paragon.R.id.textView;
import static paragon.futuralabs.com.paragon.R.id.tv1;
import static paragon.futuralabs.com.paragon.Utilities.AppConstants.Item;
import static paragon.futuralabs.com.paragon.Utilities.AppConstants.position;

/**
 * Created by Shamir on 04-06-2018.
 */

public class RVPromotionHome extends RecyclerView.Adapter<RVPromotionHome.ViewHolder> {
    List<PromotionList> list;
    Context context;
    public RVPromotionHome(Context context, List<PromotionList> list) {
        this.context = context;
        this.list=list;

    }

    @Override
    public RVPromotionHome.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.ca_promotion_list, parent, false);
        RVPromotionHome.ViewHolder viewHolder = new RVPromotionHome.ViewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RVPromotionHome.ViewHolder holder, int position) {

        String url=list.get(position).getImage();
        Picasso.with(context)
                .load(url)
                .error(R.drawable.dish_item_placeholder)
                .placeholder(R.drawable.dish_item_placeholder)
                //.resize(100,100).centerCrop()
                .into(holder.imageView);
        holder.title.setText(list.get(position).getPromotionName());
        holder.sub.setText(list.get(position).getDescription());
        holder.tvDate.setText("Expires on : "+list.get(position).getEndDate());
        holder.tvSDate.setText("Begins from : "+list.get(position).getStartDate());
        //holder.tvTime.setText(list.get(position).getStartTime()+" - "+list.get(position).getEndTime());
        holder.tvTime.setText(list.get(position).getPrmoType());
    }

    @Override
    public int getItemCount() {
        return list.size();

    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView title,sub,tvDate,tvTime,tvType,tvSDate;
        ImageView imageView;

        public ViewHolder(View itemView) {
            super(itemView);
            imageView=itemView.findViewById(R.id.imageView);
            title   =    itemView.findViewById(R.id.txt_title_promotion);
            tvDate  =    itemView.findViewById(R.id.tvDate);
            tvTime  =    itemView.findViewById(R.id.tvTime);
            tvType =    itemView.findViewById(R.id.tvType);
            tvSDate=itemView.findViewById(R.id.tvSDate);
            sub= itemView.findViewById(R.id.txt_title_promotion_sub);

            Typeface face= Typeface.createFromAsset(context.getAssets(), "fonts/PT_Serif-Caption-Web-Italic.ttf");
            title.setTypeface(face);
            //tvDate.setTypeface(face);
            //tvTime.setTypeface(face);
            sub.setTypeface(face);

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
           /* Intent in=new Intent(context, PromotionActivity.class);
            in.putExtra(position,getPosition());
            context.startActivity(in);*/
        }
    }
}
