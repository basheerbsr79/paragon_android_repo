
package paragon.futuralabs.com.paragon.models.news;

import java.util.List;
import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class Datum {

    @SerializedName("CommentsList")
    private List<paragon.futuralabs.com.paragon.models.news.CommentsList> mCommentsList;
    @SerializedName("createddate")
    private String mCreateddate;
    @SerializedName("Description")
    private String mDescription;
    @SerializedName("ExpiryDate")
    private String mExpiryDate;
    @SerializedName("Heading")
    private String mHeading;
    @SerializedName("Image")
    private String mImage;
    @SerializedName("LikeList")
    private List<paragon.futuralabs.com.paragon.models.news.LikeList> mLikeList;
    @SerializedName("Link")
    private String mLink;
    @SerializedName("NewsId")
    private Long mNewsId;
    @SerializedName("NewsName")
    private String mNewsName;
    @SerializedName("newsaddedby")
    private Long mNewsaddedby;
    @SerializedName("OutletId")
    private Long mOutletId;

    public List<paragon.futuralabs.com.paragon.models.news.CommentsList> getCommentsList() {
        return mCommentsList;
    }

    public void setCommentsList(List<paragon.futuralabs.com.paragon.models.news.CommentsList> CommentsList) {
        mCommentsList = CommentsList;
    }

    public String getCreateddate() {
        return mCreateddate;
    }

    public void setCreateddate(String createddate) {
        mCreateddate = createddate;
    }

    public String getDescription() {
        return mDescription;
    }

    public void setDescription(String Description) {
        mDescription = Description;
    }

    public String getExpiryDate() {
        return mExpiryDate;
    }

    public void setExpiryDate(String ExpiryDate) {
        mExpiryDate = ExpiryDate;
    }

    public String getHeading() {
        return mHeading;
    }

    public void setHeading(String Heading) {
        mHeading = Heading;
    }

    public String getImage() {
        return mImage;
    }

    public void setImage(String Image) {
        mImage = Image;
    }

    public List<paragon.futuralabs.com.paragon.models.news.LikeList> getLikeList() {
        return mLikeList;
    }

    public void setLikeList(List<paragon.futuralabs.com.paragon.models.news.LikeList> LikeList) {
        mLikeList = LikeList;
    }

    public String getLink() {
        return mLink;
    }

    public void setLink(String Link) {
        mLink = Link;
    }

    public Long getNewsId() {
        return mNewsId;
    }

    public void setNewsId(Long NewsId) {
        mNewsId = NewsId;
    }

    public String getNewsName() {
        return mNewsName;
    }

    public void setNewsName(String NewsName) {
        mNewsName = NewsName;
    }

    public Long getNewsaddedby() {
        return mNewsaddedby;
    }

    public void setNewsaddedby(Long newsaddedby) {
        mNewsaddedby = newsaddedby;
    }

    public Long getOutletId() {
        return mOutletId;
    }

    public void setOutletId(Long OutletId) {
        mOutletId = OutletId;
    }

}
