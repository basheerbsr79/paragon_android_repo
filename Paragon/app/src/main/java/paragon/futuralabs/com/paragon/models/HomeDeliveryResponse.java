
package paragon.futuralabs.com.paragon.models;

import java.util.List;
import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class HomeDeliveryResponse {

    @SerializedName("data")
    private List<HomeDeliveryList> mData;
    @SerializedName("message")
    private String mMessage;
    @SerializedName("status")
    private Long mStatus;

    public List<HomeDeliveryList> getData() {
        return mData;
    }

    public void setData(List<HomeDeliveryList> data) {
        mData = data;
    }

    public String getMessage() {
        return mMessage;
    }

    public void setMessage(String message) {
        mMessage = message;
    }

    public Long getStatus() {
        return mStatus;
    }

    public void setStatus(Long status) {
        mStatus = status;
    }

}
