package paragon.futuralabs.com.paragon.Adapters;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import paragon.futuralabs.com.paragon.Activity.PNMenuItems;
import paragon.futuralabs.com.paragon.Activity.PNSpecialImageView;
import paragon.futuralabs.com.paragon.R;
import paragon.futuralabs.com.paragon.models.Image;
import paragon.futuralabs.com.paragon.models.MenuCatagoryList;
import paragon.futuralabs.com.paragon.models.SpecialList;
import paragon.futuralabs.com.paragon.models.todayspecial.Datum;
import paragon.futuralabs.com.paragon.models.todayspecial.TodaysSpecialAllDetailList;

import static paragon.futuralabs.com.paragon.Utilities.AppConstants.CatId;
import static paragon.futuralabs.com.paragon.Utilities.AppConstants.CatImage;
import static paragon.futuralabs.com.paragon.Utilities.AppConstants.FROM;
import static paragon.futuralabs.com.paragon.Utilities.AppConstants.Item;

/**
 * Created by Shamir on 09-03-2018.
 */

public class RVSpecial extends RecyclerView.Adapter<RVSpecial.ViewHolder> {
    //List<ProfileList> imageList;
    List<TodaysSpecialAllDetailList> imageList;
    Context context;

    public RVSpecial(Context context, List<TodaysSpecialAllDetailList> imageList) {
        this.context = context;
        this.imageList=imageList;

    }

    @Override
    public RVSpecial.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.rv_special, parent, false);
        RVSpecial.ViewHolder viewHolder = new RVSpecial.ViewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RVSpecial.ViewHolder holder, int position) {

        String url=imageList.get(position).getImage();
        Picasso.with(context)
                .load(url)
                .error(R.drawable.menu_item_placeholder)
                .placeholder(R.drawable.menu_item_placeholder)
                .resize(600,600).centerCrop()
                .into(holder.imvImage);
        holder.textView.setText(imageList.get(position).getName());
        holder.tvTime.setText(imageList.get(position).getPrmoType());

        /*String url=imageList.get(position).getImages().get(0).getImagePath();
        Picasso.with(context)
                .load(url)
                .error(R.drawable.menu_item_placeholder)
                .placeholder(R.drawable.menu_item_placeholder)
                .resize(600,600).centerCrop()
                .into(holder.imvImage);
        holder.textView.setText(imageList.get(position).getImages().get(0).getDishName());
        holder.tvTime.setText(imageList.get(position).getTimeStartFrom().getHours()+":"+
                imageList.get(position).getTimeStartFrom().getMinutes()+" - "+
                imageList.get(position).getTimeStartTo().getHours()+":"+
                imageList.get(position).getTimeStartTo().getMinutes());*/

    }

    @Override
    public int getItemCount() {
        return imageList.size();

    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        ImageView imvImage;
        TextView textView,tvTime;

        public ViewHolder(View itemView) {
            super(itemView);
            imvImage =  itemView.findViewById(R.id.imageView);
            textView= itemView.findViewById(R.id.textView);
            tvTime= itemView.findViewById(R.id.tvTime);

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            Intent intent=new Intent(context, PNSpecialImageView.class);
            Bundle bundle=new Bundle();
            bundle.putSerializable(Item,imageList.get(getPosition()));
            intent.putExtras(bundle);
            context.startActivity(intent);
        }
    }
}

