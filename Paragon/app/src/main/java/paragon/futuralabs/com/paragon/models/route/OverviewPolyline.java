
package paragon.futuralabs.com.paragon.models.route;

import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class OverviewPolyline {

    @SerializedName("points")
    private String mPoints;

    public String getPoints() {
        return mPoints;
    }

    public void setPoints(String points) {
        mPoints = points;
    }

}
