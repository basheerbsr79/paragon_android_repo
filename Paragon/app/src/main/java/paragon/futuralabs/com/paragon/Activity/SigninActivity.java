package paragon.futuralabs.com.paragon.Activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Looper;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import paragon.futuralabs.com.paragon.R;
import paragon.futuralabs.com.paragon.Utilities.ConnectionDetector;
import paragon.futuralabs.com.paragon.Utilities.Json;
import paragon.futuralabs.com.paragon.Utilities.MyAlert;
import paragon.futuralabs.com.paragon.Utilities.NetFailedAlert;
import paragon.futuralabs.com.paragon.Utilities.Utils;
import paragon.futuralabs.com.paragon.apiManager.PNApiResponseWrapper;
import paragon.futuralabs.com.paragon.apiManager.PNNetworkConstants;
import paragon.futuralabs.com.paragon.apiManager.PNNetworkManager;
import paragon.futuralabs.com.paragon.apiManager.SplitResponse;
import paragon.futuralabs.com.paragon.models.LoginResponse;
import paragon.futuralabs.com.paragon.models.PromotionResponse;
import paragon.futuralabs.com.paragon.paragonhelper.ParagonSP;

import static paragon.futuralabs.com.paragon.Utilities.ConnectionDetector.toast;
import static paragon.futuralabs.com.paragon.apiManager.PNNetworkManager.post;

public class SigninActivity extends PNBaseActivity  {

    Context context;
    ParagonSP sp;
    EditText ed_username,ed_password;
    TextView txt_guest_login,txt_register,tvForgotPwd;
    Button btn_submit;
    String device_id;
    SplitResponse splitResponse;
    LoginResponse loginResponse;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signin);
        context = SigninActivity.this;
        final View snackbarview = getWindow().getDecorView();
        sp=new ParagonSP(getApplicationContext());
        ed_username =   findViewById(R.id.ed_username_signin);
        ed_password =   findViewById(R.id.ed_password_signin);
        txt_register =   findViewById(R.id.txt_register_signin);
        txt_guest_login =   findViewById(R.id.txt_guest_login_signin);
        tvForgotPwd= findViewById(R.id.tvForgotPwd);
        btn_submit =   findViewById(R.id.btn_submit_signin);

        //getting device_id
        device_id = Settings.Secure.getString(getApplicationContext().getContentResolver(),
                Settings.Secure.ANDROID_ID);

        txt_register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in=new Intent(SigninActivity.this,RegisterActivity.class);
                in.putExtra("from","signin");
                startActivity(in);
            }
        });
        txt_guest_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in=new Intent(SigninActivity.this,SelectResturantActivity.class);
                sp.setlogin("");
                startActivity(in);
            }
        });

        tvForgotPwd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent in=new Intent(SigninActivity.this,PNForgotPassword.class);
                startActivity(in);
            }
        });

        btn_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ed_username.getText().toString().length() == 0||ed_password.getText().toString().length() == 0) {
                    MyAlert alert = new MyAlert();
                    alert.showAlert(context, getString(R.string.plz_enter_logindetails));
                } else {

                        //checking internet
                        ConnectionDetector cd = new ConnectionDetector(getApplicationContext());
                        Boolean isInternetPresent = cd.isConnectingToInternet();
                        // true or false
                        if (isInternetPresent.equals(true)) {
                            //getLogin(ed_username.getText().toString(), ed_password.getText().toString());
                            postLogin();
                        } else {
                            NetFailedAlert netalert = new NetFailedAlert();
                            netalert.showAlert(context, snackbarview);
                        }

                }
            }
        });
    }



    @Override
    public boolean onTouchEvent(MotionEvent event) {
        InputMethodManager imm = (InputMethodManager)getSystemService(Context.
                INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        return true;
    }

    public void postLogin(){
        showProgress();
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("UserName", ed_username.getText().toString().trim());
            jsonObject.put("Password",ed_password.getText().toString().trim());
            jsonObject.put("DeviceType", "android");
            jsonObject.put("DeviceToken", sp.getdeviceId());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        post(SigninActivity.this, PNNetworkConstants.CustomerLogin, jsonObject.toString(), new PNNetworkManager.NetworkInterface() {
            @Override
            public void onResponse(PNApiResponseWrapper baseResponse) {
                dismissProgres();
                if (baseResponse.isSuccess()) {
                    MyAlert alert = new MyAlert();
                    splitResponse = new SplitResponse(baseResponse.getJsonObjectResponse());
                    if (splitResponse.getStatus().equals("1")) {
                        try {
                            // userid= splitResponse.getData().getString("CustomerId");
                            sp.setlogin("success");
                            Gson gson = new Gson();
                            loginResponse = gson.fromJson(splitResponse.getResponse(), LoginResponse.class);
                            setLoginDatas(loginResponse);

                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    } else {
                        alert.showAlert(SigninActivity.this, splitResponse.getMessage());
                        //toast(getApplicationContext(), splitResponse.getMessage());
                    }
                } else {
                    toast(getApplicationContext(), baseResponse.getMessage());
                }

            }
        });


    }

    private void setLoginDatas(LoginResponse loginResponse) {
        sp.setuserid(loginResponse.getData().get(0).getCustomerId()+"");
        sp.setname(loginResponse.getData().get(0).getName());
        sp.setphone(loginResponse.getData().get(0).getMobile());
        sp.setemail(loginResponse.getData().get(0).getEmail());
        sp.setaddress(loginResponse.getData().get(0).getAddress());
        Intent in=new Intent(context,SelectResturantActivity.class);
        in.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(in);
        finish();
    }

}
