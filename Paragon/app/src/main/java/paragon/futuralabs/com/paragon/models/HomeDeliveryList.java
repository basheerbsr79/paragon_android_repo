
package paragon.futuralabs.com.paragon.models;

import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class HomeDeliveryList {

    @SerializedName("HomeDeliveryNumber")
    private String mHomeDeliveryNumber;

    public String getHomeDeliveryNumber() {
        return mHomeDeliveryNumber;
    }

    public void setHomeDeliveryNumber(String HomeDeliveryNumber) {
        mHomeDeliveryNumber = HomeDeliveryNumber;
    }

}
