package paragon.futuralabs.com.paragon.Activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import paragon.futuralabs.com.paragon.R;
import paragon.futuralabs.com.paragon.models.brandwithpromotions.Promotion;
import paragon.futuralabs.com.paragon.models.todayspecial.Datum;
import paragon.futuralabs.com.paragon.models.todayspecial.Image;
import paragon.futuralabs.com.paragon.models.todayspecial.TodaysSpecialAllDetailList;

import static paragon.futuralabs.com.paragon.Utilities.AppConstants.CatImage;
import static paragon.futuralabs.com.paragon.Utilities.AppConstants.FROM;
import static paragon.futuralabs.com.paragon.Utilities.AppConstants.Item;
import static paragon.futuralabs.com.paragon.Utilities.AppConstants.Mgrill;
import static paragon.futuralabs.com.paragon.Utilities.AppConstants.Salkara;

public class PNSpecialImageView extends PNBaseActivity {

    LinearLayout linLayContainer;
    TextView tvHeading,tvName,tvTime,tvType;
    ImageView ic_back_arrow;
    private ImageView imageView;
    TodaysSpecialAllDetailList list;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pnspecial_image_view);
        init();
        handleEvents();
        trackEvent("Today's Special","View Today's Special Image");
    }

    private void handleEvents() {
        tvHeading.setText("Today's Special");
        String url=list.getImage();
        Picasso.with(this).load(url).error(R.drawable.menu_item_placeholder)
                .placeholder(R.drawable.menu_item_placeholder).into(imageView);
        tvName.setText(list.getName());
        tvTime.setText(list.getPrmoType());
       // tvType.setText(list.getPrmoType());
        ic_back_arrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }

    private void init() {
        linLayContainer= findViewById(R.id.linLayContainer);
        ic_back_arrow= findViewById(R.id.img_back_toolbar);
        tvHeading= findViewById(R.id.txt_title_toolbar);
        imageView =   findViewById(R.id.imageView);
        tvName= findViewById(R.id.tvName);
        tvTime= findViewById(R.id.tvTime);
        tvType=findViewById(R.id.tvType);
        Intent intent = this.getIntent();
        Bundle bundle = intent.getExtras();
        list = (TodaysSpecialAllDetailList) bundle.getSerializable(Item);
    }
}
