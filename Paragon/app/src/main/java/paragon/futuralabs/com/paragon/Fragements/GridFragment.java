package paragon.futuralabs.com.paragon.Fragements;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import paragon.futuralabs.com.paragon.Activity.PNAvailiableSoon;
import paragon.futuralabs.com.paragon.Activity.PNBrownTownMenu;
import paragon.futuralabs.com.paragon.Activity.PNFeedBack;
import paragon.futuralabs.com.paragon.Activity.PNIamComing;
import paragon.futuralabs.com.paragon.Activity.PNMenuCatagory;
import paragon.futuralabs.com.paragon.Activity.PNNewsRoom;
import paragon.futuralabs.com.paragon.Activity.PNVisitWeb;
import paragon.futuralabs.com.paragon.Activity.PNeReceipt;
import paragon.futuralabs.com.paragon.Activity.PNeReceiptDetails;
import paragon.futuralabs.com.paragon.Activity.paragon.DashBoardActivity;
import paragon.futuralabs.com.paragon.Activity.paragon.MapActivity;
import paragon.futuralabs.com.paragon.Activity.paragon.PromotionListActivity;
import paragon.futuralabs.com.paragon.Activity.paragon.ContactUsActivity;
import paragon.futuralabs.com.paragon.Activity.PNQRImage;
import paragon.futuralabs.com.paragon.Activity.PNSpecial;
import paragon.futuralabs.com.paragon.R;
import paragon.futuralabs.com.paragon.apiManager.PNApiResponseWrapper;
import paragon.futuralabs.com.paragon.apiManager.PNNetworkConstants;
import paragon.futuralabs.com.paragon.apiManager.PNNetworkManager;
import paragon.futuralabs.com.paragon.apiManager.SplitResponse;
import paragon.futuralabs.com.paragon.models.LocationResponse;
import paragon.futuralabs.com.paragon.models.OutletList;
import paragon.futuralabs.com.paragon.models.OutletResponse;
import paragon.futuralabs.com.paragon.paragonhelper.ParagonSP;

import static paragon.futuralabs.com.paragon.Utilities.AppConstants.FROM;
import static paragon.futuralabs.com.paragon.Utilities.AppConstants.Paragon;
import static paragon.futuralabs.com.paragon.Utilities.ConnectionDetector.toast;
import static paragon.futuralabs.com.paragon.apiManager.PNNetworkManager.post;

public class GridFragment extends Fragment {
    EditText ed_select_resturant;
    LinearLayout laypromation, layfeedback, laycontactus, layNewsRoom, layEReceipts, laymenu, layIam, laySpecial;
    ParagonSP sp;
    TextView tvTakeAway,tvHomeDelivery;
    SplitResponse splitResponse;
    OutletResponse outletResponse;
    List<OutletList> outletLists;
    String[] outletName, outletId;
    String selectedId = "";
    ImageView imvQRImage;
    int pos = -1;

    public GridFragment() {


    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.fragment_grid, container, false);
        // to set toolbar tiltle
        sp = new ParagonSP(getActivity());

        //declaring grid items
        laypromation =   rootView.findViewById(R.id.laypromotion);
        layfeedback =  rootView.findViewById(R.id.layfeedback);
        laycontactus =  rootView.findViewById(R.id.laycontactus);
        layNewsRoom =  rootView.findViewById(R.id.laylocation);
        layEReceipts =  rootView.findViewById(R.id.layEReceipts);
        laymenu =  rootView.findViewById(R.id.laymenu);
        layIam =  rootView.findViewById(R.id.layIamComing);
        laySpecial =  rootView.findViewById(R.id.layTodaySpecial);

        imvQRImage =  rootView.findViewById(R.id.imvQrScan);
        tvHomeDelivery=rootView.findViewById(R.id.tvHomeDelivery);
        tvTakeAway=rootView.findViewById(R.id.tvTakeAway);


        ed_select_resturant =  rootView.findViewById(R.id.ed_select_returent_dropdown);
        ed_select_resturant.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_down, 0);

        laypromation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (outletLists.size() > 0) {
                    Intent in = new Intent(getActivity(), PromotionListActivity.class);
                    startActivity(in);
                } else {
                    toast(getActivity(), "No Outlets");
                }
            }
        });
        layfeedback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (outletLists.size() > 0) {
                    Intent in = new Intent(getActivity(), PNFeedBack.class);
                    startActivity(in);
                } else {
                    toast(getActivity(), "No Outlets");
                }
            }
        });
        laycontactus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (outletLists.size() > 0) {

                    Intent in = new Intent(getActivity(), ContactUsActivity.class);
                    startActivity(in);
                } else {
                    toast(getActivity(), "No Outlets");
                }
            }
        });
        layNewsRoom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (outletLists.size() > 0) {
                    Intent in = new Intent(getActivity(), PNNewsRoom.class);
                    startActivity(in);
                } else {
                    toast(getActivity(), "No Outlets");
                }
            }
        });
        laymenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(sp.getcurrentactivity().equals("4")){
                    Intent in = new Intent(getActivity(), PNBrownTownMenu.class);
                    startActivity(in);
                }else {
                    if (outletLists.size() > 0) {
                        if (sp.getIsOnlineOrdering() || sp.getIsTakeawy()) {
                            Intent in = new Intent(getActivity(), PNMenuCatagory.class);
                            startActivity(in);
                        }else {
                            Intent in = new Intent(getActivity(), PNVisitWeb.class);
                            startActivity(in);
                        }
                    } else {
                        toast(getActivity(), "No Outlets");
                    }
                }

            }
        });
        layEReceipts.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (outletLists.size() > 0) {
                    if (sp.getIsOnlineOrdering() || sp.getIsTakeawy()) {
                        Intent in = new Intent(getActivity(), PNeReceipt.class);
                        startActivity(in);
                    }else {
                        Intent in = new Intent(getActivity(), PNAvailiableSoon.class);
                        startActivity(in);
                    }
                } else {
                    toast(getActivity(), "No Outlets");
                }
            }
        });

        layIam.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (outletLists.size() > 0) {

                    Intent in = new Intent(getActivity(), PNIamComing.class);
                    startActivity(in);
                } else {
                    toast(getActivity(), "No Outlets");
                }
            }
        });

        laySpecial.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (outletLists.size() > 0) {
                    Intent in = new Intent(getActivity(), PNSpecial.class);
                    startActivity(in);
                } else {
                    toast(getActivity(), "No Outlets");
                }
            }
        });

        ed_select_resturant.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (outletLists.size() > 0) {
                    showDropDownList(getActivity(), ed_select_resturant, outletName, ed_select_resturant.getWidth());
                } else {
                    toast(getActivity(), "No Outlets");
                }
            }
        });

        imvQRImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent in = new Intent(getActivity(), PNQRImage.class);
                startActivity(in);
            }
        });
        getOutlets();
        // Inflate the layout for this fragment
        return rootView;
    }


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    private void getOutlets() {
        final String[] list = new String[0];
        outletLists = new ArrayList(Arrays.asList(list));
       // ((DashBoardActivity) getActivity()).showProgress();
        JSONObject jsonObject = new JSONObject();

        try {
            jsonObject.put("BrandId", sp.getcurrentactivity());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        post(getActivity(), PNNetworkConstants.BindOutlets, jsonObject.toString(), new PNNetworkManager.NetworkInterface() {
            @Override
            public void onResponse(PNApiResponseWrapper baseResponse) {
              //  ((DashBoardActivity) getActivity()).dismissProgres();
                if (baseResponse.isSuccess()) {
                    splitResponse = new SplitResponse(baseResponse.getJsonObjectResponse());
                    if (splitResponse.getStatus().equals("1")) {
                        Gson gson = new Gson();
                        outletResponse = gson.fromJson(splitResponse.getResponse(), OutletResponse.class);
                        outletLists.addAll(outletResponse.getData());
                        if (outletLists.size() > 0) {
                            outletName = new String[outletLists.size()];
                            outletId = new String[outletLists.size()];
                            int index = 0;
                            for (OutletList value : outletLists) {
                                outletName[index] = (String) value.getOutletsName();
                                outletId[index] = String.valueOf((long) value.getOutletId());
                                if (!sp.getpromotionFrom().equals("")) {
                                    if (outletName[index].equalsIgnoreCase(sp.getpromotionFrom())) {
                                        pos = index;
                                    }
                                }
                                index++;
                            }
                            if (pos == -1) {
                                ed_select_resturant.setText(outletName[0]);
                                selectedId = outletId[0];
                                if (outletLists.get(0).getHomeDelivery().equals("Yes"))
                                    sp.setIsHomeDelivery(true);
                                else
                                    sp.setIsHomeDelivery(false);
                                if (outletLists.get(0).getTakeAway().equals("Yes"))
                                    sp.setIsTakeawy(true);
                                else
                                    sp.setIsTakeawy(false);

                                sp.setIsOnlineOrdering(outletLists.get(0).getIsOnlineOrdering());

                            } else {
                                ed_select_resturant.setText(outletName[pos]);
                                selectedId = outletId[pos];
                                if (outletLists.get(pos).getHomeDelivery().equals("Yes"))
                                    sp.setIsHomeDelivery(true);
                                else
                                    sp.setIsHomeDelivery(false);
                                if (outletLists.get(pos).getTakeAway().equals("Yes"))
                                    sp.setIsTakeawy(true);
                                else
                                    sp.setIsTakeawy(false);
                                sp.setIsOnlineOrdering(outletLists.get(0).getIsOnlineOrdering());
                            }
                            sp.setOutlet(selectedId);
                            setOnlineOrderIndication();
                        }

                    } else {
                        toast(getActivity(), splitResponse.getMessage());
                        sp.setOutlet("");
                    }
                } else {
                    toast(getActivity(), baseResponse.getMessage());
                }

            }
        });

    }

    public void setOnlineOrderIndication() {
        if (sp.getIsOnlineOrdering())
            tvHomeDelivery.setVisibility(View.VISIBLE);
        else
            tvHomeDelivery.setVisibility(View.GONE);
        if (sp.getIsTakeawy())
            tvTakeAway.setVisibility(View.VISIBLE);
        else
            tvTakeAway.setVisibility(View.GONE);
    }

    public void showDropDownList(final Context context, final EditText editText, String[] arrayList, int width) {
        final String[] itemClicked = {null};
        final PopupWindow pwindo;
        LayoutInflater inflater = (LayoutInflater) context.getApplicationContext()
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View layout = inflater.inflate(R.layout.custom_listview,
                null);
        pwindo = new PopupWindow(layout, ActionBar.LayoutParams.WRAP_CONTENT, ActionBar.LayoutParams.WRAP_CONTENT, true);
        pwindo.setWidth(width);
        pwindo.setBackgroundDrawable(new BitmapDrawable());
        pwindo.setFocusable(true);
        pwindo.showAsDropDown(editText, 0, 0);
        pwindo.setOutsideTouchable(true);
        pwindo.showAtLocation(layout, Gravity.CENTER, 0, 0);
        final ListView ll = (ListView) pwindo.getContentView().findViewById(R.id.lst_custom);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(context, android.R.layout.simple_list_item_1, arrayList);
        ll.setAdapter(adapter);
        ll.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                editText.setText(parent.getItemAtPosition(position).toString());
                selectedId = outletId[position];
                sp.setOutlet(selectedId);
                if (outletLists.get(position).getHomeDelivery().equals("Yes"))
                    sp.setIsHomeDelivery(true);
                else
                    sp.setIsHomeDelivery(false);
                if (outletLists.get(position).getTakeAway().equals("Yes"))
                    sp.setIsTakeawy(true);
                else
                    sp.setIsTakeawy(false);
                sp.setIsOnlineOrdering(outletLists.get(position).getIsOnlineOrdering());
                setOnlineOrderIndication();
                pwindo.dismiss();

            }
        });

    }


}
