package paragon.futuralabs.com.paragon.Activity;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import paragon.futuralabs.com.paragon.R;
import paragon.futuralabs.com.paragon.apiManager.PNApiResponseWrapper;
import paragon.futuralabs.com.paragon.apiManager.PNNetworkConstants;
import paragon.futuralabs.com.paragon.apiManager.PNNetworkManager;
import paragon.futuralabs.com.paragon.apiManager.SplitResponse;
import paragon.futuralabs.com.paragon.models.CustomerDetailsResponse;

import static paragon.futuralabs.com.paragon.Utilities.ConnectionDetector.toast;
import static paragon.futuralabs.com.paragon.apiManager.PNNetworkManager.post;

public class PNQRImage extends PNBaseActivity {

    LinearLayout linLayContainer;
    TextView tvHeading;
    ImageView ic_back_arrow,imageView;
    SplitResponse splitResponse;
    CustomerDetailsResponse customerDetailsResponse;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pnqrimage);
        init();
        handleEvents();
        getCustomerDetails();
        trackEvent("QR Image","View QR Image");
    }

    private void handleEvents() {
        tvHeading.setText("QR Image");
        ic_back_arrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }

    private void init() {
        linLayContainer= findViewById(R.id.linLayContainer);
        ic_back_arrow= findViewById(R.id.img_back_toolbar);
        imageView= findViewById(R.id.imageView);
        tvHeading= findViewById(R.id.txt_title_toolbar);
    }

    private void getCustomerDetails() {
        showProgress();
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("CustomerId",PNSP.getuserid());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        post(PNQRImage.this, PNNetworkConstants.BindCustomerDetails, jsonObject.toString(), new PNNetworkManager.NetworkInterface() {
            @Override
            public void onResponse(PNApiResponseWrapper baseResponse) {
                dismissProgres();
                if (baseResponse.isSuccess()) {
                    splitResponse = new SplitResponse(baseResponse.getJsonObjectResponse());
                    if (splitResponse.getStatus().equals("1")) {

                        Gson gson = new Gson();
                        customerDetailsResponse = gson.fromJson(splitResponse.getResponse(), CustomerDetailsResponse.class);
                        String url=customerDetailsResponse.getData().get(0).getQRCodeImage();
                        Picasso.with(PNQRImage.this)
                                .load(url)
                                .into(imageView);

                    } else {
                        toast(getApplicationContext(), splitResponse.getMessage());
                    }
                } else {
                    toast(getApplicationContext(), baseResponse.getMessage());
                }

            }
        });

    }
}
