package paragon.futuralabs.com.paragon.Adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import paragon.futuralabs.com.paragon.Activity.PNFavoriteDishes;
import paragon.futuralabs.com.paragon.Activity.PNHDMenuItems;
import paragon.futuralabs.com.paragon.Activity.PNMenuItems;
import paragon.futuralabs.com.paragon.R;
import paragon.futuralabs.com.paragon.Utilities.ConnectionDetector;
import paragon.futuralabs.com.paragon.apiManager.PNApiResponseWrapper;
import paragon.futuralabs.com.paragon.apiManager.PNNetworkConstants;
import paragon.futuralabs.com.paragon.apiManager.PNNetworkManager;
import paragon.futuralabs.com.paragon.apiManager.SplitResponse;
import paragon.futuralabs.com.paragon.models.FavoriteList;

import static paragon.futuralabs.com.paragon.Utilities.ConnectionDetector.toast;
import static paragon.futuralabs.com.paragon.apiManager.PNNetworkManager.post;

/**
 * Created by Shamir on 29-03-2018.
 */

public class RVFavorites extends RecyclerView.Adapter<RVFavorites.ViewHolder> {
    List<FavoriteList> favoriteLists;
    Context context;
    SplitResponse splitResponse;

    public RVFavorites(Context context, List<FavoriteList> favoriteLists) {
        this.context = context;
        this.favoriteLists = favoriteLists;


    }

    @Override
    public RVFavorites.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.rv_favorite_item, parent, false);
        RVFavorites.ViewHolder viewHolder = new RVFavorites.ViewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final RVFavorites.ViewHolder holder, final int position) {

       /* String url=favoriteLists.get(position).getImage();
        Picasso.with(context)
                .load(url)
                .error(R.drawable.dish_item_placeholder)
                .placeholder(R.drawable.dish_item_placeholder)
                .resize(100,100).centerCrop()
                .into(holder.imvImage);*/
        holder.textView.setText(favoriteLists.get(position).getDishName());
        holder.textViewDesc.setText(favoriteLists.get(position).getMenuCategoryName());

        if (favoriteLists.get(position).getIsVeg()==1)
            holder.imvVeg.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_veg));
        else if (favoriteLists.get(position).getIsVeg()==0)
            holder.imvVeg.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_nonveg));
        else holder.imvVeg.setVisibility(View.GONE);

        if (favoriteLists.get(position).getIsHomeDelivery()) {
            holder.view.setVisibility(View.VISIBLE);
            holder.linLayCart.setVisibility(View.VISIBLE);
        }else {
            holder.view.setVisibility(View.GONE);
            holder.linLayCart.setVisibility(View.GONE);
        }

        holder.tvPrice.setText(favoriteLists.get(position).getCurrency() + " " + favoriteLists.get(position).getPrice());
        holder.tvTotal.setText(favoriteLists.get(position).getCurrency() + " " + favoriteLists.get(position).getPrice());
        holder.imvMinus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int val = Integer.parseInt(holder.tvQuantity.getText().toString().trim());
                if (val != 1) {
                    val = val - 1;
                    holder.tvQuantity.setText(String.valueOf(val));
                    holder.tvTotal.setText( favoriteLists.get(position).getCurrency()+ " " +(val * Double.parseDouble(favoriteLists.get(position).getPrice())));
                }
            }
        });
        holder.imvPlus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int val = Integer.parseInt(holder.tvQuantity.getText().toString().trim());
                if (val < 100) {
                    val = val + 1;
                    holder.tvQuantity.setText(String.valueOf(val));
                    holder.tvTotal.setText(favoriteLists.get(position).getCurrency()+ " " +(val * Double.parseDouble(favoriteLists.get(position).getPrice())));
                }
            }
        });

        holder.tvAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addToCart(holder.tvQuantity.getText().toString().trim(), position);
            }
        });

        holder.imvDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addRemFavorite(holder.imvDelete,position,"0");
            }
        });

    }

    @Override
    public int getItemCount() {
        return favoriteLists.size();

    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView imvImage, imvVeg,imvDelete;
        TextView textView, textViewDesc, tvPrice, tvAdd, tvQuantity, tvTotal;
        ImageView imvMinus, imvPlus;
        LinearLayout linLayCart;
        TextView view;

        public ViewHolder(View itemView) {
            super(itemView);
            imvImage =   itemView.findViewById(R.id.imageView);
            imvVeg =   itemView.findViewById(R.id.imvVeg);
            imvDelete=   itemView.findViewById(R.id.imvDelete);
            textView =   itemView.findViewById(R.id.textView);
            textViewDesc =   itemView.findViewById(R.id.textViewDesc);
            tvPrice =   itemView.findViewById(R.id.tvPrice);
            imvMinus =   itemView.findViewById(R.id.imvMinus);
            imvPlus =   itemView.findViewById(R.id.imvPlus);
            tvAdd =   itemView.findViewById(R.id.tvAdd);
            tvQuantity =   itemView.findViewById(R.id.tvQuantity);
            tvTotal =  itemView.findViewById(R.id.tvTotal);
            view=  itemView.findViewById(R.id.view);
            linLayCart= itemView.findViewById(R.id.linLayCart);
        }

    }

    private void addToCart(String qty, int pos) {
        ((PNFavoriteDishes) context).showProgress();
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("CartId", "0");
            jsonObject.put("CustomerId", ((PNFavoriteDishes) context).PNSP.getuserid());
            jsonObject.put("MenuItemsId", favoriteLists.get(pos).getMenuItemsId());
            jsonObject.put("MenuCode", "12pgn");
            jsonObject.put("Quantity", qty);
            jsonObject.put("Price", favoriteLists.get(pos).getPrice());
            jsonObject.put("OutletId", ((PNFavoriteDishes) context).PNSP.getOutlet());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        post(context, PNNetworkConstants.AddToHomeDeliveryCart, jsonObject.toString(), new PNNetworkManager.NetworkInterface() {
            @Override
            public void onResponse(PNApiResponseWrapper baseResponse) {
                ((PNFavoriteDishes) context).dismissProgres();
                if (baseResponse.isSuccess()) {
                    splitResponse = new SplitResponse(baseResponse.getJsonObjectResponse());
                    if (splitResponse.getStatus().equals("1")) {
                        ConnectionDetector.toast(context, "Added to cart");

                    } else if (splitResponse.getStatus().equals("2")) {
                        ConnectionDetector.toast(context, splitResponse.getMessage());
                    } else {
                        ConnectionDetector.toast(context, splitResponse.getMessage());
                    }
                } else {
                    ConnectionDetector.toast(context, baseResponse.getMessage());
                }

            }
        });
    }

    private void addRemFavorite(final ImageView imageView, final int pos, final String status) {
        ((PNFavoriteDishes)context).showProgress();
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("CustomerId", ((PNFavoriteDishes)context).PNSP.getuserid());
            jsonObject.put("MenuItemsId", favoriteLists.get(pos).getMenuItemsId());
            jsonObject.put("Status",status);
            jsonObject.put("OutletId", ((PNFavoriteDishes)context).PNSP.getOutlet());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        post(context, PNNetworkConstants.ManageFavoriteMenu, jsonObject.toString(), new PNNetworkManager.NetworkInterface() {
            @Override
            public void onResponse(PNApiResponseWrapper baseResponse) {
                ((PNFavoriteDishes)context).dismissProgres();
                if (baseResponse.isSuccess()) {
                    splitResponse = new SplitResponse(baseResponse.getJsonObjectResponse());
                    if (splitResponse.getStatus().equals("1")) {
                        ((PNFavoriteDishes)context).getFavoriteDishes();
                        toast(context,"Dish removed from favorites");

                    } else {
                        toast(context, splitResponse.getMessage());
                    }
                } else {
                    toast(context, baseResponse.getMessage());
                }

            }
        });
    }
}

