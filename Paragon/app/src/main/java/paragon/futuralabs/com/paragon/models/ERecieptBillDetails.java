
package paragon.futuralabs.com.paragon.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

@SuppressWarnings("unused")
public class ERecieptBillDetails implements Serializable{

    @SerializedName("ERecieptBillDetails")
    private Long mBillMasterId;
    @SerializedName("DishName")
    private String mDishName;
    @SerializedName("Price")
    private Double mPrice;
    @SerializedName("Qty")
    private Long mQty;

    public Long getBillMasterId() {
        return mBillMasterId;
    }

    public void setBillMasterId(Long BillMasterId) {
        mBillMasterId = BillMasterId;
    }

    public String getDishName() {
        return mDishName;
    }

    public void setDishName(String DishName) {
        mDishName = DishName;
    }

    public Double getPrice() {
        return mPrice;
    }

    public void setPrice(Double Price) {
        mPrice = Price;
    }

    public Long getQty() {
        return mQty;
    }

    public void setQty(Long Qty) {
        mQty = Qty;
    }

}
