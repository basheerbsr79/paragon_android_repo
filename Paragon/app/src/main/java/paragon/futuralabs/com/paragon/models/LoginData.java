
package paragon.futuralabs.com.paragon.models;

import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class LoginData {

    @SerializedName("Address")
    private String mAddress;
    @SerializedName("Anniversary")
    private String mAnniversary;
    @SerializedName("BirthDayDate")
    private Long mBirthDayDate;
    @SerializedName("BirthDayMonth")
    private String mBirthDayMonth;
    @SerializedName("City")
    private String mCity;
    @SerializedName("Country")
    private String mCountry;
    @SerializedName("CustomerId")
    private Long mCustomerId;
    @SerializedName("Email")
    private String mEmail;
    @SerializedName("Mobile")
    private String mMobile;
    @SerializedName("Name")
    private String mName;
    @SerializedName("QRCodeImage")
    private String mQRCodeImage;
    @SerializedName("State")
    private String mState;

    public String getAddress() {
        return mAddress;
    }

    public void setAddress(String Address) {
        mAddress = Address;
    }

    public String getAnniversary() {
        return mAnniversary;
    }

    public void setAnniversary(String Anniversary) {
        mAnniversary = Anniversary;
    }

    public Long getBirthDayDate() {
        return mBirthDayDate;
    }

    public void setBirthDayDate(Long BirthDayDate) {
        mBirthDayDate = BirthDayDate;
    }

    public String getBirthDayMonth() {
        return mBirthDayMonth;
    }

    public void setBirthDayMonth(String BirthDayMonth) {
        mBirthDayMonth = BirthDayMonth;
    }

    public String getCity() {
        return mCity;
    }

    public void setCity(String City) {
        mCity = City;
    }

    public String getCountry() {
        return mCountry;
    }

    public void setCountry(String Country) {
        mCountry = Country;
    }

    public Long getCustomerId() {
        return mCustomerId;
    }

    public void setCustomerId(Long CustomerId) {
        mCustomerId = CustomerId;
    }

    public String getEmail() {
        return mEmail;
    }

    public void setEmail(String Email) {
        mEmail = Email;
    }

    public String getMobile() {
        return mMobile;
    }

    public void setMobile(String Mobile) {
        mMobile = Mobile;
    }

    public String getName() {
        return mName;
    }

    public void setName(String Name) {
        mName = Name;
    }

    public String getQRCodeImage() {
        return mQRCodeImage;
    }

    public void setQRCodeImage(String QRCodeImage) {
        mQRCodeImage = QRCodeImage;
    }

    public String getState() {
        return mState;
    }

    public void setState(String State) {
        mState = State;
    }

}
