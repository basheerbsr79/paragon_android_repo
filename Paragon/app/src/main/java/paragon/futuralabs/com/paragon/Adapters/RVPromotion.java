package paragon.futuralabs.com.paragon.Adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import paragon.futuralabs.com.paragon.Activity.PNCart;
import paragon.futuralabs.com.paragon.Activity.PNHDMenuItems;
import paragon.futuralabs.com.paragon.Activity.PNMenuItems;
import paragon.futuralabs.com.paragon.Activity.PNPromotionDetails;
import paragon.futuralabs.com.paragon.Activity.PNPromotionList;
import paragon.futuralabs.com.paragon.Activity.paragon.DashBoardActivity;
import paragon.futuralabs.com.paragon.R;
import paragon.futuralabs.com.paragon.apiManager.PNApiResponseWrapper;
import paragon.futuralabs.com.paragon.apiManager.PNNetworkConstants;
import paragon.futuralabs.com.paragon.apiManager.PNNetworkManager;
import paragon.futuralabs.com.paragon.apiManager.SplitResponse;
import paragon.futuralabs.com.paragon.models.MenuCatagoryList;
import paragon.futuralabs.com.paragon.models.brandwithpromotions.BrandList;
import paragon.futuralabs.com.paragon.models.brandwithpromotions.Promotion;
import paragon.futuralabs.com.paragon.paragonhelper.ParagonSP;

import static paragon.futuralabs.com.paragon.Utilities.AppConstants.BrandId;
import static paragon.futuralabs.com.paragon.Utilities.AppConstants.BrandName;
import static paragon.futuralabs.com.paragon.Utilities.AppConstants.FROM;
import static paragon.futuralabs.com.paragon.Utilities.AppConstants.Item;
import static paragon.futuralabs.com.paragon.Utilities.AppConstants.MENU;
import static paragon.futuralabs.com.paragon.Utilities.ConnectionDetector.toast;
import static paragon.futuralabs.com.paragon.apiManager.PNNetworkManager.post;

/**
 * Created by Shamir on 10-04-2018.
 */

public class RVPromotion extends RecyclerView.Adapter<RVPromotion.ViewHolder> {
   BrandList brandList;
    List<Promotion> list;
    Context context;
    public RVPromotion(Context context, BrandList brandList) {
        this.context = context;
        this.brandList=brandList;
        final String[] list1=new String[0];
        list=new ArrayList(Arrays.asList(list1));
        for (int i = 0; i < this.brandList.getPromotions().size(); i++) {
            if (brandList.getPromotions().get(i).getIsRead().equalsIgnoreCase("No")){
                list.add(brandList.getPromotions().get(i));
            }
        }



    }

    @Override
    public RVPromotion.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.rv_promotio_item, parent, false);
        RVPromotion.ViewHolder viewHolder = new RVPromotion.ViewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RVPromotion.ViewHolder holder, int position) {

        String url=list.get(position).getImage();
        Picasso.with(context)
                .load(url)
                .error(R.drawable.videoimage)
                .placeholder(R.drawable.videoimage)
                .resize(600,600).centerCrop()
                .into(holder.imvImage);
        holder.tvOutlet.setText(list.get(position).getOutletName());
        holder.tvPromotion.setText(list.get(position).getPromotionName());
        holder.tvDesc.setText(list.get(position).getPrmoType());

    }

    @Override
    public int getItemCount() {
        return list.size();

    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        ImageView imvImage;
        TextView tvOutlet,tvPromotion,tvDesc;

        public ViewHolder(View itemView) {
            super(itemView);
            imvImage =  itemView.findViewById(R.id.imageView);
            tvOutlet= itemView.findViewById(R.id.tvOutletName);
            tvPromotion= itemView.findViewById(R.id.tvPromotion);
            tvDesc= itemView.findViewById(R.id.tvDesc);

            Typeface face= Typeface.createFromAsset(context.getAssets(), "fonts/PT_Serif-Caption-Web-Italic.ttf");
            tvOutlet.setTypeface(face);
            tvPromotion.setTypeface(face);
            //tvDesc.setTypeface(face);

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            Intent intent=new Intent(context, PNPromotionDetails.class);
            Bundle bundle=new Bundle();
            bundle.putSerializable(Item,list.get(getPosition()));
            intent.putExtras(bundle);
            context.startActivity(intent);
        }
    }
}
