
package paragon.futuralabs.com.paragon.models;

import java.util.List;
import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class SpecialList {

    @SerializedName("Date")
    private String mDate;
    @SerializedName("Description")
    private String mDescription;
    @SerializedName("Images")
    private List<Image> mImages;
    @SerializedName("TodaySpecialId")
    private Long mTodaySpecialId;

    public String getDate() {
        return mDate;
    }

    public void setDate(String Date) {
        mDate = Date;
    }

    public String getDescription() {
        return mDescription;
    }

    public void setDescription(String Description) {
        mDescription = Description;
    }

    public List<Image> getImages() {
        return mImages;
    }

    public void setImages(List<Image> Images) {
        mImages = Images;
    }

    public Long getTodaySpecialId() {
        return mTodaySpecialId;
    }

    public void setTodaySpecialId(Long TodaySpecialId) {
        mTodaySpecialId = TodaySpecialId;
    }

}
