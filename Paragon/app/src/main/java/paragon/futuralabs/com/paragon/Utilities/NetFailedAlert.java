package paragon.futuralabs.com.paragon.Utilities;

import android.content.Context;
import android.support.design.widget.Snackbar;
import android.view.View;

import paragon.futuralabs.com.paragon.R;

/**
 * Created by Shamir on 26/02/16.
 */
public class NetFailedAlert{


   public  void showAlert(final Context context, final View snackbarview){

       Snackbar sn=Snackbar.make(snackbarview,context.getString(R.string.alert_net_failed),Snackbar.LENGTH_LONG).setAction(context.getString(R.string.retry), new View.OnClickListener() {
           @Override
           public void onClick(View v) {



               ConnectionDetector cd = new ConnectionDetector(context.getApplicationContext());
               Boolean isInternetPresent = cd.isConnectingToInternet();
               // true or false
               if(isInternetPresent.equals(true)){
                   Snackbar.make(snackbarview,context.getString(R.string.conected), Snackbar.LENGTH_LONG).show();
               }
               else{
                   Snackbar.make(snackbarview,context.getString(R.string.alert_net_failed), Snackbar.LENGTH_LONG).show();
               }



           }
       });

       sn.show();

    }
}
