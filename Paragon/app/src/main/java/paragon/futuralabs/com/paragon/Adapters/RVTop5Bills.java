package paragon.futuralabs.com.paragon.Adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import paragon.futuralabs.com.paragon.R;
import paragon.futuralabs.com.paragon.models.FavRestList;
import paragon.futuralabs.com.paragon.models.Top5BillList;

/**
 * Created by Shamir on 30-03-2018.
 */

public class RVTop5Bills extends RecyclerView.Adapter<RVTop5Bills.ViewHolder> {
    List<Top5BillList> lists;
    Context context;

    public RVTop5Bills(Context context,List<Top5BillList> lists) {
        this.context = context;
        this.lists = lists;

    }

    @Override
    public RVTop5Bills.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.rv_top5_bill_item, parent, false);
        RVTop5Bills.ViewHolder viewHolder = new RVTop5Bills.ViewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RVTop5Bills.ViewHolder holder, int position) {

        holder.tvName.setText(lists.get(position).getOutletsName());
        holder.tvBillNo.setText("Bill No : "+lists.get(position).getBillNo());
        holder.tvBillDate.setText("Bill Date : "+lists.get(position).getBillDate());
        holder.tvAmount.setText("Bill Amount : "+lists.get(position).getCurrency()+" "+lists.get(position).getTotalBillAmt());
        holder.tvDiscount.setText("Discount : "+lists.get(position).getDiscount()+"%");

    }

    @Override
    public int getItemCount() {
        return lists.size();

    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView tvName,tvBillNo,tvBillDate,tvAmount,tvDiscount;

        public ViewHolder(View itemView) {
            super(itemView);
            tvName =   itemView.findViewById(R.id.tvName);
            tvBillNo =   itemView.findViewById(R.id.tvBillNo);
            tvBillDate =   itemView.findViewById(R.id.tvBillDate);
            tvAmount =   itemView.findViewById(R.id.tvAmount);
            tvDiscount =   itemView.findViewById(R.id.tvDiscount);
        }

    }
}

