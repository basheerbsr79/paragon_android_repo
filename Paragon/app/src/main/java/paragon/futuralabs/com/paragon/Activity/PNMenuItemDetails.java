package paragon.futuralabs.com.paragon.Activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import paragon.futuralabs.com.paragon.R;
import paragon.futuralabs.com.paragon.models.MenuItemsList;

import static paragon.futuralabs.com.paragon.Utilities.AppConstants.FROM;
import static paragon.futuralabs.com.paragon.Utilities.AppConstants.Item;
import static paragon.futuralabs.com.paragon.Utilities.AppConstants.Mgrill;
import static paragon.futuralabs.com.paragon.Utilities.AppConstants.Salkara;

public class PNMenuItemDetails extends PNBaseActivity {

    LinearLayout linLayContainer;
    TextView tvHeading;
    ImageView ic_back_arrow;
    MenuItemsList menuItemsList;
    ImageView imageView;
    TextView tvDishName,tvVegNonVeg,tvIsRecommended,tvIsContainsNuts,tvDescription,tvPrice;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pnmenu_item_details);
        init();
        handleEvents();
    }

    private void handleEvents() {
        tvHeading.setText("Item Details");
        ic_back_arrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        String url=menuItemsList.getImage();
        Picasso.with(PNMenuItemDetails.this)
                .load(url)
                .error(R.drawable.menu_item_placeholder)
                .placeholder(R.drawable.menu_item_placeholder)
                .into(imageView);
        tvDishName.setText("Dish Name: "+menuItemsList.getDishName());
        tvVegNonVeg.setText("Veg or NonVeg: "+menuItemsList.getVegOrNonVeg());
        tvIsRecommended.setText("Recommended: "+menuItemsList.getIsRecommended());
        tvIsContainsNuts.setText("Contains Nuts: "+menuItemsList.getIsContainNuts());
        tvDescription.setText("Description: "+menuItemsList.getDescription());
        tvPrice.setText("Price: "+menuItemsList.getCurrency()+" "+menuItemsList.getPrice());

    }

    private void init() {
        linLayContainer= findViewById(R.id.linLayContainer);
        ic_back_arrow= findViewById(R.id.img_back_toolbar);
        tvHeading= findViewById(R.id.txt_title_toolbar);
        imageView=  findViewById(R.id.imageView);
        tvDishName=  findViewById(R.id.tvDishName);
        tvVegNonVeg=  findViewById(R.id.tvVegNonVeg);
        tvIsRecommended=  findViewById(R.id.tvIsRecommended);
        tvIsContainsNuts=  findViewById(R.id.tvIsContainsNuts);
        tvDescription=  findViewById(R.id.tvDescription);
        tvPrice=  findViewById(R.id.tvPrice);
        Intent intent = this.getIntent();
        Bundle bundle = intent.getExtras();
        menuItemsList = (MenuItemsList) bundle.getSerializable(Item);
    }
}
