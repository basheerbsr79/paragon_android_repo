package paragon.futuralabs.com.paragon.Adapters;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import java.util.List;

import paragon.futuralabs.com.paragon.R;
import paragon.futuralabs.com.paragon.models.feedbacksmiley.FeedBackSmileyList;
import paragon.futuralabs.com.paragon.models.feedbacksmiley.Option;

import static paragon.futuralabs.com.paragon.Activity.PNFeedBack.ans;

/**
 * Created by Shamir on 16-03-2018.
 */

public class RVFeedBackSmiley extends RecyclerView.Adapter<RVFeedBackSmiley.ViewHolder> {
    List<FeedBackSmileyList> list;
    Context context;

    public RVFeedBackSmiley(Context context, List<FeedBackSmileyList> list) {
        this.context = context;
        this.list=list;


    }

    @Override
    public RVFeedBackSmiley.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.rv_feed_smily_item, parent, false);
        RVFeedBackSmiley.ViewHolder viewHolder = new RVFeedBackSmiley.ViewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RVFeedBackSmiley.ViewHolder holder, final int position) {

        if (list.get(position).getOption().size()>0) {
            holder.tvquestion.setText(list.get(position).getQuestion());
        }
        final List<Option> option=list.get(position).getOption();
        for (int i = 0; i < option.size(); i++) {
            if(i==0){
                holder.tv1.setVisibility(View.VISIBLE);
                holder.tv1.setText(option.get(i).getOptionName());
                holder.rb1.setVisibility(View.VISIBLE);
            }else if(i==1){
                holder.tv2.setVisibility(View.VISIBLE);
                holder.tv2.setText(option.get(i).getOptionName());
                holder.rb2.setVisibility(View.VISIBLE);
                if (option.get(i).getOptionName().equalsIgnoreCase("Excellent") ||
                        option.get(i).getOptionName().equalsIgnoreCase("Fantastic") ||
                        option.get(i).getOptionName().equalsIgnoreCase("Perfect") ||
                        option.get(i).getOptionName().equalsIgnoreCase("Satisfactory")){
                    holder.rb2.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.smilyone, 0);
                    holder.tv2.setTypeface(holder.tv2.getTypeface(), Typeface.BOLD);
                }
            }if(i==2){
                holder.tv3.setVisibility(View.VISIBLE);
                holder.tv3.setText(option.get(i).getOptionName());
                holder.rb3.setVisibility(View.VISIBLE);
                if (option.get(i).getOptionName().equalsIgnoreCase("Excellent") ||
                        option.get(i).getOptionName().equalsIgnoreCase("Fantastic") ||
                        option.get(i).getOptionName().equalsIgnoreCase("Perfect") ||
                        option.get(i).getOptionName().equalsIgnoreCase("Satisfactory")){
                    holder.rb3.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.smilyone, 0);
                    holder.tv3.setTypeface(holder.tv3.getTypeface(), Typeface.BOLD);
                }
            }if(i==3){
                holder.tv4.setVisibility(View.VISIBLE);
                holder.tv4.setText(option.get(i).getOptionName());
                holder.rb4.setVisibility(View.VISIBLE);
                if (option.get(i).getOptionName().equalsIgnoreCase("Excellent") ||
                        option.get(i).getOptionName().equalsIgnoreCase("Fantastic") ||
                        option.get(i).getOptionName().equalsIgnoreCase("Perfect") ||
                        option.get(i).getOptionName().equalsIgnoreCase("Satisfactory")){
                    holder.rb4.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.smilyone, 0);
                    holder.tv4.setTypeface(holder.tv4.getTypeface(), Typeface.BOLD);
                }
            }
        }

        holder.radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                // TODO Auto-generated method stub

                switch (checkedId){
                    case R.id.rb1:
                        ans[position]=option.get(0).getFQOptionId();
                        break;
                    case R.id.rb2:
                        ans[position]=option.get(1).getFQOptionId();
                        break;
                    case R.id.rb3:
                        ans[position]=option.get(2).getFQOptionId();
                        break;
                    case R.id.rb4:
                        ans[position]=option.get(3).getFQOptionId();
                        break;
                    default:
                        ans[position]="0";
                        break;
                }


            }
        });

    }

    @Override
    public int getItemCount() {
        return list.size()-2;

    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView tvquestion,tv1,tv2,tv3,tv4;
        RadioGroup radioGroup;
        RadioButton rb1,rb2,rb3,rb4;

        public ViewHolder(View itemView) {
            super(itemView);
            tvquestion= itemView.findViewById(R.id.tvquestion);
            tv1= itemView.findViewById(R.id.tv1);
            tv2= itemView.findViewById(R.id.tv2);
            tv3= itemView.findViewById(R.id.tv3);
            tv4= itemView.findViewById(R.id.tv4);
            radioGroup= itemView.findViewById(R.id.radiogroup);
            rb1= itemView.findViewById(R.id.rb1);
            rb2= itemView.findViewById(R.id.rb2);
            rb3= itemView.findViewById(R.id.rb3);
            rb4=itemView.findViewById(R.id.rb4);

        }

    }
}

