
package paragon.futuralabs.com.paragon.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

@SuppressWarnings("unused")
public class VouchersList implements Serializable{

    @SerializedName("Currency")
    private String mCurrency;
    @SerializedName("ExpiryDate")
    private String mExpiryDate;
    @SerializedName("OutletName")
    private String mOutletName;
    @SerializedName("VoucherAmount")
    private String mVoucherAmount;
    @SerializedName("VoucherCouponsId")
    private Long mVoucherCouponsId;
    @SerializedName("VoucherDescription")
    private String mVoucherDescription;
    @SerializedName("VoucherName")
    private String mVoucherName;
    @SerializedName("VoucherCode")
    private String mVoucherCode;
    @SerializedName("VoucherPhoto")
    private String mVoucherPhoto;
    @SerializedName("VoucherPhoto1")
    private String mVoucherPhoto1;
    @SerializedName("VouchersId")
    private Long mVouchersId;
    @SerializedName("TermsAndConditions")
    private String mTermsAndConditions;
    @SerializedName("MealPeriod")
    private String mMealPeriod;
    @SerializedName("DiscountType")
    private String mDiscountType;

    public String getCurrency() {
        return mCurrency;
    }

    public void setCurrency(String Currency) {
        mCurrency = Currency;
    }

    public String getExpiryDate() {
        return mExpiryDate;
    }

    public void setExpiryDate(String ExpiryDate) {
        mExpiryDate = ExpiryDate;
    }

    public String getOutletName() {
        return mOutletName;
    }

    public void setOutletName(String OutletName) {
        mOutletName = OutletName;
    }

    public String getVoucherAmount() {
        return mVoucherAmount;
    }

    public void setVoucherAmount(String VoucherAmount) {
        mVoucherAmount = VoucherAmount;
    }

    public Long getVoucherCouponsId() {
        return mVoucherCouponsId;
    }

    public void setVoucherCouponsId(Long VoucherCouponsId) {
        mVoucherCouponsId = VoucherCouponsId;
    }

    public String getVoucherDescription() {
        return mVoucherDescription;
    }

    public void setVoucherDescription(String VoucherDescription) {
        mVoucherDescription = VoucherDescription;
    }

    public String getVoucherName() {
        return mVoucherName;
    }

    public void setVoucherName(String VoucherName) {
        mVoucherName = VoucherName;
    }

    public String getVoucherCode() {
        return mVoucherCode;
    }

    public void setVoucherCode(String VoucherCode) {
        mVoucherCode = VoucherCode;
    }

    public String getVoucherPhoto() {
        return mVoucherPhoto;
    }

    public void setVoucherPhoto(String VoucherPhoto) {
        mVoucherPhoto = VoucherPhoto;
    }

    public String getVoucherPhoto1() {
        return mVoucherPhoto1;
    }

    public void setVoucherPhoto1(String VoucherPhoto1) {
        mVoucherPhoto1 = VoucherPhoto1;
    }

    public Long getVouchersId() {
        return mVouchersId;
    }

    public void setVouchersId(Long VouchersId) {
        mVouchersId = VouchersId;
    }

    public String getTermsAndConditions() {
        return mTermsAndConditions;
    }

    public void setTermsAndConditions(String TermsAndConditions) {
        mTermsAndConditions = TermsAndConditions;
    }

    public String getMealPeriod() {
        return mMealPeriod;
    }

    public void setMealPeriod(String MealPeriod) {
        mMealPeriod = MealPeriod;
    }

    public String getDiscountType() {
        return mDiscountType;
    }

    public void setDiscountType(String DiscountType) {
        mDiscountType = DiscountType;
    }

}
