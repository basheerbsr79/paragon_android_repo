
package paragon.futuralabs.com.paragon.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

@SuppressWarnings("unused")
public class MenuCatagoryList implements Serializable{

    @SerializedName("CategoryImg")
    private String mCategoryImg;
    @SerializedName("Description")
    private String mDescription;
    @SerializedName("MenuCategoryId")
    private Long mMenuCategoryId;
    @SerializedName("MenuCategoryName")
    private String mMenuCategoryName;

    public String getCategoryImg() {
        return mCategoryImg;
    }

    public void setCategoryImg(String CategoryImg) {
        mCategoryImg = CategoryImg;
    }

    public String getDescription() {
        return mDescription;
    }

    public void setDescription(String Description) {
        mDescription = Description;
    }

    public Long getMenuCategoryId() {
        return mMenuCategoryId;
    }

    public void setMenuCategoryId(Long MenuCategoryId) {
        mMenuCategoryId = MenuCategoryId;
    }

    public String getMenuCategoryName() {
        return mMenuCategoryName;
    }

    public void setMenuCategoryName(String MenuCategoryName) {
        mMenuCategoryName = MenuCategoryName;
    }

}
