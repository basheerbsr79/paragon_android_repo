package paragon.futuralabs.com.paragon.Activity;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import paragon.futuralabs.com.paragon.Adapters.RVFavorites;
import paragon.futuralabs.com.paragon.R;
import paragon.futuralabs.com.paragon.apiManager.PNApiResponseWrapper;
import paragon.futuralabs.com.paragon.apiManager.PNNetworkConstants;
import paragon.futuralabs.com.paragon.apiManager.PNNetworkManager;
import paragon.futuralabs.com.paragon.apiManager.SplitResponse;
import paragon.futuralabs.com.paragon.models.FavoriteList;
import paragon.futuralabs.com.paragon.models.FavoriteResponse;

import static paragon.futuralabs.com.paragon.Utilities.AppConstants.FROM;
import static paragon.futuralabs.com.paragon.Utilities.AppConstants.Mgrill;
import static paragon.futuralabs.com.paragon.Utilities.AppConstants.Salkara;
import static paragon.futuralabs.com.paragon.Utilities.ConnectionDetector.toast;
import static paragon.futuralabs.com.paragon.apiManager.PNNetworkManager.post;

public class PNFavoriteDishes extends PNBaseActivity {

    LinearLayout linLayContainer;
    TextView tvHeading;
    ImageView ic_back_arrow;
    RecyclerView recyclerView;
    SplitResponse splitResponse;
    FavoriteResponse favoriteResponse;
    List<FavoriteList> favoriteLists;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pnfavorite_dishes);
        init();
        handleEvents();
        getFavoriteDishes();
        trackEvent("Favorite Dishes","View Favorite Dishes");
    }

    private void handleEvents() {
        tvHeading.setText("Dishes");
        ic_back_arrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

    }

    private void init() {
        linLayContainer= findViewById(R.id.linLayContainer);
        ic_back_arrow= findViewById(R.id.img_back_toolbar);
        tvHeading= findViewById(R.id.txt_title_toolbar);
        recyclerView= findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
    }

    public void getFavoriteDishes(){
        final String[] list=new String[0];
        favoriteLists=new ArrayList(Arrays.asList(list));
        showProgress();
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("CustomerId", PNSP.getuserid());
            jsonObject.put("OutletId", PNSP.getOutlet());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        post(PNFavoriteDishes.this, PNNetworkConstants.sp_api_menufavorite_getbycustomers, jsonObject.toString(), new PNNetworkManager.NetworkInterface() {
            @Override
            public void onResponse(PNApiResponseWrapper baseResponse) {
                dismissProgres();
                if (baseResponse.isSuccess()) {
                    splitResponse = new SplitResponse(baseResponse.getJsonObjectResponse());
                    if (splitResponse.getStatus().equals("1")) {
                        try {
                            Gson gson = new Gson();
                            favoriteResponse = gson.fromJson(splitResponse.getResponse(), FavoriteResponse.class);
                            favoriteLists.addAll(favoriteResponse.getData());
                            if (favoriteLists.size()>0)
                                setData(favoriteLists);
                            else {
                                toast(getApplicationContext(), "No favorite dish found");
                                recyclerView.setAdapter(null);
                            }

                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    } else {
                        toast(getApplicationContext(), splitResponse.getMessage());
                        recyclerView.setAdapter(null);
                    }
                } else {
                    toast(getApplicationContext(), baseResponse.getMessage());
                }

            }
        });


    }

    private void setData(List<FavoriteList> favoriteLists) {

        RVFavorites rvFavorites=new RVFavorites(PNFavoriteDishes.this,favoriteLists);
        recyclerView.setAdapter(rvFavorites);
    }

}
