
package paragon.futuralabs.com.paragon.models;

import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class FAQList {

    @SerializedName("FaqAns")
    private String mFaqAns;
    @SerializedName("FaqCategory")
    private String mFaqCategory;
    @SerializedName("FaqCategoryId")
    private Long mFaqCategoryId;
    @SerializedName("FaqId")
    private Long mFaqId;
    @SerializedName("FaqQus")
    private String mFaqQus;
    @SerializedName("FaqType")
    private String mFaqType;
    @SerializedName("FaqTypeId")
    private Long mFaqTypeId;
    @SerializedName("IsActive")
    private Boolean mIsActive;
    @SerializedName("SortOrder")
    private Long mSortOrder;

    public String getFaqAns() {
        return mFaqAns;
    }

    public void setFaqAns(String FaqAns) {
        mFaqAns = FaqAns;
    }

    public String getFaqCategory() {
        return mFaqCategory;
    }

    public void setFaqCategory(String FaqCategory) {
        mFaqCategory = FaqCategory;
    }

    public Long getFaqCategoryId() {
        return mFaqCategoryId;
    }

    public void setFaqCategoryId(Long FaqCategoryId) {
        mFaqCategoryId = FaqCategoryId;
    }

    public Long getFaqId() {
        return mFaqId;
    }

    public void setFaqId(Long FaqId) {
        mFaqId = FaqId;
    }

    public String getFaqQus() {
        return mFaqQus;
    }

    public void setFaqQus(String FaqQus) {
        mFaqQus = FaqQus;
    }

    public String getFaqType() {
        return mFaqType;
    }

    public void setFaqType(String FaqType) {
        mFaqType = FaqType;
    }

    public Long getFaqTypeId() {
        return mFaqTypeId;
    }

    public void setFaqTypeId(Long FaqTypeId) {
        mFaqTypeId = FaqTypeId;
    }

    public Boolean getIsActive() {
        return mIsActive;
    }

    public void setIsActive(Boolean IsActive) {
        mIsActive = IsActive;
    }

    public Long getSortOrder() {
        return mSortOrder;
    }

    public void setSortOrder(Long SortOrder) {
        mSortOrder = SortOrder;
    }

}
