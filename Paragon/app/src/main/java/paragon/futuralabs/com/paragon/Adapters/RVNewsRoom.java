package paragon.futuralabs.com.paragon.Adapters;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import paragon.futuralabs.com.paragon.Activity.PNNewsRoom;
import paragon.futuralabs.com.paragon.Activity.PNNewsRoomDetail;
import paragon.futuralabs.com.paragon.R;
import paragon.futuralabs.com.paragon.models.news.Datum;
import paragon.futuralabs.com.paragon.models.news.LikeList;

import static paragon.futuralabs.com.paragon.Utilities.AppConstants.position;

/**
 * Created by Shamir on 04-07-2018.
 */

public class RVNewsRoom extends RecyclerView.Adapter<RVNewsRoom.ViewHolder> {
    List<Datum> list;
    Context context;

    public RVNewsRoom(Context context, List<Datum> list) {
        this.context = context;
        this.list = list;

    }

    @Override
    public RVNewsRoom.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.rv_newsroom_item, parent, false);
        RVNewsRoom.ViewHolder viewHolder = new RVNewsRoom.ViewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RVNewsRoom.ViewHolder holder, int position) {

        String url=list.get(position).getImage();
        Picasso.with(context)
                .load(url)
                .error(R.drawable.dish_item_placeholder)
                .placeholder(R.drawable.dish_item_placeholder)
                .resize(100,100).centerCrop()
                .into(holder.imvImage);

        holder.tvName.setText(list.get(position).getNewsName());
        holder.tvHeading.setText(list.get(position).getHeading());
        holder.tvDescription.setText(list.get(position).getDescription());
        holder.tvLike.setText(list.get(position).getLikeList().size()+"");
        holder.tvComment.setText(list.get(position).getCommentsList().size()+"");

        List<LikeList> like=list.get(position).getLikeList();

        for (int i = 0; i < like.size(); i++) {
            if (like.get(i).getCustomerId().equals(((PNNewsRoom)context).PNSP.getuserid()))
                holder.ivLike.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_like));

        }

    }

    @Override
    public int getItemCount() {
        return list.size();

    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        TextView tvName,tvHeading,tvDescription,tvLike,tvComment;
        ImageView imvImage,ivLike;

        public ViewHolder(View itemView) {
            super(itemView);
            imvImage=itemView.findViewById(R.id.imageView);
            ivLike=itemView.findViewById(R.id.ivLike);
            tvName =  itemView.findViewById(R.id.tvName);
            tvHeading =   itemView.findViewById(R.id.tvHeading);
            tvDescription =  itemView.findViewById(R.id.tvDescription);
            tvLike=itemView.findViewById(R.id.tvLike);
            tvComment=itemView.findViewById(R.id.tvComment);

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {

            Intent intent=new Intent(context, PNNewsRoomDetail.class);
            Bundle bundle=new Bundle();
            bundle.putSerializable(position,getPosition()+"");
            intent.putExtras(bundle);
            context.startActivity(intent);

        }
    }
}


