package paragon.futuralabs.com.paragon.Activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import paragon.futuralabs.com.paragon.R;
import paragon.futuralabs.com.paragon.paragonhelper.ParagonSP;

public class SecondPageActivity extends AppCompatActivity {
    Context context;
    ParagonSP sp;
    Button btn_guest_login,btn_registerd_user;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second_page);
        //setting context
        context = SecondPageActivity.this;
        //used for animation
        Intent in=getIntent();
        String tests=in.getStringExtra("abc");


        //variable defntions
        btn_guest_login =   findViewById(R.id.btn_guest_user);
        btn_registerd_user =   findViewById(R.id.btn_registred_user);

        //used to pass the view in to snack bar net alert
        final View snackbarview = getWindow().getDecorView();
        sp=new ParagonSP(getApplicationContext());

        btn_guest_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in=new Intent(SecondPageActivity.this,SelectResturantActivity.class);
                sp.setlogin("");
                startActivity(in);
            }
        });
        btn_registerd_user.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in=new Intent(SecondPageActivity.this,SigninActivity.class);
                sp.setlogin("");
                startActivity(in);
            }
        });
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_HOME);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }
}
