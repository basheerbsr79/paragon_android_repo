
package paragon.futuralabs.com.paragon.models;

import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class ProfileList {

    @SerializedName("Address")
    private String mAddress;
    @SerializedName("BirthDayDate")
    private String mBirthDayDate;
    @SerializedName("BirthDayMonth")
    private String mBirthDayMonth;
    @SerializedName("BirthDayYear")
    private String mBirthDayYear;
    @SerializedName("CountryCode")
    private String mCountryCode;
    @SerializedName("CustomerId")
    private Long mCustomerId;
    @SerializedName("Email")
    private String mEmail;
    @SerializedName("FirstName")
    private String mFirstName;
    @SerializedName("LastName")
    private String mLastName;
    @SerializedName("LocationName")
    private String mLocationName;
    @SerializedName("Mobile")
    private String mMobile;
    @SerializedName("Nationality")
    private String mNationality;
    @SerializedName("Title")
    private String mTitle;
    @SerializedName("WeddingDate")
    private String mWeddingDate;
    @SerializedName("WeddingMonth")
    private String mWeddingMonth;
    @SerializedName("WeddingYear")
    private String mWeddingYear;

    public String getAddress() {
        return mAddress;
    }

    public void setAddress(String Address) {
        mAddress = Address;
    }

    public String getBirthDayDate() {
        return mBirthDayDate;
    }

    public void setBirthDayDate(String BirthDayDate) {
        mBirthDayDate = BirthDayDate;
    }

    public String getBirthDayMonth() {
        return mBirthDayMonth;
    }

    public void setBirthDayMonth(String BirthDayMonth) {
        mBirthDayMonth = BirthDayMonth;
    }

    public String getBirthDayYear() {
        return mBirthDayYear;
    }

    public void setBirthDayYear(String BirthDayYear) {
        mBirthDayYear = BirthDayYear;
    }

    public String getCountryCode() {
        return mCountryCode;
    }

    public void setCountryCode(String CountryCode) {
        mCountryCode = CountryCode;
    }

    public Long getCustomerId() {
        return mCustomerId;
    }

    public void setCustomerId(Long CustomerId) {
        mCustomerId = CustomerId;
    }

    public String getEmail() {
        return mEmail;
    }

    public void setEmail(String Email) {
        mEmail = Email;
    }

    public String getFirstName() {
        return mFirstName;
    }

    public void setFirstName(String FirstName) {
        mFirstName = FirstName;
    }

    public String getLastName() {
        return mLastName;
    }

    public void setLastName(String LastName) {
        mLastName = LastName;
    }

    public String getLocationName() {
        return mLocationName;
    }

    public void setLocationName(String LocationName) {
        mLocationName = LocationName;
    }

    public String getMobile() {
        return mMobile;
    }

    public void setMobile(String Mobile) {
        mMobile = Mobile;
    }

    public String getNationality() {
        return mNationality;
    }

    public void setNationality(String Nationality) {
        mNationality = Nationality;
    }

    public String getTitle() {
        return mTitle;
    }

    public void setTitle(String Title) {
        mTitle = Title;
    }

    public String getWeddingDate() {
        return mWeddingDate;
    }

    public void setWeddingDate(String WeddingDate) {
        mWeddingDate = WeddingDate;
    }

    public String getWeddingMonth() {
        return mWeddingMonth;
    }

    public void setWeddingMonth(String WeddingMonth) {
        mWeddingMonth = WeddingMonth;
    }

    public String getWeddingYear() {
        return mWeddingYear;
    }

    public void setWeddingYear(String WeddingYear) {
        mWeddingYear = WeddingYear;
    }

}
