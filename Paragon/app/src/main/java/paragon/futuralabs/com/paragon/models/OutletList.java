
package paragon.futuralabs.com.paragon.models;

import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class OutletList {

    @SerializedName("OutletId")
    private Long mOutletId;
    @SerializedName("OutletsName")
    private String mOutletsName;
    @SerializedName("HomeDelivery")
    private String mHomeDelivery;
    @SerializedName("TakeAway")
    private String mTakeAway;
    @SerializedName("IsOnlineOrdering")
    private Boolean mIsOnlineOrdering;

    public Long getOutletId() {
        return mOutletId;
    }

    public void setOutletId(Long OutletId) {
        mOutletId = OutletId;
    }

    public String getOutletsName() {
        return mOutletsName;
    }

    public void setOutletsName(String OutletsName) {
        mOutletsName = OutletsName;
    }

    public String getHomeDelivery() {
        return mHomeDelivery;
    }

    public void setHomeDelivery(String HomeDelivery) {
        mHomeDelivery = HomeDelivery;
    }

    public String getTakeAway() {
        return mTakeAway;
    }

    public void setTakeAway(String TakeAway) {
        mTakeAway = TakeAway;
    }

    public Boolean getIsOnlineOrdering() {
        return mIsOnlineOrdering;
    }

    public void setIsOnlineOrdering(Boolean IsOnlineOrdering) {
        mIsOnlineOrdering = IsOnlineOrdering;
    }

}
