
package paragon.futuralabs.com.paragon.models;

import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class LocationData {

    @SerializedName("Address")
    private String mAddress;
    @SerializedName("Country")
    private String mCountry;
    @SerializedName("Email")
    private String mEmail;
    @SerializedName("Latitude")
    private Double mLatitude;
    @SerializedName("Location")
    private String mLocation;
    @SerializedName("Longitude")
    private Double mLongitude;
    @SerializedName("OutletsName")
    private String mOutletsName;
    @SerializedName("Phone")
    private String mPhone;
    @SerializedName("Postcode")
    private String mPostcode;
    @SerializedName("State")
    private String mState;

    public String getAddress() {
        return mAddress;
    }

    public void setAddress(String Address) {
        mAddress = Address;
    }

    public String getCountry() {
        return mCountry;
    }

    public void setCountry(String Country) {
        mCountry = Country;
    }

    public String getEmail() {
        return mEmail;
    }

    public void setEmail(String Email) {
        mEmail = Email;
    }

    public Double getLatitude() {
        return mLatitude;
    }

    public void setLatitude(Double Latitude) {
        mLatitude = Latitude;
    }

    public String getLocation() {
        return mLocation;
    }

    public void setLocation(String Location) {
        mLocation = Location;
    }

    public Double getLongitude() {
        return mLongitude;
    }

    public void setLongitude(Double Longitude) {
        mLongitude = Longitude;
    }

    public String getOutletsName() {
        return mOutletsName;
    }

    public void setOutletsName(String OutletsName) {
        mOutletsName = OutletsName;
    }

    public String getPhone() {
        return mPhone;
    }

    public void setPhone(String Phone) {
        mPhone = Phone;
    }

    public String getPostcode() {
        return mPostcode;
    }

    public void setPostcode(String Postcode) {
        mPostcode = Postcode;
    }

    public String getState() {
        return mState;
    }

    public void setState(String State) {
        mState = State;
    }

}
