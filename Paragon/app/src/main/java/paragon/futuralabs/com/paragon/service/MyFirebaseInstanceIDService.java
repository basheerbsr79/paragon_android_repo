package paragon.futuralabs.com.paragon.service;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

import paragon.futuralabs.com.paragon.app.Config;
import paragon.futuralabs.com.paragon.paragonhelper.ParagonSP;

/**
 * Created by Shamir on 02-Dec-16.
 */
public class MyFirebaseInstanceIDService extends FirebaseInstanceIdService {
    private static final String TAG = MyFirebaseInstanceIDService.class.getSimpleName();

    public ParagonSP sp;

    @Override
    public void onTokenRefresh() {
        super.onTokenRefresh();

sp=new ParagonSP(getApplicationContext());
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        Log.i("refreshedToken",refreshedToken+"=>");

        // Saving reg id to shared preferences
        storeRegIdInPref(refreshedToken);

        // sending reg id to your server
        sendRegistrationToServer(refreshedToken);

        // Notify UI that registration has completed, so the progress indicator can be hidden.
        Intent registrationComplete = new Intent(Config.REGISTRATION_COMPLETE);

        registrationComplete.putExtra("token", refreshedToken);
        LocalBroadcastManager.getInstance(this).sendBroadcast(registrationComplete);

    }

    private void sendRegistrationToServer(final String token) {
        // sending gcm token to server
        Log.e(TAG, "sendRegistrationToServer: " + token);
        //Log.e("FCMID", "sendRegistrationToServer: " + token);

    }

    private void storeRegIdInPref(String token) {

        SharedPreferences pref = getApplicationContext().getSharedPreferences(Config.SHARED_PREF, 0);
        SharedPreferences.Editor editor = pref.edit();
        editor.putString("regId", token);
        editor.commit();

        Log.i("IvaTocken",token+ "=>");
        sp.setdeviceId(token);
    }

    public String getregToken(){
        return FirebaseInstanceId.getInstance().getToken();
    }
}