package paragon.futuralabs.com.paragon.Adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import paragon.futuralabs.com.paragon.R;
import paragon.futuralabs.com.paragon.models.BrandList;
import paragon.futuralabs.com.paragon.models.homedelivery.HDList;
import paragon.futuralabs.com.paragon.paragonhelper.ParagonSP;

import static paragon.futuralabs.com.paragon.Activity.PNHDMenuItems.posView;
import static paragon.futuralabs.com.paragon.Utilities.AppConstants.BrandId;
import static paragon.futuralabs.com.paragon.Utilities.Utils.toast;

/**
 * Created by Shamir on 16-03-2018.
 */

public class RVHD extends RecyclerView.Adapter<RVHD.ViewHolder> {
    List<HDList> hdLists;
    Context context;
    String catId;

    public RVHD(Context context, List<HDList> hdLists,String catId) {
        this.context = context;
        this.hdLists = hdLists;
        this.catId=catId;

    }

    @Override
    public RVHD.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.rv_home_delivery_item, parent, false);
        RVHD.ViewHolder viewHolder = new RVHD.ViewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final RVHD.ViewHolder holder, final int position) {

        holder.tvCatagory.setText(hdLists.get(position).getMenuCategoryName());
        RVHDMenuItem rvhdMenuItem = new RVHDMenuItem(context, hdLists.get(position).getMenuItems());
        holder.recyclerView.setAdapter(rvhdMenuItem);
        /*if (position==0) {
            if (hdLists.get(position).getMenuItems().size() > 0) {
                holder.recyclerView.setVisibility(View.GONE);
            }
        }else {
            holder.recyclerView.setVisibility(View.GONE);
        }*/
       // posView=position;
        if (catId.equals(hdLists.get(position).getMenuCategoryId()+"")) {
            holder.recyclerView.setVisibility(View.VISIBLE);
            posView=position;
        }else
            holder.recyclerView.setVisibility(View.GONE);

        holder.tvCatagory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (hdLists.get(position).getMenuItems().size() > 0) {
                    if (holder.recyclerView.getVisibility()==View.VISIBLE) {
                        holder.recyclerView.setVisibility(View.GONE);
                        holder.tvCatagory.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_down, 0);
                    }else {
                        holder.recyclerView.setVisibility(View.VISIBLE);
                        holder.tvCatagory.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_up, 0);
                    }
                    //recyclerView.scrollToPosition(posView);
                }else {
                    toast(context,"No items found");
                }
            }
        });


    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return hdLists.size();

    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView tvCatagory;
        RecyclerView recyclerView;

        public ViewHolder(View itemView) {
            super(itemView);
            tvCatagory =   itemView.findViewById(R.id.tvCatagory);
            recyclerView =   itemView.findViewById(R.id.recyclerView);
            recyclerView.setLayoutManager(new LinearLayoutManager(context));
        }

    }
}
