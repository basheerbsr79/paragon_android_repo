package paragon.futuralabs.com.paragon.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import paragon.futuralabs.com.paragon.R;
import paragon.futuralabs.com.paragon.paragonhelper.ParagonSP;

public class NotificationActivity extends PNBaseActivity {
    TextView textView_message;
    TextView textView_published_date;
    TextView tv_notification;
    ParagonSP sp;
    TextView tvHeading;
    ImageView ic_back_arrow;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification);
        sp=new ParagonSP(getApplicationContext());

        textView_message=   findViewById(R.id.notif_msg);
        textView_published_date=   findViewById(R.id.notif_published_date);
        tv_notification=   findViewById(R.id.tv_notification);
        ic_back_arrow= findViewById(R.id.img_back_toolbar);
        tvHeading= findViewById(R.id.txt_title_toolbar);

    Bundle extras=getIntent().getExtras();
    String message=extras.getString("message");
    String published_date=extras.getString("date");

    textView_message.setText(message);
    textView_published_date.setText(published_date);
        tvHeading.setText("Notification");
        ic_back_arrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });


    }

    @Override
    public void onBackPressed() {
        Intent in=new Intent(NotificationActivity.this,SelectResturantActivity.class);
        in.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(in);
        finish();
    }
}
