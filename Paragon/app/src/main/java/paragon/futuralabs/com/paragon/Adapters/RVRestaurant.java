package paragon.futuralabs.com.paragon.Adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import paragon.futuralabs.com.paragon.R;
import paragon.futuralabs.com.paragon.models.FavRestList;

/**
 * Created by Shamir on 29-03-2018.
 */

public class RVRestaurant extends RecyclerView.Adapter<RVRestaurant.ViewHolder> {
    List<FavRestList> lists;
    Context context;

    public RVRestaurant(Context context,List<FavRestList> lists) {
        this.context = context;
        this.lists = lists;

    }

    @Override
    public RVRestaurant.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.rv_fav_rest_item, parent, false);
        RVRestaurant.ViewHolder viewHolder = new RVRestaurant.ViewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RVRestaurant.ViewHolder holder, int position) {

        holder.textView.setText(lists.get(position).getOutletsName());
        holder.tvAddress.setText("Address : "+lists.get(position).getAddress());
        holder.tvPhone.setText("Phone : "+lists.get(position).getPhone());
        holder.tvEmail.setText("Email : "+lists.get(position).getEmail());
        holder.tvCustCareNo.setText("Customer Care No : "+lists.get(position).getCustomerCareNumber());

    }

    @Override
    public int getItemCount() {
        return lists.size();

    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView textView,tvAddress,tvEmail,tvPhone,tvCustCareNo;

        public ViewHolder(View itemView) {
            super(itemView);
            textView =   itemView.findViewById(R.id.textView);
            tvAddress =   itemView.findViewById(R.id.tvAddress);
            tvEmail =   itemView.findViewById(R.id.tvEmail);
            tvPhone =   itemView.findViewById(R.id.tvPhone);
            tvCustCareNo =  itemView.findViewById(R.id.tvCustCareNo);
        }

    }
}

