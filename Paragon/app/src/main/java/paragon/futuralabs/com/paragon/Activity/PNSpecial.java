package paragon.futuralabs.com.paragon.Activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import paragon.futuralabs.com.paragon.Activity.PNBaseActivity;
import paragon.futuralabs.com.paragon.Adapters.RVMenuCatagory;
import paragon.futuralabs.com.paragon.Adapters.RVSpecial;
import paragon.futuralabs.com.paragon.R;
import paragon.futuralabs.com.paragon.Utilities.MyAlert;
import paragon.futuralabs.com.paragon.apiManager.PNApiResponseWrapper;
import paragon.futuralabs.com.paragon.apiManager.PNNetworkConstants;
import paragon.futuralabs.com.paragon.apiManager.PNNetworkManager;
import paragon.futuralabs.com.paragon.apiManager.SplitResponse;
import paragon.futuralabs.com.paragon.models.Image;
import paragon.futuralabs.com.paragon.models.MenuCatagoryList;
import paragon.futuralabs.com.paragon.models.MenuCatagoryResponse;
import paragon.futuralabs.com.paragon.models.SpecialList;
import paragon.futuralabs.com.paragon.models.SpecialResponse;
import paragon.futuralabs.com.paragon.models.todayspecial.Datum;
import paragon.futuralabs.com.paragon.models.todayspecial.TodaySpecialResponse;
import paragon.futuralabs.com.paragon.models.todayspecial.TodaysSpecialAllDetailList;

import static paragon.futuralabs.com.paragon.Utilities.AppConstants.FROM;
import static paragon.futuralabs.com.paragon.Utilities.AppConstants.Mgrill;
import static paragon.futuralabs.com.paragon.Utilities.AppConstants.Salkara;
import static paragon.futuralabs.com.paragon.Utilities.ConnectionDetector.toast;
import static paragon.futuralabs.com.paragon.apiManager.PNNetworkManager.post;

public class PNSpecial extends PNBaseActivity {

    LinearLayout linLayContainer;
    TextView tvHeading;
    ImageView ic_back_arrow;
    RecyclerView recyclerView;
    SplitResponse splitResponse;
    TodaySpecialResponse specialResponse;
    List<Datum> specialLists;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pnspecial);
        init();
        handleEvents();
        getSpecials();
        trackEvent("Today's Special","View Today's Special");
    }

    private void handleEvents() {
        tvHeading.setText("Today's Special");
        ic_back_arrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }

    private void init() {
        linLayContainer= findViewById(R.id.linLayContainer);
        ic_back_arrow=findViewById(R.id.img_back_toolbar);
        tvHeading= findViewById(R.id.txt_title_toolbar);
        recyclerView= findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new GridLayoutManager(this,2));
    }

    public void getSpecials(){
        final String[] list=new String[0];
        specialLists=new ArrayList(Arrays.asList(list));
        showProgress();
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("OutletId", PNSP.getOutlet());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        post(PNSpecial.this, PNNetworkConstants.BindTodaySpecials, jsonObject.toString(), new PNNetworkManager.NetworkInterface() {
            @Override
            public void onResponse(PNApiResponseWrapper baseResponse) {
                dismissProgres();
                if (baseResponse.isSuccess()) {
                    splitResponse = new SplitResponse(baseResponse.getJsonObjectResponse());
                    if (splitResponse.getStatus().equals("1")) {
                        try {
                            Gson gson = new Gson();
                            specialResponse = gson.fromJson(splitResponse.getResponse(), TodaySpecialResponse.class);
                            specialLists.addAll(specialResponse.getData());
                            if (specialLists.size()>0)
                                setData(specialLists);
                            else {
                                MyAlert alert = new MyAlert();
                                alert.showAlertFinish(PNSpecial.this, "No specials found for today");
                            }

                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    } else {
                        MyAlert alert = new MyAlert();
                        alert.showAlertFinish(PNSpecial.this, "No specials found for today");
                    }
                } else {
                    toast(getApplicationContext(), baseResponse.getMessage());
                }

            }
        });


    }

    private void setData(List<Datum> list) {

        List<TodaysSpecialAllDetailList> detailLists;
        final String[] list1=new String[0];
        detailLists=new ArrayList(Arrays.asList(list1));
        for (int i = 0; i < list.size(); i++) {
            List<paragon.futuralabs.com.paragon.models.todayspecial.Image> images;
            images=new ArrayList(Arrays.asList(list1));
            images.addAll(list.get(i).getImages());
            for (int j = 0; j < images.size(); j++) {
                TodaysSpecialAllDetailList dlist=new TodaysSpecialAllDetailList();
                dlist.setImage(images.get(j).getImagePath());
                dlist.setName(images.get(j).getDishName());
                dlist.setPrmoType(list.get(i).getPrmoType());
                dlist.setTime(list.get(i).getTimeStartFrom().getHours()+":"+
                        list.get(i).getTimeStartFrom().getMinutes()+" - "+
                        list.get(i).getTimeStartTo().getHours()+":"+
                        list.get(i).getTimeStartTo().getMinutes());
                        detailLists.add(dlist);
            }
        }

        RVSpecial rvSpecial=new RVSpecial(PNSpecial.this,detailLists);
        recyclerView.setAdapter(rvSpecial);
    }
}
