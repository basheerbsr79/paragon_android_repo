
package paragon.futuralabs.com.paragon.models;

import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class AppVersionList {

    @SerializedName("AppType")
    private Long mAppType;
    @SerializedName("AppVersionId")
    private Long mAppVersionId;
    @SerializedName("VersionCode")
    private String mVersionCode;
    @SerializedName("VersionName")
    private String mVersionName;

    public Long getAppType() {
        return mAppType;
    }

    public void setAppType(Long AppType) {
        mAppType = AppType;
    }

    public Long getAppVersionId() {
        return mAppVersionId;
    }

    public void setAppVersionId(Long AppVersionId) {
        mAppVersionId = AppVersionId;
    }

    public String getVersionCode() {
        return mVersionCode;
    }

    public void setVersionCode(String VersionCode) {
        mVersionCode = VersionCode;
    }

    public String getVersionName() {
        return mVersionName;
    }

    public void setVersionName(String VersionName) {
        mVersionName = VersionName;
    }

}
