package paragon.futuralabs.com.paragon.apiManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Shamir on 3-2-18.
 */

public class SplitResponse {
    String status;
    String message;
    String data;
    String response;
    String CustomerId;
    String CurrentState;
    String OrderId;
    String IsPeak;
    String TotalOrderCount;
    String TotalCancelledCount;

    public SplitResponse(JSONObject jsonObject) {
        try {
            JSONArray jsonArray=jsonObject.getJSONArray("d");
            this.response=jsonArray.getJSONObject(0).toString();
            this.status = jsonArray.getJSONObject(0).optString("status");
            this.message=jsonArray.getJSONObject(0).optString("message");
            this.CustomerId=jsonArray.getJSONObject(0).optInt("CustomerId")+"";
            this.CurrentState=jsonArray.getJSONObject(0).optInt("CurrentState")+"";
            this.OrderId=jsonArray.getJSONObject(0).optInt("OrderId")+"";
            this.IsPeak=jsonArray.getJSONObject(0).optInt("IsPeak")+"";
            this.TotalOrderCount=jsonArray.getJSONObject(0).optInt("TotalOrderCount")+"";
            this.TotalCancelledCount=jsonArray.getJSONObject(0).optInt("TotalCancelledCount")+"";
            this.data=jsonArray.getJSONObject(0).optString("data");
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getCustomerId() {
        return CustomerId;
    }

    public void setCustomerId(String CustomerId) {
        this.CustomerId = CustomerId;
    }

    public String getCurrentState() {
        return CurrentState;
    }

    public void setCurrentState(String CurrentState) {
        this.CurrentState = CurrentState;
    }

    public String getOrderId() {
        return OrderId;
    }

    public void setOrderId(String OrderId) {
        this.OrderId = OrderId;
    }

    public String getIsPeak() {
        return IsPeak;
    }

    public void setIsPeak(String IsPeak) {
        this.IsPeak = IsPeak;
    }

    public String getTotalOrderCount() {
        return TotalOrderCount;
    }

    public void setTotalOrderCount(String TotalOrderCount) {
        this.TotalOrderCount = TotalOrderCount;
    }

    public String getTotalCancelledCount() {
        return TotalCancelledCount;
    }

    public void setTotalCancelledCount(String TotalCancelledCount) {
        this.TotalCancelledCount = TotalCancelledCount;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }
}
