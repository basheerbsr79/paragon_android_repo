package paragon.futuralabs.com.paragon.Activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import paragon.futuralabs.com.paragon.Adapters.ExpandListAdapter;
import paragon.futuralabs.com.paragon.R;
import paragon.futuralabs.com.paragon.Utilities.Utils;
import paragon.futuralabs.com.paragon.apiManager.PNApiResponseWrapper;
import paragon.futuralabs.com.paragon.apiManager.PNNetworkConstants;
import paragon.futuralabs.com.paragon.apiManager.PNNetworkManager;
import paragon.futuralabs.com.paragon.apiManager.SplitResponse;
import paragon.futuralabs.com.paragon.models.CartList;
import paragon.futuralabs.com.paragon.models.CartResponse;
import paragon.futuralabs.com.paragon.models.homedelivery.HDList;
import paragon.futuralabs.com.paragon.models.homedelivery.HDResponse;
import paragon.futuralabs.com.paragon.models.homedelivery.MenuItem;

import static paragon.futuralabs.com.paragon.Utilities.AppConstants.CatId;
import static paragon.futuralabs.com.paragon.Utilities.AppConstants.Item;
import static paragon.futuralabs.com.paragon.Utilities.ConnectionDetector.toast;
import static paragon.futuralabs.com.paragon.apiManager.PNNetworkManager.post;

public class PNAddToCart extends PNBaseActivity {

    ExpandableListView expandableLV;
    // ImageView imvImage;
    TextView tvHeading;
    ImageView ic_back_arrow;
    TextView tvDesc,tvCount,tvPrice;
    LinearLayout linLayCart;
    ImageView ivSearch;
    SplitResponse splitResponse;
    HDResponse hdResponse;
    List<HDList> hdLists;
    CartResponse cartResponse;
    List<CartList> cartLists;
    public static int posView=0;
    ExpandListAdapter ExpAdapter;
    List<MenuItem> allMenuItemList;

    //CollapsingToolbarLayout collapsingToolbarLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pn_add_to_cart);

        init();
        handleEvents();
        getMenuItems();
        trackEvent("Order Online","View Order Online");
    }

    @Override
    protected void onResume() {
        super.onResume();
        getCartIteams();
    }

    private void handleEvents() {
//setToolBar();
        tvHeading.setText("Order Online");
        ic_back_arrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        linLayCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(PNAddToCart.this,PNCart.class);
                startActivity(intent);
            }
        });

        ivSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
Intent intent=new Intent(PNAddToCart.this,PNSearchMenuItems.class);
                /*Bundle bundle=new Bundle();
                bundle.putSerializable(Item, (Serializable) allMenuItemList);
                intent.putExtras(bundle);*/
                startActivity(intent);

            }
        });
    }

    private void init() {
        ic_back_arrow= findViewById(R.id.img_back_toolbar);
        tvHeading= findViewById(R.id.txt_title_toolbar);
        //imvImage= findViewById(R.id.imvImage);
        ivSearch=findViewById(R.id.ivSearch);
        tvDesc= findViewById(R.id.tvDesc);
        expandableLV= findViewById(R.id.expandableLV);

        linLayCart= findViewById(R.id.linLayCart);
        tvCount= findViewById(R.id.tvCount);
        tvPrice= findViewById(R.id.tvPrice);

    }

    /*private void setToolBar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);

        toolbar.setNavigationIcon(R.drawable.ic_back_arrow);
        toolbar.setTitle("");

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               onBackPressed();
            }
        });

        collapsingToolbarLayout = (CollapsingToolbarLayout) findViewById(R.id.toolbar_layout);
       collapsingToolbarLayout.setTitle(" ");
        dynamicToolbarColor();
        toolbarTextAppernce();
    }

    private void dynamicToolbarColor() {

        Bitmap bitmap = BitmapFactory.decodeResource(getResources(),
                R.drawable.ic_back_arrow);
        Palette.from(bitmap).generate(new Palette.PaletteAsyncListener() {

            @Override
            public void onGenerated(Palette palette) {
                collapsingToolbarLayout.setContentScrimColor(palette.getMutedColor(getResources().getColor(R.color.mask)));
                collapsingToolbarLayout.setStatusBarScrimColor(palette.getMutedColor(getResources().getColor(R.color.mask)));
            }
        });
    }

    private void toolbarTextAppernce() {
        collapsingToolbarLayout.setCollapsedTitleTextAppearance(R.style.collapsedappbar);
        collapsingToolbarLayout.setExpandedTitleTextAppearance(R.style.expandedappbar);
    }*/


    public void getMenuItems(){
        final String[] list=new String[0];
        hdLists=new ArrayList(Arrays.asList(list));
        showProgress();
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("OutletId", PNSP.getOutlet());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        post(this, PNNetworkConstants.BindMenuCategoryForHomeDelivery, jsonObject.toString(), new PNNetworkManager.NetworkInterface() {
            @Override
            public void onResponse(PNApiResponseWrapper baseResponse) {
                dismissProgres();
                if (baseResponse.isSuccess()) {
                    splitResponse = new SplitResponse(baseResponse.getJsonObjectResponse());
                    if (splitResponse.getStatus().equals("1")) {
                        try {
                            Gson gson = new Gson();
                            hdResponse = gson.fromJson(splitResponse.getResponse(), HDResponse.class);
                            hdLists.addAll(hdResponse.getData());
                            if (hdLists.size()>0)
                                setData(hdLists);
                            else
                                toast(getApplicationContext(),"No items found");

                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    } else {
                        toast(getApplicationContext(), splitResponse.getMessage());
                    }
                } else {
                    toast(getApplicationContext(), baseResponse.getMessage());
                }

            }
        });


    }

    private void setData(List<HDList> hdLists) {

        ExpAdapter = new ExpandListAdapter(PNAddToCart.this, hdLists);
        expandableLV.setAdapter(ExpAdapter);
        for (int i = 0; i < hdLists.size(); i++) {
            if (getIntent().getStringExtra(CatId).equals(hdLists.get(i).getMenuCategoryId()+"")){
                expandableLV.expandGroup(i);
                expandableLV.smoothScrollToPosition(i);
            }
        }

       /* final String[] list=new String[0];
        allMenuItemList=new ArrayList(Arrays.asList(list));

        for (int i = 0; i < hdLists.size(); i++) {
            List<MenuItem> menuItems=hdLists.get(i).getMenuItems();
            allMenuItemList.addAll(menuItems);
        }

        Log.i("IvaallMenuItemList",allMenuItemList.size()+"");*/


    }

    public void getCartIteams(){
        final String[] list=new String[0];
        cartLists=new ArrayList(Arrays.asList(list));
        showProgress();
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("CustomerId", PNSP.getuserid());
            jsonObject.put("OutletId", PNSP.getOutlet());

        } catch (JSONException e) {
            e.printStackTrace();
        }
        post(this, PNNetworkConstants.BindCartItems, jsonObject.toString(), new PNNetworkManager.NetworkInterface() {
            @Override
            public void onResponse(PNApiResponseWrapper baseResponse) {
                dismissProgres();
                if (baseResponse.isSuccess()) {
                    splitResponse = new SplitResponse(baseResponse.getJsonObjectResponse());
                    if (splitResponse.getStatus().equals("1")) {
                        try {
                            Gson gson = new Gson();
                            cartResponse = gson.fromJson(splitResponse.getResponse(), CartResponse.class);
                            cartLists.addAll(cartResponse.getData());
                            if (cartLists.size()>0)
                                setCartData(cartLists);
                            else
                                linLayCart.setVisibility(View.GONE);

                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    } else {
                        linLayCart.setVisibility(View.GONE);
                    }
                } else {
                    toast(getApplicationContext(), baseResponse.getMessage());
                }

            }
        });

    }

    private void setCartData(List<CartList> cartLists) {
        linLayCart.setVisibility(View.VISIBLE);
        double tot=0;
        long qty=0;
        for (int i = 0; i < cartLists.size(); i++) {
            tot=tot+(Double.parseDouble(cartLists.get(i).getPrice())*cartLists.get(i).getQuantity());
            qty=qty+cartLists.get(i).getQuantity();
        }
        if (qty==1) {
            tvCount.setText("1 item in cart");
        }else {
            tvCount.setText(qty+" items in cart");
        }
        tvPrice.setText(cartLists.get(0).getCurrency()+" "+tot);

    }

}

