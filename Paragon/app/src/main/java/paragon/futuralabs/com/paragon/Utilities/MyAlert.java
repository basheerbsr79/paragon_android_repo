package paragon.futuralabs.com.paragon.Utilities;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.view.View;

import paragon.futuralabs.com.paragon.Activity.PNFeedBack;
import paragon.futuralabs.com.paragon.Activity.SelectResturantActivity;
import paragon.futuralabs.com.paragon.R;

/**
 * Created by Shamir on 04/03/18.
 */
public class MyAlert {
    public void showAlert(final Context context,final String message){

        final PNCustomAlertDialog dialog = new PNCustomAlertDialog(context, "", message);
        dialog.show();
        dialog.btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();

            }
        });

    }

    public void showAlertFinish(final Activity context, final String message){

        final PNCustomAlertDialog dialog = new PNCustomAlertDialog(context, "", message);
        dialog.show();
        dialog.btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                context.finish();
            }
        });

    }

    public void showAlertHome(final Activity context, final String message){

        final PNCustomAlertDialog dialog = new PNCustomAlertDialog(context, "", message);
        dialog.show();
        dialog.btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                Intent in=new Intent(context,SelectResturantActivity.class);
                in.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(in);
                context.finish();
            }
        });

    }

    public void showAlertFeedBackHome(final Activity context, final String message){

        final PNCustomAlertDialog dialog = new PNCustomAlertDialog(context, "", message);
        dialog.show();
        dialog.btnOk.setText("FeedBack");
        dialog.btncancel.setVisibility(View.VISIBLE);
        dialog.btncancel.setText("Home");
        dialog.btncancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                Intent in=new Intent(context,SelectResturantActivity.class);
                in.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(in);
                context.finish();
            }
        });

        dialog.btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                Intent in=new Intent(context,PNFeedBack.class);
                in.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(in);
                context.finish();
            }
        });

    }




}
