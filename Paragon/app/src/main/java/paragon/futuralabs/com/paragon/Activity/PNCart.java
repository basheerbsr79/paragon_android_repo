package paragon.futuralabs.com.paragon.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import paragon.futuralabs.com.paragon.Adapters.RVCart;
import paragon.futuralabs.com.paragon.R;
import paragon.futuralabs.com.paragon.apiManager.PNApiResponseWrapper;
import paragon.futuralabs.com.paragon.apiManager.PNNetworkConstants;
import paragon.futuralabs.com.paragon.apiManager.PNNetworkManager;
import paragon.futuralabs.com.paragon.apiManager.SplitResponse;
import paragon.futuralabs.com.paragon.models.CartList;
import paragon.futuralabs.com.paragon.models.CartResponse;

import static paragon.futuralabs.com.paragon.Utilities.AppConstants.Currency;
import static paragon.futuralabs.com.paragon.Utilities.AppConstants.FROM;
import static paragon.futuralabs.com.paragon.Utilities.AppConstants.Mgrill;
import static paragon.futuralabs.com.paragon.Utilities.AppConstants.Price;
import static paragon.futuralabs.com.paragon.Utilities.AppConstants.Salkara;
import static paragon.futuralabs.com.paragon.Utilities.ConnectionDetector.toast;
import static paragon.futuralabs.com.paragon.apiManager.PNNetworkManager.post;

public class PNCart extends PNBaseActivity {

    LinearLayout linLayContainer,linLayCart;
    TextView tvHeading,tvCount,tvPrice;
    ImageView ic_back_arrow;
    RecyclerView recyclerView;
    SplitResponse splitResponse;
    CartResponse cartResponse;
    List<CartList> cartLists;
    String totPrice="0",currency="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pncart);
        init();
        handleEvents();
        getCartItems();
        trackEvent("Cart","View Cart");
    }

    private void handleEvents() {
        tvHeading.setText("Cart");
        ic_back_arrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        linLayCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(PNCart.this, PNSavedAddress.class);
                intent.putExtra(Price,totPrice);
                intent.putExtra(Currency,currency);
                startActivity(intent);
            }
        });
    }

    private void init() {
        linLayContainer= findViewById(R.id.linLayContainer);
        linLayCart= findViewById(R.id.linLayCart);
        ic_back_arrow= findViewById(R.id.img_back_toolbar);
        tvHeading= findViewById(R.id.txt_title_toolbar);
        tvCount= findViewById(R.id.tvCount);
        tvPrice= findViewById(R.id.tvPrice);
        recyclerView= findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
    }

    public void getCartItems(){
        final String[] list=new String[0];
        cartLists=new ArrayList(Arrays.asList(list));
        showProgress();
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("CustomerId", PNSP.getuserid());
            jsonObject.put("OutletId", PNSP.getOutlet());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        post(PNCart.this, PNNetworkConstants.BindCartItems, jsonObject.toString(), new PNNetworkManager.NetworkInterface() {
            @Override
            public void onResponse(PNApiResponseWrapper baseResponse) {
                dismissProgres();
                if (baseResponse.isSuccess()) {
                    splitResponse = new SplitResponse(baseResponse.getJsonObjectResponse());
                    if (splitResponse.getStatus().equals("1")) {
                        try {
                            Gson gson = new Gson();
                            cartResponse = gson.fromJson(splitResponse.getResponse(), CartResponse.class);
                            cartLists.addAll(cartResponse.getData());
                            if (cartLists.size()>0)
                                setData(cartLists);
                            else {
                                setNull();
                                toast(getApplicationContext(), "No item found");
                            }

                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    } else {
                        setNull();
                        toast(getApplicationContext(), splitResponse.getMessage());
                    }
                } else {
                    setNull();
                    toast(getApplicationContext(), baseResponse.getMessage());
                }

            }
        });


    }

    private void setNull() {
        recyclerView.setAdapter(null);
        linLayCart.setVisibility(View.GONE);
    }

    private void setData(List<CartList> cartLists) {

        RVCart rvCart=new RVCart(PNCart.this,cartLists);
        recyclerView.setAdapter(rvCart);
        setCartData(cartLists);
    }

    private void setCartData(List<CartList> cartLists) {
        linLayCart.setVisibility(View.VISIBLE);

        double tot=0;
        long qty=0;
        for (int i = 0; i < cartLists.size(); i++) {
            tot=tot+(Double.parseDouble(cartLists.get(i).getPrice())*cartLists.get(i).getQuantity());
            qty=qty+cartLists.get(i).getQuantity();
        }
        if (qty==1) {
            tvCount.setText("1 item in cart");
        }else {
            tvCount.setText(qty+" items in cart");
        }
        totPrice=tot+"";
        currency=cartLists.get(0).getCurrency();
        tvPrice.setText(cartLists.get(0).getCurrency()+" "+tot);

    }
}
