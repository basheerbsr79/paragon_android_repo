package paragon.futuralabs.com.paragon.Activity;

import android.app.*;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.widget.Toast;

import paragon.futuralabs.com.paragon.R;
import paragon.futuralabs.com.paragon.Utilities.ConnectionDetector;
import paragon.futuralabs.com.paragon.Utilities.NetFailedAlert;
import paragon.futuralabs.com.paragon.paragonhelper.ParagonSP;

public class SplashActivity extends PNBaseActivity {
    private static int SPLASH_TIME_OUT = 3000;
    Context context;
    ParagonSP sp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        //this.requestWindowFeature(Window.FEATURE_NO_TITLE);

        sp = new ParagonSP(getApplicationContext());

        context = SplashActivity.this;
        /*if (android.os.Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }*/
        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                try {

                        if (!sp.getlogin().equals("success")) {
                            Intent in = new Intent(context, SigninActivity.class);
                            startActivity(in);
                            finish();
                        } else {
                            Intent in = new Intent(context, SelectResturantActivity.class);
                            startActivity(in);
                            finish();
                        }


                } catch (Exception e) {
                    e.printStackTrace();
                }
                finish();
            }
        }, SPLASH_TIME_OUT);
    }

}

