package paragon.futuralabs.com.paragon.Activity;

import android.app.DatePickerDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;

import paragon.futuralabs.com.paragon.Adapters.RVeReceipt;
import paragon.futuralabs.com.paragon.R;
import paragon.futuralabs.com.paragon.Utilities.MyAlert;
import paragon.futuralabs.com.paragon.apiManager.PNApiResponseWrapper;
import paragon.futuralabs.com.paragon.apiManager.PNNetworkConstants;
import paragon.futuralabs.com.paragon.apiManager.PNNetworkManager;
import paragon.futuralabs.com.paragon.apiManager.SplitResponse;
import paragon.futuralabs.com.paragon.models.ERecieptList;
import paragon.futuralabs.com.paragon.models.ERecieptResponse;

import static paragon.futuralabs.com.paragon.Utilities.AppConstants.FROM;
import static paragon.futuralabs.com.paragon.Utilities.AppConstants.Mgrill;
import static paragon.futuralabs.com.paragon.Utilities.AppConstants.Salkara;
import static paragon.futuralabs.com.paragon.Utilities.ConnectionDetector.toast;
import static paragon.futuralabs.com.paragon.Utilities.Utils.getCurrentDate;
import static paragon.futuralabs.com.paragon.Utilities.Utils.getPreviousDate;
import static paragon.futuralabs.com.paragon.apiManager.PNNetworkManager.post;

public class PNeReceipt extends PNBaseActivity {

    LinearLayout linLayContainer;
    TextView tvHeading;
    ImageView ic_back_arrow;
    RecyclerView recyclerView;
    //TextView tvDateFrom, tvDateTo;
    SplitResponse splitResponse;
    ERecieptResponse eReceiptResponse;
    List<ERecieptList> eReceiptLists;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pne_receipt);
        init();
        handleEvents();
        getEReceipt();
        trackEvent("eReceipt", "View eReceipt");
    }

    @Override
    protected void onResume() {

        super.onResume();
    }

    private void handleEvents() {
        tvHeading.setText("eReceipt");
        ic_back_arrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

       /* tvDateFrom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setDate(tvDateFrom, PNeReceipt.this, "From");
            }
        });
        tvDateTo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!tvDateFrom.getText().toString().trim().equals(""))
                    setDate(tvDateTo, PNeReceipt.this, "To");
                else
                    toast(PNeReceipt.this, "Please select from date");
            }
        });*/

    }

    private void init() {
        linLayContainer = findViewById(R.id.linLayContainer);
        ic_back_arrow = findViewById(R.id.img_back_toolbar);
        tvHeading = findViewById(R.id.txt_title_toolbar);
        //tvDateFrom = findViewById(R.id.tvDateFrom);
        //tvDateTo = findViewById(R.id.tvDateTo);
        recyclerView = findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
    }

    public void getEReceipt() {
        /*if (tvDateFrom.getText().toString().trim().equals("")) {
            toast(PNeReceipt.this, "Plese select from date");
            return;
        }
        if (tvDateTo.getText().toString().trim().equals("")) {
            toast(PNeReceipt.this, "Plese select to date");
            return;
        }*/
        final String[] list = new String[0];
        eReceiptLists = new ArrayList(Arrays.asList(list));
        showProgress();
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("CustomerId", PNSP.getuserid());
            jsonObject.put("datefrom", getPreviousDate());
            jsonObject.put("dateto", getCurrentDate());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        post(PNeReceipt.this, PNNetworkConstants.GetTop5eRecipt, jsonObject.toString(), new PNNetworkManager.NetworkInterface() {
            @Override
            public void onResponse(PNApiResponseWrapper baseResponse) {
                dismissProgres();
                if (baseResponse.isSuccess()) {
                    splitResponse = new SplitResponse(baseResponse.getJsonObjectResponse());
                    if (splitResponse.getStatus().equals("1")) {
                        try {
                            Gson gson = new Gson();
                            eReceiptResponse = gson.fromJson(splitResponse.getResponse(), ERecieptResponse.class);
                            eReceiptLists.addAll(eReceiptResponse.getData());
                            if (eReceiptLists.size() > 0)
                                setData(eReceiptLists);
                            else {
                                MyAlert alert = new MyAlert();
                                alert.showAlertFinish(PNeReceipt.this, "No eReceipt found");
                                recyclerView.setAdapter(null);
                            }

                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    } else {
                        MyAlert alert = new MyAlert();
                        alert.showAlertFinish(PNeReceipt.this, "No eReceipt found");
                        recyclerView.setAdapter(null);
                    }
                } else {
                    toast(getApplicationContext(), baseResponse.getMessage());
                }

            }
        });


    }

    private void setData(List<ERecieptList> lists) {

        RVeReceipt adapter = new RVeReceipt(PNeReceipt.this, lists);
        recyclerView.setAdapter(adapter);
    }

    public void setDate(final View v, final Context context, final String title) {
        final Calendar mcurrentDate = Calendar.getInstance();
        int mYear = mcurrentDate.get(Calendar.YEAR);
        int mMonth = mcurrentDate.get(Calendar.MONTH);
        int mDay = mcurrentDate.get(Calendar.DAY_OF_MONTH);
        DatePickerDialog mDatePicker = new DatePickerDialog(context, R.style.TimePickerTheme,
                new DatePickerDialog.OnDateSetListener() {
                    public void onDateSet(DatePicker datepicker,
                                          int selectedyear, int selectedmonth, int selectedday) {
                        String da = selectedday < 10 ? "0" + Integer.toString(selectedday) : Integer.toString(selectedday);
                        String m = selectedmonth < 10 ? "0" + Integer.toString(selectedmonth + 1) : Integer.toString(selectedmonth + 1);
                        String y = Integer.toString(selectedyear);
                        ((TextView) v).setText(da + "-" + m + "-" + y);
                        if (title.equals("To")) {
                            getEReceipt();
                        }
                    }
                }, mYear, mMonth, mDay);
        mDatePicker.setTitle(title + "");
        mDatePicker.getDatePicker().setMaxDate(System.currentTimeMillis());
        mDatePicker.show();
    }

}

