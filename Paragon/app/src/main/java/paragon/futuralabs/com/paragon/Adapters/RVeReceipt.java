package paragon.futuralabs.com.paragon.Adapters;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import paragon.futuralabs.com.paragon.Activity.PNeReceiptDetails;
import paragon.futuralabs.com.paragon.R;
import paragon.futuralabs.com.paragon.models.ERecieptList;

import static paragon.futuralabs.com.paragon.Utilities.AppConstants.Item;

/**
 * Created by Shamir on 30-03-2018.
 */

public class RVeReceipt extends RecyclerView.Adapter<RVeReceipt.ViewHolder> {
    List<ERecieptList> lists;
    Context context;
    String from;

    public RVeReceipt(Context context,List<ERecieptList> lists) {
        this.context = context;
        this.lists = lists;

    }

    @Override
    public RVeReceipt.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.rv_ereceipt_item, parent, false);
        RVeReceipt.ViewHolder viewHolder = new RVeReceipt.ViewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RVeReceipt.ViewHolder holder, int position) {

        holder.tvName.setText(lists.get(position).getOutletsName());
        holder.tvBillNo.setText("Bill No : "+lists.get(position).getBillNo());
        holder.tvBillDate.setText("Bill Date : "+lists.get(position).getBillDate());
        holder.tvAmount.setText("Bill Amount : "+lists.get(position).getCurrency()+" "+lists.get(position).getTotalBillAmt());
        holder.tvDiscount.setText("Discount : "+lists.get(position).getDiscount()+"%");

    }

    @Override
    public int getItemCount() {
        return lists.size();

    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView tvName,tvBillNo,tvBillDate,tvAmount,tvDiscount;

        public ViewHolder(View itemView) {
            super(itemView);
            tvName =   itemView.findViewById(R.id.tvName);
            tvBillNo =   itemView.findViewById(R.id.tvBillNo);
            tvBillDate =   itemView.findViewById(R.id.tvBillDate);
            tvAmount =   itemView.findViewById(R.id.tvAmount);
            tvDiscount =   itemView.findViewById(R.id.tvDiscount);

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            Intent intent=new Intent(context, PNeReceiptDetails.class);
            Bundle bundle=new Bundle();
            bundle.putSerializable(Item,lists.get(getPosition()));
            intent.putExtras(bundle);
            context.startActivity(intent);
        }
    }
}

