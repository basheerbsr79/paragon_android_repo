package paragon.futuralabs.com.paragon.Adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import paragon.futuralabs.com.paragon.Activity.PNSavedAddress;
import paragon.futuralabs.com.paragon.Activity.paragon.DashBoardActivity;
import paragon.futuralabs.com.paragon.R;
import paragon.futuralabs.com.paragon.models.AddressList;
import paragon.futuralabs.com.paragon.models.BrandList;
import paragon.futuralabs.com.paragon.paragonhelper.ParagonSP;

import static paragon.futuralabs.com.paragon.Utilities.AppConstants.BrandId;

/**
 * Created by Shamir on 16-03-2018.
 */

public class RVAddress extends RecyclerView.Adapter<RVAddress.ViewHolder> {
    List<AddressList> addressLists;
    Context context;
    int pos;

    public RVAddress(Context context, List<AddressList> addressLists,int pos) {
        this.context = context;
        this.addressLists=addressLists;
        this.pos=pos;

    }

    @Override
    public RVAddress.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.rv_address_item, parent, false);
        RVAddress.ViewHolder viewHolder = new RVAddress.ViewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RVAddress.ViewHolder holder, int position) {

        String address=addressLists.get(position).getAddress();
        if (!addressLists.get(position).getLandMark().trim().equals("")){
            address=address+","+addressLists.get(position).getLandMark().trim();
        }
        if (!addressLists.get(position).getPostCode().trim().equals("")){
            address=address+","+addressLists.get(position).getPostCode().trim();
        }
        holder.rbAddress.setText(address);
        if (pos==position){
            holder.rbAddress.setChecked(true);
            PNSavedAddress.addressPos=position;
        }else {
            holder.rbAddress.setChecked(false);
        }


    }

    @Override
    public int getItemCount() {
        return addressLists.size();

    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        RadioButton rbAddress;

        public ViewHolder(View itemView) {
            super(itemView);
            rbAddress =   itemView.findViewById(R.id.tvaddress);

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {

            ((PNSavedAddress)context).setData(addressLists,getPosition());

        }
    }
}

