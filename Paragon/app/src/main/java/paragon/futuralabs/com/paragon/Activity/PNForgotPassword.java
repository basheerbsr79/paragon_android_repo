package paragon.futuralabs.com.paragon.Activity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import org.json.JSONException;
import org.json.JSONObject;

import paragon.futuralabs.com.paragon.R;
import paragon.futuralabs.com.paragon.Utilities.ConnectionDetector;
import paragon.futuralabs.com.paragon.Utilities.MyAlert;
import paragon.futuralabs.com.paragon.apiManager.PNApiResponseWrapper;
import paragon.futuralabs.com.paragon.apiManager.PNNetworkConstants;
import paragon.futuralabs.com.paragon.apiManager.PNNetworkManager;
import paragon.futuralabs.com.paragon.apiManager.SplitResponse;

import static paragon.futuralabs.com.paragon.Utilities.Utils.isValidEmail;
import static paragon.futuralabs.com.paragon.Utilities.Utils.toast;
import static paragon.futuralabs.com.paragon.Utilities.Utils.validateEditText;
import static paragon.futuralabs.com.paragon.apiManager.PNNetworkManager.post;

public class PNForgotPassword extends PNBaseActivity {

    ImageView imvBack;
    EditText edEmail,edPhone;
    Button btnSubmit;
    SplitResponse splitResponse;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pnforgot_password);

        init();
        handleEvents();
    }

    private void handleEvents() {
        imvBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isValidEmail(edEmail.getText().toString().trim())) {
                    forgotPassword();
                } else if (validateEditText(PNForgotPassword.this,edPhone.getText().toString().trim())){
                    forgotPasswordPhone();
                }else {
                    toast(PNForgotPassword.this, "Enter valid email or Phone No");
                }
            }
        });
    }

    private void init() {
        imvBack =   findViewById(R.id.imvBack);
        edEmail =   findViewById(R.id.edEmail);
        edPhone=findViewById(R.id.edPhone);
        btnSubmit =   findViewById(R.id.btnSubmit);
    }

    public void forgotPassword() {
        showProgress();
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("Email", edEmail.getText().toString().trim());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        post(PNForgotPassword.this, PNNetworkConstants.CustomerForgotPassword, jsonObject.toString(), new PNNetworkManager.NetworkInterface() {
            @Override
            public void onResponse(PNApiResponseWrapper baseResponse) {
                dismissProgres();
                if (baseResponse.isSuccess()) {
                    splitResponse = new SplitResponse(baseResponse.getJsonObjectResponse());
                    if (splitResponse.getStatus().equals("1")) {
                        try {
                            MyAlert alert = new MyAlert();
                            alert.showAlertFinish(PNForgotPassword.this, "Password send to your email.");

                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    } else {
                        MyAlert alert = new MyAlert();
                        alert.showAlert(PNForgotPassword.this,  splitResponse.getMessage());
                    }
                } else {
                    ConnectionDetector.toast(getApplicationContext(), baseResponse.getMessage());
                }

            }
        });


    }

    public void forgotPasswordPhone() {
        showProgress();
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("Phone", edPhone.getText().toString().trim());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        post(PNForgotPassword.this, PNNetworkConstants.CustomerForgotPasswordPhone, jsonObject.toString(), new PNNetworkManager.NetworkInterface() {
            @Override
            public void onResponse(PNApiResponseWrapper baseResponse) {
                dismissProgres();
                if (baseResponse.isSuccess()) {
                    splitResponse = new SplitResponse(baseResponse.getJsonObjectResponse());
                    if (splitResponse.getStatus().equals("1")) {
                        try {
                            MyAlert alert = new MyAlert();
                            alert.showAlertFinish(PNForgotPassword.this, "Password send as sms.");

                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    } else {
                        MyAlert alert = new MyAlert();
                        alert.showAlert(PNForgotPassword.this,  splitResponse.getMessage());
                    }
                } else {
                    ConnectionDetector.toast(getApplicationContext(), baseResponse.getMessage());
                }

            }
        });


    }
}
