package paragon.futuralabs.com.paragon.Adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import paragon.futuralabs.com.paragon.R;
import paragon.futuralabs.com.paragon.models.FavLocationList;
import paragon.futuralabs.com.paragon.models.FavRestList;

/**
 * Created by Shamir on 30-03-2018.
 */

public class RVFavLocations extends RecyclerView.Adapter<RVFavLocations.ViewHolder> {
    List<FavLocationList> lists;
    Context context;

    public RVFavLocations(Context context,List<FavLocationList> lists) {
        this.context = context;
        this.lists = lists;

    }

    @Override
    public RVFavLocations.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.rv_fav_loc_item, parent, false);
        RVFavLocations.ViewHolder viewHolder = new RVFavLocations.ViewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RVFavLocations.ViewHolder holder, int position) {

        holder.tvLocation.setText("Location : "+lists.get(position).getLocation());
        holder.tvCity.setText("City : "+lists.get(position).getCity());

    }

    @Override
    public int getItemCount() {
        return lists.size();

    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView tvLocation,tvCity;

        public ViewHolder(View itemView) {
            super(itemView);
            tvLocation =   itemView.findViewById(R.id.tvLocation);
            tvCity =   itemView.findViewById(R.id.tvCity);
        }

    }
}


