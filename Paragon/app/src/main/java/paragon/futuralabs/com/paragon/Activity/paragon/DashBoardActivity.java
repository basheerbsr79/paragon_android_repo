package paragon.futuralabs.com.paragon.Activity.paragon;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import paragon.futuralabs.com.paragon.Activity.MapLocation;
import paragon.futuralabs.com.paragon.Activity.PNBaseActivity;
import paragon.futuralabs.com.paragon.Activity.PNCart;
import paragon.futuralabs.com.paragon.Activity.PNChangePassword;
import paragon.futuralabs.com.paragon.Activity.PNEditProfile;
import paragon.futuralabs.com.paragon.Activity.PNFAQ;
import paragon.futuralabs.com.paragon.Activity.PNFavLocations;
import paragon.futuralabs.com.paragon.Activity.PNFavoriteDishes;
import paragon.futuralabs.com.paragon.Activity.PNFavoriteRestaurants;
import paragon.futuralabs.com.paragon.Activity.PNRecentOrders;
import paragon.futuralabs.com.paragon.Activity.PNTop5Bills;
import paragon.futuralabs.com.paragon.Activity.PNVoucher;
import paragon.futuralabs.com.paragon.Activity.SelectResturantActivity;
import paragon.futuralabs.com.paragon.Activity.SigninActivity;
import paragon.futuralabs.com.paragon.Fragements.FragmentDrawer;
import paragon.futuralabs.com.paragon.Fragements.GridFragment;
import paragon.futuralabs.com.paragon.R;
import paragon.futuralabs.com.paragon.apiManager.PNApiResponseWrapper;
import paragon.futuralabs.com.paragon.apiManager.PNNetworkConstants;
import paragon.futuralabs.com.paragon.apiManager.PNNetworkManager;
import paragon.futuralabs.com.paragon.apiManager.SplitResponse;
import paragon.futuralabs.com.paragon.models.LocationResponse;
import paragon.futuralabs.com.paragon.paragonhelper.ParagonSP;

import static paragon.futuralabs.com.paragon.Activity.MapLocation.lat;
import static paragon.futuralabs.com.paragon.Activity.MapLocation.lon;
import static paragon.futuralabs.com.paragon.Utilities.AppConstants.BrandName;
import static paragon.futuralabs.com.paragon.Utilities.AppConstants.FROM;
import static paragon.futuralabs.com.paragon.Utilities.AppConstants.Paragon;
import static paragon.futuralabs.com.paragon.Utilities.ConnectionDetector.toast;
import static paragon.futuralabs.com.paragon.apiManager.PNNetworkManager.post;


public class DashBoardActivity extends PNBaseActivity implements FragmentDrawer.FragmentDrawerListener {
    public static Toolbar mToolbar;
    private FragmentDrawer drawerFragment;
    ImageView imvCart;
    Context context;
    ParagonSP sp;
    String brandId = "";
    SplitResponse splitResponse;
    LocationResponse locationResponse;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dash_board);

        //setting context
        context = DashBoardActivity.this;
        sp = new ParagonSP(getApplicationContext());
        // sp.setcurrentactivity("1");
        mToolbar = findViewById(R.id.toolbar);
        imvCart = findViewById(R.id.imvCart);
        imvCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(DashBoardActivity.this, PNCart.class);
                startActivity(intent);
            }
        });

        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        drawerFragment = (FragmentDrawer)
                getSupportFragmentManager().findFragmentById(R.id.fragment_navigation_drawer);
        drawerFragment.setUp(R.id.fragment_navigation_drawer, (DrawerLayout) findViewById(R.id.drawer_layout), mToolbar);
        drawerFragment.setDrawerListener(this);
        displayView(11);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        // getMenuInflater().inflate(R.menu.menue_dashboard, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            Toast.makeText(getBaseContext(), "clicked settings", Toast.LENGTH_LONG).show();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onDrawerItemSelected(View view, int position) {
        displayView(position);
    }

    private void displayView(int position) {
        Fragment fragment = null;
        String title = getString(R.string.app_name);
        Intent intent = null;
        switch (position) {
            case 0:
               /* fragment = new GridFragment();
                title = getString(R.string.app_name);*/
                intent = new Intent(DashBoardActivity.this, SelectResturantActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);

                break;
            case 1:
                intent = new Intent(DashBoardActivity.this, PNVoucher.class);
                break;
            case 2:
                intent = new Intent(DashBoardActivity.this, PNFavoriteDishes.class);
                break;
            case 3:
                intent = new Intent(DashBoardActivity.this, PNTop5Bills.class);
                break;
            case 4:
                intent = new Intent(DashBoardActivity.this, PNRecentOrders.class);
                break;
            case 5:
                if (!PNSP.getOutlet().equals("")) {
                    getLocation();
                } else {
                    toast(DashBoardActivity.this, "No Outlets");
                }
                break;
            case 6:
                intent = new Intent(DashBoardActivity.this, PNFAQ.class);
                break;
            case 7:
                intent = new Intent(DashBoardActivity.this, PNEditProfile.class);
                break;
            case 8:
                intent = new Intent(DashBoardActivity.this, PNChangePassword.class);
                break;
            case 9:
                intent = new Intent(DashBoardActivity.this, SigninActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                sp.setlogin("");
                break;
            case 10:
                intent = new Intent(DashBoardActivity.this, AppinfoParagonActivity.class);
                break;
            case 11:
                fragment = new GridFragment();
                title = getIntent().getStringExtra(BrandName);
                break;
            default:
                title = getString(R.string.app_name);
                break;
        }

        if (intent != null)
            startActivity(intent);

        if (fragment != null) {
            FragmentManager fragmentManager = getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.container_body, fragment);
            fragmentTransaction.commit();

            // set the toolbar title
            getSupportActionBar().setTitle(PNSP.getname().toUpperCase());

        }
    }

    public void getLocation() {
        showProgress();
        JSONObject jsonObject = new JSONObject();

        try {
            jsonObject.put("OutletId", sp.getOutlet());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        post(this, PNNetworkConstants.BindLocationDetails, jsonObject.toString(), new PNNetworkManager.NetworkInterface() {
            @Override
            public void onResponse(PNApiResponseWrapper baseResponse) {
                dismissProgres();
                if (baseResponse.isSuccess()) {
                    splitResponse = new SplitResponse(baseResponse.getJsonObjectResponse());
                    if (splitResponse.getStatus().equals("1")) {
                        Gson gson = new Gson();
                        locationResponse = gson.fromJson(splitResponse.getResponse(), LocationResponse.class);
                        /*Intent in = new Intent(DashBoardActivity.this, MapActivity.class);
                        in.putExtra("lat", locationResponse.getData().get(0).getLatitude() + "");
                        in.putExtra("log", locationResponse.getData().get(0).getLongitude() + "");
                        startActivity(in);*/

                        Intent in = new Intent(DashBoardActivity.this, MapLocation.class);
                        in.putExtra(lat, locationResponse.getData().get(0).getLatitude() + "");
                        in.putExtra(lon, locationResponse.getData().get(0).getLongitude() + "");
                        startActivity(in);


                    } else {
                        toast(DashBoardActivity.this, splitResponse.getMessage());
                    }
                } else {
                    toast(DashBoardActivity.this, baseResponse.getMessage());
                }

            }
        });

    }
}

