package paragon.futuralabs.com.paragon.Activity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import paragon.futuralabs.com.paragon.Adapters.RVHD;
import paragon.futuralabs.com.paragon.Adapters.RVHDMenuItem;
import paragon.futuralabs.com.paragon.Adapters.RVMenuItem;
import paragon.futuralabs.com.paragon.R;
import paragon.futuralabs.com.paragon.apiManager.PNApiResponseWrapper;
import paragon.futuralabs.com.paragon.apiManager.PNNetworkConstants;
import paragon.futuralabs.com.paragon.apiManager.PNNetworkManager;
import paragon.futuralabs.com.paragon.apiManager.SplitResponse;
import paragon.futuralabs.com.paragon.models.CartList;
import paragon.futuralabs.com.paragon.models.CartResponse;
import paragon.futuralabs.com.paragon.models.MenuCatagoryList;
import paragon.futuralabs.com.paragon.models.MenuItemsList;
import paragon.futuralabs.com.paragon.models.MenuItemsResponse;
import paragon.futuralabs.com.paragon.models.homedelivery.HDList;
import paragon.futuralabs.com.paragon.models.homedelivery.HDResponse;

import static paragon.futuralabs.com.paragon.Utilities.AppConstants.CatId;
import static paragon.futuralabs.com.paragon.Utilities.AppConstants.CatName;
import static paragon.futuralabs.com.paragon.Utilities.AppConstants.FROM;
import static paragon.futuralabs.com.paragon.Utilities.AppConstants.HOMEDELIVERY;
import static paragon.futuralabs.com.paragon.Utilities.AppConstants.Item;
import static paragon.futuralabs.com.paragon.Utilities.AppConstants.Mgrill;
import static paragon.futuralabs.com.paragon.Utilities.AppConstants.Salkara;
import static paragon.futuralabs.com.paragon.Utilities.ConnectionDetector.toast;
import static paragon.futuralabs.com.paragon.apiManager.PNNetworkManager.post;
import android.support.v7.graphics.Palette;

public class PNHDMenuItems extends PNBaseActivity {

    RecyclerView recyclerView;
   // ImageView imvImage;
    TextView tvHeading;
    ImageView ic_back_arrow;
    TextView tvDesc,tvCount,tvPrice;
    LinearLayout linLayCart;
    SplitResponse splitResponse;
    HDResponse hdResponse;
    List<HDList> hdLists;
    CartResponse cartResponse;
    List<CartList> cartLists;
    public static int posView=0;

    //CollapsingToolbarLayout collapsingToolbarLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pnhdmenu_items);

        init();
        handleEvents();
        getMenuItems();
        trackEvent("Order Online","View Order Online");
    }

    @Override
    protected void onResume() {
        super.onResume();
        getCartIteams();
    }

    private void handleEvents() {
//setToolBar();
        tvHeading.setText("Order Online");
        ic_back_arrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        linLayCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(PNHDMenuItems.this,PNCart.class);
                startActivity(intent);
            }
        });
    }

    private void init() {
        ic_back_arrow= findViewById(R.id.img_back_toolbar);
        tvHeading= findViewById(R.id.txt_title_toolbar);
        //imvImage= findViewById(R.id.imvImage);
        tvDesc= findViewById(R.id.tvDesc);
        recyclerView= findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setNestedScrollingEnabled(true);
        linLayCart= findViewById(R.id.linLayCart);
        tvCount= findViewById(R.id.tvCount);
        tvPrice= findViewById(R.id.tvPrice);

    }

    /*private void setToolBar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);

        toolbar.setNavigationIcon(R.drawable.ic_back_arrow);
        toolbar.setTitle("");

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               onBackPressed();
            }
        });

        collapsingToolbarLayout = (CollapsingToolbarLayout) findViewById(R.id.toolbar_layout);
       collapsingToolbarLayout.setTitle(" ");
        dynamicToolbarColor();
        toolbarTextAppernce();
    }

    private void dynamicToolbarColor() {

        Bitmap bitmap = BitmapFactory.decodeResource(getResources(),
                R.drawable.ic_back_arrow);
        Palette.from(bitmap).generate(new Palette.PaletteAsyncListener() {

            @Override
            public void onGenerated(Palette palette) {
                collapsingToolbarLayout.setContentScrimColor(palette.getMutedColor(getResources().getColor(R.color.mask)));
                collapsingToolbarLayout.setStatusBarScrimColor(palette.getMutedColor(getResources().getColor(R.color.mask)));
            }
        });
    }

    private void toolbarTextAppernce() {
        collapsingToolbarLayout.setCollapsedTitleTextAppearance(R.style.collapsedappbar);
        collapsingToolbarLayout.setExpandedTitleTextAppearance(R.style.expandedappbar);
    }*/


    public void getMenuItems(){
        final String[] list=new String[0];
        hdLists=new ArrayList(Arrays.asList(list));
        showProgress();
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("OutletId", PNSP.getOutlet());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        post(PNHDMenuItems.this, PNNetworkConstants.BindMenuCategoryForHomeDelivery, jsonObject.toString(), new PNNetworkManager.NetworkInterface() {
            @Override
            public void onResponse(PNApiResponseWrapper baseResponse) {
                dismissProgres();
                if (baseResponse.isSuccess()) {
                    splitResponse = new SplitResponse(baseResponse.getJsonObjectResponse());
                    if (splitResponse.getStatus().equals("1")) {
                        try {
                            Gson gson = new Gson();
                            hdResponse = gson.fromJson(splitResponse.getResponse(), HDResponse.class);
                            hdLists.addAll(hdResponse.getData());
                            if (hdLists.size()>0)
                                setData(hdLists);
                            else
                                toast(getApplicationContext(),"No items found");

                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    } else {
                        toast(getApplicationContext(), splitResponse.getMessage());
                    }
                } else {
                    toast(getApplicationContext(), baseResponse.getMessage());
                }

            }
        });


    }

    private void setData(List<HDList> hdLists) {
        /*String url=hdLists.get(0).getCategoryImg();
        Picasso.with(PNHDMenuItems.this)
                .load(url)
                .placeholder(R.drawable.menu_item_placeholder)
                .error(R.drawable.menu_item_placeholder)
                .into(imvImage);
        collapsingToolbarLayout.setTitle(" ");*/
        RVHD rvMenuItem=new RVHD(PNHDMenuItems.this,hdLists,getIntent().getStringExtra(CatId));
        recyclerView.setAdapter(rvMenuItem);
        recyclerView.scrollToPosition(posView);
    }

    public void getCartIteams(){
        final String[] list=new String[0];
        cartLists=new ArrayList(Arrays.asList(list));
        showProgress();
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("CustomerId", PNSP.getuserid());
            jsonObject.put("OutletId", PNSP.getOutlet());

        } catch (JSONException e) {
            e.printStackTrace();
        }
        post(PNHDMenuItems.this, PNNetworkConstants.BindCartItems, jsonObject.toString(), new PNNetworkManager.NetworkInterface() {
            @Override
            public void onResponse(PNApiResponseWrapper baseResponse) {
                dismissProgres();
                if (baseResponse.isSuccess()) {
                    splitResponse = new SplitResponse(baseResponse.getJsonObjectResponse());
                    if (splitResponse.getStatus().equals("1")) {
                        try {
                            Gson gson = new Gson();
                            cartResponse = gson.fromJson(splitResponse.getResponse(), CartResponse.class);
                            cartLists.addAll(cartResponse.getData());
                            if (cartLists.size()>0)
                                setCartData(cartLists);
                            else
                                linLayCart.setVisibility(View.GONE);

                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    } else {
                        linLayCart.setVisibility(View.GONE);
                    }
                } else {
                    toast(getApplicationContext(), baseResponse.getMessage());
                }

            }
        });


    }

    private void setCartData(List<CartList> cartLists) {
        linLayCart.setVisibility(View.VISIBLE);
        double tot=0;
        long qty=0;
        for (int i = 0; i < cartLists.size(); i++) {
            tot=tot+(Double.parseDouble(cartLists.get(i).getPrice())*cartLists.get(i).getQuantity());
            qty=qty+cartLists.get(i).getQuantity();
        }
        if (qty==1) {
            tvCount.setText("1 item in cart");
        }else {
            tvCount.setText(qty+" items in cart");
        }
        tvPrice.setText(cartLists.get(0).getCurrency()+" "+tot);

    }
}

