package paragon.futuralabs.com.paragon.Activity;

import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import paragon.futuralabs.com.paragon.Activity.paragon.DashBoardActivity;
import paragon.futuralabs.com.paragon.R;
import paragon.futuralabs.com.paragon.apiManager.PNApiResponseWrapper;
import paragon.futuralabs.com.paragon.apiManager.PNNetworkConstants;
import paragon.futuralabs.com.paragon.apiManager.PNNetworkManager;
import paragon.futuralabs.com.paragon.apiManager.SplitResponse;
import paragon.futuralabs.com.paragon.models.MenuItemsList;
import paragon.futuralabs.com.paragon.models.brandwithpromotions.Promotion;
import paragon.futuralabs.com.paragon.paragonhelper.ParagonSP;

import static paragon.futuralabs.com.paragon.Utilities.AppConstants.BrandId;
import static paragon.futuralabs.com.paragon.Utilities.AppConstants.BrandName;
import static paragon.futuralabs.com.paragon.Utilities.AppConstants.FROM;
import static paragon.futuralabs.com.paragon.Utilities.AppConstants.Item;
import static paragon.futuralabs.com.paragon.Utilities.AppConstants.Mgrill;
import static paragon.futuralabs.com.paragon.Utilities.AppConstants.Salkara;
import static paragon.futuralabs.com.paragon.Utilities.ConnectionDetector.toast;
import static paragon.futuralabs.com.paragon.apiManager.PNNetworkManager.post;

public class PNPromotionDetails extends PNBaseActivity {

    LinearLayout linLayContainer;
    TextView tvHeading;
    ImageView ic_back_arrow;
    Promotion promotion;
    ImageView imageView;
    TextView tvOutlet,tvPromotion,tvDescription,tvDate,tvTime,tvSDate;
    SplitResponse splitResponse;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pnpromotion_details);
        init();
        handleEvents();
        updateAsSeen();
        trackEvent("Promotion","View Promotion Details");
    }

    private void handleEvents() {
        tvHeading.setText("Promotion Details");
        ic_back_arrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        String url=promotion.getImage();
        Picasso.with(PNPromotionDetails.this)
                .load(url)
                .error(R.drawable.menu_item_placeholder)
                .placeholder(R.drawable.menu_item_placeholder)
                .into(imageView);
        tvOutlet.setText(promotion.getOutletName());
        tvPromotion.setText(promotion.getPromotionName());
        tvDescription.setText(promotion.getDescription());
        tvDate.setText("Expires on : "+promotion.getEndDate());
        tvSDate.setText("Begins from : "+promotion.getStartDate());
        tvTime.setText(promotion.getPrmoType());

    }

    private void init() {
        linLayContainer= findViewById(R.id.linLayContainer);
        ic_back_arrow= findViewById(R.id.img_back_toolbar);
        tvHeading= findViewById(R.id.txt_title_toolbar);
        imageView=  findViewById(R.id.imageView);
        tvOutlet=  findViewById(R.id.tvOutlet);
        tvPromotion=  findViewById(R.id.tvPromotion);
        tvDescription=  findViewById(R.id.tvDescription);
        tvDate=  findViewById(R.id.tvDate);
        tvTime=  findViewById(R.id.tvTime);
        tvSDate=findViewById(R.id.tvSDate);
        Intent intent = this.getIntent();
        Bundle bundle = intent.getExtras();
        promotion = (Promotion) bundle.getSerializable(Item);

        Typeface face= Typeface.createFromAsset(getAssets(), "fonts/PT_Serif-Caption-Web-Italic.ttf");
        tvOutlet.setTypeface(face);
        tvPromotion.setTypeface(face);
        tvDescription.setTypeface(face);
    }

    private void updateAsSeen() {
        showProgress();
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("CustomerId", PNSP.getuserid());
            jsonObject.put("PromotionId",promotion.getPromotionId());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        post(PNPromotionDetails.this, PNNetworkConstants.SaveCustomerPromotion, jsonObject.toString(), new PNNetworkManager.NetworkInterface() {
            @Override
            public void onResponse(PNApiResponseWrapper baseResponse) {
                dismissProgres();
                if (baseResponse.isSuccess()) {
                    splitResponse = new SplitResponse(baseResponse.getJsonObjectResponse());
                    if (splitResponse.getStatus().equals("1")) {

                    }
                } else {
                    //toast(context, baseResponse.getMessage());
                }

            }
        });
    }
}
