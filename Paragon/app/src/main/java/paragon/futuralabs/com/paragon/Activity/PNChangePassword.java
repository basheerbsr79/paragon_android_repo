package paragon.futuralabs.com.paragon.Activity;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import org.json.JSONException;
import org.json.JSONObject;

import paragon.futuralabs.com.paragon.R;
import paragon.futuralabs.com.paragon.Utilities.ConnectionDetector;
import paragon.futuralabs.com.paragon.Utilities.MyAlert;
import paragon.futuralabs.com.paragon.apiManager.PNApiResponseWrapper;
import paragon.futuralabs.com.paragon.apiManager.PNNetworkConstants;
import paragon.futuralabs.com.paragon.apiManager.PNNetworkManager;
import paragon.futuralabs.com.paragon.apiManager.SplitResponse;

import static paragon.futuralabs.com.paragon.Activity.RegisterActivity.listItems;
import static paragon.futuralabs.com.paragon.Utilities.Utils.setDateToEditText;
import static paragon.futuralabs.com.paragon.apiManager.PNNetworkManager.post;

public class PNChangePassword extends PNBaseActivity {
    Context context;
    ImageView ivBack;
    EditText edOldPwd,edNewPwd,edConPwd;
    Button btnUpdate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pnchange_password);

        init();
        handleEvents();
    }

    public void handleEvents() {
        ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        btnUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (validate()) {
                    changePassword();
                }
            }
        });
    }

    public void init() {
        context=PNChangePassword.this;
        ivBack=findViewById(R.id.ivBack);
        edOldPwd=findViewById(R.id.edOldPwd);
        edNewPwd=findViewById(R.id.edNewPwd);
        edConPwd=findViewById(R.id.edConPwd);
        btnUpdate=findViewById(R.id.btnUpdate);
    }

    public boolean validate(){
        MyAlert alert = new MyAlert();
        if (edOldPwd.getText().toString().toString().trim().equals("")) {
            alert.showAlert(context, "Enter Old Password");
            return false;
        }
        if (edNewPwd.getText().toString().toString().trim().equals("")) {
            alert.showAlert(context, "Enter New Password");
            return false;
        }
        if (edConPwd.getText().toString().toString().trim().equals("")) {
            alert.showAlert(context, "Enter Confirm Password");
            return false;
        }
        if (!edConPwd.getText().toString().toString().trim().equals(edNewPwd.getText().toString().toString().trim())) {
            alert.showAlert(context, "Password mismach.Please check");
            return false;
        }

        return true;
    }

    public void changePassword() {
        showProgress();
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("CustomerId", PNSP.getuserid());
            jsonObject.put("OldPassword", edOldPwd.getText().toString().trim());
            jsonObject.put("NewPassword", edNewPwd.getText().toString().trim());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        post(PNChangePassword.this, PNNetworkConstants.CustomerChagePassword, jsonObject.toString(), new PNNetworkManager.NetworkInterface() {
            @Override
            public void onResponse(PNApiResponseWrapper baseResponse) {
                dismissProgres();
                if (baseResponse.isSuccess()) {
                    splitResponse = new SplitResponse(baseResponse.getJsonObjectResponse());
                    if (splitResponse.getStatus().equals("1")) {
                        try {
                            MyAlert alert = new MyAlert();
                            alert.showAlertFinish(PNChangePassword.this, splitResponse.getMessage());

                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    } else {
                        MyAlert alert = new MyAlert();
                        alert.showAlert(PNChangePassword.this,  splitResponse.getMessage());
                    }
                } else {
                    ConnectionDetector.toast(getApplicationContext(), baseResponse.getMessage());
                }

            }
        });


    }
}
