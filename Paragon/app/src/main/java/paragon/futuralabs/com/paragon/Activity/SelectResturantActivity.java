package paragon.futuralabs.com.paragon.Activity;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;
import org.jsoup.Jsoup;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import paragon.futuralabs.com.paragon.Adapters.RVBrandHome;
import paragon.futuralabs.com.paragon.R;
import paragon.futuralabs.com.paragon.Utilities.PNCustomAlertDialog;
import paragon.futuralabs.com.paragon.apiManager.PNApiResponseWrapper;
import paragon.futuralabs.com.paragon.apiManager.PNNetworkConstants;
import paragon.futuralabs.com.paragon.apiManager.PNNetworkManager;
import paragon.futuralabs.com.paragon.apiManager.SplitResponse;
import paragon.futuralabs.com.paragon.models.AppVersionResponse;
import paragon.futuralabs.com.paragon.models.brandwithpromotions.BrandList;
import paragon.futuralabs.com.paragon.models.brandwithpromotions.BrandResponse;
import paragon.futuralabs.com.paragon.paragonhelper.ParagonSP;

import static paragon.futuralabs.com.paragon.Utilities.ConnectionDetector.toast;
import static paragon.futuralabs.com.paragon.apiManager.PNNetworkManager.post;

public class SelectResturantActivity extends PNBaseActivity {
    Context context;
    ParagonSP sp;
    RecyclerView rvBrand;
    TextView textView;
    RVBrandHome brandHomeAdapter;
    SplitResponse splitResponse;
    BrandResponse brandsResponse;
    List<BrandList> brandLists;
    AppVersionResponse appVersionResponse;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_resturant);
        //setting context
        context = SelectResturantActivity.this;
        //used for animation
        sp = new ParagonSP(getApplicationContext());

        textView = findViewById(R.id.textView);
        textView.setText("WELCOME " + PNSP.getname().toUpperCase());
       /* textView.setSelected(true);
        Animation a = AnimationUtils.loadAnimation(this, R.anim.textanim);
        a.reset();
        textView.clearAnimation();
        textView.startAnimation(a);*/
        rvBrand = findViewById(R.id.rvBrand);
        rvBrand.setLayoutManager(new LinearLayoutManager(this));
        trackEvent("Select Restaurant", "View Select Restaurant");

        getVersion();

    }

    @Override
    protected void onResume() {
        super.onResume();
        getBrands();
        //RunAnimation();
    }

    public void getBrands() {
        final String[] list = new String[0];
        brandLists = new ArrayList(Arrays.asList(list));
        showProgress();
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("CustomerId", PNSP.getuserid());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        post(SelectResturantActivity.this, PNNetworkConstants.BindBrandsWithCustomer, jsonObject.toString(), new PNNetworkManager.NetworkInterface() {
            @Override
            public void onResponse(PNApiResponseWrapper baseResponse) {
                dismissProgres();
                if (baseResponse.isSuccess()) {
                    splitResponse = new SplitResponse(baseResponse.getJsonObjectResponse());
                    if (splitResponse.getStatus().equals("1")) {
                        Gson gson = new Gson();
                        brandsResponse = gson.fromJson(splitResponse.getResponse(), BrandResponse.class);
                        brandLists.addAll(brandsResponse.getData());
                        brandHomeAdapter = new RVBrandHome(SelectResturantActivity.this, brandLists);
                        rvBrand.setAdapter(brandHomeAdapter);

                    } else {
                        toast(getApplicationContext(), splitResponse.getMessage());
                    }
                } else {
                    toast(getApplicationContext(), baseResponse.getMessage());
                }

            }
        });

    }

    private void RunAnimation() {
        Animation a = AnimationUtils.loadAnimation(this, R.anim.textanim);
        a.reset();
        textView.clearAnimation();
        textView.startAnimation(a);
    }

    public void getVersion() {
        showProgress();
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("AppType", 1);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        post(SelectResturantActivity.this, PNNetworkConstants.BindAppVersionById, jsonObject.toString(), new PNNetworkManager.NetworkInterface() {
            @Override
            public void onResponse(PNApiResponseWrapper baseResponse) {
                dismissProgres();
                if (baseResponse.isSuccess()) {
                    splitResponse = new SplitResponse(baseResponse.getJsonObjectResponse());
                    if (splitResponse.getStatus().equals("1")) {
                        String currentVersion = "";
                        try {
                            currentVersion = getPackageManager().getPackageInfo(getPackageName(), 0).versionName;
                        } catch (PackageManager.NameNotFoundException e) {
                            e.printStackTrace();
                        }
                        Gson gson = new Gson();
                        appVersionResponse = gson.fromJson(splitResponse.getResponse(), AppVersionResponse.class);
                        if (appVersionResponse.getData().size() > 0) {
                            Log.i("IvaVersions","currentVersion "+currentVersion+" appVersion "+appVersionResponse.getData().get(0).getVersionName());
                            if (!currentVersion.equalsIgnoreCase(appVersionResponse.getData().get(0).getVersionName())) {
                                alertUpdate();
                            }
                        }


                    } else {
                        toast(getApplicationContext(), splitResponse.getMessage());
                    }
                } else {
                    toast(getApplicationContext(), baseResponse.getMessage());
                }

            }
        });

    }

    public void alertUpdate() {

        final PNCustomAlertDialog dialog = new PNCustomAlertDialog(context, "", "New version available in playstore. Please update to access new features.");
        dialog.show();
        dialog.btncancel.setVisibility(View.VISIBLE);
        dialog.btnOk.setText("Update");
        dialog.btncancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                final String appPackageName = getPackageName(); // getPackageName() from Context or Activity object
                try {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                } catch (android.content.ActivityNotFoundException anfe) {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
                }
            }
        });
    }

}
