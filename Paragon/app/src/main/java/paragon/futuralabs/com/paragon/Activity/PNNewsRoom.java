package paragon.futuralabs.com.paragon.Activity;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import paragon.futuralabs.com.paragon.Adapters.RVNewsRoom;
import paragon.futuralabs.com.paragon.R;
import paragon.futuralabs.com.paragon.Utilities.MyAlert;
import paragon.futuralabs.com.paragon.apiManager.PNApiResponseWrapper;
import paragon.futuralabs.com.paragon.apiManager.PNNetworkConstants;
import paragon.futuralabs.com.paragon.apiManager.PNNetworkManager;
import paragon.futuralabs.com.paragon.apiManager.SplitResponse;
import paragon.futuralabs.com.paragon.models.IamComingResponse;
import paragon.futuralabs.com.paragon.models.news.Datum;
import paragon.futuralabs.com.paragon.models.news.NewsResponse;

import static paragon.futuralabs.com.paragon.Utilities.ConnectionDetector.toast;
import static paragon.futuralabs.com.paragon.apiManager.PNNetworkManager.post;

public class PNNewsRoom extends PNBaseActivity {

    LinearLayout linLayContainer;
    TextView tvHeading;
    ImageView ic_back_arrow;
    RecyclerView recyclerView;
    SplitResponse splitResponse;
    NewsResponse newsResponse;
    List<Datum> newsRoomLists;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pnnews_room);

        init();
        handleEvents();
        trackEvent("News Room","View News Room");
    }

    @Override
    protected void onResume() {
        getNews();
        super.onResume();
    }

    private void handleEvents() {
        tvHeading.setText("News Room");
        ic_back_arrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

    }

    private void init() {
        linLayContainer= findViewById(R.id.linLayContainer);
        ic_back_arrow= findViewById(R.id.img_back_toolbar);
        tvHeading= findViewById(R.id.txt_title_toolbar);
        recyclerView= findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
    }

    public void getNews(){
        final String[] list=new String[0];
        newsRoomLists=new ArrayList(Arrays.asList(list));
        showProgress();
        final JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("OutletId", PNSP.getOutlet());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        post(this, PNNetworkConstants.BindNews, jsonObject.toString(), new PNNetworkManager.NetworkInterface() {
            @Override
            public void onResponse(PNApiResponseWrapper baseResponse) {
                dismissProgres();
                if (baseResponse.isSuccess()) {
                    splitResponse = new SplitResponse(baseResponse.getJsonObjectResponse());
                    if (splitResponse.getStatus().equals("1")) {
                        Gson gson = new Gson();
                        newsResponse = gson.fromJson(splitResponse.getResponse(), NewsResponse.class);
                        newsRoomLists.addAll(newsResponse.getData());
                        if (newsRoomLists.size() > 0)
                            setData(newsRoomLists);
                        else {
                            MyAlert alert = new MyAlert();
                            alert.showAlertFinish(PNNewsRoom.this, "No news found");
                        }
                    }else {
                        MyAlert alert = new MyAlert();
                        alert.showAlertFinish(PNNewsRoom.this, "No news found");
                    }
                } else {
                    toast(getApplicationContext(), baseResponse.getMessage());
                }

            }
        });


    }

    private void setData(List<Datum> list) {

        RVNewsRoom rvNewsRoom=new RVNewsRoom(PNNewsRoom.this,list);
        recyclerView.setAdapter(rvNewsRoom);
    }

}
