package paragon.futuralabs.com.paragon.Activity;

import android.app.ActionBar;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.BitmapDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.XML;
import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.PropertyInfo;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapPrimitive;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import paragon.futuralabs.com.paragon.Adapters.RVAddress;
import paragon.futuralabs.com.paragon.R;
import paragon.futuralabs.com.paragon.Utilities.MyAlert;
import paragon.futuralabs.com.paragon.Utilities.PNCustomAlertDialog;
import paragon.futuralabs.com.paragon.apiManager.PNApiResponseWrapper;
import paragon.futuralabs.com.paragon.apiManager.PNNetworkConstants;
import paragon.futuralabs.com.paragon.apiManager.PNNetworkManager;
import paragon.futuralabs.com.paragon.apiManager.SplitResponse;
import paragon.futuralabs.com.paragon.models.AddressList;
import paragon.futuralabs.com.paragon.models.AddressResponse;
import paragon.futuralabs.com.paragon.models.AreaList;
import paragon.futuralabs.com.paragon.models.AreaResponse;
import paragon.futuralabs.com.paragon.models.CartList;
import paragon.futuralabs.com.paragon.models.CartResponse;
import paragon.futuralabs.com.paragon.models.soap.SoapOrderResponse;

import static paragon.futuralabs.com.paragon.Utilities.AppConstants.Currency;
import static paragon.futuralabs.com.paragon.Utilities.AppConstants.Price;
import static paragon.futuralabs.com.paragon.Utilities.ConnectionDetector.toast;
import static paragon.futuralabs.com.paragon.Utilities.Utils.getDate;
import static paragon.futuralabs.com.paragon.apiManager.PNNetworkManager.post;

public class PNSavedAddress extends PNBaseActivity {

    LinearLayout linLayContainer;
    TextView tvHeading, tvAddAddress, tvDelivers, tvTotal, tvDeliveryCharge, tvPayableAmount;
    ImageView ic_back_arrow;
    RecyclerView recyclerView;
    EditText edSpecial;
    Button btnOrder;
    CheckBox cbTakeAway, cbHomeDelivery;
    EditText edArea;
    LinearLayout llChooseArea, llTotal, llDeliveryCharge;
    SplitResponse splitResponse;
    AddressResponse addressResponse;
    List<AddressList> addressList;
    AreaResponse areaResponse;
    List<AreaList> areaLists;
    String mnimumDeliveryCharge = "0";
    public static int addressPos = -1;

    private final String NAMESPACE = "http://tempuri.org/";
    private final String URL = "http://103.35.198.137/PARAGONSERVICE/Service.asmx";
    private final String SOAP_ACTION = "http://tempuri.org/Save_Online_KOT";
    private final String METHOD_NAME = "Save_Online_KOT";

    CartResponse cartResponse;
    List<CartList> cartLists;

    SoapOrderResponse soapOrderResponse;
    String orderRefId = "";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pnsaved_address);

        init();
        handleEvents();
        trackEvent("Saved Address", "View Saved Address");
    }

    @Override
    protected void onResume() {
        super.onResume();
        getAddress();
        getCartItems();
        getAreaDetails();
    }

    private void handleEvents() {
        tvHeading.setText("Saved Address");
        ic_back_arrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        tvAddAddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(PNSavedAddress.this, PNAddAddress.class);
                startActivity(intent);
            }
        });

        edArea.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (areaLists.size() > 0) {
                    showDropDownList(PNSavedAddress.this, edArea, edArea.getWidth());
                } else {
                    toast(PNSavedAddress.this, "No area found");
                }
            }
        });

        btnOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (PNSP.getIsOnlineOrdering()) {
                    if (!cbTakeAway.isChecked()) {
                        if (addressPos == -1) {
                            toast(PNSavedAddress.this, "Please select an address");
                        } else if (edArea.getText().toString().equals("")) {
                            toast(PNSavedAddress.this, "Choose an Area");
                        } else {
                            if (!cbTakeAway.isChecked()) {
                                if (Double.parseDouble(tvTotal.getText().toString().trim()) < Double.parseDouble(mnimumDeliveryCharge)) {
                                    final PNCustomAlertDialog dialog = new PNCustomAlertDialog(PNSavedAddress.this, "", "Minimum order amount must be " + mnimumDeliveryCharge +
                                            ". If you continue you will be charged " + mnimumDeliveryCharge + ".");
                                    dialog.show();
                                    dialog.btnOk.setText("Cancel");
                                    dialog.btncancel.setVisibility(View.VISIBLE);
                                    dialog.btncancel.setText("Continue");
                                    dialog.btnOk.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            dialog.dismiss();


                                        }
                                    });
                                    dialog.btncancel.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            dialog.dismiss();
                                            saveOrder();
                                        }
                                    });

                                } else {
                                    saveOrder();
                                }
                            }
                        }
                    } else {
                        saveOrder();
                    }
                } else {
                    if (!cbTakeAway.isChecked()) {
                        toast(PNSavedAddress.this, "Please select take away option");
                    } else {
                        saveOrder();
                    }
                }
            }
        });
        cbTakeAway.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    llChooseArea.setVisibility(View.GONE);
                    llTotal.setVisibility(View.GONE);
                    //llDeliveryCharge.setVisibility(View.GONE);
                    tvAddAddress.setVisibility(View.GONE);
                    tvDelivers.setVisibility(View.GONE);
                    recyclerView.setVisibility(View.GONE);
                    cbHomeDelivery.setChecked(false);
                } else {
                    if (PNSP.getIsOnlineOrdering()) {
                        llChooseArea.setVisibility(View.VISIBLE);
                        llTotal.setVisibility(View.GONE);
                        //llDeliveryCharge.setVisibility(View.VISIBLE);
                        tvAddAddress.setVisibility(View.VISIBLE);
                        tvDelivers.setVisibility(View.VISIBLE);
                        recyclerView.setVisibility(View.VISIBLE);
                    }
                }
            }
        });

        cbHomeDelivery.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    cbTakeAway.setChecked(false);
                    if (PNSP.getIsOnlineOrdering()) {
                        llChooseArea.setVisibility(View.VISIBLE);
                        llTotal.setVisibility(View.GONE);
                        //llDeliveryCharge.setVisibility(View.VISIBLE);
                        tvAddAddress.setVisibility(View.VISIBLE);
                        tvDelivers.setVisibility(View.VISIBLE);
                        recyclerView.setVisibility(View.VISIBLE);
                    }
                } else {

                }
            }
        });

        if (PNSP.getIsTakeawy())
            cbTakeAway.setVisibility(View.VISIBLE);
        else
            cbTakeAway.setVisibility(View.GONE);

        if (PNSP.getIsHomeDelivery()) {
            cbHomeDelivery.setVisibility(View.VISIBLE);
            cbHomeDelivery.setChecked(true);
            llChooseArea.setVisibility(View.VISIBLE);
            llTotal.setVisibility(View.GONE);
            //llDeliveryCharge.setVisibility(View.VISIBLE);
            tvAddAddress.setVisibility(View.VISIBLE);
            tvDelivers.setVisibility(View.VISIBLE);
            recyclerView.setVisibility(View.VISIBLE);
            tvTotal.setText(getIntent().getStringExtra(Price));
            tvPayableAmount.setText(getIntent().getStringExtra(Currency) + " " + getIntent().getStringExtra(Price));
        } else {
            cbHomeDelivery.setVisibility(View.GONE);
            cbTakeAway.setChecked(true);
            tvPayableAmount.setText(getIntent().getStringExtra(Currency) + " " + getIntent().getStringExtra(Price));
            llChooseArea.setVisibility(View.GONE);
            llTotal.setVisibility(View.GONE);
            //llDeliveryCharge.setVisibility(View.GONE);
            tvAddAddress.setVisibility(View.GONE);
            tvDelivers.setVisibility(View.GONE);
            recyclerView.setVisibility(View.GONE);
        }
    }

    private void init() {
        linLayContainer = findViewById(R.id.linLayContainer);
        ic_back_arrow = findViewById(R.id.img_back_toolbar);
        tvHeading = findViewById(R.id.txt_title_toolbar);
        tvAddAddress = findViewById(R.id.tvAddAddress);
        tvDelivers = findViewById(R.id.tvDelivers);
        cbTakeAway = findViewById(R.id.cbTakeAway);
        cbHomeDelivery = findViewById(R.id.cbHomeDelivery);
        llChooseArea = findViewById(R.id.llChooseArea);
        llTotal = findViewById(R.id.llTotal);
        edSpecial=findViewById(R.id.edSpecial);
        llDeliveryCharge = findViewById(R.id.llDeliveryCharge);
        tvTotal = findViewById(R.id.tvTotal);
        tvDeliveryCharge = findViewById(R.id.tvDeliveryCharge);
        tvPayableAmount = findViewById(R.id.tvPayableAmount);
        edArea = findViewById(R.id.edArea);
        btnOrder = findViewById(R.id.btnOrder);
        recyclerView = findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
    }

    public void getAddress() {
        final String[] list = new String[0];
        addressList = new ArrayList(Arrays.asList(list));
        showProgress();
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("CustomerId", PNSP.getuserid());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        post(PNSavedAddress.this, PNNetworkConstants.GetAllDeliveryAddress, jsonObject.toString(), new PNNetworkManager.NetworkInterface() {
            @Override
            public void onResponse(PNApiResponseWrapper baseResponse) {
                dismissProgres();
                if (baseResponse.isSuccess()) {
                    splitResponse = new SplitResponse(baseResponse.getJsonObjectResponse());
                    if (splitResponse.getStatus().equals("1")) {
                        try {
                            Gson gson = new Gson();
                            addressResponse = gson.fromJson(splitResponse.getResponse(), AddressResponse.class);
                            addressList.addAll(addressResponse.getData());
                            if (addressList.size() > 0)
                                setData(addressList, -1);
                            else {
                                toast(getApplicationContext(), "No address found");
                            }

                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    } else {
                        toast(getApplicationContext(), splitResponse.getMessage());
                    }
                } else {
                    toast(getApplicationContext(), baseResponse.getMessage());
                }

            }
        });


    }

    public void setData(List<AddressList> addressList, int pos) {
        addressPos = -1;
        RVAddress rvAddress = new RVAddress(PNSavedAddress.this, addressList, pos);
        recyclerView.setAdapter(rvAddress);
    }

    public void saveOrder() {
        showProgress();
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("CustomerId", PNSP.getuserid());
            jsonObject.put("OutletId", PNSP.getOutlet());
            long ADID = 0;
            if (!cbTakeAway.isChecked())
                ADID = addressList.get(addressPos).getDeliveryAddressId();
            jsonObject.put("DeliveryAddressId", ADID + "");
            jsonObject.put("Discount", "0");
            int DS = 1;
            if (cbTakeAway.isChecked())
                DS = 2;
            jsonObject.put("DeilveryStatus", DS + "");
            jsonObject.put("ShippingCharge", mnimumDeliveryCharge);
            jsonObject.put("SpecialInstruction", edSpecial.getText().toString().trim());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        post(PNSavedAddress.this, PNNetworkConstants.SaveHomeDeliveryOrder, jsonObject.toString(), new PNNetworkManager.NetworkInterface() {
            @Override
            public void onResponse(PNApiResponseWrapper baseResponse) {
                dismissProgres();
                if (baseResponse.isSuccess()) {
                    splitResponse = new SplitResponse(baseResponse.getJsonObjectResponse());
                    if (splitResponse.getStatus().equals("1")) {
                        try {
                            if (PNSP.getOutlet().equals("14")) {
                                orderRefId = splitResponse.getOrderId();
                                new AsyncSOAP().execute();
                            } else {
                                orderRefId = splitResponse.getOrderId();
                                MyAlert alert = new MyAlert();
                                alert.showAlertHome(PNSavedAddress.this, "Thank you, your order is awaiting for confirmation, order number is " + orderRefId + ". Enjoy your meal and share  your feedback on the app.");
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }


                    } else {
                        toast(getApplicationContext(), splitResponse.getMessage());
                    }
                } else {
                    toast(getApplicationContext(), baseResponse.getMessage());
                }

            }
        });
    }

    private class AsyncSOAP extends AsyncTask<String, String, String> {

        @Override
        protected void onPreExecute() {
            showProgress();
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... strings) {
            saveSOAPOrder();
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            dismissProgres();
            MyAlert alert = new MyAlert();
            if (soapOrderResponse.getHotsoft().getResponse().getResponseMsg().contains("Order has been generated")) {
                alert.showAlertHome(PNSavedAddress.this, "Thank you, your order is awaiting for confirmation, order number is " + orderRefId + ". Enjoy your meal and share  your feedback on the app.");
            }

            super.onPostExecute(s);
        }
    }

    private void saveSOAPOrder() {

        JSONObject jsonString = getJson();


        //Create request
        SoapObject Request = new SoapObject(NAMESPACE, METHOD_NAME);
        PropertyInfo weightProp = new PropertyInfo();


        weightProp.setName("nOutletId");
        weightProp.setValue(14);
        weightProp.setType(Integer.class);
        Request.addProperty(weightProp);

        PropertyInfo weightProp3 = new PropertyInfo();


        weightProp3.setName("Takeaway");
        if (!cbTakeAway.isChecked())
            weightProp3.setValue(0);
        else weightProp3.setValue(1);
        weightProp3.setType(Integer.class);
        Request.addProperty(weightProp3);

        PropertyInfo weightProp1 = new PropertyInfo();
        weightProp1.setName("cJsonString");
        weightProp1.setValue(jsonString.toString());
        weightProp1.setType(String.class);
        Request.addProperty(weightProp1);

        PropertyInfo weightProp2 = new PropertyInfo();
        weightProp2.setName("cAuthkey");
        weightProp2.setValue("123");
        weightProp2.setType(String.class);
        Request.addProperty(weightProp2);

        //Create envelope
        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
                SoapEnvelope.VER11);
        envelope.dotNet = true;
        //Set output SOAP object
        envelope.setOutputSoapObject(Request);
        //Create HTTP call object
        HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);

        try {
            //Invole web service
            androidHttpTransport.call(SOAP_ACTION, envelope);
            //Get the response
            SoapPrimitive response = (SoapPrimitive) envelope.getResponse();
            // Log.i("RESPONSEEE", response.toString() + "");
            JSONObject jsonObj = null;
            try {
                try {
                    jsonObj = XML.toJSONObject(response.toString());
                    Gson gson = new Gson();
                    soapOrderResponse = gson.fromJson(jsonObj.toString(), SoapOrderResponse.class);
                } catch (JSONException e) {
                    Log.e("JSON exception", e.getMessage());
                    e.printStackTrace();
                }

            } catch (Exception e) {
                Log.e("exception", e.getMessage());
                e.printStackTrace();
            }
            Log.d("RESPONSEEE", jsonObj.toString());

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private JSONObject getJson() {

        JSONObject jsonObject = new JSONObject();

        JSONArray jsonArray1 = new JSONArray();
        JSONArray jsonArray2 = new JSONArray();
        JSONArray jsonArray3 = new JSONArray();
        JSONObject jb1 = new JSONObject();
        JSONObject jb2 = new JSONObject();

        try {
            jb1.put("ONREFFNO", orderRefId);
            jb1.put("ONREMARK", edSpecial.getText().toString().trim());
            jb1.put("TotalOrderCount", splitResponse.getTotalOrderCount());
            jb1.put("TotalCancelledCount", splitResponse.getTotalCancelledCount());
            jb1.put("ORDERDATE", getDate());
            jb1.put("LOCCODE", "5");
            jb1.put("DELIVERY_DATE", getDate());
            jsonArray1.put(jb1);
            jsonObject.put("ORDERHD", jsonArray1);
            jb2.put("NAME", PNSP.getname());
            String adrs = "", lm = "", pc = "";
            if (!cbTakeAway.isChecked()) {
                adrs = addressList.get(addressPos).getAddress();
                lm = addressList.get(addressPos).getLandMark();
                pc = "PC " + addressList.get(addressPos).getPostCode();
            }
            jb2.put("ADD1", adrs);
            jb2.put("ADD2", pc);
            jb2.put("ADD3", "");
            jb2.put("PHONE", PNSP.getphone());
            jb2.put("EMAIL", PNSP.getemail());
            jb2.put("CLANDMARK", lm);
            jsonArray2.put(jb2);
            jsonObject.put("GUESTDTL", jsonArray2);

            for (int i = 0; i < cartLists.size(); i++) {
                JSONObject jb3 = new JSONObject();
                jb3.put("ITEMCODE", cartLists.get(i).getPOSID());
                jb3.put("QUANTITY", cartLists.get(i).getQuantity());
                jb3.put("SLNO", (i + 1) + "");
                jb3.put("RATE", cartLists.get(i).getPrice());
                jb3.put("CDESC", "");
                jsonArray3.put(jb3);
            }
            jsonObject.put("ITEMDETAILS", jsonArray3);

             Log.i("JsonStringIva",jsonObject.toString());


        } catch (JSONException e) {
            e.printStackTrace();
        }

        return jsonObject;
    }

    public void getCartItems() {
        final String[] list = new String[0];
        cartLists = new ArrayList(Arrays.asList(list));
        showProgress();
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("CustomerId", PNSP.getuserid());
            jsonObject.put("OutletId", PNSP.getOutlet());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        post(PNSavedAddress.this, PNNetworkConstants.BindCartItems, jsonObject.toString(), new PNNetworkManager.NetworkInterface() {
            @Override
            public void onResponse(PNApiResponseWrapper baseResponse) {
                dismissProgres();
                if (baseResponse.isSuccess()) {
                    splitResponse = new SplitResponse(baseResponse.getJsonObjectResponse());
                    if (splitResponse.getStatus().equals("1")) {
                        try {
                            Gson gson = new Gson();
                            cartResponse = gson.fromJson(splitResponse.getResponse(), CartResponse.class);
                            cartLists.addAll(cartResponse.getData());

                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    } else {
                        //toast(getApplicationContext(), splitResponse.getMessage());
                    }
                } else {
                    //toast(getApplicationContext(), baseResponse.getMessage());
                }

            }
        });


    }

    public void getAreaDetails() {
        final String[] list = new String[0];
        areaLists = new ArrayList(Arrays.asList(list));
        showProgress();
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("OutletId", PNSP.getOutlet());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        post(PNSavedAddress.this, PNNetworkConstants.BindOrderAreaDetails, jsonObject.toString(), new PNNetworkManager.NetworkInterface() {
            @Override
            public void onResponse(PNApiResponseWrapper baseResponse) {
                dismissProgres();
                if (baseResponse.isSuccess()) {
                    splitResponse = new SplitResponse(baseResponse.getJsonObjectResponse());
                    if (splitResponse.getStatus().equals("1")) {
                        try {
                            Gson gson = new Gson();
                            areaResponse = gson.fromJson(splitResponse.getResponse(), AreaResponse.class);
                            areaLists.addAll(areaResponse.getData());

                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    } else {
                        //toast(getApplicationContext(), splitResponse.getMessage());
                    }
                } else {
                    //toast(getApplicationContext(), baseResponse.getMessage());
                }

            }
        });


    }

    public void showDropDownList(final Context context, final EditText editText, int width) {
        String[] arrayList = new String[areaLists.size()];
        for (int i = 0; i < areaLists.size(); i++) {
            arrayList[i] = areaLists.get(i).getAreaName();
        }
        final PopupWindow pwindo;
        LayoutInflater inflater = (LayoutInflater) context.getApplicationContext()
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View layout = inflater.inflate(R.layout.custom_listview,
                null);
        pwindo = new PopupWindow(layout, ActionBar.LayoutParams.WRAP_CONTENT, ActionBar.LayoutParams.WRAP_CONTENT, true);
        pwindo.setWidth(width);
        pwindo.setBackgroundDrawable(new BitmapDrawable());
        pwindo.setFocusable(true);
        pwindo.showAsDropDown(editText, 0, 0);
        pwindo.setOutsideTouchable(true);
        pwindo.showAtLocation(layout, Gravity.CENTER, 0, 0);
        final ListView ll = (ListView) pwindo.getContentView().findViewById(R.id.lst_custom);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(context, android.R.layout.simple_list_item_1, arrayList);
        ll.setAdapter(adapter);
        ll.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                editText.setText(parent.getItemAtPosition(position).toString());
                mnimumDeliveryCharge = areaLists.get(position).getMinimumOrderAmount();
                tvDeliveryCharge.setText(mnimumDeliveryCharge);
                //double totAmt=Double.parseDouble(tvTotal.getText().toString())+Double.parseDouble(mnimumDeliveryCharge);
                //tvPayableAmount.setText(getIntent().getStringExtra(Currency)+" "+totAmt);

                pwindo.dismiss();

            }
        });

    }

}
