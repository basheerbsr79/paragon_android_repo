package paragon.futuralabs.com.paragon.apiManager;

import org.json.JSONObject;

/**
 * Created by Shamir on 19-10-2017.
 */

public class PNApiResponseWrapper {
    public PNApiResponseWrapper(JSONObject jsonObjectResponse, boolean success, String message) {
        this.jsonObjectResponse = jsonObjectResponse;
        this.success = success;
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    private String message;
    private JSONObject jsonObjectResponse;
    private boolean success;

    public JSONObject getJsonObjectResponse() {
        return jsonObjectResponse;
    }

    public void setJsonObjectResponse(JSONObject jsonObjectResponse) {
        this.jsonObjectResponse = jsonObjectResponse;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }
}

