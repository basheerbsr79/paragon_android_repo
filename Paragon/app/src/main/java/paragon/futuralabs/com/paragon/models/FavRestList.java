
package paragon.futuralabs.com.paragon.models;

import com.google.gson.annotations.SerializedName;
@SuppressWarnings("unused")
public class FavRestList {

    @SerializedName("Address")
    private String mAddress;
    @SerializedName("CustomerCareNumber")
    private String mCustomerCareNumber;
    @SerializedName("Email")
    private String mEmail;
    @SerializedName("OutletsName")
    private String mOutletsName;
    @SerializedName("Phone")
    private String mPhone;

    public String getAddress() {
        return mAddress;
    }

    public void setAddress(String Address) {
        mAddress = Address;
    }

    public String getCustomerCareNumber() {
        return mCustomerCareNumber;
    }

    public void setCustomerCareNumber(String CustomerCareNumber) {
        mCustomerCareNumber = CustomerCareNumber;
    }

    public String getEmail() {
        return mEmail;
    }

    public void setEmail(String Email) {
        mEmail = Email;
    }

    public String getOutletsName() {
        return mOutletsName;
    }

    public void setOutletsName(String OutletsName) {
        mOutletsName = OutletsName;
    }

    public String getPhone() {
        return mPhone;
    }

    public void setPhone(String Phone) {
        mPhone = Phone;
    }

}
