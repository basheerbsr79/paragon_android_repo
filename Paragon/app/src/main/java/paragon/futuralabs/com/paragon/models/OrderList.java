
package paragon.futuralabs.com.paragon.models;

import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class OrderList {

    @SerializedName("Currency")
    private String mCurrency;
    @SerializedName("CustomerId")
    private Long mCustomerId;
    @SerializedName("Discount")
    private Long mDiscount;
    @SerializedName("name")
    private String mName;
    @SerializedName("OrderAmt")
    private Double mOrderAmt;
    @SerializedName("OrderDate")
    private String mOrderDate;
    @SerializedName("OrderId")
    private Long mOrderId;
    @SerializedName("OutletsName")
    private String mOutletsName;
    @SerializedName("OrderStatus")
    private String mOrderStatus;

    public String getCurrency() {
        return mCurrency;
    }

    public void setCurrency(String Currency) {
        mCurrency = Currency;
    }

    public Long getCustomerId() {
        return mCustomerId;
    }

    public void setCustomerId(Long CustomerId) {
        mCustomerId = CustomerId;
    }

    public Long getDiscount() {
        return mDiscount;
    }

    public void setDiscount(Long Discount) {
        mDiscount = Discount;
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        mName = name;
    }

    public Double getOrderAmt() {
        return mOrderAmt;
    }

    public void setOrderAmt(Double OrderAmt) {
        mOrderAmt = OrderAmt;
    }

    public String getOrderDate() {
        return mOrderDate;
    }

    public void setOrderDate(String OrderDate) {
        mOrderDate = OrderDate;
    }

    public Long getOrderId() {
        return mOrderId;
    }

    public void setOrderId(Long OrderId) {
        mOrderId = OrderId;
    }

    public String getOutletsName() {
        return mOutletsName;
    }

    public void setOutletsName(String OutletsName) {
        mOutletsName = OutletsName;
    }

    public String getOrderStatus() {
        return mOrderStatus;
    }

    public void setOrderStatus(String OrderStatus) {
        mOrderStatus = OrderStatus;
    }

}
