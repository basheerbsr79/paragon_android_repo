
package paragon.futuralabs.com.paragon.models;

import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class CartList {

    @SerializedName("CartId")
    private Long mCartId;
    @SerializedName("CustomerId")
    private Long mCustomerId;
    @SerializedName("DishName")
    private String mDishName;
    @SerializedName("Image")
    private String mImage;
    @SerializedName("MenuCode")
    private String mMenuCode;
    @SerializedName("MenuItemsId")
    private Long mMenuItemsId;
    @SerializedName("Price")
    private String mPrice;
    @SerializedName("Quantity")
    private Long mQuantity;
    @SerializedName("Currency")
    private String mCurrency;
    @SerializedName("POSID")
    private String mPOSID;

    public Long getCartId() {
        return mCartId;
    }

    public void setCartId(Long CartId) {
        mCartId = CartId;
    }

    public Long getCustomerId() {
        return mCustomerId;
    }

    public void setCustomerId(Long CustomerId) {
        mCustomerId = CustomerId;
    }

    public String getDishName() {
        return mDishName;
    }

    public void setDishName(String DishName) {
        mDishName = DishName;
    }

    public String getImage() {
        return mImage;
    }

    public void setImage(String Image) {
        mImage = Image;
    }

    public String getMenuCode() {
        return mMenuCode;
    }

    public void setMenuCode(String MenuCode) {
        mMenuCode = MenuCode;
    }

    public Long getMenuItemsId() {
        return mMenuItemsId;
    }

    public void setMenuItemsId(Long MenuItemsId) {
        mMenuItemsId = MenuItemsId;
    }

    public String getPrice() {
        return mPrice;
    }

    public void setPrice(String Price) {
        mPrice = Price;
    }

    public Long getQuantity() {
        return mQuantity;
    }

    public void setQuantity(Long Quantity) {
        mQuantity = Quantity;
    }

    public String getCurrency() {
        return mCurrency;
    }

    public void setCurrency(String Currency) {
        mCurrency = Currency;
    }

    public String getPOSID() {
        return mPOSID;
    }

    public void setPOSID(String POSID) {
        mPOSID = POSID;
    }

}
