
package paragon.futuralabs.com.paragon.models.news;

import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class CommentsList {

    @SerializedName("AddedDate")
    private String mAddedDate;
    @SerializedName("Comments")
    private String mComments;
    @SerializedName("CustomerName")
    private String mCustomerName;

    public String getAddedDate() {
        return mAddedDate;
    }

    public void setAddedDate(String AddedDate) {
        mAddedDate = AddedDate;
    }

    public String getComments() {
        return mComments;
    }

    public void setComments(String Comments) {
        mComments = Comments;
    }

    public String getCustomerName() {
        return mCustomerName;
    }

    public void setCustomerName(String CustomerName) {
        mCustomerName = CustomerName;
    }

}
