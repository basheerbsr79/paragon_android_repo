
package paragon.futuralabs.com.paragon.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

@SuppressWarnings("unused")
public class MenuItemsList implements Serializable {

    @SerializedName("Availability")
    private String mAvailableTime;
    @SerializedName("Currency")
    private String mCurrency;
    @SerializedName("Description")
    private String mDescription;
    @SerializedName("DishName")
    private String mDishName;
    @SerializedName("Image")
    private String mImage;
    @SerializedName("IsContainNuts")
    private Boolean mIsContainNuts;
    @SerializedName("IsRecommended")
    private Boolean mIsRecommended;
    @SerializedName("MenuItemsId")
    private Long mMenuItemsId;
    @SerializedName("POSID")
    private String mPOSID;
    @SerializedName("Price")
    private String mPrice;
    @SerializedName("VegOrNonVeg")
    private int mVegOrNonVeg;

    @SerializedName("IsFavorite")
    private Long mIsFavorite;

    @SerializedName("IsHomeDelivery")
    private Boolean mIsHomeDelivery;
    @SerializedName("IsOnlineOrdering")
    private Boolean mIsOnlineOrdering;
    @SerializedName("IsTakeawy")
    private Boolean mIsTakeawy;

    public String getAvailableTime() {
        return mAvailableTime;
    }

    public void setAvailableTime(String AvailableTime) {
        mAvailableTime = AvailableTime;
    }

    public String getCurrency() {
        return mCurrency;
    }

    public void setCurrency(String Currency) {
        mCurrency = Currency;
    }

    public String getDescription() {
        return mDescription;
    }

    public void setDescription(String Description) {
        mDescription = Description;
    }

    public String getDishName() {
        return mDishName;
    }

    public void setDishName(String DishName) {
        mDishName = DishName;
    }

    public String getImage() {
        return mImage;
    }

    public void setImage(String Image) {
        mImage = Image;
    }

    public Boolean getIsContainNuts() {
        return mIsContainNuts;
    }

    public void setIsContainNuts(Boolean IsContainNuts) {
        mIsContainNuts = IsContainNuts;
    }

    public Boolean getIsRecommended() {
        return mIsRecommended;
    }

    public void setIsRecommended(Boolean IsRecommended) {
        mIsRecommended = IsRecommended;
    }

    public Long getMenuItemsId() {
        return mMenuItemsId;
    }

    public void setMenuItemsId(Long MenuItemsId) {
        mMenuItemsId = MenuItemsId;
    }

    public String getPOSID() {
        return mPOSID;
    }

    public void setPOSID(String POSID) {
        mPOSID = POSID;
    }

    public String getPrice() {
        return mPrice;
    }

    public void setPrice(String Price) {
        mPrice = Price;
    }

    public int getVegOrNonVeg() {
        return mVegOrNonVeg;
    }

    public void setVegOrNonVeg(int VegOrNonVeg) {
        mVegOrNonVeg = VegOrNonVeg;
    }

    public Long getIsFavorite() {
        return mIsFavorite;
    }

    public void setIsFavorite(Long IsFavorite) {
        mIsFavorite = IsFavorite;
    }

    public Boolean getIsHomeDelivery() {
        return mIsHomeDelivery;
    }

    public void setIsHomeDelivery(Boolean IsHomeDelivery) {
        mIsHomeDelivery = IsHomeDelivery;
    }

    public Boolean getIsOnlineOrdering() {
        return mIsOnlineOrdering;
    }

    public void setIsOnlineOrdering(Boolean IsOnlineOrdering) {
        mIsOnlineOrdering = IsOnlineOrdering;
    }

    public Boolean getIsTakeawy() {
        return mIsTakeawy;
    }

    public void setIsTakeawy(Boolean IsTakeawy) {
        mIsTakeawy = IsTakeawy;
    }
}
