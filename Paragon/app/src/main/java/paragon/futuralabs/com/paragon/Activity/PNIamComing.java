package paragon.futuralabs.com.paragon.Activity;

import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import paragon.futuralabs.com.paragon.Adapters.RVIamComing;
import paragon.futuralabs.com.paragon.Adapters.RVTop5Bills;
import paragon.futuralabs.com.paragon.R;
import paragon.futuralabs.com.paragon.apiManager.PNApiResponseWrapper;
import paragon.futuralabs.com.paragon.apiManager.PNNetworkConstants;
import paragon.futuralabs.com.paragon.apiManager.PNNetworkManager;
import paragon.futuralabs.com.paragon.apiManager.SplitResponse;
import paragon.futuralabs.com.paragon.models.IamComingList;
import paragon.futuralabs.com.paragon.models.IamComingResponse;
import paragon.futuralabs.com.paragon.models.Top5BillList;
import paragon.futuralabs.com.paragon.models.Top5BillResponse;

import static paragon.futuralabs.com.paragon.Utilities.AppConstants.FROM;
import static paragon.futuralabs.com.paragon.Utilities.AppConstants.Mgrill;
import static paragon.futuralabs.com.paragon.Utilities.AppConstants.Paragon;
import static paragon.futuralabs.com.paragon.Utilities.AppConstants.Salkara;
import static paragon.futuralabs.com.paragon.Utilities.ConnectionDetector.toast;
import static paragon.futuralabs.com.paragon.apiManager.PNNetworkManager.post;

public class PNIamComing extends PNBaseActivity {

    LinearLayout linLayContainer;
    TextView tvHeading;
    ImageView ic_back_arrow;
    RecyclerView recyclerView;
    Button fbtn;
    SwipeRefreshLayout swiperefresh;

    IamComingResponse iamComingResponse;
    List<IamComingList> iamComingLists;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pniam_coming_list);

        init();
        handleEvents();
    }

    @Override
    protected void onResume() {
        getIamComingList(true);
        super.onResume();
    }

    private void handleEvents() {
        trackEvent("I Am Coming","View I Am Coming");
        tvHeading.setText("I am coming");
        ic_back_arrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        fbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(PNIamComing.this,PNIamComingAdd.class);
                intent.putExtra(FROM, Paragon);
                startActivity(intent);
            }
        });

        swiperefresh.setOnRefreshListener(
                new SwipeRefreshLayout.OnRefreshListener() {
                    @Override
                    public void onRefresh() {
                        getIamComingList(false);
                    }
                }
        );
    }

    private void init() {
        swiperefresh=findViewById(R.id.swiperefresh);
        linLayContainer= findViewById(R.id.linLayContainer);
        ic_back_arrow= findViewById(R.id.img_back_toolbar);
        tvHeading=findViewById(R.id.txt_title_toolbar);
        recyclerView= findViewById(R.id.recyclerView);
        fbtn=  findViewById(R.id.fbtn);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
    }

    public void getIamComingList(final boolean isShow){
        final String[] list=new String[0];
        iamComingLists=new ArrayList(Arrays.asList(list));
        if (isShow)
        showProgress();
        swiperefresh.setRefreshing(false);
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("OutletId", PNSP.getOutlet());
            jsonObject.put("CustomerId", PNSP.getuserid());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        post(PNIamComing.this, PNNetworkConstants.BindImComingWithCustomer, jsonObject.toString(), new PNNetworkManager.NetworkInterface() {
            @Override
            public void onResponse(PNApiResponseWrapper baseResponse) {
                if (isShow)
                dismissProgres();
                if (baseResponse.isSuccess()) {
                    splitResponse = new SplitResponse(baseResponse.getJsonObjectResponse());
                    if (splitResponse.getStatus().equals("1")) {
                        try {
                            Gson gson = new Gson();
                            iamComingResponse = gson.fromJson(splitResponse.getResponse(), IamComingResponse.class);
                            iamComingLists.addAll(iamComingResponse.getData());
                            if (iamComingLists.size()>0)
                                setData(iamComingLists);
                            else {
                                recyclerView.setAdapter(null);
                                //toast(getApplicationContext(), "No data found");
                            }

                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    } else {
                        recyclerView.setAdapter(null);
                        //toast(getApplicationContext(), splitResponse.getMessage());
                    }
                } else {
                    toast(getApplicationContext(), baseResponse.getMessage());
                }

            }
        });


    }

    public void setData(List<IamComingList> lists) {
        RVIamComing adapter=new RVIamComing(PNIamComing.this,lists);
        recyclerView.setAdapter(adapter);
    }
}
