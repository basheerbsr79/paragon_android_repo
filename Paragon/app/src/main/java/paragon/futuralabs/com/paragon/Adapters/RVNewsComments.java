package paragon.futuralabs.com.paragon.Adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import paragon.futuralabs.com.paragon.R;
import paragon.futuralabs.com.paragon.models.news.CommentsList;

/**
 * Created by Shamir on 02-08-2018.
 */

public class RVNewsComments extends RecyclerView.Adapter<RVNewsComments.ViewHolder> {
    List<CommentsList> list;
    Context context;

    public RVNewsComments(Context context, List<CommentsList> list) {
        this.context = context;
        this.list = list;

    }

    @Override
    public RVNewsComments.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.rv_news_comment_item, parent, false);
        RVNewsComments.ViewHolder viewHolder = new RVNewsComments.ViewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RVNewsComments.ViewHolder holder, int position) {


        holder.tvName.setText(list.get(position).getCustomerName());
        holder.tvComment.setText(list.get(position).getComments());
        holder.tvDate.setText(list.get(position).getAddedDate());

    }

    @Override
    public int getItemCount() {
        return list.size();

    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        TextView tvName,tvComment,tvDate;

        public ViewHolder(View itemView) {
            super(itemView);
            tvName =  itemView.findViewById(R.id.tvName);
            tvComment=itemView.findViewById(R.id.tvComment);
            tvDate=itemView.findViewById(R.id.tvDate);

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {

        }
    }
}



