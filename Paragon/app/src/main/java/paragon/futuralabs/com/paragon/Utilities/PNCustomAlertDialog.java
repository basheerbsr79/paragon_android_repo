package paragon.futuralabs.com.paragon.Utilities;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import paragon.futuralabs.com.paragon.R;


/**
 * Created by Shamir on 19-10-2017.
 */

public class PNCustomAlertDialog extends Dialog implements
        View.OnClickListener {

    public Activity c;
    public Context context;
    public Dialog d;
    public TextView Header,Message;
    public Button btnOk,btncancel;
    public String message,header;

    public PNCustomAlertDialog(Activity a, String header, String message) {
        super(a);
        // TODO Auto-generated constructor stub
        this.c = a;
            this.header=header;
        this.message=message;
    }

    public PNCustomAlertDialog(Context a, String header, String message) {
        super(a);
        // TODO Auto-generated constructor stub
        this.context = a;
        this.header=header;
        this.message=message;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.custom_dialog);
        setCancelable(false);
        getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        btnOk = (Button) findViewById(R.id.btnOk);
        btncancel=(Button)findViewById(R.id.btnCancel);
       // Header=(TextView) findViewById(R.id.tvTitle);
        Message=(TextView) findViewById(R.id.tvMessage);
//Header.setText(header);
        Message.setText(message);
        btnOk.setOnClickListener(this);
        btncancel.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnOk:
                dismiss();
                break;
            case R.id.btnCancel:
                dismiss();
                break;
            default:
                break;
        }
        dismiss();
    }
}
