package paragon.futuralabs.com.paragon.Adapters;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import paragon.futuralabs.com.paragon.Activity.PNHDMenuItems;
import paragon.futuralabs.com.paragon.Activity.PNMenuItems;
import paragon.futuralabs.com.paragon.Activity.PNVoucherDetails;
import paragon.futuralabs.com.paragon.R;
import paragon.futuralabs.com.paragon.models.MenuCatagoryList;
import paragon.futuralabs.com.paragon.models.VouchersList;

import static paragon.futuralabs.com.paragon.Utilities.AppConstants.FROM;
import static paragon.futuralabs.com.paragon.Utilities.AppConstants.Item;
import static paragon.futuralabs.com.paragon.Utilities.AppConstants.MENU;

/**
 * Created by Shamir on 30-03-2018.
 */

public class RVVoucher extends RecyclerView.Adapter<RVVoucher.ViewHolder> {
    List<VouchersList> lists;
    Context context;

    public RVVoucher(Context context, List<VouchersList> lists) {
        this.context = context;
        this.lists=lists;

    }

    @Override
    public RVVoucher.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.rv_voucher_item, parent, false);
        RVVoucher.ViewHolder viewHolder = new RVVoucher.ViewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RVVoucher.ViewHolder holder, int position) {
        String url=lists.get(position).getVoucherPhoto();
        if (!url.equals("")) {
            Picasso.with(context)
                    .load(url)
                    .error(R.drawable.videoimage)
                    .placeholder(R.drawable.videoimage)
                    .resize(600, 600).centerCrop()
                    .into(holder.imvImage);
        }
        holder.textView.setText(lists.get(position).getOutletName());
        holder.textViewDesc.setText(lists.get(position).getVoucherName());
        holder.tvExpire.setText("Valid upto : "+lists.get(position).getExpiryDate());

    }

    @Override
    public int getItemCount() {
        return lists.size();

    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        ImageView imvImage;
        TextView textView,textViewDesc,tvExpire;

        public ViewHolder(View itemView) {
            super(itemView);
            imvImage =   itemView.findViewById(R.id.imageView);
            textView= itemView.findViewById(R.id.textView);
            textViewDesc= itemView.findViewById(R.id.textViewDesc);
            tvExpire= itemView.findViewById(R.id.tvExpire);

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            Intent intent=new Intent(context, PNVoucherDetails.class);
            Bundle bundle=new Bundle();
            bundle.putSerializable(Item,lists.get(getPosition()));
            intent.putExtras(bundle);
            context.startActivity(intent);
        }
    }
}
