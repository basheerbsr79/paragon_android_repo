
package paragon.futuralabs.com.paragon.models;

import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class FavoriteList {

    @SerializedName("Currency")
    private String mCurrency;
    @SerializedName("DishName")
    private String mDishName;
    @SerializedName("Image")
    private String mImage;
    @SerializedName("IsHomeDelivery")
    private Boolean mIsHomeDelivery;
    @SerializedName("IsVeg")
    private int mIsVeg;
    @SerializedName("MenuCategoryName")
    private String mMenuCategoryName;
    @SerializedName("MenuItemsId")
    private Long mMenuItemsId;
    @SerializedName("OutletsName")
    private String mOutletsName;
    @SerializedName("Price")
    private String mPrice;

    public String getCurrency() {
        return mCurrency;
    }

    public void setCurrency(String Currency) {
        mCurrency = Currency;
    }

    public String getDishName() {
        return mDishName;
    }

    public void setDishName(String DishName) {
        mDishName = DishName;
    }

    public String getImage() {
        return mImage;
    }

    public void setImage(String Image) {
        mImage = Image;
    }

    public Boolean getIsHomeDelivery() {
        return mIsHomeDelivery;
    }

    public void setIsHomeDelivery(Boolean IsHomeDelivery) {
        mIsHomeDelivery = IsHomeDelivery;
    }

    public int getIsVeg() {
        return mIsVeg;
    }

    public void setIsVeg(int IsVeg) {
        mIsVeg = IsVeg;
    }

    public String getMenuCategoryName() {
        return mMenuCategoryName;
    }

    public void setMenuCategoryName(String MenuCategoryName) {
        mMenuCategoryName = MenuCategoryName;
    }

    public Long getMenuItemsId() {
        return mMenuItemsId;
    }

    public void setMenuItemsId(Long MenuItemsId) {
        mMenuItemsId = MenuItemsId;
    }

    public String getOutletsName() {
        return mOutletsName;
    }

    public void setOutletsName(String OutletsName) {
        mOutletsName = OutletsName;
    }

    public String getPrice() {
        return mPrice;
    }

    public void setPrice(String Price) {
        mPrice = Price;
    }

}
