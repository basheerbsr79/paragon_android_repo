package paragon.futuralabs.com.paragon.Activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import paragon.futuralabs.com.paragon.Adapters.RVFavLocations;
import paragon.futuralabs.com.paragon.Adapters.RVTop5Bills;
import paragon.futuralabs.com.paragon.R;
import paragon.futuralabs.com.paragon.apiManager.PNApiResponseWrapper;
import paragon.futuralabs.com.paragon.apiManager.PNNetworkConstants;
import paragon.futuralabs.com.paragon.apiManager.PNNetworkManager;
import paragon.futuralabs.com.paragon.apiManager.SplitResponse;
import paragon.futuralabs.com.paragon.models.FavLocationList;
import paragon.futuralabs.com.paragon.models.FavLocationResponse;
import paragon.futuralabs.com.paragon.models.Top5BillList;
import paragon.futuralabs.com.paragon.models.Top5BillResponse;

import static paragon.futuralabs.com.paragon.Utilities.AppConstants.FROM;
import static paragon.futuralabs.com.paragon.Utilities.AppConstants.Mgrill;
import static paragon.futuralabs.com.paragon.Utilities.AppConstants.Salkara;
import static paragon.futuralabs.com.paragon.Utilities.ConnectionDetector.toast;
import static paragon.futuralabs.com.paragon.apiManager.PNNetworkManager.post;

public class PNTop5Bills extends PNBaseActivity {

    LinearLayout linLayContainer;
    TextView tvHeading;
    ImageView ic_back_arrow;
    RecyclerView recyclerView;
    SplitResponse splitResponse;
    Top5BillResponse top5BillResponse;
    List<Top5BillList> top5BillLists;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pntop5_bills);
        init();
        handleEvents();
        getLocations();
        trackEvent("Top 5 Bills","View Top 5 Bills");
    }

    private void handleEvents() {
        tvHeading.setText("Top 5 Bills");
        ic_back_arrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

    }

    private void init() {
        linLayContainer= findViewById(R.id.linLayContainer);
        ic_back_arrow= findViewById(R.id.img_back_toolbar);
        tvHeading= findViewById(R.id.txt_title_toolbar);
        recyclerView= findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
    }

    public void getLocations(){
        final String[] list=new String[0];
        top5BillLists=new ArrayList(Arrays.asList(list));
        showProgress();
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("CustomerId", PNSP.getuserid());
            // jsonObject.put("CustomerId", "1");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        post(PNTop5Bills.this, PNNetworkConstants.GetTop5billing, jsonObject.toString(), new PNNetworkManager.NetworkInterface() {
            @Override
            public void onResponse(PNApiResponseWrapper baseResponse) {
                dismissProgres();
                if (baseResponse.isSuccess()) {
                    splitResponse = new SplitResponse(baseResponse.getJsonObjectResponse());
                    if (splitResponse.getStatus().equals("1")) {
                        try {
                            Gson gson = new Gson();
                            top5BillResponse = gson.fromJson(splitResponse.getResponse(), Top5BillResponse.class);
                            top5BillLists.addAll(top5BillResponse.getData());
                            if (top5BillLists.size()>0)
                                setData(top5BillLists);
                            else
                                toast(getApplicationContext(),"No bills found");

                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    } else {
                        toast(getApplicationContext(), splitResponse.getMessage());
                    }
                } else {
                    toast(getApplicationContext(), baseResponse.getMessage());
                }

            }
        });


    }

    private void setData(List<Top5BillList> lists) {
        RVTop5Bills adapter=new RVTop5Bills(PNTop5Bills.this,lists);
        recyclerView.setAdapter(adapter);
    }

}

