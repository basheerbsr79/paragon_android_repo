
package paragon.futuralabs.com.paragon.models;

import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class NationalitiesList {

    @SerializedName("Nationality")
    private String mNationality;

    public String getNationality() {
        return mNationality;
    }

    public void setNationality(String Nationality) {
        mNationality = Nationality;
    }

}
