
package paragon.futuralabs.com.paragon.models;

import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class CityList {

    @SerializedName("City")
    private String mCity;
    @SerializedName("CityId")
    private Long mCityID;

    public String getCity() {
        return mCity;
    }

    public void setCity(String City) {
        mCity = City;
    }

    public Long getCityID() {
        return mCityID;
    }

    public void setCityID(Long CityID) {
        mCityID = CityID;
    }

}
