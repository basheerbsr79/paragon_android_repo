
package paragon.futuralabs.com.paragon.models.homedelivery;

import java.io.Serializable;
import java.util.List;
import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class HDList implements Serializable{

    @SerializedName("CategoryImg")
    private String mCategoryImg;
    @SerializedName("Description")
    private String mDescription;
    @SerializedName("MenuCategoryId")
    private Long mMenuCategoryId;
    @SerializedName("MenuCategoryName")
    private String mMenuCategoryName;
    @SerializedName("MenuItems")
    private List<MenuItem> mMenuItems;

    public String getCategoryImg() {
        return mCategoryImg;
    }

    public void setCategoryImg(String CategoryImg) {
        mCategoryImg = CategoryImg;
    }

    public String getDescription() {
        return mDescription;
    }

    public void setDescription(String Description) {
        mDescription = Description;
    }

    public Long getMenuCategoryId() {
        return mMenuCategoryId;
    }

    public void setMenuCategoryId(Long MenuCategoryId) {
        mMenuCategoryId = MenuCategoryId;
    }

    public String getMenuCategoryName() {
        return mMenuCategoryName;
    }

    public void setMenuCategoryName(String MenuCategoryName) {
        mMenuCategoryName = MenuCategoryName;
    }

    public List<MenuItem> getMenuItems() {
        return mMenuItems;
    }

    public void setMenuItems(List<MenuItem> MenuItems) {
        mMenuItems = MenuItems;
    }

}
