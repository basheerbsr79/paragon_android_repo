
package paragon.futuralabs.com.paragon.models.todayspecial;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

@SuppressWarnings("unused")
public class TimeStartTo implements Serializable{

    @SerializedName("Days")
    private Long mDays;
    @SerializedName("Hours")
    private Long mHours;
    @SerializedName("Milliseconds")
    private Long mMilliseconds;
    @SerializedName("Minutes")
    private Long mMinutes;
    @SerializedName("Seconds")
    private Long mSeconds;
    @SerializedName("Ticks")
    private Long mTicks;
    @SerializedName("TotalDays")
    private Double mTotalDays;
    @SerializedName("TotalHours")
    private Double mTotalHours;
    @SerializedName("TotalMilliseconds")
    private Long mTotalMilliseconds;
    @SerializedName("TotalMinutes")
    private Long mTotalMinutes;
    @SerializedName("TotalSeconds")
    private Long mTotalSeconds;

    public Long getDays() {
        return mDays;
    }

    public void setDays(Long Days) {
        mDays = Days;
    }

    public Long getHours() {
        return mHours;
    }

    public void setHours(Long Hours) {
        mHours = Hours;
    }

    public Long getMilliseconds() {
        return mMilliseconds;
    }

    public void setMilliseconds(Long Milliseconds) {
        mMilliseconds = Milliseconds;
    }

    public Long getMinutes() {
        return mMinutes;
    }

    public void setMinutes(Long Minutes) {
        mMinutes = Minutes;
    }

    public Long getSeconds() {
        return mSeconds;
    }

    public void setSeconds(Long Seconds) {
        mSeconds = Seconds;
    }

    public Long getTicks() {
        return mTicks;
    }

    public void setTicks(Long Ticks) {
        mTicks = Ticks;
    }

    public Double getTotalDays() {
        return mTotalDays;
    }

    public void setTotalDays(Double TotalDays) {
        mTotalDays = TotalDays;
    }

    public Double getTotalHours() {
        return mTotalHours;
    }

    public void setTotalHours(Double TotalHours) {
        mTotalHours = TotalHours;
    }

    public Long getTotalMilliseconds() {
        return mTotalMilliseconds;
    }

    public void setTotalMilliseconds(Long TotalMilliseconds) {
        mTotalMilliseconds = TotalMilliseconds;
    }

    public Long getTotalMinutes() {
        return mTotalMinutes;
    }

    public void setTotalMinutes(Long TotalMinutes) {
        mTotalMinutes = TotalMinutes;
    }

    public Long getTotalSeconds() {
        return mTotalSeconds;
    }

    public void setTotalSeconds(Long TotalSeconds) {
        mTotalSeconds = TotalSeconds;
    }

}
