
package paragon.futuralabs.com.paragon.models.feedbacksmiley;

import java.util.List;
import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class FeedBackSmileyResponse {

    @SerializedName("data")
    private List<FeedBackSmileyList> mData;
    @SerializedName("message")
    private String mMessage;
    @SerializedName("status")
    private Long mStatus;

    public List<FeedBackSmileyList> getData() {
        return mData;
    }

    public void setData(List<FeedBackSmileyList> data) {
        mData = data;
    }

    public String getMessage() {
        return mMessage;
    }

    public void setMessage(String message) {
        mMessage = message;
    }

    public Long getStatus() {
        return mStatus;
    }

    public void setStatus(Long status) {
        mStatus = status;
    }

}
