
package paragon.futuralabs.com.paragon.models;

import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class ContactUsData {

    @SerializedName("CareersMailId")
    private String mCareersMailId;
    @SerializedName("CateringMailId")
    private String mCateringMailId;
    @SerializedName("CateringNumber")
    private String mCateringNumber;
    @SerializedName("CustomerCareMailId")
    private String mCustomerCareMailId;
    @SerializedName("CustomerCareNumber")
    private String mCustomerCareNumber;
    @SerializedName("Email")
    private String mEmail;
    @SerializedName("Phone")
    private String mPhone;
    @SerializedName("ReceptionPhon")
    private String mReceptionPhon;
    @SerializedName("TakewayNumber")
    private String mTakewayNumber;

    public String getCareersMailId() {
        return mCareersMailId;
    }

    public void setCareersMailId(String CareersMailId) {
        mCareersMailId = CareersMailId;
    }

    public String getCateringMailId() {
        return mCateringMailId;
    }

    public void setCateringMailId(String CateringMailId) {
        mCateringMailId = CateringMailId;
    }

    public String getCateringNumber() {
        return mCateringNumber;
    }

    public void setCateringNumber(String CateringNumber) {
        mCateringNumber = CateringNumber;
    }

    public String getCustomerCareMailId() {
        return mCustomerCareMailId;
    }

    public void setCustomerCareMailId(String CustomerCareMailId) {
        mCustomerCareMailId = CustomerCareMailId;
    }

    public String getCustomerCareNumber() {
        return mCustomerCareNumber;
    }

    public void setCustomerCareNumber(String CustomerCareNumber) {
        mCustomerCareNumber = CustomerCareNumber;
    }

    public String getEmail() {
        return mEmail;
    }

    public void setEmail(String Email) {
        mEmail = Email;
    }

    public String getPhone() {
        return mPhone;
    }

    public void setPhone(String Phone) {
        mPhone = Phone;
    }

    public String getReceptionPhon() {
        return mReceptionPhon;
    }

    public void setReceptionPhon(String ReceptionPhon) {
        mReceptionPhon = ReceptionPhon;
    }

    public String getTakewayNumber() {
        return mTakewayNumber;
    }

    public void setTakewayNumber(String TakewayNumber) {
        mTakewayNumber = TakewayNumber;
    }

}
