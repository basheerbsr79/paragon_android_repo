package paragon.futuralabs.com.paragon.Activity;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import paragon.futuralabs.com.paragon.Adapters.RVFavorites;
import paragon.futuralabs.com.paragon.Adapters.RVVoucher;
import paragon.futuralabs.com.paragon.R;
import paragon.futuralabs.com.paragon.apiManager.PNApiResponseWrapper;
import paragon.futuralabs.com.paragon.apiManager.PNNetworkConstants;
import paragon.futuralabs.com.paragon.apiManager.PNNetworkManager;
import paragon.futuralabs.com.paragon.apiManager.SplitResponse;
import paragon.futuralabs.com.paragon.models.VouchersList;
import paragon.futuralabs.com.paragon.models.VouchersResponse;

import static paragon.futuralabs.com.paragon.Utilities.AppConstants.FROM;
import static paragon.futuralabs.com.paragon.Utilities.AppConstants.Mgrill;
import static paragon.futuralabs.com.paragon.Utilities.AppConstants.Salkara;
import static paragon.futuralabs.com.paragon.Utilities.ConnectionDetector.toast;
import static paragon.futuralabs.com.paragon.apiManager.PNNetworkManager.post;

public class PNVoucher extends PNBaseActivity {

    LinearLayout linLayContainer;
    TextView tvHeading;
    ImageView ic_back_arrow;
    RecyclerView recyclerView;
    SplitResponse splitResponse;
    VouchersResponse vouchersResponse;
    List<VouchersList> vouchersLists;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pnvoucher);
        init();
        handleEvents();
        getVouchers();
        trackEvent("Vouchers","View Vouchers");
    }

    private void handleEvents() {
        tvHeading.setText("Vouchers");
        ic_back_arrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

    }

    private void init() {
        linLayContainer= findViewById(R.id.linLayContainer);
        ic_back_arrow= findViewById(R.id.img_back_toolbar);
        tvHeading= findViewById(R.id.txt_title_toolbar);
        recyclerView= findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
    }

    public void getVouchers(){
        final String[] list=new String[0];
        vouchersLists=new ArrayList(Arrays.asList(list));
        showProgress();
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("CustomerId", PNSP.getuserid());
           // jsonObject.put("CustomerId", "1");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        post(PNVoucher.this, PNNetworkConstants.MyVoucher, jsonObject.toString(), new PNNetworkManager.NetworkInterface() {
            @Override
            public void onResponse(PNApiResponseWrapper baseResponse) {
                dismissProgres();
                if (baseResponse.isSuccess()) {
                    splitResponse = new SplitResponse(baseResponse.getJsonObjectResponse());
                    if (splitResponse.getStatus().equals("1")) {
                        try {
                            Gson gson = new Gson();
                            vouchersResponse = gson.fromJson(splitResponse.getResponse(), VouchersResponse.class);
                            vouchersLists.addAll(vouchersResponse.getData());
                            if (vouchersLists.size()>0)
                                setData(vouchersLists);
                            else
                                toast(getApplicationContext(),"No vouchers found");

                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    } else {
                        toast(getApplicationContext(), splitResponse.getMessage());
                    }
                } else {
                    toast(getApplicationContext(), baseResponse.getMessage());
                }

            }
        });


    }

    private void setData(List<VouchersList> lists) {

        RVVoucher rvVoucher=new RVVoucher(PNVoucher.this,lists);
        recyclerView.setAdapter(rvVoucher);
    }

}

