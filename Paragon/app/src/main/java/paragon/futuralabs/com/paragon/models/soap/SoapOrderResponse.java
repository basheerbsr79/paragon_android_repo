
package paragon.futuralabs.com.paragon.models.soap;

import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class SoapOrderResponse {

    @SerializedName("Hotsoft")
    private paragon.futuralabs.com.paragon.models.soap.Hotsoft mHotsoft;

    public paragon.futuralabs.com.paragon.models.soap.Hotsoft getHotsoft() {
        return mHotsoft;
    }

    public void setHotsoft(paragon.futuralabs.com.paragon.models.soap.Hotsoft Hotsoft) {
        mHotsoft = Hotsoft;
    }

}
