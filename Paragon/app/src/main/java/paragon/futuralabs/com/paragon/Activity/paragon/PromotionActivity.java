package paragon.futuralabs.com.paragon.Activity.paragon;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.viewpagerindicator.CirclePageIndicator;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import paragon.futuralabs.com.paragon.Activity.PNBaseActivity;
import paragon.futuralabs.com.paragon.Activity.PNMenuCatagory;
import paragon.futuralabs.com.paragon.Adapters.CustomPagerAdapter;
import paragon.futuralabs.com.paragon.R;
import paragon.futuralabs.com.paragon.Utilities.ConnectionDetector;
import paragon.futuralabs.com.paragon.Utilities.Json;
import paragon.futuralabs.com.paragon.Utilities.MyAlert;
import paragon.futuralabs.com.paragon.Utilities.NetFailedAlert;
import paragon.futuralabs.com.paragon.apiManager.PNApiResponseWrapper;
import paragon.futuralabs.com.paragon.apiManager.PNNetworkConstants;
import paragon.futuralabs.com.paragon.apiManager.PNNetworkManager;
import paragon.futuralabs.com.paragon.apiManager.SplitResponse;
import paragon.futuralabs.com.paragon.models.OutletList;
import paragon.futuralabs.com.paragon.models.OutletResponse;
import paragon.futuralabs.com.paragon.models.PromotionList;
import paragon.futuralabs.com.paragon.models.PromotionResponse;
import paragon.futuralabs.com.paragon.paragonhelper.ParagonSP;

import static paragon.futuralabs.com.paragon.Utilities.AppConstants.FROM;
import static paragon.futuralabs.com.paragon.Utilities.AppConstants.Paragon;
import static paragon.futuralabs.com.paragon.Utilities.AppConstants.Salkara;
import static paragon.futuralabs.com.paragon.Utilities.AppConstants.position;
import static paragon.futuralabs.com.paragon.Utilities.ConnectionDetector.toast;
import static paragon.futuralabs.com.paragon.apiManager.PNNetworkManager.post;

public class PromotionActivity extends PNBaseActivity {

    Context context;
    ImageView img_back_toolbar,img_menu_toolbar;
    TextView txt_title_toolbar;
    Button btn_view_menu;
    ViewPager mViewPager;
    CirclePageIndicator mIndicator;
    ParagonSP sp;
    int position_viewpager;
    SplitResponse splitResponse;
    PromotionResponse promotionResponse;
    List<PromotionList> promotionLists;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_promotion);
        //setting context
        context = PromotionActivity.this;

        //decalaring variables
        img_back_toolbar= findViewById(R.id.img_back_toolbar);
        img_menu_toolbar= findViewById(R.id.img_menu_toolbar);
        txt_title_toolbar= findViewById(R.id.txt_title_toolbar);
        btn_view_menu= findViewById(R.id.btn_vew_menu);
        //setting toolbar title
        txt_title_toolbar.setText(getString(R.string.tiltle_promotion));
        //back event
        img_back_toolbar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               onBackPressed();
            }
        });
        btn_view_menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(context, PNMenuCatagory.class);
                in.putExtra(FROM,Paragon);
                startActivity(in);
            }
        });
        sp=new ParagonSP(getApplicationContext());
        position_viewpager=getIntent().getIntExtra(position,0);
        final View snackbarview = getWindow().getDecorView();
        ConnectionDetector cd = new ConnectionDetector(getApplicationContext());
        Boolean isInternetPresent = cd.isConnectingToInternet();
        // true or false
        if (isInternetPresent.equals(true)) {
            getPromotions();
            trackEvent("Promotion","View Promotion Item");
        } else {
            NetFailedAlert netalert = new NetFailedAlert();
            netalert.showAlert(context, snackbarview);
        }
    }


    private void getPromotions() {
        final String[] list=new String[0];
        promotionLists=new ArrayList(Arrays.asList(list));
        showProgress();
        JSONObject jsonObject = new JSONObject();

        try {
            jsonObject.put("OutletId",sp.getOutlet());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        post(PromotionActivity.this, PNNetworkConstants.BindPromotions, jsonObject.toString(), new PNNetworkManager.NetworkInterface() {
            @Override
            public void onResponse(PNApiResponseWrapper baseResponse) {
                dismissProgres();
                if (baseResponse.isSuccess()) {
                    splitResponse = new SplitResponse(baseResponse.getJsonObjectResponse());
                    if (splitResponse.getStatus().equals("1")) {
                        Gson gson = new Gson();
                        promotionResponse = gson.fromJson(splitResponse.getResponse(), PromotionResponse.class);
                        promotionLists.addAll(promotionResponse.getData());
                        if (promotionLists.size()>0) {
                            setPromotions(promotionLists);
                        }else {
                            MyAlert alert=new MyAlert();
                            alert.showAlert(context,"no promotion found");
                        }

                    } else {
                        toast(getApplicationContext(), splitResponse.getMessage());
                    }
                } else {
                    toast(getApplicationContext(), baseResponse.getMessage());
                }

            }
        });

    }

    private void setPromotions(List<PromotionList> promotionLists) {

        mViewPager = findViewById(R.id.graphs);

        CustomPagerAdapter mCustomPagerAdapter = new CustomPagerAdapter(context,promotionLists);
        mViewPager.setAdapter(mCustomPagerAdapter);
        mIndicator = findViewById(R.id.indicator);
        mIndicator.setViewPager(mViewPager);
        mViewPager.setCurrentItem(position_viewpager);

    }
}
