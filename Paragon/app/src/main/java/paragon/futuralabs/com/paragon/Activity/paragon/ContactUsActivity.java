package paragon.futuralabs.com.paragon.Activity.paragon;

import android.app.ActionBar;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.BitmapDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Looper;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.squareup.picasso.Picasso;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Calendar;

import paragon.futuralabs.com.paragon.Activity.PNBaseActivity;
import paragon.futuralabs.com.paragon.Activity.PNeReceipt;
import paragon.futuralabs.com.paragon.R;
import paragon.futuralabs.com.paragon.Utilities.MyAlert;
import paragon.futuralabs.com.paragon.Utilities.NetFailedAlert;
import paragon.futuralabs.com.paragon.apiManager.PNApiResponseWrapper;
import paragon.futuralabs.com.paragon.apiManager.PNNetworkConstants;
import paragon.futuralabs.com.paragon.apiManager.PNNetworkManager;
import paragon.futuralabs.com.paragon.apiManager.SplitResponse;
import paragon.futuralabs.com.paragon.models.ContactUsResponse;
import paragon.futuralabs.com.paragon.models.CustomerDetailsResponse;
import paragon.futuralabs.com.paragon.paragonhelper.ParagonSP;

import static paragon.futuralabs.com.paragon.Utilities.ConnectionDetector.toast;
import static paragon.futuralabs.com.paragon.Utilities.Utils.isValidEmail;
import static paragon.futuralabs.com.paragon.Utilities.Utils.validateEditText;
import static paragon.futuralabs.com.paragon.apiManager.PNNetworkManager.post;

public class ContactUsActivity extends PNBaseActivity {
    Context context;
    ImageView img_back_toolbar, img_menu_toolbar;
    TextView txt_title_toolbar;
    EditText name, email, phone, message,edNoOfGuest,edOccasion,edDate,edLocAddress,edPosition,edLocation;
    LinearLayout llCatering,llCarrers,llForOthers;
    TextView tvCatering,tvCareers,tvGeneral;
    EditText edName,edPhoneNo;
    RadioButton rbForYou,rbForOthers;
    Button submit;
    ParagonSP sp;
    TextView tvCateringPhoneNo, tvCateringEmail, tvCareerEmail, tvCustCareNo, tvCustCareEmail,tvReception,tvTakeaway;
    SplitResponse splitResponse;
    ContactUsResponse contactUsResponse;
    CustomerDetailsResponse customerDetailsResponse;
    public static String[] types = new String[]{"Catering", "Careers", "General"};
    public static String[] occasion = new String[]{"Breakfast", "Lunch", "Hi Tea","Dinner"};
    int enqtypeId = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact_us);

        init();
        handleEvents();

    }

    public void handleEvents() {
        // edSelectType.setText(types[0]);
        setType(0);

        tvCatering.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setType(0);
            }
        });

        tvCareers.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setType(1);
            }
        });

        tvGeneral.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setType(2);
            }
        });


      rbForYou.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
          @Override
          public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
              if (isChecked)
                  llForOthers.setVisibility(View.GONE);
          }
      });

        rbForOthers.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked)
                    llForOthers.setVisibility(View.VISIBLE);
            }
        });



        edOccasion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDropDownList(ContactUsActivity.this, edOccasion, occasion, edOccasion.getWidth());
            }
        });

        edDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setDate(edDate, ContactUsActivity.this, "Select Date");
            }
        });

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (validate()) {
                    postContact();
                }
            }
        });

        getContactDetails();
        getCustomerDetails();
        trackEvent("Contact Us", "View Contact Us");
    }

    public void init() {
        //setting context
        context = ContactUsActivity.this;
        //decalaring variables
        img_back_toolbar = findViewById(R.id.img_back_toolbar);
        img_menu_toolbar = findViewById(R.id.img_menu_toolbar);
        txt_title_toolbar = findViewById(R.id.txt_title_toolbar);
        //setting toolbar title
        txt_title_toolbar.setText(getString(R.string.title_contactus));
        //back event
        img_back_toolbar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        sp = new ParagonSP(getApplicationContext());
        final View snackbarview = getWindow().getDecorView();
        name = findViewById(R.id.name);
        email = findViewById(R.id.email);
        phone = findViewById(R.id.phone);
        tvTakeaway=findViewById(R.id.tvTakeaway);
        tvReception=findViewById(R.id.tvReception);
        //edSelectType = findViewById(R.id.edSelectType);
        message = findViewById(R.id.message);
        submit = findViewById(R.id.submit);
        tvCateringPhoneNo = findViewById(R.id.tvCateringPhoneNo);
        tvCateringEmail = findViewById(R.id.tvCateringEmail);
        tvCareerEmail = findViewById(R.id.tvCareerEmail);
        tvCustCareNo = findViewById(R.id.tvCustCareNo);
        tvCustCareEmail = findViewById(R.id.tvCustCareEmail);
        edNoOfGuest= findViewById(R.id.edNoOfGuest);
        edOccasion= findViewById(R.id.edOccasion);
        edDate= findViewById(R.id.edDate);
        edLocAddress= findViewById(R.id.edLocAddress);
        edPosition= findViewById(R.id.edPosition);
        edLocation= findViewById(R.id.edLocation);
        llCatering= findViewById(R.id.llCatering);
        llCarrers= findViewById(R.id.llCarrers);

        tvCatering= findViewById(R.id.tvCatering);
        tvCareers= findViewById(R.id.tvCareers);
        tvGeneral= findViewById(R.id.tvGeneral);

        edName= findViewById(R.id.edName);
        edPhoneNo= findViewById(R.id.edPhoneNo);
        rbForYou= findViewById(R.id.rbForYou);
        rbForOthers= findViewById(R.id.rbForOthers);
        llForOthers=findViewById(R.id.llForOthers);


    }

    public void getContactDetails() {
        showProgress();
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("OutletId", sp.getOutlet());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        post(ContactUsActivity.this, PNNetworkConstants.BindContactUsDetails, jsonObject.toString(), new PNNetworkManager.NetworkInterface() {
            @Override
            public void onResponse(PNApiResponseWrapper baseResponse) {
                dismissProgres();
                if (baseResponse.isSuccess()) {
                    splitResponse = new SplitResponse(baseResponse.getJsonObjectResponse());
                    if (splitResponse.getStatus().equals("1")) {
                        try {
                            Gson gson = new Gson();
                            contactUsResponse = gson.fromJson(splitResponse.getResponse(), ContactUsResponse.class);
                            setData(contactUsResponse);

                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    } else {
                        toast(getApplicationContext(), splitResponse.getMessage());
                    }
                } else {
                    toast(getApplicationContext(), baseResponse.getMessage());
                }

            }
        });


    }


    private void getCustomerDetails() {
        showProgress();
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("CustomerId", PNSP.getuserid());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        post(ContactUsActivity.this, PNNetworkConstants.BindCustomerDetails, jsonObject.toString(), new PNNetworkManager.NetworkInterface() {
            @Override
            public void onResponse(PNApiResponseWrapper baseResponse) {
                dismissProgres();
                if (baseResponse.isSuccess()) {
                    splitResponse = new SplitResponse(baseResponse.getJsonObjectResponse());
                    if (splitResponse.getStatus().equals("1")) {

                        Gson gson = new Gson();
                        customerDetailsResponse = gson.fromJson(splitResponse.getResponse(), CustomerDetailsResponse.class);

                    } else {
                        toast(getApplicationContext(), splitResponse.getMessage());
                    }
                } else {
                    toast(getApplicationContext(), baseResponse.getMessage());
                }

            }
        });

    }

    private void setData(ContactUsResponse contactUsResponse) {
        tvCateringPhoneNo.setText(contactUsResponse.getData().get(0).getCateringNumber());
        tvCateringEmail.setText(contactUsResponse.getData().get(0).getCateringMailId());
        tvCareerEmail.setText(contactUsResponse.getData().get(0).getCareersMailId());
        tvCustCareNo.setText(contactUsResponse.getData().get(0).getCustomerCareNumber());
        tvCustCareEmail.setText(contactUsResponse.getData().get(0).getCustomerCareMailId());
        tvReception.setText("Reception : "+contactUsResponse.getData().get(0).getReceptionPhon());
        tvTakeaway.setText("Take Away : "+contactUsResponse.getData().get(0).getTakewayNumber());
        name.setText(sp.getname());
        email.setText(sp.getemail());
        phone.setText(sp.getphone());

    }

    public void postContact() {
        showProgress();
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("OutletId", sp.getOutlet());
            jsonObject.put("Name", name.getText().toString().trim());
            jsonObject.put("Email", email.getText().toString().trim());
            jsonObject.put("Mobile", phone.getText().toString().trim());
            jsonObject.put("Message", message.getText().toString().trim());
            jsonObject.put("enqtype", enqtypeId);
            jsonObject.put("Position", edPosition.getText().toString().trim());
            if (enqtypeId==1)
            jsonObject.put("LocaAddress", edLocAddress.getText().toString().trim());
            else
                jsonObject.put("LocaAddress", edLocation.getText().toString().trim());
            if (enqtypeId==1){
                if (rbForYou.isChecked()){
                    jsonObject.put("forwho", "You");
                    jsonObject.put("forothername", "");
                    jsonObject.put("forotherphoneno", "");
                }else {
                    jsonObject.put("forwho", "Others");
                    jsonObject.put("forothername", edName.getText().toString().trim());
                    jsonObject.put("forotherphoneno", edPhoneNo.getText().toString().trim());
                }
            }else {
                jsonObject.put("forwho", "");
                jsonObject.put("forothername", "");
                jsonObject.put("forotherphoneno", "");
            }
            jsonObject.put("Occasion", edOccasion.getText().toString().trim());
            if (edNoOfGuest.getText().toString().trim().equals(""))
            jsonObject.put("NoofGuest", "0");
            else
                jsonObject.put("NoofGuest", edNoOfGuest.getText().toString().trim());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        post(this, PNNetworkConstants.SaveContactUsMessage, jsonObject.toString(), new PNNetworkManager.NetworkInterface() {
            @Override
            public void onResponse(PNApiResponseWrapper baseResponse) {
                dismissProgres();
                if (baseResponse.isSuccess()) {
                    splitResponse = new SplitResponse(baseResponse.getJsonObjectResponse());
                    if (splitResponse.getStatus().equals("1")) {
                        MyAlert alert = new MyAlert();
                        alert.showAlertFinish(ContactUsActivity.this, "Thank you for contacting us, Paragon team will surely revert to you.");

                    } else {
                        toast(getApplicationContext(), splitResponse.getMessage());
                    }
                } else {
                    toast(getApplicationContext(), baseResponse.getMessage());
                }

            }
        });


    }

    public void showDropDownList(final Context context, final EditText editText, String[] arrayList, int width) {
        final PopupWindow pwindo;
        LayoutInflater inflater = (LayoutInflater) context.getApplicationContext()
                .getSystemService(LAYOUT_INFLATER_SERVICE);
        View layout = inflater.inflate(R.layout.custom_listview,
                null);
        pwindo = new PopupWindow(layout, ActionBar.LayoutParams.WRAP_CONTENT, ActionBar.LayoutParams.WRAP_CONTENT, true);
        pwindo.setWidth(width);
        pwindo.setBackgroundDrawable(new BitmapDrawable());
        pwindo.setFocusable(true);
        pwindo.showAsDropDown(editText, 0, 0);
        pwindo.setOutsideTouchable(true);
        pwindo.showAtLocation(layout, Gravity.CENTER, 0, 0);
        final ListView ll = (ListView) pwindo.getContentView().findViewById(R.id.lst_custom);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(context, android.R.layout.simple_list_item_1, arrayList);
        // Assign adapter to ListView
        ll.setAdapter(adapter);
        ll.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                editText.setText(parent.getItemAtPosition(position).toString());
                pwindo.dismiss();

            }
        });

    }

    public void setDate(final View v, final Context context, final String title) {
        final Calendar mcurrentDate = Calendar.getInstance();
        int mYear = mcurrentDate.get(Calendar.YEAR);
        int mMonth = mcurrentDate.get(Calendar.MONTH);
        int mDay = mcurrentDate.get(Calendar.DAY_OF_MONTH);
        DatePickerDialog mDatePicker = new DatePickerDialog(context, R.style.TimePickerTheme,
                new DatePickerDialog.OnDateSetListener() {
                    public void onDateSet(DatePicker datepicker,
                                          int selectedyear, int selectedmonth, int selectedday) {
                        String da = selectedday < 10 ? "0" + Integer.toString(selectedday) : Integer.toString(selectedday);
                        String m = selectedmonth < 9 ? "0" + Integer.toString(selectedmonth + 1) : Integer.toString(selectedmonth + 1);
                        String y = Integer.toString(selectedyear);
                        ((TextView) v).setText(da + "-" + m + "-" + y);
                    }
                }, mYear, mMonth, mDay);
        mDatePicker.setTitle(title + "");
        mDatePicker.getDatePicker().setMinDate(System.currentTimeMillis());
        mDatePicker.show();
    }

    public boolean validate() {

        if (!validateEditText(getApplicationContext(), name.getText().toString().trim())) {
            toast(this, "Enter name");
            return false;
        }
        if (!validateEditText(getApplicationContext(), phone.getText().toString().trim())) {
            toast(this, "Enter phone number");
            return false;
        }
        if (!isValidEmail(email.getText().toString().trim())) {
            toast(this, "Enter valid email");
            return false;
        }

        if (enqtypeId==1){
            if (rbForOthers.isChecked()){
                if (!validateEditText(getApplicationContext(), edName.getText().toString().trim())) {
                    toast(this, "Enter others name");
                    return false;
                }
                if (!validateEditText(getApplicationContext(), edPhoneNo.getText().toString().trim())) {
                    toast(this, "Enter others phone number");
                    return false;
                }
            }
            if (!validateEditText(getApplicationContext(), edNoOfGuest.getText().toString().trim())) {
                toast(this, "Enter no of guests");
                return false;
            }
            if (!validateEditText(getApplicationContext(), edOccasion.getText().toString().trim())) {
                toast(this, "Select occasion");
                return false;
            }
            if (!validateEditText(getApplicationContext(), edDate.getText().toString().trim())) {
                toast(this, "Select date");
                return false;
            }
            if (!validateEditText(getApplicationContext(), edLocAddress.getText().toString().trim())) {
                toast(this, "Enter Location/Address");
                return false;
            }
        }
        if (enqtypeId==2){
            if (!validateEditText(getApplicationContext(), edPosition.getText().toString().trim())) {
                toast(this, "Enter position");
                return false;
            }
            if (!validateEditText(getApplicationContext(), edLocation.getText().toString().trim())) {
                toast(this, "Enter location");
                return false;
            }
        }
        if (enqtypeId==3){
            if (!validateEditText(getApplicationContext(), message.getText().toString().trim())) {
                toast(this, "Enter message");
                return false;
            }
        }
        return true;
    }

    public void setType(int p){
            enqtypeId = (p + 1);
            if (p==0) {
                llCatering.setVisibility(View.VISIBLE);
                message.setVisibility(View.VISIBLE);
                llCarrers.setVisibility(View.GONE);
                tvCatering.setBackground(getResources().getDrawable(R.drawable.border_red));
                tvCareers.setBackground(getResources().getDrawable(R.drawable.border_grey));
                tvGeneral.setBackground(getResources().getDrawable(R.drawable.border_grey));
            }else if (p==1){
                llCatering.setVisibility(View.GONE);
                message.setVisibility(View.GONE);
                llCarrers.setVisibility(View.VISIBLE);
                tvCatering.setBackground(getResources().getDrawable(R.drawable.border_grey));
                tvCareers.setBackground(getResources().getDrawable(R.drawable.border_red));
                tvGeneral.setBackground(getResources().getDrawable(R.drawable.border_grey));
            }else if (p==2){
                llCatering.setVisibility(View.GONE);
                message.setVisibility(View.VISIBLE);
                llCarrers.setVisibility(View.GONE);
                tvCatering.setBackground(getResources().getDrawable(R.drawable.border_grey));
                tvCareers.setBackground(getResources().getDrawable(R.drawable.border_grey));
                tvGeneral.setBackground(getResources().getDrawable(R.drawable.border_red));
            }

    }
}
