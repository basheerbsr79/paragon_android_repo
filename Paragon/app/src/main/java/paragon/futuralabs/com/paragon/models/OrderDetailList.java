
package paragon.futuralabs.com.paragon.models;

import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class OrderDetailList {

    @SerializedName("Currency")
    private String mCurrency;
    @SerializedName("CustomerId")
    private Long mCustomerId;
    @SerializedName("CustomerName")
    private String mCustomerName;
    @SerializedName("DeliveryAddress")
    private String mDeliveryAddress;
    @SerializedName("DeliveryAddressId")
    private Long mDeliveryAddressId;
    @SerializedName("DeliveryType")
    private String mDeliveryType;
    @SerializedName("Discount")
    private Long mDiscount;
    @SerializedName("DishName")
    private String mDishName;
    @SerializedName("Email")
    private String mEmail;
    @SerializedName("ExAmt")
    private Double mExAmt;
    @SerializedName("MenuCode")
    private String mMenuCode;
    @SerializedName("MenuItemsId")
    private Long mMenuItemsId;
    @SerializedName("Mobile")
    private String mMobile;
    @SerializedName("OrderAmount")
    private Double mOrderAmount;
    @SerializedName("OrderAmt")
    private Double mOrderAmt;
    @SerializedName("OrderDate")
    private String mOrderDate;
    @SerializedName("OrderId")
    private Long mOrderId;
    @SerializedName("OrderStatus")
    private String mOrderStatus;
    @SerializedName("OrderStatusId")
    private Long mOrderStatusId;
    @SerializedName("OutletAddress")
    private String mOutletAddress;
    @SerializedName("OutletId")
    private Long mOutletId;
    @SerializedName("OutletName")
    private String mOutletName;
    @SerializedName("OutletPhone")
    private String mOutletPhone;
    @SerializedName("Price")
    private Double mPrice;
    @SerializedName("Quantity")
    private Long mQuantity;
    @SerializedName("ShippingCharge")
    private Long mShippingCharge;

    public String getCurrency() {
        return mCurrency;
    }

    public void setCurrency(String Currency) {
        mCurrency = Currency;
    }

    public Long getCustomerId() {
        return mCustomerId;
    }

    public void setCustomerId(Long CustomerId) {
        mCustomerId = CustomerId;
    }

    public String getCustomerName() {
        return mCustomerName;
    }

    public void setCustomerName(String CustomerName) {
        mCustomerName = CustomerName;
    }

    public String getDeliveryAddress() {
        return mDeliveryAddress;
    }

    public void setDeliveryAddress(String DeliveryAddress) {
        mDeliveryAddress = DeliveryAddress;
    }

    public Long getDeliveryAddressId() {
        return mDeliveryAddressId;
    }

    public void setDeliveryAddressId(Long DeliveryAddressId) {
        mDeliveryAddressId = DeliveryAddressId;
    }

    public String getDeliveryType() {
        return mDeliveryType;
    }

    public void setDeliveryType(String DeliveryType) {
        mDeliveryType = DeliveryType;
    }

    public Long getDiscount() {
        return mDiscount;
    }

    public void setDiscount(Long Discount) {
        mDiscount = Discount;
    }

    public String getDishName() {
        return mDishName;
    }

    public void setDishName(String DishName) {
        mDishName = DishName;
    }

    public String getEmail() {
        return mEmail;
    }

    public void setEmail(String Email) {
        mEmail = Email;
    }

    public Double getExAmt() {
        return mExAmt;
    }

    public void setExAmt(Double ExAmt) {
        mExAmt = ExAmt;
    }

    public String getMenuCode() {
        return mMenuCode;
    }

    public void setMenuCode(String MenuCode) {
        mMenuCode = MenuCode;
    }

    public Long getMenuItemsId() {
        return mMenuItemsId;
    }

    public void setMenuItemsId(Long MenuItemsId) {
        mMenuItemsId = MenuItemsId;
    }

    public String getMobile() {
        return mMobile;
    }

    public void setMobile(String Mobile) {
        mMobile = Mobile;
    }

    public Double getOrderAmount() {
        return mOrderAmount;
    }

    public void setOrderAmount(Double OrderAmount) {
        mOrderAmount = OrderAmount;
    }

    public Double getOrderAmt() {
        return mOrderAmt;
    }

    public void setOrderAmt(Double OrderAmt) {
        mOrderAmt = OrderAmt;
    }

    public String getOrderDate() {
        return mOrderDate;
    }

    public void setOrderDate(String OrderDate) {
        mOrderDate = OrderDate;
    }

    public Long getOrderId() {
        return mOrderId;
    }

    public void setOrderId(Long OrderId) {
        mOrderId = OrderId;
    }

    public String getOrderStatus() {
        return mOrderStatus;
    }

    public void setOrderStatus(String OrderStatus) {
        mOrderStatus = OrderStatus;
    }

    public Long getOrderStatusId() {
        return mOrderStatusId;
    }

    public void setOrderStatusId(Long OrderStatusId) {
        mOrderStatusId = OrderStatusId;
    }

    public String getOutletAddress() {
        return mOutletAddress;
    }

    public void setOutletAddress(String OutletAddress) {
        mOutletAddress = OutletAddress;
    }

    public Long getOutletId() {
        return mOutletId;
    }

    public void setOutletId(Long OutletId) {
        mOutletId = OutletId;
    }

    public String getOutletName() {
        return mOutletName;
    }

    public void setOutletName(String OutletName) {
        mOutletName = OutletName;
    }

    public String getOutletPhone() {
        return mOutletPhone;
    }

    public void setOutletPhone(String OutletPhone) {
        mOutletPhone = OutletPhone;
    }

    public Double getPrice() {
        return mPrice;
    }

    public void setPrice(Double Price) {
        mPrice = Price;
    }

    public Long getQuantity() {
        return mQuantity;
    }

    public void setQuantity(Long Quantity) {
        mQuantity = Quantity;
    }

    public Long getShippingCharge() {
        return mShippingCharge;
    }

    public void setShippingCharge(Long ShippingCharge) {
        mShippingCharge = ShippingCharge;
    }

}
