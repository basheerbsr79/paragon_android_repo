package paragon.futuralabs.com.paragon.Activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import paragon.futuralabs.com.paragon.Adapters.RVeReceiptItems;
import paragon.futuralabs.com.paragon.R;
import paragon.futuralabs.com.paragon.models.ERecieptBillDetails;
import paragon.futuralabs.com.paragon.models.ERecieptList;
import paragon.futuralabs.com.paragon.models.brandwithpromotions.Promotion;

import static paragon.futuralabs.com.paragon.Utilities.AppConstants.Item;
import static paragon.futuralabs.com.paragon.Utilities.Utils.setTimeFormat;

public class PNeReceiptDetails extends PNBaseActivity {

    TextView tvHeading;
    ImageView ic_back_arrow, imageView;
    TextView tvOutletName, tvaddress, tvPhoneNo, tvGSTNo, tvLevel, tvTableNo, tvBillNo, tvDate, tvTime,
            tvTotal, tvTotalPayable;
    RecyclerView rvItems;

    ERecieptList eRecieptList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pne_receipt_details);

        init();
        handleEvents();
    }

    private void handleEvents() {
        tvHeading.setText("eReceipt Details");
        ic_back_arrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        String url = PNSP.getBrandImage();
        Picasso.with(this)
                .load(url)
                .resize(600, 200)
                .into(imageView);

        tvOutletName.setText(eRecieptList.getOutletsName());
        tvaddress.setText(eRecieptList.getOutletaddress());
        tvPhoneNo.setText(eRecieptList.getOutletPhone());
        tvLevel.setText(eRecieptList.getDineType());
        //tvTableNo.setText(eRecieptList.getT());
        tvBillNo.setText("Bill No: " + eRecieptList.getBillNo());
        tvDate.setText("Date: " + eRecieptList.getBillDate());
        String[] split = eRecieptList.getBillTime().split(":");
        tvTime.setText("Time: " + setTimeFormat(split[0] + ":" + split[1]+":00"));
        double total = 0;
        for (int i = 0; i < eRecieptList.getBillMasterId().size(); i++) {
            ERecieptBillDetails list = eRecieptList.getBillMasterId().get(i);
            total = total + (list.getPrice() * list.getQty());
        }

        tvTotal.setText(eRecieptList.getCurrency() + " " + total);
        tvTotalPayable.setText(eRecieptList.getCurrency() + " " + eRecieptList.getTotalBillAmt());

        RVeReceiptItems rVeReceiptItems = new RVeReceiptItems(this, eRecieptList.getBillMasterId());
        rvItems.setAdapter(rVeReceiptItems);

    }

    private void init() {
        ic_back_arrow =   findViewById(R.id.img_back_toolbar);
        tvHeading =   findViewById(R.id.txt_title_toolbar);
        imageView =   findViewById(R.id.imageView);
        tvOutletName =   findViewById(R.id.tvOutletName);
        tvaddress =   findViewById(R.id.tvaddress);
        tvPhoneNo =   findViewById(R.id.tvPhoneNo);
        tvGSTNo =   findViewById(R.id.tvGSTNo);
        tvLevel =   findViewById(R.id.tvLevel);
        tvTableNo =   findViewById(R.id.tvTableNo);
        tvBillNo =   findViewById(R.id.tvBillNo);
        tvDate =   findViewById(R.id.tvDate);
        tvTime =   findViewById(R.id.tvTime);
        tvTotal =   findViewById(R.id.tvTotal);
        tvTotalPayable =   findViewById(R.id.tvTotalPayable);
        rvItems =   findViewById(R.id.rvItems);
        rvItems.setLayoutManager(new LinearLayoutManager(this));

        Intent intent = this.getIntent();
        Bundle bundle = intent.getExtras();
        eRecieptList = (ERecieptList) bundle.getSerializable(Item);
    }
}
