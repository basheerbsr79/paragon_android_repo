package paragon.futuralabs.com.paragon.Activity;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import paragon.futuralabs.com.paragon.Adapters.RVFAQ;
import paragon.futuralabs.com.paragon.Adapters.RVVoucher;
import paragon.futuralabs.com.paragon.R;
import paragon.futuralabs.com.paragon.apiManager.PNApiResponseWrapper;
import paragon.futuralabs.com.paragon.apiManager.PNNetworkConstants;
import paragon.futuralabs.com.paragon.apiManager.PNNetworkManager;
import paragon.futuralabs.com.paragon.apiManager.SplitResponse;
import paragon.futuralabs.com.paragon.models.FAQList;
import paragon.futuralabs.com.paragon.models.FAQResponse;
import paragon.futuralabs.com.paragon.models.VouchersList;
import paragon.futuralabs.com.paragon.models.VouchersResponse;

import static paragon.futuralabs.com.paragon.Utilities.ConnectionDetector.toast;
import static paragon.futuralabs.com.paragon.apiManager.PNNetworkManager.post;

public class PNFAQ extends PNBaseActivity {

    LinearLayout linLayContainer;
    TextView tvHeading;
    ImageView ic_back_arrow;
    RecyclerView recyclerView;
    FAQResponse faqResponse;
    List<FAQList> faqLists;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pnfaq);
        init();
        handleEvents();
        getFAQ();
    }

    private void handleEvents() {
        tvHeading.setText("FAQ");
        ic_back_arrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

    }

    private void init() {
        linLayContainer= findViewById(R.id.linLayContainer);
        ic_back_arrow= findViewById(R.id.img_back_toolbar);
        tvHeading=findViewById(R.id.txt_title_toolbar);
        recyclerView= findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
    }

    public void getFAQ(){
        final String[] list=new String[0];
        faqLists=new ArrayList(Arrays.asList(list));
        showProgress();
        JSONObject jsonObject = new JSONObject();
        post(this, PNNetworkConstants.BindFaqsDetails, jsonObject.toString(), new PNNetworkManager.NetworkInterface() {
            @Override
            public void onResponse(PNApiResponseWrapper baseResponse) {
                dismissProgres();
                if (baseResponse.isSuccess()) {
                    splitResponse = new SplitResponse(baseResponse.getJsonObjectResponse());
                    if (splitResponse.getStatus().equals("1")) {
                        try {
                            Gson gson = new Gson();
                            faqResponse = gson.fromJson(splitResponse.getResponse(), FAQResponse.class);
                            if (faqResponse.getData().size()>0)
                                setData(faqResponse);
                            else
                                toast(getApplicationContext(),"No faq found");

                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    } else {
                        toast(getApplicationContext(), splitResponse.getMessage());
                    }
                } else {
                    toast(getApplicationContext(), baseResponse.getMessage());
                }

            }
        });


    }

    private void setData(FAQResponse response) {

        if (response.getData().size()>0){
            for (int i = 0; i < response.getData().size(); i++) {
                if (response.getData().get(i).getFaqTypeId()==1)
                faqLists.add(response.getData().get(i));
            }
            if (faqLists.size()>0) {
                RVFAQ rvfaq = new RVFAQ(PNFAQ.this, faqLists);
                recyclerView.setAdapter(rvfaq);
            }else
                toast(getApplicationContext(),"No faq found");
        }

    }
}
