
package paragon.futuralabs.com.paragon.models.todayspecial;

import java.io.Serializable;
import java.util.List;
import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class Datum implements Serializable{

    @SerializedName("Date")
    private String mDate;
    @SerializedName("Day")
    private String mDay;
    @SerializedName("Images")
    private List<Image> mImages;
    @SerializedName("IsFromMenu")
    private Boolean mIsFromMenu;
    @SerializedName("OutletsName")
    private String mOutletsName;
    @SerializedName("PrmoType")
    private String mPrmoType;
    @SerializedName("TimeStartFrom")
    private paragon.futuralabs.com.paragon.models.todayspecial.TimeStartFrom mTimeStartFrom;
    @SerializedName("TimeStartTo")
    private paragon.futuralabs.com.paragon.models.todayspecial.TimeStartTo mTimeStartTo;
    @SerializedName("TodaySpecialId")
    private Long mTodaySpecialId;

    public String getDate() {
        return mDate;
    }

    public void setDate(String Date) {
        mDate = Date;
    }

    public String getDay() {
        return mDay;
    }

    public void setDay(String Day) {
        mDay = Day;
    }

    public List<Image> getImages() {
        return mImages;
    }

    public void setImages(List<Image> Images) {
        mImages = Images;
    }

    public Boolean getIsFromMenu() {
        return mIsFromMenu;
    }

    public void setIsFromMenu(Boolean IsFromMenu) {
        mIsFromMenu = IsFromMenu;
    }

    public String getOutletsName() {
        return mOutletsName;
    }

    public void setOutletsName(String OutletsName) {
        mOutletsName = OutletsName;
    }

    public String getPrmoType() {
        return mPrmoType;
    }

    public void setPrmoType(String PrmoType) {
        mPrmoType = PrmoType;
    }

    public paragon.futuralabs.com.paragon.models.todayspecial.TimeStartFrom getTimeStartFrom() {
        return mTimeStartFrom;
    }

    public void setTimeStartFrom(paragon.futuralabs.com.paragon.models.todayspecial.TimeStartFrom TimeStartFrom) {
        mTimeStartFrom = TimeStartFrom;
    }

    public paragon.futuralabs.com.paragon.models.todayspecial.TimeStartTo getTimeStartTo() {
        return mTimeStartTo;
    }

    public void setTimeStartTo(paragon.futuralabs.com.paragon.models.todayspecial.TimeStartTo TimeStartTo) {
        mTimeStartTo = TimeStartTo;
    }

    public Long getTodaySpecialId() {
        return mTodaySpecialId;
    }

    public void setTodaySpecialId(Long TodaySpecialId) {
        mTodaySpecialId = TodaySpecialId;
    }

}
