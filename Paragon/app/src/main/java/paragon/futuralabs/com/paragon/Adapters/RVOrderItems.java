package paragon.futuralabs.com.paragon.Adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import paragon.futuralabs.com.paragon.R;
import paragon.futuralabs.com.paragon.models.ERecieptBillDetails;
import paragon.futuralabs.com.paragon.models.OrderDetailList;

/**
 * Created by Shamir on 13-07-2018.
 */

public class RVOrderItems extends RecyclerView.Adapter<RVOrderItems.ViewHolder> {
    List<OrderDetailList> list;
    Context context;

    public RVOrderItems(Context context, List<OrderDetailList> list) {
        this.context = context;
        this.list=list;
    }

    @Override
    public RVOrderItems.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.rv_erec_bill_item, parent, false);
        RVOrderItems.ViewHolder viewHolder = new RVOrderItems.ViewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RVOrderItems.ViewHolder holder, int position) {

        holder.tvDish.setText(list.get(position).getDishName());
        holder.tvQty.setText(list.get(position).getQuantity()+"");
        holder.tvRate.setText(list.get(position).getPrice()+"");
        double tot=list.get(position).getQuantity()*list.get(position).getPrice();
        holder.tvAmount.setText(tot+"");

    }

    @Override
    public int getItemCount() {
        return list.size();

    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView tvDish, tvQty, tvRate, tvAmount;

        public ViewHolder(View itemView) {
            super(itemView);
            tvDish =   itemView.findViewById(R.id.tvDish);
            tvQty =   itemView.findViewById(R.id.tvQty);
            tvRate =   itemView.findViewById(R.id.tvRate);
            tvAmount =   itemView.findViewById(R.id.tvAmount);
        }

    }
}
