package paragon.futuralabs.com.paragon.Adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import paragon.futuralabs.com.paragon.Activity.PNCart;
import paragon.futuralabs.com.paragon.R;
import paragon.futuralabs.com.paragon.apiManager.PNApiResponseWrapper;
import paragon.futuralabs.com.paragon.apiManager.PNNetworkConstants;
import paragon.futuralabs.com.paragon.apiManager.PNNetworkManager;
import paragon.futuralabs.com.paragon.apiManager.SplitResponse;
import paragon.futuralabs.com.paragon.models.CartList;

import static paragon.futuralabs.com.paragon.Utilities.ConnectionDetector.toast;
import static paragon.futuralabs.com.paragon.apiManager.PNNetworkManager.post;

/**
 * Created by Shamir on 12-03-2018.
 */

public class RVCart extends RecyclerView.Adapter<RVCart.ViewHolder> {
    List<CartList> cartLists;
    Context context;
    SplitResponse splitResponse;

    public RVCart(Context context, List<CartList> cartLists) {
        this.context = context;
        this.cartLists=cartLists;

    }

    @Override
    public RVCart.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.rv_cart_item, parent, false);
        RVCart.ViewHolder viewHolder = new RVCart.ViewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final RVCart.ViewHolder holder, final int position) {

        /*String url=cartLists.get(position).getImage();
        Picasso.with(context)
                .load(url)
                .resize(100,100).centerCrop()
                .error(R.drawable.dish_item_placeholder)
                .placeholder(R.drawable.dish_item_placeholder)
                .into(holder.imvImage);*/
        holder.textView.setText(cartLists.get(position).getDishName());
        holder.tvPrice.setText(cartLists.get(position).getCurrency()+" "+cartLists.get(position).getPrice());
        holder.tvQuantity.setText(cartLists.get(position).getQuantity()+"");
        holder.tvTotal.setText(cartLists.get(position).getCurrency()+" "+(Double.parseDouble(cartLists.get(position).getPrice())*cartLists.get(position).getQuantity()));
        holder.imvMinus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int val=Integer.parseInt(holder.tvQuantity.getText().toString().trim());
                if (val!=1){
                    val=val-1;
                    holder.tvQuantity.setText(String.valueOf(val));
                    holder.tvTotal.setText(cartLists.get(position).getCurrency()+" "+(val*Double.parseDouble(cartLists.get(position).getPrice())));
                }
            }
        });
        holder.imvPlus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int val=Integer.parseInt(holder.tvQuantity.getText().toString().trim());
                if (val<100){
                    val=val+1;
                    holder.tvQuantity.setText(String.valueOf(val));
                    holder.tvTotal.setText(cartLists.get(position).getCurrency()+" "+(val*Double.parseDouble(cartLists.get(position).getPrice())));
                }
            }
        });

        holder.tvUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                UpdateCart(holder.tvQuantity.getText().toString().trim(),position);
            }
        });
        holder.imvDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                deleteItem(position);
            }
        });

    }

    @Override
    public int getItemCount() {
        return cartLists.size();

    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView imvImage,imvDelete;
        TextView textView,textViewDesc,tvConNuts,tvRecommended,tvPrice,tvUpdate,tvQuantity,tvTotal;
        ImageView imvMinus,imvPlus;

        public ViewHolder(View itemView) {
            super(itemView);
            imvImage =   itemView.findViewById(R.id.imageView);
            imvDelete =  itemView.findViewById(R.id.imvDelete);
            textView= itemView.findViewById(R.id.textView);
            textViewDesc= itemView.findViewById(R.id.textViewDesc);
            tvConNuts= itemView.findViewById(R.id.tvConNuts);
            tvRecommended= itemView.findViewById(R.id.tvRecommended);
            tvPrice= itemView.findViewById(R.id.tvPrice);
            imvMinus=  itemView.findViewById(R.id.imvMinus);
            imvPlus=  itemView.findViewById(R.id.imvPlus);
            tvUpdate= itemView.findViewById(R.id.tvUpdate);
            tvQuantity= itemView.findViewById(R.id.tvQuantity);
            tvTotal= itemView.findViewById(R.id.tvTotal);

        }

    }

    private void UpdateCart(String qty,int pos) {
        ((PNCart)context).showProgress();
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("CartId", cartLists.get(pos).getCartId());
            jsonObject.put("Quantity", qty);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        post(context, PNNetworkConstants.UpdateCartItemQuantity, jsonObject.toString(), new PNNetworkManager.NetworkInterface() {
            @Override
            public void onResponse(PNApiResponseWrapper baseResponse) {
                ((PNCart)context).dismissProgres();
                if (baseResponse.isSuccess()) {
                    splitResponse = new SplitResponse(baseResponse.getJsonObjectResponse());
                    if (splitResponse.getStatus().equals("1")) {
                        toast(context,splitResponse.getMessage());
                        ((PNCart)context).getCartItems();

                    } else {
                        toast(context, splitResponse.getMessage());
                    }
                } else {
                    toast(context, baseResponse.getMessage());
                }

            }
        });
    }

    private void deleteItem(int pos) {
        ((PNCart)context).showProgress();
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("CartId", cartLists.get(pos).getCartId());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        post(context, PNNetworkConstants.DeleteCartItem, jsonObject.toString(), new PNNetworkManager.NetworkInterface() {
            @Override
            public void onResponse(PNApiResponseWrapper baseResponse) {
                ((PNCart)context).dismissProgres();
                if (baseResponse.isSuccess()) {
                    splitResponse = new SplitResponse(baseResponse.getJsonObjectResponse());
                    if (splitResponse.getStatus().equals("1")) {
                        toast(context,"Removed from cart");
                        ((PNCart)context).getCartItems();

                    } else {
                        toast(context, splitResponse.getMessage());
                        ((PNCart)context).getCartItems();
                    }
                } else {
                    toast(context, baseResponse.getMessage());
                }

            }
        });
    }
}

