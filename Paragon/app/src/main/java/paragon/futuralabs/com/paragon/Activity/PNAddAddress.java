package paragon.futuralabs.com.paragon.Activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import paragon.futuralabs.com.paragon.R;
import paragon.futuralabs.com.paragon.Utilities.MyAlert;
import paragon.futuralabs.com.paragon.apiManager.PNApiResponseWrapper;
import paragon.futuralabs.com.paragon.apiManager.PNNetworkConstants;
import paragon.futuralabs.com.paragon.apiManager.PNNetworkManager;
import paragon.futuralabs.com.paragon.apiManager.SplitResponse;

import static paragon.futuralabs.com.paragon.Utilities.AppConstants.FROM;
import static paragon.futuralabs.com.paragon.Utilities.AppConstants.Mgrill;
import static paragon.futuralabs.com.paragon.Utilities.AppConstants.Salkara;
import static paragon.futuralabs.com.paragon.Utilities.ConnectionDetector.toast;
import static paragon.futuralabs.com.paragon.Utilities.Utils.validateEditText;
import static paragon.futuralabs.com.paragon.apiManager.PNNetworkManager.post;

public class PNAddAddress extends PNBaseActivity {

    LinearLayout linLayContainer;
    TextView tvHeading;
    ImageView ic_back_arrow;
    EditText edName,edPhone,edAddress,edPostCode,edLandmark;
    Button btnAdd;
    SplitResponse splitResponse;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pnadd_address);
        init();
        handleEvents();
        trackEvent("Add Address","View Add Address");
    }

    private void handleEvents() {
        tvHeading.setText("Add Address");
        edName.setText(PNSP.getname());
        edPhone.setText(PNSP.getphone());
        ic_back_arrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (validate()){
                    saveAddress();
                }
            }
        });
    }

    public boolean validate() {
        if (!validateEditText(getApplicationContext(), edName.getText().toString().trim())) {
            toast(this, "Enter Name");
            return false;
        }
        if (!validateEditText(getApplicationContext(), edPhone.getText().toString().trim())) {
            toast(this, "Enter Contact Number");
            return false;
        }
        if (!validateEditText(getApplicationContext(), edAddress.getText().toString().trim())) {
            toast(this, "Enter Address");
            return false;
        }
        if (!validateEditText(getApplicationContext(), edPostCode.getText().toString().trim())) {
            toast(this, "Enter Post Code");
            return false;
        }
        if (!validateEditText(getApplicationContext(), edLandmark.getText().toString().trim())) {
            toast(this, "Enter LandMark");
            return false;
        }
        return true;
    }

    private void init() {
        linLayContainer= findViewById(R.id.linLayContainer);
        ic_back_arrow= findViewById(R.id.img_back_toolbar);
        tvHeading= findViewById(R.id.txt_title_toolbar);
        edName= findViewById(R.id.edName);
        edPhone= findViewById(R.id.edPhone);
        edAddress= findViewById(R.id.edAddress);
        edPostCode= findViewById(R.id.edPostCode);
        edLandmark= findViewById(R.id.edLandmark);
        btnAdd= findViewById(R.id.btnAdd);
    }

    private void saveAddress() {
        showProgress();
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("DeliveryAddressId", "0");
            jsonObject.put("CustomerId", PNSP.getuserid());
            jsonObject.put("Address", edAddress.getText().toString().trim());
            jsonObject.put("PostCode", edPostCode.getText().toString().trim());
            jsonObject.put("LandMark", edLandmark.getText().toString().trim());
            jsonObject.put("ContactName", edName.getText().toString().trim());
            jsonObject.put("ContactNumber", edPhone.getText().toString().trim());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        post(PNAddAddress.this, PNNetworkConstants.SaveDeliveryAddress, jsonObject.toString(), new PNNetworkManager.NetworkInterface() {
            @Override
            public void onResponse(PNApiResponseWrapper baseResponse) {
                dismissProgres();
                if (baseResponse.isSuccess()) {
                    splitResponse = new SplitResponse(baseResponse.getJsonObjectResponse());

                    MyAlert alert = new MyAlert();
                    if (splitResponse.getStatus().equals("1")) {

                        try {
                            trackEvent("Add Address","Address Added");
                            alert.showAlertFinish(PNAddAddress.this, splitResponse.getMessage());
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    } else {
                        toast(getApplicationContext(), splitResponse.getMessage());
                    }
                } else {
                    toast(getApplicationContext(), baseResponse.getMessage());
                }

            }
        });
    }
}
