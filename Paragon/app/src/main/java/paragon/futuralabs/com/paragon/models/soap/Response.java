
package paragon.futuralabs.com.paragon.models.soap;

import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class Response {

    @SerializedName("ResponseId")
    private Long mResponseId;
    @SerializedName("ResponseMsg")
    private String mResponseMsg;

    public Long getResponseId() {
        return mResponseId;
    }

    public void setResponseId(Long ResponseId) {
        mResponseId = ResponseId;
    }

    public String getResponseMsg() {
        return mResponseMsg;
    }

    public void setResponseMsg(String ResponseMsg) {
        mResponseMsg = ResponseMsg;
    }

}
