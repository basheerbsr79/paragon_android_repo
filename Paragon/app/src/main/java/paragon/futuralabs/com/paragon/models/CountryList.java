
package paragon.futuralabs.com.paragon.models;

import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class CountryList {

    @SerializedName("Country")
    private String mCountry;
    @SerializedName("CountryId")
    private Long mCountryId;
    @SerializedName("CountryCode")
    private String mCountryCode;

    public String getCountry() {
        return mCountry;
    }

    public void setCountry(String Country) {
        mCountry = Country;
    }

    public Long getCountryId() {
        return mCountryId;
    }

    public void setCountryId(Long CountryId) {
        mCountryId = CountryId;
    }

    public String getCountryCode() {
        return mCountryCode;
    }

    public void setCountryCode(String CountryCode) {
        mCountryCode = CountryCode;
    }

}
