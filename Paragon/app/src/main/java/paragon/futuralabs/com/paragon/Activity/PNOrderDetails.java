package paragon.futuralabs.com.paragon.Activity;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import paragon.futuralabs.com.paragon.Adapters.RVOrderItems;
import paragon.futuralabs.com.paragon.R;
import paragon.futuralabs.com.paragon.apiManager.PNApiResponseWrapper;
import paragon.futuralabs.com.paragon.apiManager.PNNetworkConstants;
import paragon.futuralabs.com.paragon.apiManager.PNNetworkManager;
import paragon.futuralabs.com.paragon.apiManager.SplitResponse;
import paragon.futuralabs.com.paragon.models.OrderDetailList;
import paragon.futuralabs.com.paragon.models.OrderDetailResponse;

import static paragon.futuralabs.com.paragon.Utilities.AppConstants.OrderId;
import static paragon.futuralabs.com.paragon.Utilities.ConnectionDetector.toast;
import static paragon.futuralabs.com.paragon.Utilities.Utils.getConvertedDate;
import static paragon.futuralabs.com.paragon.Utilities.Utils.setTimeFormat;
import static paragon.futuralabs.com.paragon.apiManager.PNNetworkManager.post;

public class PNOrderDetails extends PNBaseActivity {

    TextView tvHeading;
    ImageView ic_back_arrow, imageView;
    TextView tvOutletName, tvOutletAddress, tvPhoneNo, tvType, tvStatus, tvOrderId, tvDate, tvTime,
            tvOrderAmt, tvDeliveryCharge, tvTotal;
    RecyclerView rvItems;

    OrderDetailResponse orderDetailResponse;
    List<OrderDetailList> orderDetailLists;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pnorder_details);

        init();
        handleEvents();
        getOrderDetails();
    }

    private void handleEvents() {
        tvHeading.setText("Order Details");
        ic_back_arrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        String url = PNSP.getBrandImage();
        Picasso.with(this)
                .load(url)
                .resize(600, 200)
                .into(imageView);

    }

    private void init() {
        ic_back_arrow = findViewById(R.id.img_back_toolbar);
        tvHeading = findViewById(R.id.txt_title_toolbar);
        imageView = findViewById(R.id.imageView);
        tvOutletName = findViewById(R.id.tvOutletName);
        tvOutletAddress = findViewById(R.id.tvOutletAddress);
        tvPhoneNo = findViewById(R.id.tvPhoneNo);
        tvType = findViewById(R.id.tvType);
        tvStatus = findViewById(R.id.tvStatus);
        tvOrderId = findViewById(R.id.tvOrderId);
        tvDate = findViewById(R.id.tvDate);
        tvTime = findViewById(R.id.tvTime);
        tvOrderAmt = findViewById(R.id.tvOrderAmt);
        tvDeliveryCharge = findViewById(R.id.tvDeliveryCharge);
        tvTotal = findViewById(R.id.tvTotal);
        rvItems = findViewById(R.id.rvItems);
        rvItems.setLayoutManager(new LinearLayoutManager(this));
    }

    public void getOrderDetails() {
        final String[] list = new String[0];
        orderDetailLists = new ArrayList(Arrays.asList(list));
        showProgress();
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("OrderId", getIntent().getStringExtra(OrderId));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        post(this, PNNetworkConstants.BindOrderDetailsCustomer, jsonObject.toString(), new PNNetworkManager.NetworkInterface() {
            @Override
            public void onResponse(PNApiResponseWrapper baseResponse) {
                dismissProgres();
                if (baseResponse.isSuccess()) {
                    splitResponse = new SplitResponse(baseResponse.getJsonObjectResponse());
                    if (splitResponse.getStatus().equals("1")) {
                        try {
                            Gson gson = new Gson();
                            orderDetailResponse = gson.fromJson(splitResponse.getResponse(), OrderDetailResponse.class);
                            orderDetailLists.addAll(orderDetailResponse.getData());
                            if (orderDetailLists.size() > 0)
                                setData(orderDetailLists);
                           // else
                                //toast(getApplicationContext(), "No news found");

                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    } else {
                        toast(getApplicationContext(), splitResponse.getMessage());
                    }
                } else {
                    toast(getApplicationContext(), baseResponse.getMessage());
                }

            }
        });


    }

    private void setData(List<OrderDetailList> list) {

        tvOutletName.setText(list.get(0).getOutletName());
        tvOutletAddress.setText(list.get(0).getOutletAddress());
        tvPhoneNo.setText(list.get(0).getOutletPhone());
        tvType.setText(list.get(0).getDeliveryType());
        tvStatus.setText(list.get(0).getOrderStatus());
        tvOrderId.setText("Order Id : " + list.get(0).getOrderId());
        String[] splitDT = list.get(0).getOrderDate().split(" ");
        tvDate.setText("Date : " + getConvertedDate(splitDT[0]));
        tvTime.setText("Time : " + setTimeFormat(splitDT[1]));
        tvOrderAmt.setText(list.get(0).getCurrency() + " "+list.get(0).getExAmt());
        tvDeliveryCharge.setText(list.get(0).getCurrency() + " "+list.get(0).getShippingCharge());
        tvTotal.setText(list.get(0).getCurrency() + " "+list.get(0).getOrderAmount());

        RVOrderItems rvOrderItems = new RVOrderItems(this, orderDetailLists);
        rvItems.setAdapter(rvOrderItems);
    }
}
