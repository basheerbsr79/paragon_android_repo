package paragon.futuralabs.com.paragon.Activity.paragon;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import paragon.futuralabs.com.paragon.Activity.PNBaseActivity;
import paragon.futuralabs.com.paragon.R;
import paragon.futuralabs.com.paragon.paragonhelper.ParagonSP;

public class AppinfoParagonActivity extends PNBaseActivity {
    Context context;
    ImageView img_back_toolbar;
    TextView txt_title_toolbar;
    ParagonSP sp;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_appinfo_paragon);
        //setting context
        context = AppinfoParagonActivity.this;
        //decalaring variables
        img_back_toolbar=findViewById(R.id.img_back_toolbar);
        txt_title_toolbar=findViewById(R.id.txt_title_toolbar);
        //define shared prdfence
        sp=new ParagonSP(getApplicationContext());
        //setting toolbar title
        txt_title_toolbar.setText(getString(R.string.paragon));
        img_back_toolbar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        trackEvent("App Info","View App Info");
    }
}
