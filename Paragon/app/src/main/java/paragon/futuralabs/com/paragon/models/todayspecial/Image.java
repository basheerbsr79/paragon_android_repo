
package paragon.futuralabs.com.paragon.models.todayspecial;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

@SuppressWarnings("unused")
public class Image implements Serializable{

    @SerializedName("DishName")
    private String mDishName;
    @SerializedName("ImagePath")
    private String mImagePath;

    public String getDishName() {
        return mDishName;
    }

    public void setDishName(String DishName) {
        mDishName = DishName;
    }

    public String getImagePath() {
        return mImagePath;
    }

    public void setImagePath(String ImagePath) {
        mImagePath = ImagePath;
    }

}
