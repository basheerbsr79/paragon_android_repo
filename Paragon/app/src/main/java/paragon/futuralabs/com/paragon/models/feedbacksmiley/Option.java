
package paragon.futuralabs.com.paragon.models.feedbacksmiley;

import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class Option {

    @SerializedName("FQOptionId")
    private String mFQOptionId;
    @SerializedName("OptionName")
    private String mOptionName;

    public String getFQOptionId() {
        return mFQOptionId;
    }

    public void setFQOptionId(String FQOptionId) {
        mFQOptionId = FQOptionId;
    }

    public String getOptionName() {
        return mOptionName;
    }

    public void setOptionName(String OptionName) {
        mOptionName = OptionName;
    }

}
