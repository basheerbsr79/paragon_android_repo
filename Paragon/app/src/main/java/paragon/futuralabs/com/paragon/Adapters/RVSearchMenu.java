package paragon.futuralabs.com.paragon.Adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import paragon.futuralabs.com.paragon.Activity.PNAddToCart;
import paragon.futuralabs.com.paragon.Activity.PNHDMenuItems;
import paragon.futuralabs.com.paragon.Activity.PNMenuItems;
import paragon.futuralabs.com.paragon.Activity.PNSearchMenuItems;
import paragon.futuralabs.com.paragon.R;
import paragon.futuralabs.com.paragon.apiManager.PNApiResponseWrapper;
import paragon.futuralabs.com.paragon.apiManager.PNNetworkConstants;
import paragon.futuralabs.com.paragon.apiManager.PNNetworkManager;
import paragon.futuralabs.com.paragon.apiManager.SplitResponse;
import paragon.futuralabs.com.paragon.models.MenuItemsList;
import paragon.futuralabs.com.paragon.models.homedelivery.MenuItem;

import static paragon.futuralabs.com.paragon.Utilities.ConnectionDetector.toast;
import static paragon.futuralabs.com.paragon.apiManager.PNNetworkManager.post;

/**
 * Created by Shamir on 07-08-2018.
 */

public class RVSearchMenu extends RecyclerView.Adapter<RVSearchMenu.ViewHolder> {
    List<MenuItem> menuItemsList;
    Context context;
    String type;
    SplitResponse splitResponse;

    public RVSearchMenu(Context context, List<MenuItem> list,String type) {
        this.context = context;
        this.type=type;
        final String[] list1=new String[0];
        menuItemsList=new ArrayList(Arrays.asList(list1));

        for (int i = 0; i < list.size(); i++) {
            if (list.get(i).getDishName().toLowerCase().contains(type.toLowerCase())){
                menuItemsList.add(list.get(i));
            }
        }

    }

    @Override
    public RVSearchMenu.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.rv_search_menu_item, parent, false);
        RVSearchMenu.ViewHolder viewHolder = new RVSearchMenu.ViewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final RVSearchMenu.ViewHolder holder, final int position) {

        holder.textView.setText(menuItemsList.get(position).getDishName());
        holder.textViewDesc.setText(menuItemsList.get(position).getDescription());
        if (menuItemsList.get(position).getVegOrNonVeg()==1)
            holder.imvVeg.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_veg));
        else if (menuItemsList.get(position).getVegOrNonVeg()==0)
            holder.imvVeg.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_nonveg));
        else holder.imvVeg.setVisibility(View.GONE);

        if (menuItemsList.get(position).getInStock()==0) {
            holder.llAddToCart.setVisibility(View.GONE);
            holder.llOutOfStock.setVisibility(View.VISIBLE);
        }else{
            holder.llAddToCart.setVisibility(View.VISIBLE);
            holder.llOutOfStock.setVisibility(View.GONE);
        }
        /*if (menuItemsList.get(position).getIsContainNuts())
            holder.tvConNuts.setText("Contain Nuts");
        else
            holder.tvConNuts.setText("No Nuts");
        if (menuItemsList.get(position).getIsRecommended())
            holder.tvRecommended.setText("Recommended");
        else
            holder.tvRecommended.setText("Not Recommended");*/
        holder.tvPrice.setText(menuItemsList.get(position).getCurrency()+" "+menuItemsList.get(position).getPrice());
        holder.tvTotal.setText(menuItemsList.get(position).getCurrency()+" "+menuItemsList.get(position).getPrice());
        holder.imvMinus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int val=Integer.parseInt(holder.tvQuantity.getText().toString().trim());
                if (val!=1){
                    val=val-1;
                    holder.tvQuantity.setText(String.valueOf(val));
                    holder.tvTotal.setText(menuItemsList.get(position).getCurrency()+" "+
                            (val*Double.parseDouble(menuItemsList.get(position).getPrice())));
                }
            }
        });
        holder.imvPlus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int val=Integer.parseInt(holder.tvQuantity.getText().toString().trim());
                if (val<100){
                    val=val+1;
                    holder.tvQuantity.setText(String.valueOf(val));
                    holder.tvTotal.setText(menuItemsList.get(position).getCurrency()+" "+
                            (val*Double.parseDouble(menuItemsList.get(position).getPrice())));
                }
            }
        });

        holder.tvAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addToCart(holder.tvQuantity.getText().toString().trim(),menuItemsList.get(position));
            }
        });


    }

    @Override
    public int getItemCount() {
        return menuItemsList.size();

    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        //LinearLayout linLay;
        ImageView imvImage,imvVeg;
        TextView textView,textViewDesc,tvPrice,tvAdd,tvQuantity,tvTotal;
        ImageView imvMinus,imvPlus;
LinearLayout llAddToCart,llOutOfStock;
        public ViewHolder(View convertView) {
            super(convertView);
            //linLay=convertView.findViewById(R.id.linLay);
            imvImage =  convertView.findViewById(R.id.imageView);
            imvVeg =   convertView.findViewById(R.id.imvVeg);
            textView= convertView.findViewById(R.id.textView);
            textViewDesc= convertView.findViewById(R.id.textViewDesc);
            //tvConNuts= convertView.findViewById(R.id.tvConNuts);
            //tvRecommended= convertView.findViewById(R.id.tvRecommended);
            tvPrice= convertView.findViewById(R.id.tvPrice);
            imvMinus=  convertView.findViewById(R.id.imvMinus);
            imvPlus=  convertView.findViewById(R.id.imvPlus);
            tvAdd= convertView.findViewById(R.id.tvAdd);
            tvQuantity= convertView.findViewById(R.id.tvQuantity);
            tvTotal= convertView.findViewById(R.id.tvTotal);
            llAddToCart=convertView.findViewById(R.id.llAddToCart);
            llOutOfStock=convertView.findViewById(R.id.llOutOfStock);

        }


    }

    public void addToCart(String qty,MenuItem menuItemsList) {
        ((PNSearchMenuItems)context).showProgress();
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("CartId", "0");
            jsonObject.put("CustomerId",((PNSearchMenuItems)context).PNSP.getuserid());
            jsonObject.put("MenuItemsId",menuItemsList.getMenuItemsId());
            jsonObject.put("MenuCode", "12pgn");
            jsonObject.put("Quantity", qty);
            jsonObject.put("Price", menuItemsList.getPrice());
            jsonObject.put("OutletId", ((PNSearchMenuItems)context).PNSP.getOutlet());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        post(context, PNNetworkConstants.AddToHomeDeliveryCart, jsonObject.toString(), new PNNetworkManager.NetworkInterface() {
            @Override
            public void onResponse(PNApiResponseWrapper baseResponse) {
                ((PNSearchMenuItems)context).dismissProgres();
                if (baseResponse.isSuccess()) {
                    splitResponse = new SplitResponse(baseResponse.getJsonObjectResponse());
                    if (splitResponse.getStatus().equals("1")) {
                        toast(context,"Added to cart");
                        //((PNAddToCart)context).getCartIteams();

                    } else if (splitResponse.getStatus().equals("2")) {
                        toast(context,splitResponse.getMessage());
                        //((PNAddToCart)context).getCartIteams();
                    }else
                    {
                        toast(context, splitResponse.getMessage());
                    }
                } else {
                    toast(context, baseResponse.getMessage());
                }

            }
        });
    }
}
