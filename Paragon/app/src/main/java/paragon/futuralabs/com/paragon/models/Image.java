
package paragon.futuralabs.com.paragon.models;
import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class Image {

    @SerializedName("DishName")
    private String mDishName;
    @SerializedName("ImagePath")
    private String mImagePath;

    public String getDishName() {
        return mDishName;
    }

    public void setDishName(String DishName) {
        mDishName = DishName;
    }

    public String getImagePath() {
        return mImagePath;
    }

    public void setImagePath(String ImagePath) {
        mImagePath = ImagePath;
    }

}
