package paragon.futuralabs.com.paragon.Activity;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.support.v4.app.ActivityCompat;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import paragon.futuralabs.com.paragon.Adapters.RVMenuCatagory;
import paragon.futuralabs.com.paragon.R;
import paragon.futuralabs.com.paragon.apiManager.PNApiResponseWrapper;
import paragon.futuralabs.com.paragon.apiManager.PNNetworkConstants;
import paragon.futuralabs.com.paragon.apiManager.PNNetworkManager;
import paragon.futuralabs.com.paragon.apiManager.SplitResponse;
import paragon.futuralabs.com.paragon.models.HomeDeliveryResponse;
import paragon.futuralabs.com.paragon.models.MenuCatagoryList;
import paragon.futuralabs.com.paragon.models.MenuCatagoryResponse;

import static com.google.android.gms.tasks.Tasks.call;
import static paragon.futuralabs.com.paragon.Utilities.AppConstants.FROM;
import static paragon.futuralabs.com.paragon.Utilities.AppConstants.HOMEDELIVERY;
import static paragon.futuralabs.com.paragon.Utilities.AppConstants.Mgrill;
import static paragon.futuralabs.com.paragon.Utilities.AppConstants.Salkara;
import static paragon.futuralabs.com.paragon.Utilities.ConnectionDetector.toast;
import static paragon.futuralabs.com.paragon.apiManager.PNNetworkManager.post;

public class PNHomeDeliveryCatagory extends PNBaseActivity {
    LinearLayout linLayContainer;
    TextView tvHeading;
    ImageView ic_back_arrow,imvCart;
    RecyclerView recyclerView;
    SplitResponse splitResponse;
    MenuCatagoryResponse menuCatagoryResponse;
    List<MenuCatagoryList> menuCatagoryLists;
    HomeDeliveryResponse homeDeliveryResponse;
    private static final int PERMISSION_REQUEST_CODE = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pnhome_delivery);

        init();
        handleEvents();
        //getHomeDeliveryNo();
        getMenuCatagory();
    }

    private void handleEvents() {
        tvHeading.setText("Catagory");
        ic_back_arrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
       /* tvPhoneNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!tvPhoneNo.getText().toString().trim().equals("Home Delivery not available"))
                    call(tvPhoneNo.getText().toString());
            }
        });*/
        imvCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(PNHomeDeliveryCatagory.this,PNCart.class);
                startActivity(intent);
            }
        });
    }

    private boolean checkPermission()
    {

        String permission = "android.permission.CALL_PHONE";
        int res = getApplicationContext().checkCallingOrSelfPermission(permission);
        return (res == PackageManager.PERMISSION_GRANTED);
    }

    private void call(String number) {
        if(checkPermission()==true) {
            Intent in = new Intent(Intent.ACTION_CALL, Uri.parse("tel:"+number));
            try {
                startActivity(in);
            } catch (android.content.ActivityNotFoundException ex) {
                Toast.makeText(getApplicationContext(), "Call not proccesd", Toast.LENGTH_SHORT).show();
            }
        }else{
            ActivityCompat.requestPermissions(PNHomeDeliveryCatagory.this, new String[]{android.Manifest.permission.CALL_PHONE}, PERMISSION_REQUEST_CODE);
        }
    }

    private void init() {
        linLayContainer= findViewById(R.id.linLayContainer);
        ic_back_arrow= findViewById(R.id.img_back_toolbar);
        imvCart= findViewById(R.id.imvCart);
        tvHeading= findViewById(R.id.txt_title_toolbar);
        recyclerView= findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    Toast.makeText(PNHomeDeliveryCatagory.this, "Permission Granted, Now you can call.", Toast.LENGTH_SHORT).show();

                } else {

                    Toast.makeText(PNHomeDeliveryCatagory.this, "Permission Denied, You cannot call.", Toast.LENGTH_SHORT).show();

                }
                break;


        }
    }

    public void getMenuCatagory(){
        final String[] list=new String[0];
        menuCatagoryLists=new ArrayList(Arrays.asList(list));
        showProgress();
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("OutletId", PNSP.getOutlet());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        post(PNHomeDeliveryCatagory.this, PNNetworkConstants.BindMenuCategory, jsonObject.toString(), new PNNetworkManager.NetworkInterface() {
            @Override
            public void onResponse(PNApiResponseWrapper baseResponse) {
                dismissProgres();
                if (baseResponse.isSuccess()) {
                    splitResponse = new SplitResponse(baseResponse.getJsonObjectResponse());
                    if (splitResponse.getStatus().equals("1")) {
                        try {
                            Gson gson = new Gson();
                            menuCatagoryResponse = gson.fromJson(splitResponse.getResponse(), MenuCatagoryResponse.class);
                            menuCatagoryLists.addAll(menuCatagoryResponse.getData());
                            if (menuCatagoryLists.size()>0)
                                setData(menuCatagoryLists);
                            else
                                toast(getApplicationContext(),"No catagories found");

                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    } else {
                        toast(getApplicationContext(), splitResponse.getMessage());
                    }
                } else {
                    toast(getApplicationContext(), baseResponse.getMessage());
                }

            }
        });


    }

    private void setData(List<MenuCatagoryList> menuCatagoryLists) {

        RVMenuCatagory rvMenuCatagory=new RVMenuCatagory(PNHomeDeliveryCatagory.this,menuCatagoryLists,HOMEDELIVERY);
        recyclerView.setAdapter(rvMenuCatagory);
    }

    public void getHomeDeliveryNo(){
        showProgress();
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("OutletId", PNSP.getOutlet());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        post(PNHomeDeliveryCatagory.this, PNNetworkConstants.BindHomeDelivery, jsonObject.toString(), new PNNetworkManager.NetworkInterface() {
            @Override
            public void onResponse(PNApiResponseWrapper baseResponse) {
                dismissProgres();
                if (baseResponse.isSuccess()) {
                    splitResponse = new SplitResponse(baseResponse.getJsonObjectResponse());
                    if (splitResponse.getStatus().equals("1")) {
                        try {
                            Gson gson = new Gson();
                            homeDeliveryResponse = gson.fromJson(splitResponse.getResponse(), HomeDeliveryResponse.class);
                            setData(homeDeliveryResponse);

                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    } else {
                        toast(getApplicationContext(), splitResponse.getMessage());
                    }
                } else {
                    toast(getApplicationContext(), baseResponse.getMessage());
                }

            }
        });


    }

    private void setData(HomeDeliveryResponse homeDeliveryResponse) {
       // tvPhoneNo.setText(homeDeliveryResponse.getData().get(0).getHomeDeliveryNumber());

    }
}
