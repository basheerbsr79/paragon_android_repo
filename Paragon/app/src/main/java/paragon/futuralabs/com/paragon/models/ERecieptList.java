
package paragon.futuralabs.com.paragon.models;

import java.io.Serializable;
import java.util.List;
import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class ERecieptList implements Serializable{

    @SerializedName("BillDate")
    private String mBillDate;
    @SerializedName("BillDate1")
    private String mBillDate1;
    @SerializedName("BillMasterId")
    private List<ERecieptBillDetails> mERecieptBillDetails;
    @SerializedName("BillNo")
    private String mBillNo;
    @SerializedName("BillTime")
    private String mBillTime;
    @SerializedName("Currency")
    private String mCurrency;
    @SerializedName("DineType")
    private String mDineType;
    @SerializedName("Discount")
    private Long mDiscount;
    @SerializedName("outletaddress")
    private String mOutletaddress;
    @SerializedName("OutletsName")
    private String mOutletsName;
    @SerializedName("TotalBillAmt")
    private Double mTotalBillAmt;

    @SerializedName("OutletPhone")
    private String mOutletPhone;

    public String getBillDate() {
        return mBillDate;
    }

    public void setBillDate(String BillDate) {
        mBillDate = BillDate;
    }

    public String getBillDate1() {
        return mBillDate1;
    }

    public void setBillDate1(String BillDate1) {
        mBillDate1 = BillDate1;
    }

    public List<ERecieptBillDetails> getBillMasterId() {
        return mERecieptBillDetails;
    }

    public void setBillMasterId(List<ERecieptBillDetails> ERecieptBillDetails) {
        mERecieptBillDetails = ERecieptBillDetails;
    }

    public String getBillNo() {
        return mBillNo;
    }

    public void setBillNo(String BillNo) {
        mBillNo = BillNo;
    }

    public String getBillTime() {
        return mBillTime;
    }

    public void setBillTime(String BillTime) {
        mBillTime = BillTime;
    }

    public String getCurrency() {
        return mCurrency;
    }

    public void setCurrency(String Currency) {
        mCurrency = Currency;
    }

    public String getDineType() {
        return mDineType;
    }

    public void setDineType(String DineType) {
        mDineType = DineType;
    }

    public Long getDiscount() {
        return mDiscount;
    }

    public void setDiscount(Long Discount) {
        mDiscount = Discount;
    }

    public String getOutletaddress() {
        return mOutletaddress;
    }

    public void setOutletaddress(String outletaddress) {
        mOutletaddress = outletaddress;
    }

    public String getOutletsName() {
        return mOutletsName;
    }

    public void setOutletsName(String OutletsName) {
        mOutletsName = OutletsName;
    }

    public Double getTotalBillAmt() {
        return mTotalBillAmt;
    }

    public void setTotalBillAmt(Double TotalBillAmt) {
        mTotalBillAmt = TotalBillAmt;
    }

    public String getOutletPhone() {
        return mOutletPhone;
    }

    public void setOutletPhone(String OutletPhone) {
        mOutletPhone = OutletPhone;
    }

}
