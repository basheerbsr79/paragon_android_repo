package paragon.futuralabs.com.paragon.Activity;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;

import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;

import paragon.futuralabs.com.paragon.Activity.paragon.DashBoardActivity;
import paragon.futuralabs.com.paragon.Adapters.RVFeedBackSmiley;
import paragon.futuralabs.com.paragon.R;
import paragon.futuralabs.com.paragon.Utilities.MyAlert;
import paragon.futuralabs.com.paragon.apiManager.PNApiResponseWrapper;
import paragon.futuralabs.com.paragon.apiManager.PNNetworkConstants;
import paragon.futuralabs.com.paragon.apiManager.PNNetworkManager;
import paragon.futuralabs.com.paragon.apiManager.SplitResponse;
import paragon.futuralabs.com.paragon.models.feedbacksmiley.FeedBackSmileyList;
import paragon.futuralabs.com.paragon.models.feedbacksmiley.FeedBackSmileyResponse;
import paragon.futuralabs.com.paragon.models.feedbacksmiley.Option;

import static paragon.futuralabs.com.paragon.Utilities.AppConstants.FROM;
import static paragon.futuralabs.com.paragon.Utilities.AppConstants.Mgrill;
import static paragon.futuralabs.com.paragon.Utilities.AppConstants.Salkara;
import static paragon.futuralabs.com.paragon.Utilities.ConnectionDetector.toast;
import static paragon.futuralabs.com.paragon.Utilities.Utils.getCurrentDate;
import static paragon.futuralabs.com.paragon.Utilities.Utils.isValidEmail;
import static paragon.futuralabs.com.paragon.Utilities.Utils.validateEditText;
import static paragon.futuralabs.com.paragon.apiManager.PNNetworkManager.post;

public class PNFeedBack extends PNBaseActivity {

    LinearLayout linLayContainer;
    TextView tvHeading;
    ImageView ic_back_arrow;
    RecyclerView recyclerView;
    EditText edName, edPhone, edEmail, edAddress,edWaiterName, edComment;
    Button btnSubmit;
    TextView tvDate,tvtext;
    RadioButton rbPrevious,rbNow;
    SplitResponse splitResponse;
    FeedBackSmileyResponse feedBackSmileyResponse;
    List<FeedBackSmileyList> feedBackSmilyLists;
    List<Option> optionList;

    public static String ans[];

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pnfeed_back);

        init();
        handleEvents();
        getFeedBackQuestions();
        trackEvent("FeedBack","View FeedBack");
    }

    private void handleEvents() {
        tvHeading.setText("Feedback");
        edName.setText(PNSP.getname());
        edPhone.setText(PNSP.getphone());
        edEmail.setText(PNSP.getemail());
        tvtext.setText("Dear "+PNSP.getname().trim()+", "+getResources().getString(R.string.feedbacktext));

        rbNow.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b){
                    tvDate.setVisibility(View.GONE);
                }
            }
        });

        rbPrevious.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b){
                    tvDate.setVisibility(View.VISIBLE);
                }
            }
        });

        tvDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setDate(view,PNFeedBack.this,"Select Date");
            }
        });

        ic_back_arrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (validate()) {
                    postFeedBack();
                }
            }
        });
    }

    private void init() {
        linLayContainer =   findViewById(R.id.linLayContainer);
        ic_back_arrow =   findViewById(R.id.img_back_toolbar);
        tvHeading =   findViewById(R.id.txt_title_toolbar);
        tvtext=findViewById(R.id.tvtext);
        edName =   findViewById(R.id.edna);
        edPhone =   findViewById(R.id.edphone);
        edEmail =   findViewById(R.id.edemail);
        edAddress =   findViewById(R.id.edAddress);
        edWaiterName=findViewById(R.id.edWaiterName);
        edComment =   findViewById(R.id.edComment);
        btnSubmit =  findViewById(R.id.btnSubmit);
        tvDate= findViewById(R.id.tvDate);
        rbNow= findViewById(R.id.rbNow);
        rbPrevious= findViewById(R.id.rbPrevious);
        recyclerView =   findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
    }

    public boolean validate() {
        /*if (!validateEditText(getApplicationContext(), edName.getText().toString().trim())) {
            toast(this, "Enter Name");
            return false;
        }
        if (!validateEditText(getApplicationContext(), edPhone.getText().toString().trim())) {
            toast(this, "Enter Phone Number");
            return false;
        }
        if (!isValidEmail(edEmail.getText().toString().trim())) {
            toast(this, "Enter Valid Email");
            return false;
        }*/
        if (rbPrevious.isChecked()){
            if (!validateEditText(getApplicationContext(), tvDate.getText().toString().trim())) {
                toast(this, "Select date");
                return false;
            }
        }
        return true;
    }

    private void getFeedBackQuestions() {
        final String[] list = new String[0];
        feedBackSmilyLists = new ArrayList(Arrays.asList(list));
        optionList = new ArrayList(Arrays.asList(list));
        showProgress();
        JSONObject jsonObject = new JSONObject();


        post(PNFeedBack.this, PNNetworkConstants.BindFeedbackQuestions, jsonObject.toString(), new PNNetworkManager.NetworkInterface() {
            @Override
            public void onResponse(PNApiResponseWrapper baseResponse) {
                dismissProgres();
                if (baseResponse.isSuccess()) {
                    splitResponse = new SplitResponse(baseResponse.getJsonObjectResponse());
                    if (splitResponse.getStatus().equals("1")) {
                        Gson gson = new Gson();
                        feedBackSmileyResponse = gson.fromJson(splitResponse.getResponse(), FeedBackSmileyResponse.class);
                        feedBackSmilyLists.addAll(feedBackSmileyResponse.getData());
                        if (feedBackSmilyLists.size() > 0) {
                            setQuestion(feedBackSmilyLists);
                        } else {
                        }

                    } else {
                        toast(getApplicationContext(), splitResponse.getMessage());
                    }
                } else {
                    toast(getApplicationContext(), baseResponse.getMessage());
                }

            }
        });

    }

    private void setQuestion(List<FeedBackSmileyList> feedBackSmilyLists) {
        ans = new String[feedBackSmilyLists.size() - 2];
        for (int i = 0; i < feedBackSmilyLists.size() - 2; i++) {
            ans[i] = "0";
        }
        RVFeedBackSmiley rvFeedBackSmiley = new RVFeedBackSmiley(PNFeedBack.this, feedBackSmilyLists);
        recyclerView.setAdapter(rvFeedBackSmiley);

    }

    private void postFeedBack() {
        final MyAlert myAlert = new MyAlert();
        for (int i = 0; i < ans.length; i++) {
            if (ans[i].equals("0")) {
                myAlert.showAlert(PNFeedBack.this, "Please provide your ratings for all the Questions.");
                return;
            }
        }
        showProgress();
        String selected = "";
        for (int i = 0; i < ans.length; i++) {
            selected = selected + feedBackSmilyLists.get(i).getFQId() + "_" + ans[i] + "|";
        }

        selected = selected.substring(0, selected.length() - 1);

        JSONObject jsonObject = new JSONObject();

        try {
            jsonObject.put("OutletId", PNSP.getOutlet());
            jsonObject.put("Name", edName.getText().toString().trim());
            jsonObject.put("Address", PNSP.getaddress());
            jsonObject.put("Email", edEmail.getText().toString().trim());
            jsonObject.put("Mobile", edPhone.getText().toString().trim());
            jsonObject.put("FeedbackType", "1");
            jsonObject.put("Comments", edComment.getText().toString().trim());
            jsonObject.put("Feedback", selected);
            jsonObject.put("CustomerId", PNSP.getuserid());
            int pSt=1;
            String d=getCurrentDate();
            if (rbPrevious.isChecked()) {
                pSt = 2;
                d=tvDate.getText().toString().trim();
            }
            jsonObject.put("PriorityStatus", pSt);
            jsonObject.put("PreBillDate", d);
            jsonObject.put("NameOfOrderTaking", edWaiterName.getText().toString().trim());


        } catch (JSONException e) {
            e.printStackTrace();
        }

        post(PNFeedBack.this, PNNetworkConstants.SaveCustomerFeedback, jsonObject.toString(), new PNNetworkManager.NetworkInterface() {
            @Override
            public void onResponse(PNApiResponseWrapper baseResponse) {
                dismissProgres();
                if (baseResponse.isSuccess()) {
                    splitResponse = new SplitResponse(baseResponse.getJsonObjectResponse());
                    if (splitResponse.getStatus().equals("1")) {
                        trackEvent("FeedBack","Added FeedBack");
                        if (splitResponse.getCurrentState().equals("1") ||
                                splitResponse.getCurrentState().equals("2")) {
                            myAlert.showAlertFinish(PNFeedBack.this, "We sincerely thank you for your suggestion. " +
                                    "Paragon team strives in our traditional dining ritual to create cheerful memories. Our CRM team will personally attend to your concerns.");
                        }else if (splitResponse.getCurrentState().equals("3") ||
                                splitResponse.getCurrentState().equals("4")) {
                            myAlert.showAlertFinish(PNFeedBack.this, "Thank you for your generous time and effort,  Paragon team strives in our traditional dining rituals to create cheerful memories.");
                        }

                    } else {
                        toast(getApplicationContext(), splitResponse.getMessage());
                    }
                } else {
                    toast(getApplicationContext(), baseResponse.getMessage());
                }

            }
        });

    }

    public void setDate(final View v, final Context context, final String title) {
        final Calendar mcurrentDate = Calendar.getInstance();
        int mYear = mcurrentDate.get(Calendar.YEAR);
        int mMonth = mcurrentDate.get(Calendar.MONTH);
        int mDay = mcurrentDate.get(Calendar.DAY_OF_MONTH);
        DatePickerDialog mDatePicker = new DatePickerDialog(context, R.style.TimePickerTheme,
                new DatePickerDialog.OnDateSetListener() {
                    public void onDateSet(DatePicker datepicker,
                                          int selectedyear, int selectedmonth, int selectedday) {
                        String da = selectedday < 10 ? "0" + Integer.toString(selectedday) : Integer.toString(selectedday);
                        String m = selectedmonth < 9 ? "0" + Integer.toString(selectedmonth + 1) : Integer.toString(selectedmonth + 1);
                        String y = Integer.toString(selectedyear);
                        ((TextView) v).setText(da + "-" + m + "-" + y);
                    }
                }, mYear, mMonth, mDay);
        mDatePicker.setTitle(title + "");
        mDatePicker.getDatePicker().setMaxDate(System.currentTimeMillis());
        mDatePicker.show();
    }
}
